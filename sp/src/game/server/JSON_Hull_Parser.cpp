#include "cbase.h"
#include "JSON_Hull_Parser.h"
#include "tier0/icommandline.h"

#include <fstream>
#include <iostream>


using namespace rapidjson;


CustomHull L_JSON_HullParser::getHullData(std::string hullName)
{
	CustomHull parsed = CustomHull();
	std::string rootDir = UTIL_GetModRootPath();
	rootDir = rootDir + "\\resource\\";
	std::string fileToParse = rootDir + "npc_Hull_Data.json";
	std::string line, text;
	std::ifstream in(fileToParse);
	
	
	while (std::getline(in, line))
	{
		text += line + "\n";
	}
	
	const char* data = text.c_str();
	CustomHull::singleHullSize_s smallHullStruc;
	CustomHull::singleHullSize_s normalHullStruc;
	Document document;
	document.Parse(data);
	Value& rootJSON = document["JL_NPCData"];
	for (SizeType i = 0; i < rootJSON.Size(); i++)
	{
		if (rootJSON[i].IsObject()) {
			if (rootJSON[i]["npc_HullData"].IsObject())
			{
				Value& npc_HullData = rootJSON[i]["npc_HullData"];

				if (npc_HullData["hullDataName"].IsString())
				{
					string hullDataName = npc_HullData["hullDataName"].GetString();
					if (hullDataName == hullName)
					{
						parsed.hullDataName = hullDataName;
						if (npc_HullData["hullDescription"].IsString())
						{
							string hullDesc = npc_HullData["hullDescription"].GetString();
							parsed.hullDataDesc = hullDesc;
						}
						Value& small_HullSize = npc_HullData["smallHullSize"];
						if (small_HullSize.IsObject())
						{
							//Parse Small Hull Scale Vector
							Value& small_hullVectorScaleValue = small_HullSize["hullVectorScale"];
							Vector smallHullVectorScale = parseJSONVector(small_hullVectorScaleValue);

							//Parse Small Hull Min Vector
							Value& small_hullMinValue = small_HullSize["hullMinVector"];
							Vector smallHullMinVector = parseJSONVector(small_hullMinValue);

							//Parse Small Hull Max Vector
							Value& small_hullMaxValue = small_HullSize["hullMaxVector"];
							Vector smallHullMaxVector = parseJSONVector(small_hullMaxValue);

							smallHullStruc.scale = smallHullVectorScale;
							smallHullStruc.min = smallHullMinVector;
							smallHullStruc.max = smallHullMaxVector;


						}
						Value& normal_HullSize = npc_HullData["normalHullSize"];
						if (normal_HullSize.IsObject())
						{
							//Parse Small Hull Scale Vector
							Value& normal_hullVectorScaleValue = normal_HullSize["hullVectorScale"];
							Vector normalHullVectorScale = parseJSONVector(normal_hullVectorScaleValue);

							//Parse Small Hull Min Vector
							Value& normal_hullMinValue = normal_HullSize["hullMinVector"];
							Vector normalHullMinVector = parseJSONVector(normal_hullMinValue);

							//Parse Small Hull Max Vector
							Value& normal_hullMaxValue = normal_HullSize["hullMaxVector"];
							Vector normalHullMaxVector = parseJSONVector(normal_hullMaxValue);

							normalHullStruc.scale = normalHullVectorScale;
							normalHullStruc.min = normalHullMinVector;
							normalHullStruc.max = normalHullMaxVector;
						}
						parsed.smallSize = smallHullStruc;
						parsed.normalSize = normalHullStruc;
						parsed.IsValidHull = true;
						return parsed;
					}
					
				}
			}
					
		}
		else
		{
			DevLog("is not array");
		}
	}
	return parsed;
}

Vector L_JSON_HullParser::parseJSONVector(Value& targetValue)
{
	int x = targetValue["x"].GetInt();
	int y = targetValue["y"].GetInt();
	int z = targetValue["z"].GetInt();
	Vector tmpVector = Vector(x, y, z);

	return tmpVector;

}
