#include "cbase.h"
#include "hl2_player.h"
// #include "weapon_card.h"

class CWeaponCard;

class CLogicCardReader : public CLogicalEntity
{
	DECLARE_CLASS( CLogicCardReader, CLogicalEntity );

private:

	DECLARE_DATADESC();

	int m_nCardID;
	int m_nAccess_Level;

public:
	COutputEvent m_Has_Valid_Card;
	COutputEvent m_No_Valid_Card;

	void Request_Card_Check(inputdata_t &inputdata);
	void Activate ( void );

	EHANDLE m_hPlayer;
};

LINK_ENTITY_TO_CLASS( logic_cardreader, CLogicCardReader);

BEGIN_DATADESC( CLogicCardReader )
	DEFINE_KEYFIELD(m_nCardID, FIELD_INTEGER, "Card_Reader_ID"),
	DEFINE_KEYFIELD(m_nAccess_Level, FIELD_INTEGER, "Access_Level"),
	DEFINE_OUTPUT(m_Has_Valid_Card, "Has_Valid_Card"),
	DEFINE_OUTPUT(m_No_Valid_Card, "No_Valid_Card"),

	DEFINE_INPUTFUNC(FIELD_VOID, "Request_Card_Check", Request_Card_Check),

	//DEFINE_FIELD( m_hPlayer, FIELD_EHANDLE ),
END_DATADESC()


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CLogicCardReader::Request_Card_Check(inputdata_t &inputdata)
{
	if( m_hPlayer == NULL )
	{
		DevMsg("No Player for Card reader %d, at access level %d\n", m_nCardID, m_nAccess_Level);
	}
		
	CHL2_Player *pPlayer = dynamic_cast<CHL2_Player*>(UTIL_GetLocalPlayer());

	if (!pPlayer) { return; }



	int Card_ID = 0;

	for (int i = pPlayer->m_iNoteCount; i > 0; i--)
	{   // Getting the Status of the Document/Card that carries the same ID as this Cardreader expects
	    Card_ID = pPlayer->m_iNotes.Get(i);
		Msg("\nPosition %d has Note ID #%d \n", i, Card_ID);
	
		// Deliberately not checking for Key/Card type, allowing Documents to act as key: 
		// if (pPlayer->Get_Doc_Info(Card_ID, false) == 3 && 
		if (Card_ID == m_nCardID) 
		{   // First tryig all card/document IDs, otherwise by high Admin access below
			m_Has_Valid_Card.FireOutput(this, this);

			// For Items/Documents Doc_Status has a different meaning then Objectives and Notes:
			// Notes + Maps: 1 = Unreaded, 2 = Readed
			// Objectives: 0 = resolved, 1-3 = Priority stages, 4 = Hidden
			// Items, Keys and ID Cards: 0 = Used (can be dropped), 4-1 = Access level, the lower the number the greater access

			pPlayer->m_iDoc_Status.Set(i, 0); // Change status to 0, readed (Indicator icon of the list switches from red to green)
			Msg("Player has the valid card ID #%d\n", Card_ID);
			return;
		}
	}


	// Cycling down all note slots 
	for (int i = pPlayer->m_iNoteCount; i > 0; i--)
	{
		Card_ID = pPlayer->m_iNotes.Get(i);
		int Card_Access_Level = pPlayer->Get_Doc_Info(Card_ID, true);
		int Doument_Type = pPlayer->Get_Doc_Info(Card_ID, false); // false gets the type

		Msg("Slot nr %d, Access Level is %d, required Level is %d or smaller.\n", i, Card_Access_Level, m_nAccess_Level);

		// Doument_Type 0 means Item, Key or ID Card
	    if (Doument_Type == 0 & Card_Access_Level > 0 && Card_Access_Level <= m_nAccess_Level)
		{			
			m_Has_Valid_Card.FireOutput(this, this);
			Msg("Player has not the required card ID #%d. Instead he used a card with Access Level %d\n", m_nCardID, Card_Access_Level); // card->Get_Access_Level());

			return; // Otherwise it fires false
		}	
		else if (m_nAccess_Level == 0) { Msg("Player has not the required card ID #%d. \nThis reader can't be opened by higher Access Levels.\n"); }
	}




	/* // Check the Card Reader Weapon itself instead of Inventory
	for (int i = 0; i < pPlayer->WeaponCount(); ++i)
	{
		CBaseCombatWeapon* weapon = pPlayer->GetWeapon(i);
		if (weapon)
		{   //if (weapon->IsItem()) {
			   if (!strcmp(weapon->GetName(), "weapon_card")) // If player has a weapon_card
			   {   //dynamic_cast<CWeaponCard*>
					CWeaponCard *card = (CWeaponCard*)weapon;


					// Getting the Status of the Document/Card that carries the same ID as this Cardreader expects
					// int Card_Access_Level = pPlayer->Get_Doc_Status(m_nCardID);
					int Card_Access_Level = pPlayer->Get_Doc_Status(0);
					Msg("Access Level is %d\n", Card_Access_Level);


					if (card->GetId() == m_nCardID)
					{
						m_Has_Valid_Card.FireOutput(this, this);
						Msg("Player has the valid card ID #%d\n", card->GetId());
						
						return;
					}
					// else if (card->Get_Access_Level() != 0 && card->Get_Access_Level() <= m_nAccess_Level) // Directly targeting the weapon_card
					else if (Card_Access_Level > 0 && Card_Access_Level <= m_nAccess_Level)
					{
						m_Has_Valid_Card.FireOutput(this, this);
						Msg("Player has not the required card ID #%d. Instead he used Card #%d with Access Level %d\n", m_nCardID, card->GetId(), Card_Access_Level); // card->Get_Access_Level());

						return;
					}
				}
			//}
		}
	}
	*/

	m_No_Valid_Card.FireOutput(this, this);
	Msg("Player has no valid card\n");
	//pPlayer->GetWeapon(0);
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CLogicCardReader::Activate ( void )
{
	BaseClass::Activate();

	if ( m_hPlayer == NULL )
	{
		m_hPlayer = UTIL_GetLocalPlayer();
	}
}


//-----------------------------------------------------------------------------
// End of File
//-----------------------------------------------------------------------------