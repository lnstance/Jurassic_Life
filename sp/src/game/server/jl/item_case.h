//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: Crate of items for Jurassic-Life
//
//=============================================================================//
#ifndef CASE_H
#define CASE_H

#ifdef _WIN32
#pragma once
#endif


#include "cbase.h"
//#include "player.h"
//#include "gamerules.h"
#include "items.h"
//#include "ammodef.h"
//#include "eventlist.h"
//#include "npcevent.h"
//			  
//#include <game/client/iviewport.h>
//
//// memdbgon must be the last include file in a .cpp file!!!
//#include "tier0/memdbgon.h"

// ==================================================================
// Ammo crate which will supply infinite ammo of the specified type
// ==================================================================



// Ammo crate

class CJL_Item_Case : public CBaseAnimating
{
public:
	DECLARE_CLASS( CJL_Item_Case, CBaseAnimating );
	
	DECLARE_SERVERCLASS();
	int UpdateTransmitState() { return SetTransmitState(FL_EDICT_ALWAYS); } // always send to all clients
	CNetworkArray(CBaseCombatWeaponHandle, m_nCaseItems, MAX_JL_ITEMS);
	CNetworkVar(int, Scrollbar_Size); // integer
	

	/*
	int Slot_1, Slot_2, Slot_3, Slot_4, Slot_5, Slot_6, Slot_7, Slot_8, Slot_9, Slot_10;
	int Slot_11, Slot_12, Slot_13, Slot_14, Slot_15, Slot_16, Slot_17, Slot_18, Slot_19, Slot_20;
	int Slot_21, Slot_22, Slot_23, Slot_24, Slot_25, Slot_26, Slot_27, Slot_28, Slot_29, Slot_30;
	int Slot_31, Slot_32, Slot_33, Slot_34, Slot_35, Slot_36, Slot_37, Slot_38, Slot_39, Slot_40;
	int Slot_41, Slot_42, Slot_43, Slot_44, Slot_45, Slot_46, Slot_47, Slot_48, Slot_49, Slot_50;

	int Slot_51, Slot_52, Slot_53, Slot_54, Slot_55, Slot_56, Slot_57, Slot_58, Slot_59, Slot_60;
	int Slot_61, Slot_62, Slot_63, Slot_64, Slot_65, Slot_66, Slot_67, Slot_68, Slot_69, Slot_70;
	int Slot_71, Slot_72, Slot_73, Slot_74, Slot_75, Slot_76, Slot_77, Slot_78, Slot_79, Slot_80;
	int Slot_81, Slot_82, Slot_83, Slot_84, Slot_85, Slot_86, Slot_87, Slot_88, Slot_89, Slot_90; 
	int Slot_91, Slot_92, Slot_93, Slot_94, Slot_95, Slot_96, Slot_97, Slot_98, Slot_99, Slot_100;

	int Slot_101, Slot_102, Slot_103, Slot_104, Slot_105, Slot_106, Slot_107, Slot_108, Slot_109, Slot_110;
	int Slot_111, Slot_112, Slot_113, Slot_114, Slot_115, Slot_116, Slot_117, Slot_118, Slot_119, Slot_120;
	*/



	void	Spawn( void );
	void	Precache( void );
	bool	CreateVPhysics( void );

	virtual void HandleAnimEvent( animevent_t *pEvent );

	void	OnRestore( void );

	//FIXME: May not want to have this used in a radius
	int		ObjectCaps( void ) { return (BaseClass::ObjectCaps() | (FCAP_IMPULSE_USE|FCAP_USE_IN_RADIUS)); };
	void	Use( CBaseEntity *pActivator, CBaseEntity *pCaller, USE_TYPE useType, float value );

	void	InputKill( inputdata_t &data );
	void	CrateThink( void );

	void	Close( void );
	void	Open( void );

	// Support Functions
	bool	Compare(char* value_1, char* value_2);
	bool	Contains_Character(char* value, char* character = " ", int string_size = 80);
	char*	Split_Array(char* value_1, char* value_2, char delimiter, int word_number);



	void	Move_Items_Into_Player_Crate(void);


	// CHandle <CBaseCombatWeaponHandle> m_nCaseItems[50];
	// CBaseCombatWeaponHandle Get_Crate_Item(int i) { return m_nCaseItems[i]; };
    // void					Set_Crate_Item(int i, CBaseCombatWeaponHandle n) { m_nCaseItems[i] = n; };

	CBaseCombatWeaponHandle Get_Crate_Item(int i) { return m_nCaseItems[i]; };
	void					Set_Crate_Item(int i, CBaseCombatWeaponHandle n) { m_nCaseItems.Set(i, n); };

	
	virtual int OnTakeDamage( const CTakeDamageInfo &info );
	CHandle <CBasePlayer> m_hActivator;

	string_t Slot[140 + 1]; // +1 because we skip the 0 slot

	// Outputs
	COutputEvent	m_OnUsed;
	// COutputEvent	m_OnSlot[120];	 // Fired when the input value matches one of the case values.
	// COutputVariant  m_OnDefault;	 // Fired when no match was found.


protected:

	int		m_nAmmoType;
	int		m_nAmmoIndex;

	static const char *m_lpzModelNames;
	void Localize_Text(char* Entry, char* Result);


	float	m_flCloseTime;


	bool open;

	DECLARE_DATADESC();
};


#endif // CASE_H