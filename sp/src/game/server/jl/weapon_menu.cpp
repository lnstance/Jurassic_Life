//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose:		Crowbar - an old favorite
//
// $NoKeywords: $
//=============================================================================//

#include "cbase.h"
#include "basehlcombatweapon.h"
#include "ai_basenpc.h"
#include "npcevent.h"
#include "mathlib/mathlib.h"
#include "soundent.h"
#include "basebludgeonweapon.h"


/* Currently not Used
#include "player.h"
#include "gamerules.h"
#include "ammodef.h"
#include "vstdlib/random.h"
#include "in_buttons.h"
*/

// #include "Iinventory.h"

#include "gamestats.h"


// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"


//-----------------------------------------------------------------------------
// C_Weapon_Radio
//-----------------------------------------------------------------------------
#ifndef WEAPON_MENU
#define WEAPON_MENU

#include "basebludgeonweapon.h"

#if defined( _WIN32 )
#pragma once
#endif

#ifdef HL2MP
#error weapon_hands.h must not be included in hl2mp. The windows compiler will use the wrong class elsewhere if it is.
#endif


//-----------------------------------------------------------------------------
// CWeapon_Radio
//-----------------------------------------------------------------------------

class CWeapon_Menu : public CBaseHLBludgeonWeapon
{
public:
	DECLARE_CLASS(CWeapon_Menu, CBaseHLBludgeonWeapon);

	DECLARE_SERVERCLASS();
	//DECLARE_ACTTABLE();

	float fl_Next_Charge = 0;

	CWeapon_Menu();

	virtual void WeaponIdle(void); // called when no buttons pressed
	void		DryFire(void);
	void		AddViewKick(void);
	float		GetDamageForActivity(Activity hitActivity);

	virtual int WeaponMeleeAttack1Condition(float flDot, float flDist);
	virtual int WeaponMeleeAttack2Condition(float flDot, float flDist);
	void		PrimaryAttack(void);
	void		SecondaryAttack();


	// Animation event
	virtual void Operator_HandleAnimEvent(animevent_t *pEvent, CBaseCombatCharacter *pOperator);

private:
	// Animation event handlers
	void HandleAnimEventMeleeHit(animevent_t *pEvent, CBaseCombatCharacter *pOperator);
};

#endif 



IMPLEMENT_SERVERCLASS_ST(CWeapon_Menu, DT_Weapon_Menu)
END_SEND_TABLE()

//#ifndef HL2MP
LINK_ENTITY_TO_CLASS(weapon_menu, CWeapon_Menu);
PRECACHE_WEAPON_REGISTER(weapon_menu);
//#endif
/*
acttable_t CWeapon_Menu::m_acttable[] = 
{
	{ ACT_MELEE_ATTACK1,	ACT_MELEE_ATTACK_SWING, true },
	{ ACT_IDLE,				ACT_IDLE_ANGRY_MELEE,	false },
	{ ACT_IDLE_ANGRY,		ACT_IDLE_ANGRY_MELEE,	false },
};

IMPLEMENT_ACTTABLE(CWeapon_Menu);
*/

//-----------------------------------------------------------------------------
// Constructor
//-----------------------------------------------------------------------------
CWeapon_Menu::CWeapon_Menu(void)
{
	// PrimaryAttack(); // Switch right back to lat weapon

	// SetThink(&CWeapon_Radio::HideThink);
	// SetNextThink(gpGlobals->curtime + 0.5f);
}

//-----------------------------------------------------------------------------
// Purpose: Get the damage amount for the animation we're doing
// Input  : hitActivity - currently played activity
// Output : Damage amount
//-----------------------------------------------------------------------------
float CWeapon_Menu::GetDamageForActivity(Activity hitActivity)
{
	return 0;
}

//-----------------------------------------------------------------------------
// Purpose: Add in a view kick for this weapon
//-----------------------------------------------------------------------------
void CWeapon_Menu::AddViewKick(void)
{
	CBasePlayer *pPlayer  = ToBasePlayer( GetOwner() );
	
	if ( pPlayer == NULL )
		return;

	QAngle punchAng;

	punchAng.x = random->RandomFloat( 1.0f, 2.0f );
	punchAng.y = random->RandomFloat( -2.0f, -1.0f );
	punchAng.z = 0.0f;
	
	pPlayer->ViewPunch( punchAng ); 
}


//-----------------------------------------------------------------------------
// Attempt to lead the target (needed because citizens can't hit manhacks with the crowbar!)
//-----------------------------------------------------------------------------

int CWeapon_Menu::WeaponMeleeAttack1Condition(float flDot, float flDist)
{

	return COND_CAN_MELEE_ATTACK1;
}

int CWeapon_Menu::WeaponMeleeAttack2Condition(float flDot, float flDist)
{
	return COND_CAN_MELEE_ATTACK2;
}


//-----------------------------------------------------------------------------
// Animation event handlers
//-----------------------------------------------------------------------------
void CWeapon_Menu::HandleAnimEventMeleeHit(animevent_t *pEvent, CBaseCombatCharacter *pOperator)
{
	
}

//-----------------------------------------------------------------------------
// Think
//-----------------------------------------------------------------------------
void CWeapon_Menu::WeaponIdle()
{
	CBasePlayer *pPlayer = ToBasePlayer(GetOwner());
	if (pPlayer && gpGlobals->curtime > fl_Next_Charge)
	{
		PrimaryAttack();

		// Every 10 seconds
		fl_Next_Charge = gpGlobals->curtime + 2.0f;
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CWeapon_Menu::PrimaryAttack(void)
{
	CBasePlayer *pPlayer = ToBasePlayer(GetOwner());
	CBaseCombatWeapon * Last_Weapon = pPlayer->Weapon_GetLast();


	if (Last_Weapon && Last_Weapon != this) { pPlayer->SetActiveWeapon(Last_Weapon); }
	else { pPlayer->SwitchToNextBestWeapon(this); }  


	engine->ServerCommand("ToggleInventory\n"); // jl_showinventory
	// engine->ServerCommand("host_timescale 0.02\n");	// Bullettime
	// pPlayer->m_bGamePaused = true;	


	//jl_showinventory = 1;
	//ToggleInventory();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CWeapon_Menu::SecondaryAttack(void)
{
	PrimaryAttack();
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CWeapon_Menu::DryFire(void)
{
	WeaponSound(EMPTY);
	SendWeaponAnim(ACT_VM_DRYFIRE);
	// need to handle Event for sound !! red

	m_flNextPrimaryAttack = gpGlobals->curtime + SequenceDuration();
}

//-----------------------------------------------------------------------------
// Animation event
//-----------------------------------------------------------------------------
void CWeapon_Menu::Operator_HandleAnimEvent(animevent_t *pEvent, CBaseCombatCharacter *pOperator)
{
	switch( pEvent->event )
	{
	case EVENT_WEAPON_MELEE_HIT:
		HandleAnimEventMeleeHit( pEvent, pOperator );
		break;

	default:
		BaseClass::Operator_HandleAnimEvent( pEvent, pOperator );
		break;
	}
}
