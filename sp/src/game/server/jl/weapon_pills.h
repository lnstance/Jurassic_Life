//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose:
//
//=============================================================================//

#ifndef WEAPON_PILLS
#define WEAPON_PILLS

#include "basebludgeonweapon.h"

#if defined( _WIN32 )
#pragma once
#endif

#ifdef HL2MP
#error weapon_hands.h must not be included in hl2mp. The windows compiler will use the wrong class elsewhere if it is.
#endif


//-----------------------------------------------------------------------------
// CWeaponCrowbar
//-----------------------------------------------------------------------------

class CWeaponHealthVial : public CBaseHLBludgeonWeapon
{
public:
	DECLARE_CLASS(CWeaponHealthVial, CBaseHLBludgeonWeapon);

	DECLARE_SERVERCLASS();
	// DECLARE_DATADESC();
	// DECLARE_ACTTABLE();

	float fl_Next_Charge = 0;

	CWeaponHealthVial(void);

	//void		WeaponIdle( void );						// called when no buttons pressed

	void		AddViewKick( void );
	float		GetDamageForActivity( Activity hitActivity );
	/*virtual void Precache(void);
	virtual void Spawn(void);*/

	virtual int WeaponMeleeAttack1Condition( float flDot, float flDist );
	virtual int WeaponMeleeAttack2Condition( float flDot, float flDist );
	void		PrimaryAttack( void );
	void		SecondaryAttack( void );
	int			i_Clip_Size = 0;
	const char*		Model_Name = 0;
	

	// Animation event
	virtual void Operator_HandleAnimEvent( animevent_t *pEvent, CBaseCombatCharacter *pOperator );

private:	
	// Animation event handlers
	void HandleAnimEventMeleeHit( animevent_t *pEvent, CBaseCombatCharacter *pOperator );
};

#endif 
