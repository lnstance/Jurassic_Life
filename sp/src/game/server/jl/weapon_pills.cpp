//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose:		Crowbar - an old favorite
//
// $NoKeywords: $
//=============================================================================//

#include "cbase.h"
#include "basehlcombatweapon.h"
#include "ai_basenpc.h"
#include "npcevent.h"
#include "mathlib/mathlib.h"
/* Currently not Used
#include "player.h"
#include "gamerules.h"
#include "ammodef.h"
#include "vstdlib/random.h"
#include "in_buttons.h"
*/
#include "soundent.h"
#include "basebludgeonweapon.h"
#include "weapon_pills.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"


//-----------------------------------------------------------------------------
// CWeaponHealthVial
//-----------------------------------------------------------------------------

IMPLEMENT_SERVERCLASS_ST(CWeaponHealthVial, DT_WeaponHealthVial)
END_SEND_TABLE()

//#ifndef HL2MP
LINK_ENTITY_TO_CLASS(weapon_healthvial, CWeaponHealthVial);
PRECACHE_WEAPON_REGISTER(weapon_healthvial);
//#endif


// Start of our data description for the class
//BEGIN_DATADESC(CWeaponHealthVial)
//
//// Links our input name from Hammer to our input member function
//DEFINE_INPUTFUNC(FIELD_STRING, "Model_Name", Model_Name),
//
//
//END_DATADESC()


/*
acttable_t CWeaponPills::m_acttable[] = 
{
	{ ACT_MELEE_ATTACK1,	ACT_MELEE_ATTACK_SWING, true },
	{ ACT_IDLE,				ACT_IDLE_ANGRY_MELEE,	false },
	{ ACT_IDLE_ANGRY,		ACT_IDLE_ANGRY_MELEE,	false },
};

IMPLEMENT_ACTTABLE(CWeaponPills);
*/

//-----------------------------------------------------------------------------
// Constructor
//-----------------------------------------------------------------------------
CWeaponHealthVial::CWeaponHealthVial(void)
{
	i_Clip_Size = this->GetWpnData().iMaxClip1;

	/*Model_Name = this->GetWpnData().szViewModel;
	PrecacheModel(Model_Name);*/
	// SetModel(this->GetWpnData().szViewModel);

}


////-----------------------------------------------------------------------------
//// Purpose: Precache assets used by the entity
////-----------------------------------------------------------------------------
//void CWeaponHealthVial::Precache(void)
//{
//	Model_Name = "models/jl/items/Amoxillin_can_wm_single.mdl"; // this->GetWpnData().szViewModel;
//	PrecacheModel(Model_Name);
//
//
//	BaseClass::Precache();
//}
//
//
////-----------------------------------------------------------------------------
//// Purpose: Sets up the entity's initial state
////-----------------------------------------------------------------------------
//void CWeaponHealthVial::Spawn(void)
//{
//	Precache();
//
//	SetModel(Model_Name);
//	// CollisionProp()->UseTriggerBounds(true, 100);
//	/*SetSolid(SOLID_BBOX);
//	UTIL_SetSize(this, -Vector(20, 20, 20), Vector(20, 20, 20));*/
//
//}
//

//-----------------------------------------------------------------------------
// Purpose: Get the damage amount for the animation we're doing
// Input  : hitActivity - currently played activity
// Output : Damage amount
//-----------------------------------------------------------------------------
float CWeaponHealthVial::GetDamageForActivity(Activity hitActivity)
{
	return 0;
}

//-----------------------------------------------------------------------------
// Purpose: Add in a view kick for this weapon
//-----------------------------------------------------------------------------
void CWeaponHealthVial::AddViewKick(void)
{
	CBasePlayer *pPlayer  = ToBasePlayer( GetOwner() );
	
	if ( pPlayer == NULL )
		return;

	QAngle punchAng;

	punchAng.x = random->RandomFloat( 1.0f, 2.0f );
	punchAng.y = random->RandomFloat( -2.0f, -1.0f );
	punchAng.z = 0.0f;
	
	pPlayer->ViewPunch( punchAng ); 
}


//-----------------------------------------------------------------------------
// Attempt to lead the target (needed because citizens can't hit manhacks with the crowbar!)
//-----------------------------------------------------------------------------

int CWeaponHealthVial::WeaponMeleeAttack1Condition(float flDot, float flDist)
{
	return COND_CAN_MELEE_ATTACK1;
}

int CWeaponHealthVial::WeaponMeleeAttack2Condition(float flDot, float flDist)
{
	return COND_CAN_MELEE_ATTACK2;
}


//-----------------------------------------------------------------------------
// Animation event handlers
//-----------------------------------------------------------------------------
void CWeaponHealthVial::HandleAnimEventMeleeHit(animevent_t *pEvent, CBaseCombatCharacter *pOperator)
{
	
}

//------------------------------------------------------------------------------
// Purpose :
// Input   :
// Output  :
//------------------------------------------------------------------------------
void CWeaponHealthVial::PrimaryAttack()
{   
	CBasePlayer *pPlayer = ToBasePlayer(GetOwner());
	if (pPlayer->GetHealth() < 100 && gpGlobals->curtime > fl_Next_Charge)
	{		
		i_Clip_Size = this->GetWpnData().iMaxClip1;

		if (HasPrimaryAmmo())
		{
			pPlayer->TakeHealth(this->GetWpnData().iDefaultClip1, DMG_GENERIC); // Was iDefaultClip1
			m_iClip1--;  // All Pills used 
		}


		int Secondary = m_iClip2; // pPlayer->GetAmmoCount(this->GetSecondaryAmmoType());
		
		if (Secondary < 1 && !HasPrimaryAmmo()) // If no more Medkits are available, remove the Medkit Weapon
		{   /*for (int i = 0; i<8; i++)
			{   if (pPlayer->GetWeapon(i) == (CBaseCombatWeapon*)this)
				{
					pPlayer->SetWeapon(i, NULL);
					break;
				}
			}*/

			pPlayer->SwitchToNextBestWeapon(this);
			DestroyItem();
		}


		if (!HasPrimaryAmmo() && Secondary) // No "else" because we want the pack removed instantly
		{   
			m_iClip2--; 
			// pPlayer->RemoveAmmo(1, m_iSecondaryAmmoType);
			m_iClip1 = i_Clip_Size; // Reloading Health Points for next usage
		} 


		
		// Every 0.15 seconds
		fl_Next_Charge = gpGlobals->curtime + 0.5f; 
	}
}

//------------------------------------------------------------------------------
// Purpose :
// Input   :
// Output  :
//------------------------------------------------------------------------------
void CWeaponHealthVial::SecondaryAttack()
{
	CBasePlayer *pPlayer = ToBasePlayer(GetOwner());
	if (pPlayer->GetHealth() < 100 && gpGlobals->curtime > fl_Next_Charge)
	{
		i_Clip_Size = this->GetWpnData().iMaxClip1;
		int Secondary = m_iClip2; // pPlayer->GetAmmoCount(this->GetSecondaryAmmoType());

		
		for (int i = 0; i < i_Clip_Size; i++) // Looping to use as many pills.. 
		{   if (pPlayer->GetHealth() < 100) // As necessary
			{
				if (HasPrimaryAmmo())
				{
					pPlayer->TakeHealth(this->GetWpnData().iDefaultClip1, DMG_GENERIC);
					m_iClip1--;  // All Pills used 
				}

				if (!HasPrimaryAmmo() && Secondary) // No "else" because we want the pack removed instantly
				{
					m_iClip2--; // Ammo 1 are the available Pills
					// pPlayer->RemoveAmmo(1, m_iSecondaryAmmoType);
					m_iClip1 = i_Clip_Size; // Reloading Health Points for next usage
					break; // Preventing usage of other Pill cans
				}
			}	
		}


		if (Secondary < 1) // If no more Pills are available, remove the Pill Can Weapon
		{   pPlayer->SwitchToNextBestWeapon(this);
			DestroyItem();
		}

		// Every 0.15 seconds
		fl_Next_Charge = gpGlobals->curtime + 0.15f;
	}
}



//-----------------------------------------------------------------------------
// Animation event
//-----------------------------------------------------------------------------
void CWeaponHealthVial::Operator_HandleAnimEvent(animevent_t *pEvent, CBaseCombatCharacter *pOperator)
{
	switch( pEvent->event )
	{
	case EVENT_WEAPON_MELEE_HIT:
		HandleAnimEventMeleeHit( pEvent, pOperator );
		break;

	default:
		BaseClass::Operator_HandleAnimEvent( pEvent, pOperator );
		break;
	}
}
