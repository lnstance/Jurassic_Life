//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose:		Crowbar - an old favorite
//
// $NoKeywords: $
//=============================================================================//

#include "cbase.h"
#include "basehlcombatweapon.h"
#include "ai_basenpc.h"
#include "npcevent.h"
#include "mathlib/mathlib.h"


/* Currently not Used
#include "gamerules.h"
#include "ammodef.h"
#include "vstdlib/random.h"
#include "in_buttons.h"
*/

#include "player.h"
#include "soundent.h"
#include "basebludgeonweapon.h"
#include "weapon_healthkit.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"


//-----------------------------------------------------------------------------
// CWeaponHealthKit
//-----------------------------------------------------------------------------

IMPLEMENT_SERVERCLASS_ST(CWeaponHealthKit, DT_WeaponHealthkit)
END_SEND_TABLE()

//#ifndef HL2MP
LINK_ENTITY_TO_CLASS(weapon_healthkit, CWeaponHealthKit);
PRECACHE_WEAPON_REGISTER(weapon_healthkit);
//#endif
/*
acttable_t CWeaponHealthKit::m_acttable[] = 
{
	{ ACT_MELEE_ATTACK1,	ACT_MELEE_ATTACK_SWING, true },
	{ ACT_IDLE,				ACT_IDLE_ANGRY_MELEE,	false },
	{ ACT_IDLE_ANGRY,		ACT_IDLE_ANGRY_MELEE,	false },
};

IMPLEMENT_ACTTABLE(CWeaponHealthKit);
*/

//-----------------------------------------------------------------------------
// Constructor
//-----------------------------------------------------------------------------
CWeaponHealthKit::CWeaponHealthKit(void)
{	// Innitial Ammo Setup
	// m_iClip1 = this->GetWpnData().iMaxClip1;

	// if (m_iClip2 < 0) { m_iClip2 = 1; }

}

//-----------------------------------------------------------------------------
// Purpose: Get the damage amount for the animation we're doing
// Input  : hitActivity - currently played activity
// Output : Damage amount
//-----------------------------------------------------------------------------
float CWeaponHealthKit::GetDamageForActivity(Activity hitActivity)
{
	return 0;
}


//-----------------------------------------------------------------------------
// Purpose: Add in a view kick for this weapon
//-----------------------------------------------------------------------------
void CWeaponHealthKit::AddViewKick(void)
{
	CBasePlayer *pPlayer  = ToBasePlayer( GetOwner() );
	
	if ( pPlayer == NULL )
		return;

	QAngle punchAng;

	punchAng.x = random->RandomFloat( 1.0f, 2.0f );
	punchAng.y = random->RandomFloat( -2.0f, -1.0f );
	punchAng.z = 0.0f;
	
	pPlayer->ViewPunch( punchAng ); 
}


//-----------------------------------------------------------------------------
// Attempt to lead the target (needed because citizens can't hit manhacks with the crowbar!)
//-----------------------------------------------------------------------------

int CWeaponHealthKit::WeaponMeleeAttack1Condition(float flDot, float flDist)
{

	return COND_CAN_MELEE_ATTACK1;
}

int CWeaponHealthKit::WeaponMeleeAttack2Condition(float flDot, float flDist)
{
	return COND_CAN_MELEE_ATTACK2;
}


//-----------------------------------------------------------------------------
// Animation event handlers
//-----------------------------------------------------------------------------
void CWeaponHealthKit::HandleAnimEventMeleeHit(animevent_t *pEvent, CBaseCombatCharacter *pOperator)
{
	
}

//------------------------------------------------------------------------------
// Purpose :
// Input   :
// Output  :
//------------------------------------------------------------------------------
void CWeaponHealthKit::PrimaryAttack()
{   
	CBasePlayer *pPlayer = ToBasePlayer(GetOwner());
	if (pPlayer->GetHealth() < 100 && gpGlobals->curtime > fl_Next_Charge)
	{		
		if (HasPrimaryAmmo())
		{   pPlayer->TakeHealth(1, DMG_GENERIC);
			m_iClip1--; // Transferring Health points from Medkit to Player
		}		
		
		int Secondary = m_iClip2; // pPlayer->GetAmmoCount(this->GetSecondaryAmmoType());

		if (Secondary < 1 && !HasPrimaryAmmo()) // If no more Medkits are available, remove the Medkit Weapon
		{   pPlayer->SwitchToNextBestWeapon(this);
			DestroyItem();
		}

		if (!HasPrimaryAmmo() && Secondary)
		{   // m_iClip2--; // Ammo 2 are the available Medkits, we just consumed one			

			// pPlayer->RemoveAmmo(1, m_iSecondaryAmmoType);
			m_iClip2--;

			m_iClip1 = this->GetWpnData().iMaxClip1; // Reloading Health Points for next usage
		}



		// Every 0.15 seconds
		fl_Next_Charge = gpGlobals->curtime + 0.15f; 
	}
}

//------------------------------------------------------------------------------
// Purpose :
// Input   :
// Output  :
//------------------------------------------------------------------------------
void CWeaponHealthKit::SecondaryAttack()
{
	PrimaryAttack();
}



//-----------------------------------------------------------------------------
// Animation event
//-----------------------------------------------------------------------------
void CWeaponHealthKit::Operator_HandleAnimEvent(animevent_t *pEvent, CBaseCombatCharacter *pOperator)
{
	switch( pEvent->event )
	{
	case EVENT_WEAPON_MELEE_HIT:
		HandleAnimEventMeleeHit( pEvent, pOperator );
		break;

	default:
		BaseClass::Operator_HandleAnimEvent( pEvent, pOperator );
		break;
	}
}

