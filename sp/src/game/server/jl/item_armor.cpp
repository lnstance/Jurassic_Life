//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: Handling for the suit batteries.
//
// $NoKeywords: $
//=============================================================================//

#include "cbase.h"
#include "hl2_player.h"
#include "basecombatweapon.h"
#include "gamerules.h"
#include "items.h"
#include "engine/IEngineSound.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

// Todo; Turn this into a weapon
class CItem_Armor : public CItem
{
public:
	DECLARE_CLASS(CItem_Armor, CItem);

	void Spawn( void )
	{ 
		Precache( );
		SetModel( "models/items/battery.mdl" );
		BaseClass::Spawn( );
	}
	void Precache( void )
	{
		PrecacheModel ("models/items/battery.mdl");

		PrecacheScriptSound( "ItemBattery.Touch" );

	}
	bool MyTouch( CBasePlayer *pPlayer )
	{
		CHL2_Player *pHL2Player = dynamic_cast<CHL2_Player *>( pPlayer );

		// JL Imperial; For original way batteries worked you could pass any int value into "ApplyBattery(2)",
		// which will act as multipliyer. Means apply that many batteries. 
		// The amount of Armor points one battery itself applies is defined in the convar "sk_battery" in hl2_player.cpp

		// But I changed that to directly insert that value by ApplyBattery() now, and to grab health value from weapon_script.txt
		return ( pHL2Player && pHL2Player->ApplyBattery(50) );
	}
};

LINK_ENTITY_TO_CLASS(item_armor, CItem_Armor);
PRECACHE_REGISTER(item_armor);

