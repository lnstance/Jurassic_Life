//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose:		Crowbar - an old favorite
//
// $NoKeywords: $
//=============================================================================//

#include "cbase.h"
#include "basehlcombatweapon.h"
#include "ai_basenpc.h"
#include "npcevent.h"
#include "mathlib/mathlib.h"
#include "soundent.h"
#include "basebludgeonweapon.h"

/* Currently not Used
#include "player.h"
#include "gamerules.h"
#include "ammodef.h"
#include "vstdlib/random.h"
#include "in_buttons.h"
*/

#include "gamestats.h"


// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"


//-----------------------------------------------------------------------------
// C_Weapon_Radio
//-----------------------------------------------------------------------------
#ifndef WEAPON_RADIO
#define WEAPON_RADIO

#include "basebludgeonweapon.h"

#if defined( _WIN32 )
#pragma once
#endif

#ifdef HL2MP
#error weapon_hands.h must not be included in hl2mp. The windows compiler will use the wrong class elsewhere if it is.
#endif


//-----------------------------------------------------------------------------
// CWeapon_Radio
//-----------------------------------------------------------------------------

class CWeapon_Radio : public CBaseHLBludgeonWeapon
{
public:
	DECLARE_CLASS(CWeapon_Radio, CBaseHLBludgeonWeapon);

	DECLARE_SERVERCLASS();
	//DECLARE_ACTTABLE();

	float fl_Next_Charge = 0;

	CWeapon_Radio();

	virtual void WeaponIdle(void); // called when no buttons pressed
	void		DryFire(void);
	void		AddViewKick(void);
	float		GetDamageForActivity(Activity hitActivity);

	virtual int WeaponMeleeAttack1Condition(float flDot, float flDist);
	virtual int WeaponMeleeAttack2Condition(float flDot, float flDist);
	void		PrimaryAttack(void);
	void		SecondaryAttack();


	// Animation event
	virtual void Operator_HandleAnimEvent(animevent_t *pEvent, CBaseCombatCharacter *pOperator);

private:
	// Animation event handlers
	void HandleAnimEventMeleeHit(animevent_t *pEvent, CBaseCombatCharacter *pOperator);
};

#endif 



IMPLEMENT_SERVERCLASS_ST(CWeapon_Radio, DT_Weapon_Radio)
END_SEND_TABLE()

//#ifndef HL2MP
LINK_ENTITY_TO_CLASS(weapon_radio, CWeapon_Radio);
PRECACHE_WEAPON_REGISTER(weapon_radio);
//#endif
/*
acttable_t CWeapon_Radio::m_acttable[] = 
{
	{ ACT_MELEE_ATTACK1,	ACT_MELEE_ATTACK_SWING, true },
	{ ACT_IDLE,				ACT_IDLE_ANGRY_MELEE,	false },
	{ ACT_IDLE_ANGRY,		ACT_IDLE_ANGRY_MELEE,	false },
};

IMPLEMENT_ACTTABLE(CWeapon_Radio);
*/

//-----------------------------------------------------------------------------
// Constructor
//-----------------------------------------------------------------------------
CWeapon_Radio::CWeapon_Radio(void)
{
	fl_Next_Charge = 0;
	// SetThink(&CWeapon_Radio::HideThink);
	// SetNextThink(gpGlobals->curtime + 0.5f);
}

//-----------------------------------------------------------------------------
// Purpose: Get the damage amount for the animation we're doing
// Input  : hitActivity - currently played activity
// Output : Damage amount
//-----------------------------------------------------------------------------
float CWeapon_Radio::GetDamageForActivity(Activity hitActivity)
{
	return 0;
}

//-----------------------------------------------------------------------------
// Purpose: Add in a view kick for this weapon
//-----------------------------------------------------------------------------
void CWeapon_Radio::AddViewKick(void)
{
	CBasePlayer *pPlayer  = ToBasePlayer( GetOwner() );
	
	if ( pPlayer == NULL )
		return;

	QAngle punchAng;

	punchAng.x = random->RandomFloat( 1.0f, 2.0f );
	punchAng.y = random->RandomFloat( -2.0f, -1.0f );
	punchAng.z = 0.0f;
	
	pPlayer->ViewPunch( punchAng ); 
}


//-----------------------------------------------------------------------------
// Attempt to lead the target (needed because citizens can't hit manhacks with the crowbar!)
//-----------------------------------------------------------------------------

int CWeapon_Radio::WeaponMeleeAttack1Condition(float flDot, float flDist)
{

	return COND_CAN_MELEE_ATTACK1;
}

int CWeapon_Radio::WeaponMeleeAttack2Condition(float flDot, float flDist)
{
	return COND_CAN_MELEE_ATTACK2;
}


//-----------------------------------------------------------------------------
// Animation event handlers
//-----------------------------------------------------------------------------
void CWeapon_Radio::HandleAnimEventMeleeHit(animevent_t *pEvent, CBaseCombatCharacter *pOperator)
{
	
}

//-----------------------------------------------------------------------------
// Think
//-----------------------------------------------------------------------------
void CWeapon_Radio::WeaponIdle()
{
	CBasePlayer *pPlayer = ToBasePlayer(GetOwner());
	if (pPlayer && gpGlobals->curtime > fl_Next_Charge)
	{
		if (HasPrimaryAmmo())
		{   // if (pPlayer->GetAmmoCount(m_iPrimaryAmmoType))
			m_iClip1--; // Lowering Battery power every once in a while 			
		}	

		if (!HasPrimaryAmmo())
		{   m_iClip2--;
			DryFire();
			// m_iClip1 = this->GetWpnData().iMaxClip1; 
		}

		// Every 90 seconds
		fl_Next_Charge = gpGlobals->curtime + 90.0f;
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CWeapon_Radio::PrimaryAttack(void)
{
	CPASAttenuationFilter filter(this);
	EmitSound(filter, entindex(), "Weapon_Bugbait.Splat");

	SendWeaponAnim(ACT_VM_SECONDARYATTACK);
	// m_flNextPrimaryAttack = gpGlobals->curtime + SequenceDuration();
	// SendWeaponAnim(ACT_VM_HAULBACK);

	/*m_flTimeWeaponIdle = FLT_MAX;
	m_flNextPrimaryAttack = FLT_MAX;*/

	/*m_iPrimaryAttacks++;
	gamestats->Event_WeaponFired(pPlayer, true, GetClassname());*/
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CWeapon_Radio::SecondaryAttack(void)
{
	// Squeeze!
	CPASAttenuationFilter filter(this);
	EmitSound(filter, entindex(), "Weapon_Bugbait.Splat");

	SendWeaponAnim(ACT_VM_HAULBACK);
	// m_flNextSecondaryAttack = gpGlobals->curtime + SequenceDuration();

	/*CBasePlayer *pOwner = ToBasePlayer(GetOwner());
	if (pOwner)
	{
		m_iSecondaryAttacks++;
		gamestats->Event_WeaponFired(pOwner, false, GetClassname());
	}*/
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CWeapon_Radio::DryFire(void)
{
	WeaponSound(EMPTY);
	SendWeaponAnim(ACT_VM_DRYFIRE);
	// need to handle Event for sound !! red

	m_flNextPrimaryAttack = gpGlobals->curtime + SequenceDuration();
}

//-----------------------------------------------------------------------------
// Animation event
//-----------------------------------------------------------------------------
void CWeapon_Radio::Operator_HandleAnimEvent(animevent_t *pEvent, CBaseCombatCharacter *pOperator)
{
	switch( pEvent->event )
	{
	case EVENT_WEAPON_MELEE_HIT:
		HandleAnimEventMeleeHit( pEvent, pOperator );
		break;

	default:
		BaseClass::Operator_HandleAnimEvent( pEvent, pOperator );
		break;
	}
}
