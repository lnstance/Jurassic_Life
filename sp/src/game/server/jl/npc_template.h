//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: Dilophosaurus header
//
//=============================================================================//
#ifndef NPC_MELEE
#define NPC_MELEE
#ifdef _WIN32
#pragma once
#endif

#include "ai_basenpc.h"
#include "soundent.h"

class CMelee_NPC : public CAI_BaseNPC
{
	DECLARE_CLASS(CMelee_NPC, CAI_BaseNPC);
	DECLARE_DATADESC();
	DEFINE_CUSTOM_AI;

public:
	void	Precache( void );
	void	Spawn( void );
	Class_T Classify( void );
private:
	enum
	{
		SCHED_I_SCHEDULE = BaseClass::NEXT_SCHEDULE,
		SCHED_I_SCHEDULE2,
		NEXT_SCHEDULE
	};

	enum 
	{
		TASK_I_TASK = BaseClass::NEXT_TASK,
		TASK_I_TASK2,
		NEXT_TASK
	};

	enum 
	{
		COND_I_CONDITION = BaseClass::NEXT_CONDITION,
		COND_I_CONDITION2,
		NEXT_CONDITION
	};
};

LINK_ENTITY_TO_CLASS(npc_melee, CMelee_NPC);

//---------------------------------------------------------
// Save/Restore
//---------------------------------------------------------
BEGIN_DATADESC(CMelee_NPC)
END_DATADESC()


AI_BEGIN_CUSTOM_NPC(npc_melee, CMelee_NPC)
AI_END_CUSTOM_NPC()

#endif // NPC_MELEE