//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose:
//
//=============================================================================//

#ifndef WEAPON_HEALTHKIT
#define WEAPON_HEALTHKIT

#include "basebludgeonweapon.h"

#if defined( _WIN32 )
#pragma once
#endif

#ifdef HL2MP
#error weapon_hands.h must not be included in hl2mp. The windows compiler will use the wrong class elsewhere if it is.
#endif


//-----------------------------------------------------------------------------
// CWeaponCrowbar
//-----------------------------------------------------------------------------

class CWeaponHealthKit : public CBaseHLBludgeonWeapon
{
public:
	DECLARE_CLASS(CWeaponHealthKit, CBaseHLBludgeonWeapon);

	DECLARE_SERVERCLASS();
	//DECLARE_ACTTABLE();

	float fl_Next_Charge = 0;

	CWeaponHealthKit();

	//void		WeaponIdle( void );						// called when no buttons pressed

	void		AddViewKick( void );
	float		GetDamageForActivity( Activity hitActivity );

	virtual int WeaponMeleeAttack1Condition( float flDot, float flDist );
	virtual int WeaponMeleeAttack2Condition( float flDot, float flDist );
	void		PrimaryAttack( void );
	void		SecondaryAttack();


	// Animation event
	virtual void Operator_HandleAnimEvent( animevent_t *pEvent, CBaseCombatCharacter *pOperator );

private:	
	// Animation event handlers
	void HandleAnimEventMeleeHit( animevent_t *pEvent, CBaseCombatCharacter *pOperator );
};

#endif // WEAPON_HANDS_H
