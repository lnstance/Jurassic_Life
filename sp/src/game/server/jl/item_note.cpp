//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: Crate of items for Jurassic-Life
//
//=============================================================================//

#include "cbase.h"
#include "player.h"
#include "hl2_player.h" // Needed to get the Player
#include "gamerules.h"
#include "items.h"
#include "ammodef.h"
#include "eventlist.h"
#include "npcevent.h"
			  
#include <game/client/iviewport.h>

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"



// ==================================================================
// Notes, Objectives and Maps are handled by this Class
// ==================================================================

#define SF_REMOVE_AFTER_USED				0x0001
#define SF_BLINK							0x0002

class CJL_Item_Note : public CBaseAnimating
{
public:
	DECLARE_CLASS( CJL_Item_Note, CBaseAnimating );

	void	Spawn( void );
	void	Think(void);
	void	Precache( void );
	bool	CreateVPhysics1( void );

	//FIXME: May not want to have this used in a radius
	int		ObjectCaps( void ) { return (BaseClass::ObjectCaps() | (FCAP_IMPULSE_USE|FCAP_USE_IN_RADIUS)); };
	void	Use( CBaseEntity *pActivator, CBaseEntity *pCaller, USE_TYPE useType, float value );


private:
	void	InputKill( inputdata_t &data );
	void	Input_Change_Status(inputdata_t &data);
	void	Input_Activate_Objective(inputdata_t &data);

	
	virtual int OnTakeDamage( const CTakeDamageInfo &info );


protected:
	int m_iNoteId;
	bool m_bUsed;

	int X_Ratio = 7;
	int Y_Ratio = 10;
	int Document_Type = 0; // 0 = Notes, 1 = Objectives, 2 = Maps
	int Document_Status = 1;
	// char* Note_Text, Image_Path; // Inactive because I can't get a string array working in Player, 
	// And because Image Paths cost additional slots in max networked variables.

	COutputEvent	m_OnUsed;
	COutputEvent	m_On_Status_Changed;
	COutputEvent    m_On_Objective_Activated;

	DECLARE_DATADESC();
};

LINK_ENTITY_TO_CLASS( item_inventory, CJL_Item_Note );

BEGIN_DATADESC( CJL_Item_Note )
	DEFINE_KEYFIELD( m_iNoteId,	FIELD_INTEGER, "NoteId" ),
	DEFINE_KEYFIELD(Document_Type, FIELD_INTEGER, "Document_Type"),
	DEFINE_KEYFIELD(Document_Status, FIELD_INTEGER, "Document_Status"),
	// Replaced Ratios by Localization text because of network array limitations
	// DEFINE_KEYFIELD(X_Ratio, FIELD_INTEGER, "X_Ratio"), 
	// DEFINE_KEYFIELD(Y_Ratio, FIELD_INTEGER, "Y_Ratio"),

	// DEFINE_KEYFIELD(Note_Text, FIELD_STRING, "Note_Text"),
	// DEFINE_KEYFIELD(Image_Path, FIELD_STRING, "Image_Path"),


	DEFINE_FIELD( m_bUsed, FIELD_BOOLEAN ),
	
	DEFINE_OUTPUT( m_OnUsed, "OnUsed" ),
	DEFINE_OUTPUT(m_On_Objective_Activated, "On_Objective_Activated"),
	DEFINE_OUTPUT(m_On_Status_Changed, "On_Status_Changed"),

	DEFINE_INPUTFUNC(FIELD_VOID, "Kill", InputKill ),
	DEFINE_INPUTFUNC(FIELD_INTEGER, "Change_Objective_Status", Input_Change_Status),
	DEFINE_INPUTFUNC(FIELD_INTEGER, "Activate_Objective", Input_Activate_Objective),
	

	DEFINE_THINKFUNC(Think),
END_DATADESC()


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CJL_Item_Note::Spawn( void )
{
	BaseClass::Spawn();
	m_bUsed = false;

	if (Document_Type != 2) // Objectives are supposed to stay Invisible
	{   Precache();

		// Model string is set in the .fgd file
		const char * Model = STRING(GetModelName());
		if (strcmp(Model, "") == 0) { Model = "models/props_lab/clipboard.mdl"; } // Failsafe

		SetModel(Model);
		SetMoveType(MOVETYPE_VPHYSICS);
		SetSolid(SOLID_VPHYSICS);
		CreateVPhysics();

		SetSolid(SOLID_VPHYSICS);
		AddSolidFlags(FSOLID_NOT_STANDABLE);

		m_takedamage = DAMAGE_EVENTS_ONLY;
		if (HasSpawnFlags(SF_BLINK))
		{
			SetEffects(EF_ITEM_BLINK);
		}
	}

}

//------------------------------------------------------------------------------
// Purpose:
//------------------------------------------------------------------------------
bool CJL_Item_Note::CreateVPhysics1( void )
{
	return ( VPhysicsInitStatic() != NULL );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CJL_Item_Note::Precache( void )
{
	const char * Model = STRING(GetModelName());
	if (strcmp(Model, "") == 0) { Model = "models/props_lab/clipboard.mdl"; } // Failsafe

	PrecacheModel(Model);
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CJL_Item_Note::Think(void)
{
	
	SetNextThink(gpGlobals->curtime + 1.0f);
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CJL_Item_Note::Use( CBaseEntity *pActivator, CBaseEntity *pCaller, USE_TYPE useType, float value )
{
	if (!m_bUsed && Document_Type != 2) // Cause 2 Hidden Objectives
	{
		m_bUsed = true;
		CBasePlayer *pPlayer = ToBasePlayer( pActivator );

		if ( pPlayer == NULL ) return;

		// This is how Documents Work:
		// This function utilizes the Player.h/Player.cpp to store these values in networked Int arrays of Player.cpp 
		// Then they are synced between Server and Client and on the Client side they can be called in c_baseplayer.h and c_baseplayer.cpp
		// Inventory.cpp calls these values from the player int array and displays them to the User or makes c_baseplayer.h change their values.
			
		pPlayer->AddNote(Document_Type, Document_Status, m_iNoteId);	
		m_OnUsed.FireOutput( pActivator, this );

		if (HasSpawnFlags(SF_REMOVE_AFTER_USED && Document_Type != 2)) // 2 Is Objective Type
		{
			UTIL_Remove( this );
		}
		if (HasSpawnFlags(SF_BLINK) && Document_Type != 2)
		{
			SetEffects(0);
		}
	}
}

//-----------------------------------------------------------------------------
// Purpose: allows the crate to open up when hit by a crowbar
//-----------------------------------------------------------------------------
int CJL_Item_Note::OnTakeDamage( const CTakeDamageInfo &info )
{
	// if it's the player hitting us with a crowbar, open up
	CBasePlayer *player = ToBasePlayer(info.GetAttacker());
	if (player)
	{
		CBaseCombatWeapon *weapon = player->GetActiveWeapon();

		if (weapon && !stricmp(weapon->GetName(), "weapon_crowbar") && !stricmp(weapon->GetName(), "weapon_rusticmachete"))
		{
			// play the normal use sound
			player->EmitSound( "HL2Player.Use" );
			// open the crate
			//Use(info.GetAttacker(), info.GetAttacker(), USE_TOGGLE, 0.0f);
		}
	}

	// don't actually take any damage
	return 0;
}

//-----------------------------------------------------------------------------
// CAUTION: The INPUTFUNC above needs to be FIELD_INTEGER to work properly!! Otherwise it always receives Eg. 0 as FIELD_VOID 
//-----------------------------------------------------------------------------
void CJL_Item_Note::Input_Change_Status(inputdata_t &data)
{	// Grabbs the Value from Parameter Override
	int New_Status = data.value.Int();
	
	if (New_Status < 5) // Cause there is only 0-4
	{
		Document_Status = New_Status;
		
		CHL2_Player *Player = dynamic_cast<CHL2_Player*>(UTIL_GetLocalPlayer());
		if (Player)
		{	// Set the Status for the correct Note ID
			Player->Set_Doc_Status(m_iNoteId, New_Status);
			m_On_Status_Changed.FireOutput(this, this);			
		}
	}

	// DevMsg("Note ID is %d and Status is %d\n", m_iNoteId, New_Status);
}

//-----------------------------------------------------------------------------
// CAUTION: The INPUTFUNC above needs to be FIELD_INTEGER to work properly.
//-----------------------------------------------------------------------------
void CJL_Item_Note::Input_Activate_Objective(inputdata_t &data)
{
	CHL2_Player *Player = dynamic_cast<CHL2_Player*>(UTIL_GetLocalPlayer());
	if (Player)
	{
		int Target_Note_ID = data.value.Int(); // Specified in Hammer Parameter 
		// If not specified we assume this Document's Note ID is meaned.
		if (!Target_Note_ID) { Target_Note_ID = m_iNoteId; m_bUsed = true; }

		// Document_Type:
		// 0 = ID Cards and Keys, 1 = Notes, 2 = Objectives, 3 = Maps;

		// Document_Status:
		// Notes + Maps: 1 = Unreaded, 2 = Readed
		// Objectives: 0 = resolved, 1-3 = Priority stages, 4 = Hidden
		// Items, Keys and ID Cards: 0 = Used (can be dropped), 4-1 = Access level, the lower the number the greater access

		Player->AddNote(Document_Type, Document_Status, Target_Note_ID); // , X_Ratio, Y_Ratio);
		m_On_Objective_Activated.FireOutput(this, this);
		m_OnUsed.FireOutput(this, this);
	}
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CJL_Item_Note::InputKill( inputdata_t &data )
{
	UTIL_Remove( this );
}

