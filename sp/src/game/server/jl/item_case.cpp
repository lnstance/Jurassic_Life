//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: Crate of items for Jurassic-Life
//
//=============================================================================//
#include "cbase.h"
#include "player.h"
#include "gamerules.h"
// #include "items.h"
#include "ammodef.h"
#include "eventlist.h"
#include "npcevent.h"


#include "string_t.h"

#include "item_dynamic_resupply.h"

#include <game/client/iviewport.h>
#include "item_case.h"


#include <vgui/ILocalize.h>
#include <vgui_controls/Panel.h> // Needed for ILocalize

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"



// Ammo types
enum
{
	AMMOCRATE,
	NUM_AMMO_CRATE_TYPES,
};


LINK_ENTITY_TO_CLASS(jl_item_case, CJL_Item_Case);


// Server data table describing networked member variables (SendProps)
// DO NOT create this in the header! Put it in the main CPP file.
IMPLEMENT_SERVERCLASS_ST(CJL_Item_Case, DT_Item_Case)
	SendPropInt(SENDINFO(Scrollbar_Size), 8, SPROP_UNSIGNED),
	// SendPropFloat(SENDINFO(m_fMyFloat), 0, SPROP_NOSCALE),

	// Template was m_hViewModel and m_nCaseItems in player.cpp 
	SendPropArray3(SENDINFO_ARRAY3(m_nCaseItems), SendPropEHandle(SENDINFO_ARRAY(m_nCaseItems))),
	
END_SEND_TABLE()


BEGIN_DATADESC(CJL_Item_Case)

	//DEFINE_KEYFIELD( m_nAmmoType,	FIELD_INTEGER, "AmmoType" ),	

	//DEFINE_FIELD( m_flCloseTime, FIELD_FLOAT ),	
	// These can be recreated
	//DEFINE_FIELD( m_nAmmoIndex,		FIELD_INTEGER ),
	//DEFINE_FIELD( m_lpzModelNames,	FIELD_ ),
	//DEFINE_FIELD( m_lpzAmmoNames,	FIELD_ ),
	//DEFINE_FIELD( m_nAmmoAmounts,	FIELD_INTEGER ),
	//DEFINE_AUTO_ARRAY( m_nItems,FIELD_INTEGER),

	DEFINE_FIELD(m_hActivator, FIELD_EHANDLE),
	DEFINE_AUTO_ARRAY(m_nCaseItems, FIELD_EHANDLE),
	DEFINE_KEYFIELD(Scrollbar_Size, FIELD_INTEGER, "Scrollbar_Size"),


	// Weapon Slot Keys
	// Not present, we got a virtual selection function as slot 0
	// DEFINE_KEYFIELD(Slot[0], FIELD_STRING, "Slot_00"),
	DEFINE_KEYFIELD(Slot[1], FIELD_STRING, "Slot_01"),
	DEFINE_KEYFIELD(Slot[2], FIELD_STRING, "Slot_02"),
	DEFINE_KEYFIELD(Slot[3], FIELD_STRING, "Slot_03"),
	DEFINE_KEYFIELD(Slot[4], FIELD_STRING, "Slot_04"),
	DEFINE_KEYFIELD(Slot[5], FIELD_STRING, "Slot_05"),
	DEFINE_KEYFIELD(Slot[6], FIELD_STRING, "Slot_06"),
	DEFINE_KEYFIELD(Slot[7], FIELD_STRING, "Slot_07"),
	DEFINE_KEYFIELD(Slot[8], FIELD_STRING, "Slot_08"),
	DEFINE_KEYFIELD(Slot[9], FIELD_STRING, "Slot_09"),

	DEFINE_KEYFIELD(Slot[10], FIELD_STRING, "Slot_10"),
	DEFINE_KEYFIELD(Slot[11], FIELD_STRING, "Slot_11"),
	DEFINE_KEYFIELD(Slot[12], FIELD_STRING, "Slot_12"),
	DEFINE_KEYFIELD(Slot[13], FIELD_STRING, "Slot_13"),
	DEFINE_KEYFIELD(Slot[14], FIELD_STRING, "Slot_14"),
	DEFINE_KEYFIELD(Slot[15], FIELD_STRING, "Slot_15"),
	DEFINE_KEYFIELD(Slot[16], FIELD_STRING, "Slot_16"),
	DEFINE_KEYFIELD(Slot[17], FIELD_STRING, "Slot_17"),
	DEFINE_KEYFIELD(Slot[18], FIELD_STRING, "Slot_18"),
	DEFINE_KEYFIELD(Slot[19], FIELD_STRING, "Slot_19"),
	DEFINE_KEYFIELD(Slot[20], FIELD_STRING, "Slot_20"),

	DEFINE_KEYFIELD(Slot[21], FIELD_STRING, "Slot_21"),
	DEFINE_KEYFIELD(Slot[22], FIELD_STRING, "Slot_22"),
	DEFINE_KEYFIELD(Slot[23], FIELD_STRING, "Slot_23"),
	DEFINE_KEYFIELD(Slot[24], FIELD_STRING, "Slot_24"),
	DEFINE_KEYFIELD(Slot[25], FIELD_STRING, "Slot_25"),
	DEFINE_KEYFIELD(Slot[26], FIELD_STRING, "Slot_26"),
	DEFINE_KEYFIELD(Slot[27], FIELD_STRING, "Slot_27"),
	DEFINE_KEYFIELD(Slot[28], FIELD_STRING, "Slot_28"),
	DEFINE_KEYFIELD(Slot[29], FIELD_STRING, "Slot_29"),
	DEFINE_KEYFIELD(Slot[30], FIELD_STRING, "Slot_30"),

	DEFINE_KEYFIELD(Slot[31], FIELD_STRING, "Slot_31"),
	DEFINE_KEYFIELD(Slot[32], FIELD_STRING, "Slot_32"),
	DEFINE_KEYFIELD(Slot[33], FIELD_STRING, "Slot_33"),
	DEFINE_KEYFIELD(Slot[34], FIELD_STRING, "Slot_34"),
	DEFINE_KEYFIELD(Slot[35], FIELD_STRING, "Slot_35"),
	DEFINE_KEYFIELD(Slot[36], FIELD_STRING, "Slot_36"),
	DEFINE_KEYFIELD(Slot[37], FIELD_STRING, "Slot_37"),
	DEFINE_KEYFIELD(Slot[38], FIELD_STRING, "Slot_38"),
	DEFINE_KEYFIELD(Slot[39], FIELD_STRING, "Slot_39"),
	DEFINE_KEYFIELD(Slot[40], FIELD_STRING, "Slot_40"),

	DEFINE_KEYFIELD(Slot[41], FIELD_STRING, "Slot_41"),
	DEFINE_KEYFIELD(Slot[42], FIELD_STRING, "Slot_42"),
	DEFINE_KEYFIELD(Slot[43], FIELD_STRING, "Slot_43"),
	DEFINE_KEYFIELD(Slot[44], FIELD_STRING, "Slot_44"),
	DEFINE_KEYFIELD(Slot[45], FIELD_STRING, "Slot_45"),
	DEFINE_KEYFIELD(Slot[46], FIELD_STRING, "Slot_46"),
	DEFINE_KEYFIELD(Slot[47], FIELD_STRING, "Slot_47"),
	DEFINE_KEYFIELD(Slot[48], FIELD_STRING, "Slot_48"),
	DEFINE_KEYFIELD(Slot[49], FIELD_STRING, "Slot_49"),
	DEFINE_KEYFIELD(Slot[50], FIELD_STRING, "Slot_50"),

	DEFINE_KEYFIELD(Slot[51], FIELD_STRING, "Slot_51"),
	DEFINE_KEYFIELD(Slot[52], FIELD_STRING, "Slot_52"),
	DEFINE_KEYFIELD(Slot[53], FIELD_STRING, "Slot_53"),
	DEFINE_KEYFIELD(Slot[54], FIELD_STRING, "Slot_54"),
	DEFINE_KEYFIELD(Slot[55], FIELD_STRING, "Slot_55"),
	DEFINE_KEYFIELD(Slot[56], FIELD_STRING, "Slot_56"),
	DEFINE_KEYFIELD(Slot[57], FIELD_STRING, "Slot_57"),
	DEFINE_KEYFIELD(Slot[58], FIELD_STRING, "Slot_58"),
	DEFINE_KEYFIELD(Slot[59], FIELD_STRING, "Slot_59"),
	DEFINE_KEYFIELD(Slot[60], FIELD_STRING, "Slot_60"),

	DEFINE_KEYFIELD(Slot[61], FIELD_STRING, "Slot_61"),
	DEFINE_KEYFIELD(Slot[62], FIELD_STRING, "Slot_62"),
	DEFINE_KEYFIELD(Slot[63], FIELD_STRING, "Slot_63"),
	DEFINE_KEYFIELD(Slot[64], FIELD_STRING, "Slot_64"),
	DEFINE_KEYFIELD(Slot[65], FIELD_STRING, "Slot_65"),
	DEFINE_KEYFIELD(Slot[66], FIELD_STRING, "Slot_66"),
	DEFINE_KEYFIELD(Slot[67], FIELD_STRING, "Slot_67"),
	DEFINE_KEYFIELD(Slot[68], FIELD_STRING, "Slot_68"),
	DEFINE_KEYFIELD(Slot[69], FIELD_STRING, "Slot_69"),
	DEFINE_KEYFIELD(Slot[70], FIELD_STRING, "Slot_70"),

	DEFINE_KEYFIELD(Slot[71], FIELD_STRING, "Slot_71"),
	DEFINE_KEYFIELD(Slot[72], FIELD_STRING, "Slot_72"),
	DEFINE_KEYFIELD(Slot[73], FIELD_STRING, "Slot_73"),
	DEFINE_KEYFIELD(Slot[74], FIELD_STRING, "Slot_74"),
	DEFINE_KEYFIELD(Slot[75], FIELD_STRING, "Slot_75"),
	DEFINE_KEYFIELD(Slot[76], FIELD_STRING, "Slot_76"),
	DEFINE_KEYFIELD(Slot[78], FIELD_STRING, "Slot_77"),
	DEFINE_KEYFIELD(Slot[78], FIELD_STRING, "Slot_78"),
	DEFINE_KEYFIELD(Slot[79], FIELD_STRING, "Slot_79"),
	DEFINE_KEYFIELD(Slot[80], FIELD_STRING, "Slot_80"),

	DEFINE_KEYFIELD(Slot[81], FIELD_STRING, "Slot_81"),
	DEFINE_KEYFIELD(Slot[82], FIELD_STRING, "Slot_82"),
	DEFINE_KEYFIELD(Slot[83], FIELD_STRING, "Slot_83"),
	DEFINE_KEYFIELD(Slot[84], FIELD_STRING, "Slot_84"),
	DEFINE_KEYFIELD(Slot[85], FIELD_STRING, "Slot_85"),
	DEFINE_KEYFIELD(Slot[86], FIELD_STRING, "Slot_86"),
	DEFINE_KEYFIELD(Slot[87], FIELD_STRING, "Slot_87"),
	DEFINE_KEYFIELD(Slot[88], FIELD_STRING, "Slot_88"),
	DEFINE_KEYFIELD(Slot[89], FIELD_STRING, "Slot_89"),
	DEFINE_KEYFIELD(Slot[90], FIELD_STRING, "Slot_90"),

	DEFINE_KEYFIELD(Slot[91], FIELD_STRING, "Slot_91"),
	DEFINE_KEYFIELD(Slot[92], FIELD_STRING, "Slot_92"),
	DEFINE_KEYFIELD(Slot[93], FIELD_STRING, "Slot_93"),
	DEFINE_KEYFIELD(Slot[94], FIELD_STRING, "Slot_94"),
	DEFINE_KEYFIELD(Slot[95], FIELD_STRING, "Slot_95"),
	DEFINE_KEYFIELD(Slot[96], FIELD_STRING, "Slot_96"),
	DEFINE_KEYFIELD(Slot[97], FIELD_STRING, "Slot_97"),
	DEFINE_KEYFIELD(Slot[98], FIELD_STRING, "Slot_98"),
	DEFINE_KEYFIELD(Slot[99], FIELD_STRING, "Slot_99"),
	DEFINE_KEYFIELD(Slot[100], FIELD_STRING, "Slot_100"),

	DEFINE_KEYFIELD(Slot[101], FIELD_STRING, "Slot_101"),
	DEFINE_KEYFIELD(Slot[102], FIELD_STRING, "Slot_102"),
	DEFINE_KEYFIELD(Slot[103], FIELD_STRING, "Slot_103"),
	DEFINE_KEYFIELD(Slot[104], FIELD_STRING, "Slot_104"),
	DEFINE_KEYFIELD(Slot[105], FIELD_STRING, "Slot_105"),
	DEFINE_KEYFIELD(Slot[106], FIELD_STRING, "Slot_106"),
	DEFINE_KEYFIELD(Slot[107], FIELD_STRING, "Slot_107"),
	DEFINE_KEYFIELD(Slot[108], FIELD_STRING, "Slot_108"),
	DEFINE_KEYFIELD(Slot[109], FIELD_STRING, "Slot_109"),
	DEFINE_KEYFIELD(Slot[110], FIELD_STRING, "Slot_110"),

	DEFINE_KEYFIELD(Slot[111], FIELD_STRING, "Slot_111"),
	DEFINE_KEYFIELD(Slot[112], FIELD_STRING, "Slot_112"),
	DEFINE_KEYFIELD(Slot[113], FIELD_STRING, "Slot_113"),
	DEFINE_KEYFIELD(Slot[114], FIELD_STRING, "Slot_114"),
	DEFINE_KEYFIELD(Slot[115], FIELD_STRING, "Slot_115"),
	DEFINE_KEYFIELD(Slot[116], FIELD_STRING, "Slot_116"),
	DEFINE_KEYFIELD(Slot[117], FIELD_STRING, "Slot_117"),
	DEFINE_KEYFIELD(Slot[118], FIELD_STRING, "Slot_118"),
	DEFINE_KEYFIELD(Slot[119], FIELD_STRING, "Slot_119"),
	DEFINE_KEYFIELD(Slot[120], FIELD_STRING, "Slot_120"),

	DEFINE_KEYFIELD(Slot[121], FIELD_STRING, "Slot_121"),
	DEFINE_KEYFIELD(Slot[122], FIELD_STRING, "Slot_122"),
	DEFINE_KEYFIELD(Slot[123], FIELD_STRING, "Slot_123"),
	DEFINE_KEYFIELD(Slot[124], FIELD_STRING, "Slot_124"),
	DEFINE_KEYFIELD(Slot[125], FIELD_STRING, "Slot_125"),
	DEFINE_KEYFIELD(Slot[126], FIELD_STRING, "Slot_126"),
	DEFINE_KEYFIELD(Slot[127], FIELD_STRING, "Slot_127"),
	DEFINE_KEYFIELD(Slot[128], FIELD_STRING, "Slot_128"),
	DEFINE_KEYFIELD(Slot[129], FIELD_STRING, "Slot_129"),
	DEFINE_KEYFIELD(Slot[130], FIELD_STRING, "Slot_130"),

	DEFINE_KEYFIELD(Slot[131], FIELD_STRING, "Slot_131"),
	DEFINE_KEYFIELD(Slot[132], FIELD_STRING, "Slot_132"),
	DEFINE_KEYFIELD(Slot[133], FIELD_STRING, "Slot_133"),
	DEFINE_KEYFIELD(Slot[134], FIELD_STRING, "Slot_134"),
	DEFINE_KEYFIELD(Slot[135], FIELD_STRING, "Slot_135"),
	DEFINE_KEYFIELD(Slot[136], FIELD_STRING, "Slot_136"),
	DEFINE_KEYFIELD(Slot[137], FIELD_STRING, "Slot_137"),
	DEFINE_KEYFIELD(Slot[138], FIELD_STRING, "Slot_138"),
	DEFINE_KEYFIELD(Slot[139], FIELD_STRING, "Slot_139"),
	DEFINE_KEYFIELD(Slot[140], FIELD_STRING, "Slot_140"),




	DEFINE_OUTPUT(m_OnUsed, "OnUsed"),

	DEFINE_INPUTFUNC(FIELD_VOID, "Kill", InputKill),

	DEFINE_THINKFUNC(CrateThink),

END_DATADESC()


//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------

// Models names
// const char *CJL_Item_Case::m_lpzModelNames = "models/items/ammocrate.mdl";


//------------------------------------------------------------------------------
// Purpose:
//------------------------------------------------------------------------------
bool CJL_Item_Case::CreateVPhysics( void )
{
	return ( VPhysicsInitStatic() != NULL );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CJL_Item_Case::Precache( void )
{   // SetModelName(AllocPooledString(m_lpzModelNames));
	//m_nAmmoIndex = GetAmmoDef()->Index( m_lpzAmmoNames[m_nAmmoType] );

	// Model string is set in the .fgd file
	const char * Model = STRING(GetModelName());
	if (strcmp(Model, "") == 0) { Model = "models/jl/boxtop.mdl"; } // Failsafe
	
	PrecacheModel(Model);
	SetModel(Model);

	PrecacheScriptSound( "AmmoCrate.Open" );
	PrecacheScriptSound( "AmmoCrate.Close" );
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CJL_Item_Case::OnRestore( void )
{
	BaseClass::OnRestore();

	// Restore our internal state
	Precache();
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CJL_Item_Case::Spawn( void )
{
	Precache(); // Deals with the model
	BaseClass::Spawn();


	SetMoveType( MOVETYPE_NONE );
	SetSolid( SOLID_VPHYSICS );
	CreateVPhysics();

	ResetSequence( LookupSequence( "Idle" ) );
	SetBodygroup( 1, true );

	m_flCloseTime = gpGlobals->curtime;
	m_flAnimTime = gpGlobals->curtime;
	m_flPlaybackRate = 0.0;
	SetCycle( 0 );

	m_takedamage = DAMAGE_EVENTS_ONLY;

	SetSolid( SOLID_BBOX );
	AddSolidFlags( FSOLID_NOT_STANDABLE );

	open = true;



	// Spawning all Inventory of this Case:
	// CBaseCombatWeapon *pSpawn = NULL;
	// pSpawn = CreateEntityByName(STRING(m_strItemClass));

	/*CBaseCombatWeapon *Weapon = (CBaseCombatWeapon *)CreateEntityByName("weapon_357");
	if (Weapon)
	{*/
		/*pWeapon->SetAbsOrigin(GetAbsOrigin());
		pWeapon->SetOwner(this);
		pWeapon->SetIndex(index);
		DispatchSpawn(pWeapon);
		pWeapon->FollowEntity(this);*/
		// m_nCaseItems[0] = pWeapon;

		// pWeapon->SetLocalOrigin(GetLocalOrigin());
		// pWeapon->AddSpawnFlags(SF_NORESPAWN);

		// pPlayer->SetWeapon(0, pWeapon);
		// DispatchSpawn(pWeapon);



		/*
		// Spawn at position of this class and give its position a little randomness...
		Vector vecOrigin;
		CollisionProp()->RandomPointInBounds(Vector(0.25, 0.25, 0.25), Vector(0.75, 0.75, 0.75), &vecOrigin);
		// Set it to the origin of this class!
		// Weapon->SetAbsOrigin(GetAbsOrigin() + Vector(0.0, 0.0, 200.0));
		Weapon->SetAbsOrigin(Vector(0.0, 0.0, 200.0));

		QAngle vecAngles;
		vecAngles.x = random->RandomFloat(-20.0f, 20.0f);
		vecAngles.y = random->RandomFloat(0.0f, 360.0f);
		vecAngles.z = random->RandomFloat(-20.0f, 20.0f);
		Weapon->SetAbsAngles(vecAngles);

		Vector vecActualVelocity;
		vecActualVelocity.Random(-10.0f, 10.0f);
		//		vecActualVelocity += vecVelocity;
		Weapon->SetAbsVelocity(vecActualVelocity);

		QAngle angVel;
		//AngularImpulseToQAngle(angImpulse, angVel);
		Weapon->SetLocalAngularVelocity(angVel);
		*/

		//Weapon->Spawn(); // Just as invisible entity without Origin or Velocity

		// Place it into that slot of this crate
		// m_nCaseItems.Set(6, (CHandle<CBaseCombatWeapon>)Weapon);
		// m_nCaseItems.Set(4, Weapon);


		// None of this works.. to hide model:
		// Weapon->SetWeaponVisible(false);
		// Weapon->PrecacheModel(""); Weapon->SetModel(""); 
		// Weapon->m_nViewModelIndex = NULL;
		// Weapon->m_iWorldModelIndex = NULL;

		// Weapon->AddEffects(EF_DIMLIGHT);
		// EF_BONEMERGE // = Performs bone merge on client side

	// }


	// pPlayer->GiveNamedItem("weapon_357");



	/*
	char* Spawn_List [] =
	{	"weapon_spas12",
		"weapon_smg1", 		
		"weapon_frag",
		"weapon_357",				 		
	};
	*/

	/*
	int Slots[120] = // This just points to the values of the slots above when called
	{   Slot_1, Slot_2, Slot_3, Slot_4, Slot_5, Slot_6, Slot_7, Slot_8, Slot_9, Slot_10,
		Slot_11, Slot_12, Slot_13, Slot_14, Slot_15, Slot_16, Slot_17, Slot_18, Slot_19, Slot_20,
		Slot_21, Slot_22, Slot_23, Slot_24, Slot_25, Slot_26, Slot_27, Slot_28, Slot_29, Slot_30,
		Slot_31, Slot_32, Slot_33, Slot_34, Slot_35, Slot_36, Slot_37, Slot_38, Slot_39, Slot_40,
		Slot_41, Slot_42, Slot_43, Slot_44, Slot_45, Slot_46, Slot_47, Slot_48, Slot_49, Slot_50,
		Slot_51, Slot_52, Slot_53, Slot_54, Slot_55, Slot_56, Slot_57, Slot_58, Slot_59, Slot_60,
		Slot_61, Slot_62, Slot_63, Slot_64, Slot_65, Slot_66, Slot_67, Slot_68, Slot_69, Slot_70,
		Slot_71, Slot_72, Slot_73, Slot_74, Slot_75, Slot_76, Slot_77, Slot_78, Slot_79, Slot_80,
		Slot_81, Slot_82, Slot_83, Slot_84, Slot_85, Slot_86, Slot_87, Slot_88, Slot_89, Slot_90,
		Slot_91, Slot_92, Slot_93, Slot_94, Slot_95, Slot_96, Slot_97, Slot_98, Slot_99, Slot_100,
		Slot_101, Slot_102, Slot_103, Slot_104, Slot_105, Slot_106, Slot_107, Slot_108, Slot_109, Slot_110,
		Slot_111, Slot_112, Slot_113, Slot_114, Slot_115, Slot_116, Slot_117, Slot_118, Slot_119, Slot_120
	};
	*/


	int Slot_Number = 0;
	int Slot_Type = 0;
	

	
	// for (int i = 0; i < ARRAYSIZE(Spawn_List); ++i) // For each string in the 2D Array

	// Spawning the Weapons into this Crate/Case 
	for (int i = 0; i < ARRAYSIZE(Slot); ++i)
	{   
		if (!STRING(Slot[i])[0]) { continue; }




		char* Current_Slot = (char*)STRING(Slot[i]);
		char Value_1[80] = "\0";
		char Value_2[80] = "\0";
		char Value_3[80] = "\0";

	
		

		bool Delimited = false;

		char* Delimiter = " "; // These variants are legit
		if (Contains_Character(Current_Slot, " ", 10)) { Delimited = true; }
		else if (Contains_Character(Current_Slot, ".", 10)) { Delimited = true; Delimiter = "."; }
		else if (Contains_Character(Current_Slot, ",", 10)) { Delimited = true; Delimiter = ","; }


		if (Delimited) // If Delimited we use the 1st parameter as Current_Slot (weapon name) to be spawned 
		{					
			Split_Array(Current_Slot, Value_1, *Delimiter, 1);
			Split_Array(Current_Slot, Value_2, *Delimiter, 2);
			Split_Array(Current_Slot, Value_3, *Delimiter, 3);


			if (Compare(Value_1, "item") || Compare(Value_1, "note") || Compare(Value_1, "objective") || Compare(Value_1, "map"))
			{ Current_Slot = "weapon_item"; } // Choosing a weapon type to spawn as proxy for our item
			else { Current_Slot = Value_1; } // Otherwise we just stick to the 1st value as weapon name

			// DevMsg("1st Value is %s\n", Value_1);
			// DevMsg("2nd Value is %s\n", Value_2);
			// DevMsg("3rd Value is %s\n", Value_3);
		}



		/*
		char Entity[8]; Entity[0] = 0;
		// Format Slot_ + content of what ever was set in hammer to be value of value i in Slots[]
		Q_snprintf(Entity, sizeof(Entity), "Slot_%d", Slots[i]);
		if (!Entity[0] | !Slots[i]) { continue; }
	
		char Weapon_Name[80]; Weapon_Name[0] = 0;
		Localize_Text(Entity, Weapon_Name);
		*/


		// DevMsg("Name is %s\n", Weapon_Name);

		/*
		EHANDLE Weapon_Handle;
		Weapon_Handle = CreateEntityByName(Weapon_Name);
		if (Weapon_Handle == NULL) { continue; }
		Weapon_Handle->AddSpawnFlags(SF_NORESPAWN);
		*/



		// CBaseCombatWeapon *Weapon = (CBaseCombatWeapon *)CreateEntityByName((char*)Spawn_List[i]);
		// CBaseCombatWeapon *Weapon = dynamic_cast<CBaseCombatWeapon*>((CBaseEntity*)Weapon_Handle);
		// CBaseCombatWeapon *Weapon = (CBaseCombatWeapon *)CreateEntityByName(Weapon_Name);


		CBaseCombatWeapon *Weapon = (CBaseCombatWeapon *)CreateEntityByName(Current_Slot);
		if (Weapon)
		{
			
			CBaseEntity *pMoveParent = GetMoveParent();
	
			if (pMoveParent) 
			{
				Vector Loc = pMoveParent->GetLocalOrigin();
				Weapon->SetAbsOrigin(Loc);
			}
			// If no parent is available it just spawns all that stuff over the crate...
			else { Weapon->SetAbsOrigin(Vector(0.0, 0.0, 200.0)); }


	
			// Must not spawn outside of the Level/skydome brushes, otherwise it will be removed by some automatic code ..
			Weapon->Spawn();

			//DispatchSpawn(Weapon); // As EHANDLE entity

			// Preventing a stack of weapons from crashing with eachother, which slows down the map!!
			Weapon->SetMoveType(MOVETYPE_NONE);	

		

	
			// if (strcmp(Weapon->GetName(), "weapon_item") == 0 && Delimited) // If that weapon is of the required class
			if (Delimited)
			{						
				// Document_Status:
				// Notes + Maps: 1 = Unreaded, 2 = Readed
				// Objectives: 0 = resolved, 1-3 = Priority stages, 4 = Hidden
				// Items, Keys and ID Cards: 0 = Used (can be dropped), 4-1 = Access level, the lower the number the greater access


				// Behind the curtains, we just use maxhealth and the ammo peroperty to store information about the entity!			
				// GetMaxHealth() = Item Type
				// GetPrimaryAmmoCount() = Note/Item ID
				// GetSecondaryAmmoCount() = Item Status

			
				// Possible for GetMaxHealth():   5 = Item, 7 = note, 8 = objective, 9 = map	
				int Item_Spawns_As = 7; // Defaults to Note
				if (Compare(Value_1, "item")) { Item_Spawns_As = 5; }
				else if (Compare(Value_1, "objective")) { Item_Spawns_As = 8; }
				else if (Compare(Value_1, "map")) { Item_Spawns_As = 9; }


				// This is how you set Item type, Item Status and Item ID in memory (for each object):
				Weapon->SetMaxHealth(Item_Spawns_As);

	

				if (Value_2 != "\0") { Weapon->SetPrimaryAmmoCount(atoi(Value_2)); }
				if (Value_3 != "\0") { Weapon->SetSecondaryAmmoCount(atoi(Value_3)); }


				DevMsg("Setting Item Type to %d\n", Weapon->GetMaxHealth());
				DevMsg("Setting Item ID to %d\n", Weapon->GetPrimaryAmmoCount());
				DevMsg("Setting Item State to %d\n\n", Weapon->GetSecondaryAmmoCount());
			}



			int Item_Type = Weapon->GetItemType(); // Weapon->GetWpnData().iItemType;					
			// DevMsg("%s has item type %d, slot type is %d\n", Weapon->GetName(), Item_Type, Slot_Type);
		

			
			int Slot_Size_Switch = 0;
			int Counter = 0;


			// ================ Sorting Weapons into a slot of the right size ===============

			for (int i = 0; i< 122; i++) // 122 Slots per Crate
			{
				Counter++; // Preparing for next iteration
				if (Counter > 4) { Counter = 1; Slot_Size_Switch++; } // Reset
				if (Slot_Size_Switch > 2){ Slot_Size_Switch = 0; } // Cycling between type 0, 1 and 2


				if (m_nCaseItems[i]) { continue; } // Slot is not free

				if (Item_Type > 2 && Slot_Size_Switch == 1) {} // Medium slot size is standard for all Item_Types larger then 2
				else if (Item_Type != Slot_Size_Switch) { continue; } // Because wrong slot type


				m_nCaseItems.Set(i, Weapon); // Finally place it into the next available Free_Slot of its type!
				DevMsg("Count %d\n", Counter);
				DevMsg("%s has slot %d, slot type is %d\n", Weapon->GetName(), i, Slot_Size_Switch);
				// DevMsg("%s has item type %d, slot type is %d\n", Weapon->GetName(), Item_Type, Slot_Size_Switch);
				break;
			}

			
		}
    }


}





//------------------------------------------------------------------------------
// Purpose:
//------------------------------------------------------------------------------
void CJL_Item_Case::Move_Items_Into_Player_Crate()
{		
	int Empty_Slots = 0;


	for (int i = 0; i < ARRAYSIZE(Slot); ++i)
	{
		int Slot_Shift = 0; // Reset
		int Free_Slot = -1;


		CBasePlayer *Player = (CBasePlayer*)gEntList.FindEntityByClassname(NULL, "player");
		if (!Player) { continue; }


		if (Empty_Slots > 20) { return; } // No point in looping for ever in a empty crate..

		// Getting weapon in the own object slots:
		CBaseCombatWeapon *Weapon = m_nCaseItems[i];

		if (Weapon) { Empty_Slots = 0; } // Reset
		else { Empty_Slots++; continue; }

		
		m_nCaseItems.Set(i, NULL); // Clearing in this crate
		

		
		int Item_Type = Weapon->GetItemType(); // Weapon->GetWpnData().iItemType;					
		// DevMsg("%s has item type %d, slot type is %d\n", Weapon->GetName(), Item_Type, Slot_Type);





		int Slot_Size_Switch = 0;
		int Counter = 0;

		// ================ Sorting Weapons into a slot of the right size ===============

		for (int i = 0; i< 122; i++) // 122 Slots per Crate
		{
			Counter++; // Preparing for next iteration
			if (Counter > 4) { Counter = 1; Slot_Size_Switch++; } // Reset
			if (Slot_Size_Switch > 2){ Slot_Size_Switch = 0; } // Cycling between type 0, 1 and 2


			if (Player->Get_Player_Case(i)) { continue; } // Slot is not free

			if (Item_Type > 2 && Slot_Size_Switch == 1) {} // Medium slot size is standard for all Item_Types larger then 2
			else if (Item_Type != Slot_Size_Switch) { continue; } // Because wrong slot type


			Player->Set_Player_Case(i, Weapon); // Finally place it into the next available Free_Slot of its type!

			// DevMsg("Count %d\n", Counter);
			// DevMsg("%s has slot %d, slot type is %d\n", Weapon->GetName(), i, Slot_Size_Switch);
			// DevMsg("%s has item type %d, slot type is %d\n", Weapon->GetName(), Item_Type, Slot_Size_Switch);
			break;
		}

	}
}


//-----------------------------------------------------------------------------
// Purpose: 
// Input  : *pActivator - 
//			*pCaller - 
//			useType - 
//			value - 
//-----------------------------------------------------------------------------
void CJL_Item_Case::Use( CBaseEntity *pActivator, CBaseEntity *pCaller, USE_TYPE useType, float value )
{
	CBasePlayer *pPlayer = ToBasePlayer( pActivator );

	if ( pPlayer == NULL )
		return;


	// This command could be used to inject any Note/Item/Objective once a crate is used:
	// pPlayer->AddNote(1, 1, 2); 





	pPlayer->Set_Active_Case(this); // So we can refer to it in ButtonSlot.cpp!
	// Msg("Type is %d\n", Get_Crate_Item(0)->GetWpnData().iItemType);
	// Msg("Type is %d\n", pPlayer->Get_Active_Case()->Get_Crate_Item(0)->GetWpnData().iItemType);


	// Need to search the player entity now, because we missed the timing to do that on map spawn.
	if (Scrollbar_Size == 0) { Move_Items_Into_Player_Crate(); }


	m_OnUsed.FireOutput( pActivator, this );

	int iSequence = LookupSequence( "Open" );

	// See if we're not opening already
	if ( GetSequence() != iSequence )
	{
		Vector mins, maxs;
		trace_t tr;

		CollisionProp()->WorldSpaceAABB( &mins, &maxs );

		Vector vOrigin = GetAbsOrigin();
		vOrigin.z += ( maxs.z - mins.z );
		mins = (mins - GetAbsOrigin()) * 0.2f;
		maxs = (maxs - GetAbsOrigin()) * 0.2f;
		mins.z = ( GetAbsOrigin().z - vOrigin.z );  
		
		UTIL_TraceHull( vOrigin, vOrigin, mins, maxs, MASK_SOLID, this, COLLISION_GROUP_NONE, &tr );

		if ( tr.startsolid || tr.allsolid )
			 return;
			
		m_hActivator = pPlayer;

		// Animate!
		ResetSequence( iSequence );

		// Make sound
		CPASAttenuationFilter sndFilter( this, "AmmoCrate.Open" );
		EmitSound( sndFilter, entindex(), "AmmoCrate.Open" );

		// Start thinking to make it return
		SetThink( &CJL_Item_Case::CrateThink );
		SetNextThink( gpGlobals->curtime + 0.1f );
	}




	
	ConVar *pOpen  = cvar->FindVar( "jl_showcasepanel" );
	pOpen->SetValue( 1 );
	open = true;
}

//-----------------------------------------------------------------------------
// Purpose: allows the crate to open up when hit by a crowbar
//-----------------------------------------------------------------------------
int CJL_Item_Case::OnTakeDamage( const CTakeDamageInfo &info )
{
	// if it's the player hitting us with a crowbar, open up
	CBasePlayer *player = ToBasePlayer(info.GetAttacker());
	if (player)
	{
		CBaseCombatWeapon *weapon = player->GetActiveWeapon();

		if (weapon && !stricmp(weapon->GetName(), "weapon_crowbar"))
		{
			// play the normal use sound
			player->EmitSound( "HL2Player.Use" );
			// open the crate
			//Use(info.GetAttacker(), info.GetAttacker(), USE_TOGGLE, 0.0f);
		}
	}

	// don't actually take any damage
	return 0;
}


//-----------------------------------------------------------------------------
// Purpose: Catches the monster-specific messages that occur when tagged
//			animation frames are played.
// Input  : *pEvent - 
//-----------------------------------------------------------------------------
void CJL_Item_Case::HandleAnimEvent( animevent_t *pEvent )
{
	if ( pEvent->event == AE_AMMOCRATE_PICKUP_AMMO )
	{
		if ( m_hActivator )
		{
			//if ( m_spawnflags & SF_CASE_SHOTGUN )
				//m_hActivator->Weapon_Create("Weapon_Pistol");

			m_hActivator = NULL;
		}
		return;
	}

	BaseClass::HandleAnimEvent( pEvent );
}

	
//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CJL_Item_Case::CrateThink( void )
{
	StudioFrameAdvance();
	DispatchAnimEvents( this );

	SetNextThink( gpGlobals->curtime + 0.1f );

	if ( open == true)
	{
		if ( GetSequence() == LookupSequence( "Open" ) )
			if ( IsSequenceFinished() )
			{
				ConVar *pOpen  = cvar->FindVar( "jl_showcasepanel" );
				//DevMsg("Var %d %d \n",pOpen->GetInt(),open);
				if (!pOpen->GetInt()) 
				{
					open = false;
					ResetSequence( LookupSequence( "Close" ) );
					CPASAttenuationFilter sndFilter( this, "AmmoCrate.Close" );
					EmitSound( sndFilter, entindex(), "AmmoCrate.Close" );

					CBasePlayer *pPlayer = (CBasePlayer*)gEntList.FindEntityByClassname(NULL, "player");
					pPlayer->Set_Active_Case(NULL); 
				}
			}

	}
	else
	{
		if ( GetSequence() == LookupSequence( "Close" ) && IsSequenceFinished() )
		{
			ResetSequence( LookupSequence( "Idle" ) );
			//SetBodygroup( 1, true );
			//SetThink( NULL );			
		}
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : &data - 
//-----------------------------------------------------------------------------
void CJL_Item_Case::InputKill( inputdata_t &data )
{
	UTIL_Remove( this );
}

void CJL_Item_Case::Close( void )
{
	open = false;
}

void CJL_Item_Case::Open( void )
{
	open = true;
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CJL_Item_Case::Localize_Text(char* Entry, char* Result)
{   /*char Buffer[1024]; Buffer[0] = 0;*/
	g_pVGuiLocalize->ConvertUnicodeToANSI(g_pVGuiLocalize->Find(Entry), Result, 1024);
	// Result* = (char*)&Buffer;
}




//-----------------------------------------------------------------------------
// Purpose: String Comparsion
//-----------------------------------------------------------------------------
bool CJL_Item_Case::Compare(char* value_1, char* value_2)
{
	if (strcmp(value_1, value_2) == 0) { return true; }
	return false;
}


//-----------------------------------------------------------------------------
// Loop until the delimiter char sign is found ',' then return the first value in value_2
//-----------------------------------------------------------------------------
bool CJL_Item_Case::Contains_Character(char* value, char* character, int string_size)
{	
	// Defaults to 80, which is used in most cases but can be overwritten if necessary
	for (int i = 0; i < string_size; i++)
	{
		if (value[i] == *character)
		{
			return true;
		}	
	}
	return false;
}


//-----------------------------------------------------------------------------
// Loop until the delimiter char sign is found ',' then return the first value in value_2
//-----------------------------------------------------------------------------
char* CJL_Item_Case::Split_Array(char* value_1, char* value_2, char delimiter, int word_number)
{
	int Word_Count = 0;
	int Charater_Slot = 0;

	for (int i = 0; i < 80; i++) // 80 is used in most cases
	{
		if (value_1[i] == delimiter)
		{
			Word_Count++; // We raise wordcount after each delimiter, if count matches desired word we return it
			if (Word_Count == word_number) { value_2[Charater_Slot] = '\0'; return value_2; }
			else { Charater_Slot = 0; } // Otherwise we start overwriting by the next word/value
		}
		else if (value_1[i] != ' ') // Ignoring emptyspaces
		{   // We transfere values over to the new char array
			value_2[Charater_Slot] = value_1[i];
			Charater_Slot++;
		}
	}
	return null;
}
