#include "mathlib/vector.h"
#include <string>
#include "Custom_Hull.h"
#include "util.h"
#include "dbg.h"
#include "jl\rapidjson\rapidjson.h"
#include "jl\rapidjson\document.h"

using namespace rapidjson;

class L_JSON_HullParser
{
public:
	static CustomHull getHullData(std::string hullName);


	static Vector parseJSONVector(Value& targetValue);
	
};