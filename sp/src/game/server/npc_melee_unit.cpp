//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
// Template class for Melee based NPC
// Written by Imperial for Dinos in Jurassic Life

//=============================================================================//
#include "cbase.h"
#include "ai_default.h"
#include "ai_task.h"
#include "ai_schedule.h"
#include "ai_hull.h"
#include "soundent.h"
#include "game.h"
#include "npcevent.h"
#include "entitylist.h"
#include "activitylist.h"
#include "ai_basenpc.h"
#include "engine/IEngineSound.h"

#include "npc_playercompanion.h"
#include "ai_blended_movement.h"
#include "ai_behavior_follow.h"
#include "ai_behavior_assault.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"



#define	UNIT_MODEL	"models/jl/dinosaurs/velociraptor.mdl"


//=========================================================
//=========================================================

typedef CAI_BlendingHost< CAI_BehaviorHost<CAI_BlendedNPC> > CAI_Base_Melee_NPC;


// class CMelee_NPC : public CNPC_PlayerCompanion
class CMelee_NPC : public CAI_Base_Melee_NPC
// class CMelee_NPC : public CAI_BaseNPC // Simpler Variant
{
	// DECLARE_CLASS(CMelee_NPC, CNPC_PlayerCompanion);
	DECLARE_CLASS(CMelee_NPC, CAI_Base_Melee_NPC);
	// DECLARE_CLASS(CMelee_NPC, CAI_BaseNPC); // Simpler Variant


private:

	DEFINE_CUSTOM_AI;
	//=========================================================
	// Private Activities
	//=========================================================
	int	ACT_CUSTOMACTIVITY = -1;

	//=========================================================
	// Custom Schedules
	//=========================================================
	enum
	{
		SCHED_MYCUSTOMSCHEDULE = LAST_SHARED_SCHEDULE,
		SCHED_JUMP,
	};

	//=========================================================
	// Custom Tasks
	//=========================================================
	enum
	{
		TASK_MYCUSTOMTASK = LAST_SHARED_TASK,
		TASK_JUMP,
	};


	//=========================================================
	// Custom Conditions
	//=========================================================
	enum
	{
		COND_MYCUSTOMCONDITION = LAST_SHARED_CONDITION,
		CONDITION_JUMP,
	};
	//=========================================================


public:
	void	Precache(void);
	void	Spawn(void);
	Class_T Classify(void);
	void Start_Task(const Task_t *pTask);

	DECLARE_DATADESC();

	// This is a dummy field. In order to provide save/restore
	// code in this file, we must have at least one field
	// for the code to operate on. Delete this field when
	// you are ready to do your own save/restore for this
	// character.
	int		m_iDeleteThisField;

};

LINK_ENTITY_TO_CLASS(npc_melee, CMelee_NPC);
IMPLEMENT_CUSTOM_AI(npc_melee_ai, CMelee_NPC);


//---------------------------------------------------------
// Save/Restore
//---------------------------------------------------------
//AI_BEGIN_CUSTOM_NPC(npc_melee, CMelee_NPC)
//	
//	DECLARE_CONDITION(CONDITION_JUMP)
//	DECLARE_TASK(TASK_JUMP)
//
//	//==================================================
//	// Jump
//	//==================================================
//
//	DEFINE_SCHEDULE
//	(
//		SCHED_JUMP,
//
//		"	Tasks"
//		/*"		TASK_STOP_MOVING				0"
//		"		TASK_RAPTOR_FACE_JUMP			0"*/
//		"		TASK_PLAY_SEQUENCE				ACTIVITY:ACT_RAPTOR_JUMP_START"
//		"		TASK_JUMP				0"
//		/*""
//		"	Interrupts"
//		"		COND_TASK_FAILED"*/
//	)
//
//
//AI_END_CUSTOM_NPC()

//---------------------------------------------------------
// Save/Restore
//---------------------------------------------------------
BEGIN_DATADESC(CMelee_NPC)

DEFINE_FIELD(m_iDeleteThisField, FIELD_INTEGER),

END_DATADESC()

//-----------------------------------------------------------------------------
// Purpose: Initialize the custom schedules
//-----------------------------------------------------------------------------
void CMelee_NPC::InitCustomSchedules(void)
{
	INIT_CUSTOM_AI(CMelee_NPC);

	// Not necessary since I alredy declared them in the Melee Class iself
	// ADD_CUSTOM_TASK(CMelee_NPC, TASK_MYCUSTOMTASK);
	// ADD_CUSTOM_SCHEDULE(CMelee_NPC, SCHED_MYCUSTOMSCHEDULE);
	// ADD_CUSTOM_ACTIVITY(CMelee_NPC, ACT_CUSTOMACTIVITY);
	// ADD_CUSTOM_CONDITION(CMelee_NPC, COND_MYCUSTOMCONDITION);
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CMelee_NPC::Precache(void)
{
	PrecacheModel(UNIT_MODEL);

	BaseClass::Precache();
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CMelee_NPC::Spawn(void)
{
	Precache();

	SetModel(UNIT_MODEL);
	SetHullType(HULL_HUMAN);
	SetHullSizeNormal();

	SetSolid(SOLID_BBOX);
	AddSolidFlags(FSOLID_NOT_STANDABLE);
	SetMoveType(MOVETYPE_STEP);
	SetBloodColor(BLOOD_COLOR_RED);
	m_iHealth = 20;
	m_flFieldOfView = 0.5;
	m_NPCState = NPC_STATE_NONE;

	CapabilitiesClear();
	//CapabilitiesAdd( bits_CAP_NONE );

	NPCInit();
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
Class_T	CMelee_NPC::Classify(void)
{
	return	CLASS_NONE;
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CMelee_NPC::Start_Task(const Task_t *pTask)
{
	switch (pTask->iTask)
	{
	case TASK_JUMP:
		if (FindGestureLayer(ACT_MP_JUMP) == -1)
			AddGesture(ACT_MP_JUMP);
		TaskComplete();

		//if (CheckLanding())
		//{
		//	TaskComplete();
		//}

		break;

	default:
		BaseClass::StartTask(pTask);
	}
}