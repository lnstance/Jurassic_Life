#ifndef CUSTOM_HULL
#define CUSTOM_HULL

#ifdef _WIN32
#pragma once
#endif


#include <math.h>
#include <float.h>
#include "mathlib/vector.h"
#include <string>

using namespace std;



class CustomHull
{
public :
	string hullDataName = "hullDataName";
	string hullDataDesc = "hullDataDesc";
	bool IsValidHull = false;

	typedef struct singleHullSize_s
	{
		Vector scale, min, max;

		singleHullSize_s()
		{
			scale = Vector(1, 1, 1);
			min = Vector(0, 0, 0);
			max = Vector(1, 1, 1);
		}

		singleHullSize_s(Vector InScale, Vector InMin, Vector InMax)
		{
			scale = InScale;
			min = InMin;
			max = InMax;
		}

	} singleHullSize_t;

	singleHullSize_s smallSize;
	singleHullSize_s normalSize;
	

	

	CustomHull(void);
	CustomHull(singleHullSize_s InSmallSize, singleHullSize_s InNormalSize);
	singleHullSize_s getHullSizeData(bool isSmallHull) { return isSmallHull ? smallSize : normalSize; };
};

inline CustomHull::CustomHull(void)
{
	smallSize = singleHullSize_s();
	normalSize = singleHullSize_s();
}

inline CustomHull::CustomHull(singleHullSize_s InSmallSize, singleHullSize_s InNormalSize)
{

	smallSize = InSmallSize;
	normalSize = InNormalSize;
}
#endif