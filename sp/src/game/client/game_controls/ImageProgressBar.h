#ifndef IMAGEPROGRESSBAR_H
#define IMAGEPROGRESSBAR_H

#ifdef _WIN32
#pragma once
#endif

#include <vgui/VGUI.h>
#include <vgui_controls/ProgressBar.h>

using namespace vgui;

class ImageProgressBar : public ContinuousProgressBar
{
	DECLARE_CLASS_SIMPLE(ImageProgressBar, ContinuousProgressBar);

public:
	ImageProgressBar(Panel *parent, const char *panelName);
	ImageProgressBar(Panel *parent, const char *panelName, const char *topTexturename, const char *bottomTextureName);

	virtual void Paint(void);

	// ============================================================
	// Added by Imperial in order to enable Transparent Bars
	virtual void PaintBackground(void); 

	// Allowing Armor Bars to be drawn in custom RGBA Colors specified by Set_RGBA() in child classes
	int R = 255;
	int G = 255;
	int B = 255;
	int A = 255;

	int Get_R() { return R; };
	int Get_G() { return G; };
	int Get_B() { return B; };
	int Get_A() { return A; };

	void Set_RGBA(int R0, int G0, int B0, int A0)
	{	R = R0;
		G = G0;
		B = B0;
		A = A0;
	}; 
	// ============================================================


	virtual void ApplySettings(KeyValues *inResourceData);
	virtual void GetSettings(KeyValues *outResourceData);

	void	SetTopTexture(const char *topTextureName);
	void	SetBottomTexture(const char *bottomTextureName);

private:
	int	m_iTopTextureId;
	int	m_iBottomTextureId;
	char	m_szTopTextureName[64];
	char	m_szBottomTextureName[64];
};

#endif