//#include "hud.h"
#include "cbase.h"
#include "c_imperialware.h"

using namespace vgui;
#include "vgui/ILocalize.h"








//-----------------------------------------------------------------------------
// Purpose: Count Characters (testing whether char arrays are empty)
//-----------------------------------------------------------------------------
// ((i >= 'a' &&  i <= 'z') || (i >= 'A' && i <= 'Z'))
int CImperial::Character_Count(char* value_1)
{   // Looping through all Chars in the value_1 array
	int Counter = 0;

	for (int i = 0; i < sizeof(value_1); i++)
	{
		if (value_1[i] == '\0') { return Counter; }
		Counter++;
	}
	return Counter;
}

//-----------------------------------------------------------------------------
// Purpose: String Comparsion, Result is passed into the Buffer
//-----------------------------------------------------------------------------
void  CImperial::Append(char* Value, char* Suffix, char* Buffer)
{
	// char s_Texture_Name[80]; s_Texture_Name[0] = 0;
	sprintf(Buffer, "%s%s", Value, Suffix);
	// Value = Buffer;
}


//-----------------------------------------------------------------------------
// Loop until the delimiter char sign is found ',' then return the first value in value_2
//-----------------------------------------------------------------------------
char* CImperial::Split_Array(char* value_1, char* value_2, char delimiter, int word_number)
{
	int Word_Count = 0;
	int Charater_Slot = 0;

	for (int i = 0; i < 80; i++) // 80 is used in most cases
	{
		if (value_1[i] == delimiter)
		{
			Word_Count++; // We raise wordcount after each delimiter, if count matches desired word we return it
			if (Word_Count == word_number) { value_2[Charater_Slot] = '\0'; return value_2; }
			else { Charater_Slot = 0; } // Otherwise we start overwriting by the next word/value
		}
		else if (value_1[i] != ' ') // Ignoring emptyspaces
		{   // We transfere values over to the new char array
			value_2[Charater_Slot] = value_1[i];
			Charater_Slot++;
		}
	}
	return null;
}


//-----------------------------------------------------------------------------
// Purpose: String Comparsion
//-----------------------------------------------------------------------------
// Note; We only compare until the loop passed the Size of value_2.
// prefix to trim, source array
void CImperial::Trimm_Prefix(char* value_1, char* value_2)
{
	int Filled_Slots = 0;

	for (int i = 0; i < sizeof(value_2); i++)
	{
		if (i > sizeof(value_1))
		{
			value_2[Filled_Slots] = value_2[i]; // Shifting ahead
			Filled_Slots++;
		}
		// else { value_2[i] = 0; }
	}
}


//-----------------------------------------------------------------------------
// Purpose: String Comparsion
//-----------------------------------------------------------------------------
// source array, suffix to trim
void CImperial::Trimm_Suffix(char* value_1, char* value_2)
{
	int Empty_Slots = sizeof(value_1);

	for (int i = sizeof(value_1) + sizeof(value_2); i == 0; i--)
	{
		if (i = sizeof(value_1) - sizeof(value_2) && Empty_Slots != 0)
		{
			value_1[Empty_Slots] = value_1[i]; // Shifting back
			Empty_Slots--;
		}
	}
}


//-----------------------------------------------------------------------------
// Purpose: Grab a string from the resource/Modname_Language.txt
//-----------------------------------------------------------------------------
char* CImperial::Localize_Text(char* Entry, int Buffer_Size) // Caution! Raise value if you need more then 1024 signs
{
	if (Buffer_Size >= 4096)
	{
		char Buffer[4096];
		Buffer[0] = 0;
		g_pVGuiLocalize->ConvertUnicodeToANSI(g_pVGuiLocalize->Find(Entry), Buffer, 4096);
		return (char*)Buffer;
	}
	else if (Buffer_Size >= 2048)
	{
		char Buffer[2048];
		Buffer[0] = 0;
		g_pVGuiLocalize->ConvertUnicodeToANSI(g_pVGuiLocalize->Find(Entry), Buffer, 2048);
		return (char*)Buffer;
	}
	else
	{
		char Buffer[1024];
		Buffer[0] = 0;
		g_pVGuiLocalize->ConvertUnicodeToANSI(g_pVGuiLocalize->Find(Entry), Buffer, 1024);
		return (char*)Buffer;

	}
}


//-----------------------------------------------------------------------------
// Purpose: Draw_Text
//-----------------------------------------------------------------------------
void CImperial::Draw_Text(int x, int y, char* text, int R, int G, int B, int Alpha)
{
	surface()->DrawSetTextFont(hFont2);
	surface()->DrawSetTextColor(R, G, B, Alpha);
	// surface()->DrawSetTextPos(xpos + sizeX / 2, ypos + sizeY / 4);
	surface()->DrawSetTextPos(x, y);


	// surface()->DrawSetTextScale(2.0, 2.0);
	mbstowcs(&unicode[0], text, 80); // Max 80 characters
	surface()->DrawPrintText(unicode, wcslen(unicode));
}

void CImperial::Draw_Text(int x, int y, char* text, Color Color_Value)
{   surface()->DrawSetTextFont(hFont2);
	surface()->DrawSetTextColor(Color_Value);
	surface()->DrawSetTextPos(x, y);

	mbstowcs(&unicode[0], text, 80); 
	surface()->DrawPrintText(unicode, wcslen(unicode));
}

//-----------------------------------------------------------------------------
// Purpose: Draw Number
//-----------------------------------------------------------------------------
void CImperial::Draw_Number(int x, int y, int value, int R, int G, int B, int Alpha)
{
	surface()->DrawSetTextFont(hFont2);
	surface()->DrawSetTextColor(R, G, B, Alpha);
	surface()->DrawSetTextPos(x, y);

	swprintf(unicode, L"%d", value);
	surface()->DrawPrintText(unicode, wcslen(unicode));
}


void CImperial::Draw_Number(int x, int y, int value, Color Color_Value)
{
	surface()->DrawSetTextFont(hFont2);
	surface()->DrawSetTextColor(Color_Value);
	surface()->DrawSetTextPos(x, y);

	swprintf(unicode, L"%d", value);
	surface()->DrawPrintText(unicode, wcslen(unicode));
}

//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
int CImperial::Get_Texture_ID(int i)
{
	switch (i)
	{
	case 1:
	case 17:
	case 33:
		return i_Item_Icon_1;
		break;
	case 2:
	case 18:
	case 34:
		return i_Item_Icon_2;
		break;
	case 3:
	case 19:
	case 35:
		return i_Item_Icon_3;
		break;
	case 4:
	case 20:
	case 36:
		return i_Item_Icon_4;
		break;
	case 5:
	case 21:
	case 37:
		return i_Item_Icon_5;
		break;
	case 6:
	case 22:
	case 38:
		return i_Item_Icon_6;
		break;
	case 7:
	case 23:
	case 39:
		return i_Item_Icon_7;
		break;
	case 8:
	case 24:
	case 40:
		return i_Item_Icon_8;
		break;

	case 9:
	case 25:
	case 41:
		return i_Item_Icon_9;
		break;
	case 10:
	case 26:
	case 42:
		return i_Item_Icon_10;
		break;
	case 11:
	case 27:
	case 43:
		return i_Item_Icon_11;
		break;
	case 12:
	case 28:
	case 44:
		return i_Item_Icon_12;
		break;
	case 13:
	case 29:
	case 45:
		return i_Item_Icon_13;
		break;
	case 14:
	case 30:
	case 46:
		return i_Item_Icon_14;
		break;
	case 15:
	case 31:
	case 47:
		return i_Item_Icon_15;
		break;
	case 16:
	case 32:
	case 48:
		return i_Item_Icon_16;
		break;
	}

	return i_Item_Icon_16;

	// Distributing them eqaually to 48 Item slots, this prevents crashes due to over-refreshing the art entry
	// Otherwise it would also drain the Framerate a lot!

}


//-----------------------------------------------------------------------------
// 252, 25, 25, 255 // Red 
// 255, 60, 60, 255 Dock Red
// 150, 255, 50, 255 // Orange  252, 205, 125, 255 // Cooler Orange
// 50, 150, 230, 255 // Dark Blue
// 220, 220, 255, 255 // Bright Blue
// 50, 150, 230, 255 // Blue?
// 180, 100, 220, 255 // Purple
//-----------------------------------------------------------------------------
Color CImperial::Text_To_Color(char* Color_1_Txt)
{
	char Col_R[80];
	char Col_G[80];
	char Col_B[80];
	char Col_Alpha[80];

	// Extracting color values into a string array
	char delimiter = ','; // Both variants legit
	if (Match(Color_1_Txt, ".")) { delimiter = '.'; }

	Split_Array(Color_1_Txt, Col_R, delimiter, 1);
	Split_Array(Color_1_Txt, Col_G, delimiter, 2);
	Split_Array(Color_1_Txt, Col_B, delimiter, 3);
	Split_Array(Color_1_Txt, Col_Alpha, delimiter, 4);

	Color_1R = V_atof(Col_R);
	Color_1G = V_atof(Col_G);
	Color_1B = V_atof(Col_B);
	Color_1Alpha = V_atof(Col_Alpha);

	return Color(Color_1R, Color_1G, Color_1B, Color_1Alpha);
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------



