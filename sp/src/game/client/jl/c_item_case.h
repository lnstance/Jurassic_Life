//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//
#if !defined( C_ITEM_CASE )
#define C_ITEM_CASE
#ifdef _WIN32
#pragma once
#endif



#if defined( CLIENT_DLL )
#define CJL_Item_Case C_Item_Case
#endif


#include "cbase.h"
#include "c_baseplayer.h"
#include "c_baseentity.h"

#include "c_basehlcombatweapon.h"
// #include "c_weapon__stubs.h"
#include "materialsystem/IMaterial.h"

#include "IGameUIFuncs.h" // for key bindings 

#include "basecombatweapon_shared.h"
#include <vgui_controls/Scrollbar.h>


// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"



//-----------------------------------------------------------------------------
// Header:
//-----------------------------------------------------------------------------

class C_Item_Case : public C_BaseAnimating
{
public:
	DECLARE_CLASS(C_Item_Case, C_BaseAnimating); 
	DECLARE_CLIENTCLASS(); 

	// networked variables as defined in server class
	/*int	m_nMyInteger;
	float	m_fMyFloat;*/

	int Scrollbar_Size;
	CHandle<C_BaseCombatWeapon> m_nCaseItems[MAX_JL_ITEMS];


	int Get_Scrollbar_Size() { return Scrollbar_Size; };

	/*
	void ClientThink(void)
	{	C_BasePlayer *player = C_BasePlayer::GetLocalPlayer();

		// if (player->Get_Active_Case()) Msg("Name is %s\n", player->Get_Active_Case()->GetClassname());

		// if (player->GetUseEntity player->FindUseEntity() == this)

		// if (player->m_Active_Case->m_nCaseItems[0]) Msg("Found\n");

		// DevMsg("This Scrollbar Value is %d\n", Scrollbar_Size);

		
		if (m_nCaseItems[0])
		{
			if (m_nCaseItems[0]->GetWpnData().iconInactive) // && IN_USE)
			{
				Msg("Weapon Name is %s\n", m_nCaseItems[0]->GetWpnData().iconInactive->szTextureFile);
			}
		}	
		
	}

	void OnDataChanged(DataUpdateType_t updateType)
	{
		BaseClass::OnDataChanged(updateType);
		if (updateType == DATA_UPDATE_CREATED)
		{
			SetNextClientThink(CLIENT_THINK_ALWAYS);
		}
	}
	*/
		
};


#endif // C_ITEM_CASE