//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#include "cbase.h"
#include "hud.h"
#include "hud_macros.h"
#include "iclientmode.h"
// #include "clienteffectprecachesystem.h"
#include "c_te_effect_dispatch.h"


#include "hud_weapon_bars.h"
#include "util_shared.h"  // Random generators
#include "fmtstr.h"

using namespace vgui;

#include "hudelement.h"
#include "hud_numericdisplay.h"
#include "vgui/ISurface.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"


// This function seems only to specify from which height blood shall splat when player is Hit
// The actual Blood splat effect is done in client/fx_blood.cpp
void BloodSplatCallback( const CEffectData & data )
{
/*[JL re-enable the effect
	Msg("SPLAT!\n");

	int		x,y;

	// Find our screen position to start from
	x = XRES(320);
	y = YRES(240);

	// Draw the ammo label
	CHudTexture	*pSplat = gHUD.GetIcon( "hud_blood1" );
	
  // FIXME:  This can only occur during vgui::Paint() stuff
	pSplat->DrawSelf( x, y, gHUD.m_clrNormal);
//JL] */
}

DECLARE_CLIENT_EFFECT( "HudBloodSplat", BloodSplatCallback );





//-----------------------------------------------------------------------------
// Blood Drop(s)
//-----------------------------------------------------------------------------

class C_Hud_Blood_Drop : public Panel
{
	DECLARE_CLASS_SIMPLE(C_Hud_Blood_Drop, Panel);

public:
	C_Hud_Blood_Drop(Panel *parent, const char *panelName);
	C_Hud_Blood_Drop(Panel *parent, const char *panelName, int posx, int posy, int size);
	~C_Hud_Blood_Drop();
	void OnDeletePanel();
	void DeletePanel(); //VPANEL vguiPanel);
	virtual void Paint();
	virtual void OnThink();

private:
	C_Hud_Blood_Drop * This_Drop;
	int xpos = 0;
	int ypos = 0;

	int Drop_Size = 20;
	int Drop_Texture = 0;

	int Tick = -1;
	int Kamikaze_Time = -1;
	int Red_Value = -1;
	int Color_Values = -1;
	int Alpha = -1;
};

DECLARE_BUILD_FACTORY(C_Hud_Blood_Drop);

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
C_Hud_Blood_Drop::C_Hud_Blood_Drop(Panel *parent, const char *panelName) : Panel(parent, panelName)
{	
	SetParent(parent);
	SetPos(xpos, ypos);
	SetSize(Drop_Size, Drop_Size);
	C_Hud_Blood_Drop::SetPaintBackgroundEnabled(false);

	Red_Value = SharedRandomInt((char *)"", 120, 255, (int)panelName);
	Color_Values = SharedRandomInt((char *)"", 40, 120, (int)panelName);
	Alpha = SharedRandomInt((char *)"", 180, 255, (int)panelName);

	Drop_Texture = vgui::surface()->CreateNewTextureID();
	vgui::surface()->DrawSetTextureFile(Drop_Texture, panelName, true, false);
}


C_Hud_Blood_Drop::C_Hud_Blood_Drop(Panel *parent, const char *panelName, int posx, int posy, int size) : Panel(parent, panelName)
{
	// SetPaintBackgroundEnabled(false);
	Drop_Size = size;

	SetParent(parent);
	SetPos(posx, posy);
	SetSize(Drop_Size, Drop_Size);
	C_Hud_Blood_Drop::SetPaintBackgroundEnabled(false);

	Kamikaze_Time = gpGlobals->curtime + 4.0f; // Make Harakiri in this many seconds

	// We seed panelName into a random value
	Red_Value = SharedRandomInt((char *)"", 120, 255, (int)panelName);
	Color_Values = SharedRandomInt((char *)"", 40, 120, (int)panelName);


	Alpha = 255; // Some of them will NOT be translucent
	if (SharedRandomInt((char *)"", 0, 1, (int)panelName))
	{ Alpha = SharedRandomInt((char *)"", 180, 255, (int)panelName); }


	Drop_Texture = vgui::surface()->CreateNewTextureID();
	// When this HUD is Created it is named to something like "HUD/Blood/Drop_02", the number changes randomly; thus we use the panel name as Texture!
	vgui::surface()->DrawSetTextureFile(Drop_Texture, panelName, true, false);
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------

C_Hud_Blood_Drop::~C_Hud_Blood_Drop()
{

}


void C_Hud_Blood_Drop::DeletePanel() 
{
	if (This_Drop)
	{
		This_Drop->SetParent((vgui::Panel *)NULL);
		delete This_Drop;
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void C_Hud_Blood_Drop::OnThink()
{
	if (gpGlobals->curtime > Kamikaze_Time + 10) // Then it's finally time for Harakikri XD
	{
		DeletePanel();
		// BaseClass::DeletePanel(Panel_Name);
		// C_Hud_Blood_Drop::~C_Hud_Blood_Drop();
		// this->DeletePanel();
		// OnDeletePanel();

		// SetAlpha(255 - 255 * (gpGlobals->curtime - Tick));

		// if (!SharedRandomInt((char *)"", 0, 3, gpGlobals->curtime)) { C_Hud_Blood_Drop::~C_Hud_Blood_Drop(); }
		
	}

	else if (gpGlobals->curtime > Tick) // Setting next Tick
	{
		Tick = gpGlobals->curtime + 8.0f; // 0.15f;
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void C_Hud_Blood_Drop::Paint()
{   // if (!Should_Draw) return;	

	surface()->DrawSetColor(Red_Value, Color_Values, Color_Values, Alpha);
	surface()->DrawSetTexture(Drop_Texture);
	surface()->DrawTexturedRect(xpos, ypos, xpos + Drop_Size, ypos + Drop_Size);
}







//-----------------------------------------------------------------------------
// Purpose: Container for Blood Drops
//-----------------------------------------------------------------------------

class C_Hud_Blood : public CHudElement, public Panel
{
	DECLARE_CLASS_SIMPLE(C_Hud_Blood, vgui::Panel);

public:
	C_Hud_Blood(const char *pElementName);
	// C_Hud_Blood(Panel *parent, const char *pElementName);
	virtual void OnThink();
	void MsgFunc_Damage(bf_read &msg);
	void ApplySchemeSettings(vgui::IScheme *pScheme);	

private:
	virtual void Paint();
	bool Should_Draw = false;

	int xpos = 0;
	int ypos = 0;
	int width = 1200;
	int height = 800;
	int Drop_Size = 10;

	int Patch = 0;
	int Drop_01 = 0;
	int Drop_02 = 0;

	int	i_Health = 0;
	int	i_Damage = 0;
};

DECLARE_HUDELEMENT(C_Hud_Blood);
DECLARE_HUD_MESSAGE(C_Hud_Blood, Damage);

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------

// C_Hud_Blood::C_Hud_Blood(Panel *parent, const char *pElementName) : CHudElement(pElementName), BaseClass(NULL, "Hud_Blood")
C_Hud_Blood::C_Hud_Blood(const char *pElementName) : CHudElement(pElementName), BaseClass(NULL, "Hud_Blood")
{   
	C_Hud_Blood::SetPaintBackgroundEnabled(false);
	// SetZPos(3); // Specified in HudLayout.res

	Panel *pParent = g_pClientMode->GetViewport();
	SetParent(pParent);

	Patch = vgui::surface()->CreateNewTextureID();
	vgui::surface()->DrawSetTextureFile(Patch, "HUD/Blood/Patch", true, false);


    SetHiddenBits(HIDEHUD_HEALTH | HIDEHUD_PLAYERDEAD); // | HIDEHUD_NEEDSUIT );
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void C_Hud_Blood::OnThink()
{
	C_BasePlayer *Player = C_BasePlayer::GetLocalPlayer();
	
	if (Player)
	{	if (MAX(Player->GetHealth(), 0) < 34) { Should_Draw = true; }
		else { Should_Draw = false; }
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void C_Hud_Blood::Paint()
{
}

//-----------------------------------------------------------------------------
// Purpose: hud scheme settings
//-----------------------------------------------------------------------------
void C_Hud_Blood::ApplySchemeSettings(vgui::IScheme *pScheme)
{
	// if (!Should_Draw) return;


	C_BasePlayer *Player = C_BasePlayer::GetLocalPlayer();

	xpos = SharedRandomInt((char *)"", 0, ScreenWidth(), gpGlobals->curtime);
	ypos = SharedRandomInt((char *)"", 0, ScreenHeight(), gpGlobals->curtime);


	int Apply = 0;
	float posx = 0;
	float posy = 0;


	for (int i = 0; i < SharedRandomInt((char *)"", 0, 14, gpGlobals->curtime); i++)
	// for (int i = 0; i < i_Damage * 10; i++)
	{		
		//if (!SharedRandomInt((char *)"", 0, 3, gpGlobals->curtime)) continue;

		Drop_Size = SharedRandomInt((char *)"", 0, 50, i);

	
		// Only perhaps being applied, that prevents the value from a scaling pattern
	    Apply = SharedRandomInt((char *)"", 0, 1, i * 3);
		if (Apply != 0) { posx = SharedRandomFloat((char *)"", 0, width, gpGlobals->curtime + i); }

		Apply = SharedRandomInt((char *)"", 0, 1, i /2);
		if (Apply != 0) { posy = SharedRandomFloat((char *)"", 0, height, gpGlobals->curtime - i); }

		
		// Format to append a random Int between parameter 3 and 4
		new C_Hud_Blood_Drop(this, CFmtStr("HUD/Blood/Drop_0%d", SharedRandomInt((char *)"", 2, 6, gpGlobals->curtime - i))
			, posx, posy, Drop_Size); // parent, name, xpos, ypos, size

		/*int number = 3;
		char numberstring[(((sizeof number) * CHAR_BIT) + 2) / 3 + 2];
		sprintf(numberstring, "%d", number);


		/*if (Apply != 0) { Dro->~C_Hud_Blood_Drop(); }
		else return;*/
	}


	C_Hud_Blood::SetPaintBackgroundEnabled(false);
	BaseClass::ApplySchemeSettings(pScheme);	
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void C_Hud_Blood::MsgFunc_Damage(bf_read &msg)
{ i_Damage = msg.ReadByte(); }





