//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#include "cbase.h"
#include "hud_status_bars.h"
#include "iclientmode.h"
#include "hud_macros.h"

// #include <vgui_controls/Panel.h>
// #include "hud.h"
#include "hud_weapon_bars.h"

#include <vgui/ISurface.h>
#include "vgui_controls/AnimationController.h"
#include "vgui/ILocalize.h"

#include "hud_numericdisplay.h"
#include "c_basehlplayer.h" // Contains info for Stamina


// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"



using namespace vgui;

#ifdef HL2_EPISODIC
DECLARE_HUDELEMENT(CHealth_Bar);
DECLARE_HUD_MESSAGE(CHealth_Bar, Battery);
#endif // HL2_EPISODIC


static ConVar iHud_Show_Image_Behind("iHud_Show_Image_Behind", "1", FCVAR_ARCHIVE, "Shows Image behind the Hud.");

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CHealth_Bar::CHealth_Bar( const char *pElementName ) : CHudElement( pElementName ), BaseClass( NULL, "Hud_Status_Bars" )
{
	vgui::Panel *pParent = g_pClientMode->GetViewport();
	SetParent( pParent );
	
	CHealth_Bar::SetPaintBackgroundEnabled(false);

	i_Texture_Slot = vgui::surface()->CreateNewTextureID();
	vgui::surface()->DrawSetTextureFile(i_Texture_Slot, "HUD/Weapons/Weapon_Bar_Background", true, false);


	// Stamina bar could also be "Bar_Green_Slim"
	Stamina_Bar = Create_Status_Bar("Hud/Horizontal/Bar_Gray_Slim", i_Bar_X_Offset, Bar_Position_1, false, i_Bar_Length, 14, 'E');
	Armor_Bar = Create_Status_Bar("Hud/Horizontal/Bar_Blue_Slim", i_Bar_X_Offset, Bar_Position_2, false, i_Bar_Length, 14, 'E');
	Health_Bar = Create_Status_Bar("Hud/Horizontal/Bar_Red_Slim", i_Bar_X_Offset, Bar_Position_3, true, i_Bar_Length, 14, 'E');


	SetHiddenBits(HIDEHUD_HEALTH | HIDEHUD_PLAYERDEAD); // | HIDEHUD_NEEDSUIT );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHealth_Bar::Init(void)
{
	HOOK_HUD_MESSAGE(CHealth_Bar, Battery);
	Reset_Settings = true;
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : *pScheme - 
//-----------------------------------------------------------------------------

void CHealth_Bar::ApplySchemeSettings( vgui::IScheme *pScheme )
{
	 BaseClass::ApplySchemeSettings(pScheme);
	 Reset_Settings = true;
}


int CHealth_Bar::Get_Armor() { return New_Armor; }

//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
void CHealth_Bar::Paint()
{
    CHud_Weapon_Bar *Weapon_Bar;

	if (Reset_Settings || Weapon_Bar->Get_Changes())
	{
		int set_x = 0;
		int set_y = 0; // Set directly from this c++ file	

		if (Weapon_Bar->Get_Mini_Mode())
		{
			if (Weapon_Bar->Get_Right_Sided()) { set_x = ScreenWidth() - 144; } // -144 or -148
			if (!Weapon_Bar->Get_Top_Sided()) { set_y = ScreenHeight() - 113; } // Otherwise stays 0
			SetPos(set_x, set_y);

			return;
		}
		else
		{
			if (Weapon_Bar->Get_Right_Sided()) { set_x = ScreenWidth() - 144; } // -144 or -148
			if (!Weapon_Bar->Get_Top_Sided()) { set_y = ScreenHeight() - 176; } // Otherwise stays 0
			SetPos(set_x, set_y);
		}

		Reset_Settings = false;
	}
	

	//int set_x = 0;
	//int set_y = 0; // Set directly from this c++ file	 	
	//if (Weapon_Bar->Get_Right_Sided()) { set_x = ScreenWidth() - 144; } // -144 or -148
	//if (!Weapon_Bar->Get_Top_Sided()) { set_y = ScreenHeight() - 160; } // Otherwise stays 0
	//SetPos(set_x, set_y);



	// When one starts a new HUD item, he chooses right at the beginning to USE or NOT use hudanimations.txt at all (for all graphical panels).
	// Because merging values specified in this file with hudanimations results in a mess!
	// if (Weapon_Bar->Get_Right_Sided()) { g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("Status_Bars_Top_Right"); }
	// else { g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("Status_Bars_Top_Left"); }


	if (!Has_Weapon || !iHud_Show_Image_Behind.GetBool()) return;
	

	// Mini Mode isn't supposed to draw any background tile in size of the Panel.
	surface()->DrawSetColor(255, 255, 255, 255);
	surface()->DrawSetTexture(i_Texture_Slot);

	// Skipping the first 18 to not draw under the 1st bar slot, because otherwise the UI would be too clunky
	
	surface()->DrawTexturedRect(0, 18, 144, 141);
	// surface()->DrawTexturedRect(0, 18, this->GetWide(), this->GetTall());

	// BaseClass::Paint();
}

/*
//-----------------------------------------------------------------------------
void CHealth_Bar::OnTick()
{	
} */


void CHealth_Bar::Think()
{	
	C_BasePlayer *Player = C_BasePlayer::GetLocalPlayer();
	if (Player)
	{   
		if (Player->GetWeapon(0)) { Has_Weapon = true; }
		else  { Has_Weapon = false; }
		New_Health = MAX(Player->GetHealth(), 0); // Never below zero		
	}


	if (New_Armor > 0)
	{   // Armor is visible, so we keep Stamina over Armor and Health bars.
		Stamina_Bar->SetPos(i_Bar_X_Offset, Bar_Position_1); // higher slot
		Armor_Bar->SetPos(i_Bar_X_Offset, Bar_Position_2); // lower slot
	}
	else // Effectively using the available space.
	{   // Inverting Slot so the invisible Armor bar is not between Stamina Bar and Health Bar under it
		Armor_Bar->SetPos(i_Bar_X_Offset, Bar_Position_1); // higher slot
		Stamina_Bar->SetPos(i_Bar_X_Offset, Bar_Position_2); // lower slot 
	}


	
	C_BaseHLPlayer *pPlayer = (C_BaseHLPlayer *)C_BasePlayer::GetLocalPlayer();
	New_Stamina = pPlayer->m_HL2Local.m_flSuitPower; // Stamina

	// Because it isn't supposed to show up with 100%
	if (New_Stamina > 99.0f) { Stamina_Bar->SetVisible(false); }	
	else if (Stamina != New_Stamina)
	{   Stamina = New_Stamina;
	    Stamina_Bar->SetVisible(true);
		Stamina_Bar->SetProgress((float)Stamina / 100);						
	}


	if (New_Armor == 0) { Armor_Bar->SetVisible(false); }
	else if (Armor != New_Armor) // If different from last cycle
	{   Armor = New_Armor;
	    Armor_Bar->SetVisible(true);
	    Armor_Bar->SetProgress((float)Armor / 100); 				
	}

	
	// Only update the fade if we've changed health
	if (New_Health == Health) { return; }
	Health = New_Health;

	// Colorfull Power Bar
	if (Health < 34) { Health_Bar->SetTopTexture("Hud/Horizontal/Bar_Red_Slim"); }
	else if (Health < 67) { Health_Bar->SetTopTexture("Hud/Horizontal/Bar_Orange_Slim"); }
	else { Health_Bar->SetTopTexture("Hud/Horizontal/Bar_Green_Slim"); } // Bar_Dark_Green_Slim


	Health_Bar->SetProgress((float)Health / 100);
	// SetNextThink(gpGlobals->curtime + 0.2f);	
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------

ImageProgressBar* CHealth_Bar::Create_Status_Bar(const char* Bar_Texture, int x = 0, int y = 0, bool Visible = true, int width = 140, int height = 14, char direction = 'E')
{
	Bar = new ImageProgressBar(this, "Health_Bar");
	Bar->SetPos(x, y);
	Bar->SetSize(width, height);
	Bar->SetTopTexture(Bar_Texture);
	
	Bar->SetProgress(10.0f); // Innitial Setup
	Bar->SetVisible(Visible);
	if (direction == 'E' || direction == 'W') // its Horizontal
	{   Bar->SetProgressDirection(ProgressBar::PROGRESS_EAST);
		Bar->SetBottomTexture("Hud/Horizontal/Bar_Background_Slim");
	}
	else if (direction == 'N' || direction == 'S') // its Vertical
	{   Bar->SetProgressDirection(ProgressBar::PROGRESS_NORTH);
		Bar->SetBottomTexture("Hud/Vertical/Bar_Background_Slim");
	}

	return Bar;
}

//-----------------------------------------------------------------------------
// Purpose: (Imperial) This Message passes new value of the Armor /Suit Battery
//-----------------------------------------------------------------------------
void CHealth_Bar::MsgFunc_Battery(bf_read &msg)
{
	New_Armor = msg.ReadShort();
}

//-----------------------------------------------------------------------------
// Purpose: Draw Tile Background
//-----------------------------------------------------------------------------
void CHealth_Bar::Draw_Icon(int texture)
{
	surface()->DrawSetTexture(texture);
	surface()->DrawTexturedRect(0, 0, this->GetTall(), this->GetWide());	
}

