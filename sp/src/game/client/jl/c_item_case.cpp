//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//
#include "cbase.h"
#include "c_baseplayer.h"

#include "c_basehlcombatweapon.h"
// #include "c_weapon__stubs.h"
#include "materialsystem/IMaterial.h"

#include "c_item_case.h"


// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"


#ifdef CJL_Item_Case
#undef CJL_Item_Case
#endif

//-----------------------------------------------------------------------------
// Class Linkings: 
//-----------------------------------------------------------------------------
LINK_ENTITY_TO_CLASS(jl_item_case, C_Item_Case);


// Link data table DT_Item_Case to client class and map variables (RecvProps)
// DO NOT create this in the header! Put it in the main CPP file.

IMPLEMENT_CLIENTCLASS_DT(C_Item_Case, DT_Item_Case, CJL_Item_Case)	
	//RecvPropFloat(RECVINFO(m_fMyFloat)),

	RecvPropInt(RECVINFO(Scrollbar_Size)),
	RecvPropArray3(RECVINFO_ARRAY(m_nCaseItems), RecvPropEHandle(RECVINFO(m_nCaseItems[0]))),	

END_RECV_TABLE()


//-----------------------------------------------------------------------------
// Functions: 
//-----------------------------------------------------------------------------
