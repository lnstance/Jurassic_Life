#include "hudelement.h"
#include <vgui_controls/Panel.h>

#include "convar.h"
#include "ImageProgressBar.h"
#include "vgui/ILocalize.h"


//#include "input.h"
//#include "weapon_selection.h"
// #include "hud_weaponselection.h"




// using namespace vgui;


// Switches, defined by the user settings
static ConVar iHud_Mini_Mode("iHud_Mini_Mode", "0", FCVAR_ARCHIVE, "Toggles HUD MiniMode.");
static ConVar iHud_Invert_Selection("iHud_Invert_Selection", "0", FCVAR_ARCHIVE, "Inverts HUD Selection Icon.");
static ConVar iHud_Invert_Scroll("iHud_Invert_Scroll", "0", FCVAR_ARCHIVE, "Inverts scroll direction of Mousewheel.");

static ConVar iHud_Top_Sided("iHud_Top_Sided", "0", FCVAR_ARCHIVE, "Draws Hud on either Top or Bottom of the screen.");
static ConVar iHud_Right_Sided("iHud_Right_Sided", "1", FCVAR_ARCHIVE, "Should the Hud render at right or left?");

static ConVar iHud_Right_Sided_Ammo("iHud_Right_Sided_Ammo", "0", FCVAR_ARCHIVE, "Should the Ammo Bar render from Right to Left? -1 turns it translucent.");
static ConVar iHud_Show_Changes("iHud_Show_Changes", "0", FCVAR_NONE, "Shows all changes, otherwise you need to restart level or type Hud_Reloadscheme.");

static ConVar iHud_Show_Background("iHud_Show_Background", "0", FCVAR_ARCHIVE, "Render Background Tile in Weapon Selection.");
static ConVar iHud_Show_Selector("iHud_Show_Selector", "0", FCVAR_ARCHIVE, "Render Foreground in Weapon Selection.");


static ConVar iHud_Color_1("iHud_Color_1", "220, 220, 255, 255", FCVAR_ARCHIVE, "HUD Color RGBA setting (4x 0-255).");
static ConVar iHud_Color_2("iHud_Color_2", "180, 100, 220, 255", FCVAR_ARCHIVE, "HUD Color RGBA setting (4x 0-255).");
static ConVar iHud_Color_Depleted("iHud_Color_Depleted", "240, 60, 60, 255", FCVAR_ARCHIVE, "HUD Color RGBA setting (4x 0-255).");
static ConVar iHud_Color_Texture_Filter("iHud_Color_Texture_Filter", "255, 255, 255, 255", FCVAR_ARCHIVE, "Draws Hud Weapon textures. Use 255, 255, 255, 255 to turn off.");


class CHud_Weapon_Bar : public CHudElement, public Panel
{
	DECLARE_CLASS_SIMPLE(CHud_Weapon_Bar, vgui::Panel);

	// Example, how to set Convars;
	// ConVar *b_Mini_Mode = cvar->FindVar("Hud_Mini_Mode");

public:
	CHud_Weapon_Bar(const char *pElementName);


	// Color Custom_Color = Color(60, 60, 100, 255);

	virtual void OnThink();
	virtual void   LevelInit();
	char* Append_To_Char(char* value_1, char* value_2);
	char* Split_Array(char* value_1, char* value_2, char delimiter, int word_number);
	void Grab_Color(static ConVar Entry, int result[]);


	void Draw_Icon(int texture);
	void Append(char* Value, char* Suffix, char* Buffer);
	void ApplySchemeSettings(vgui::IScheme *pScheme);
	
	const int StartY = 20; // From top
	const int EndY = 80; // From bottom
	int range = 0;

	int xpos = 0;
	int ypos = 0;
	int width = 256;
	int height = 128;
	int Tick = -1;
	float m_fLastChange;

	char* s_Img_Selection = null;
	char Ammo_Orientation = 'E'; // E-ast = From Left to Right

	// "Hud/Vertical/Bar_Background_Slim" for N and S but "Hud/Horizontal/Bar_Background_Slim" for W and E
	char* Background_Bar = "Hud/Horizontal/Bar_Background_Slim";

	//====== Text Labels ======
	vgui::HFont hFont;
	vgui::HFont hFont2;
	int			charWidth;
	int			charWidth2;
	wchar_t		unicode[80];

	void Draw_Text(int x, int y, char* text, Color Color_Value = Color(255, 255, 255, 255)); // Defaults as White
	void Draw_Text(int x, int y, char* text, int R, int G, int B, int Alpha);
	//=========================


	char* Localize_Text(char* Entry, int Buffer_Size = 1024) // Caution! Entry must not hove more then 1024 signs
	{
		if (Buffer_Size >= 4096)
		{   char Buffer[4096];
			Buffer[0] = 0;
			g_pVGuiLocalize->ConvertUnicodeToANSI(g_pVGuiLocalize->Find(Entry), Buffer, 4096);
			return (char*)Buffer;
		}
		else if (Buffer_Size >= 2048)
		{  char Buffer[2048];
			Buffer[0] = 0;
			g_pVGuiLocalize->ConvertUnicodeToANSI(g_pVGuiLocalize->Find(Entry), Buffer, 2048);
			return (char*)Buffer;
		}
		else
		{  char Buffer[1024];
			Buffer[0] = 0;
			g_pVGuiLocalize->ConvertUnicodeToANSI(g_pVGuiLocalize->Find(Entry), Buffer, 1024);
			return (char*)Buffer;

		}
	}



	// Switches, defined by the user settings
	bool   Mini_Mode = iHud_Mini_Mode.GetBool();
	bool   Invert_Selection = iHud_Invert_Selection.GetBool();

	bool   Right_Sided = iHud_Right_Sided.GetBool();
	bool   Top_Sided = iHud_Top_Sided.GetBool();

	int    Right_Sided_Ammo = iHud_Right_Sided_Ammo.GetInt();
	bool   Show_Changes = false;
	
	bool   Show_Background = iHud_Show_Background.GetBool();
	bool   Show_Selector = iHud_Show_Selector.GetBool();

	bool   Reset_Settings = false;
	void   Resetting_Settings();
	

	// Getters
	bool Get_Mini_Mode();
	bool Get_Invert_Selection();

	bool Get_Top_Sided();
	bool Get_Right_Sided();

	int  Get_Right_Sided_Ammo();
	bool Get_Changes();

	bool Get_Show_Background();
	bool Get_Show_Selector();

	

	Color Color_1;
	Color Color_2;
	Color Color_Depleted;
	Color Color_Filter;


	char* Get_Color_1_Value();
	char* Get_Color_2_Value();
	char* Get_Color_Depleted_Value();
	char* Get_Color_Filter_Value();

	char* Get_Name(C_BaseCombatWeapon * Weapon);


	bool   Compare(char* value_1, char* value_2);
	int Character_Count(char* value_1);

	bool b_Fuse_Ammo_Bars = false;
	int i_Ammo_Bar_Size = 128; // 100; // 126;
	int i_Clip_Size_1 = 0; 
	int i_Clip_Size_2 = 0;
	int i_Ammo_2_Salves = 0;
	int i_Ammo_1 = 0; 
	int i_Ammo_2 = 0;
	int i_Last_Ammo = -1;

	int Color_1R, Color_1G, Color_1B, Color_1A = 255;
	int Color_2R, Color_2G, Color_2B, Color_2A = 255;
	int Color_Texture_R, Color_Texture_G, Color_Texture_B, Color_Texture_A = 255;

	/* // They fail to pass the value
	int Get_Color_1R();
	int Get_Color_1G();
	int Get_Color_1B();
	int Get_Color_1A();

	int Get_Color_Texture_R();
	int Get_Color_Texture_G();
	int Get_Color_Texture_B();
	int Get_Color_Texture_A();
	*/

	
	ImageProgressBar *Bar; // Temporary bar used in Create_Status_Bar();
	ImageProgressBar *Primary_Ammo_Bar;
	ImageProgressBar *Primary_Ammo_Bar_Empty;

	ImageProgressBar *Secondary_Ammo_Bar;
	ImageProgressBar *Secondary_Ammo_Bar_Empty;
	
	ImageProgressBar* Create_Status_Bar(const char* Background_Texture, int x, int y, bool Visible, int width, int height, char direction, bool Colorize);

	
protected:
	virtual void Paint();
	int i_Texture_Selected;
	int i_Texture_Slot;
	int i_Weapon_Icon;
	int i_Revolver_Drumm;
	int i_Revolver_Icon;
	int i_Grenade_Icon;
	int i_Grenade_Icon_Inactive;

	
};