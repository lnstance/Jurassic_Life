//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#include "cbase.h"
#include "weapon_selection.h"
#include "iclientmode.h"
#include "history_resource.h"
#include "input.h"

#include <KeyValues.h>
#include <vgui/IScheme.h>
#include <vgui/ISurface.h>
#include <vgui/ISystem.h>
#include <vgui_controls/AnimationController.h>
#include <vgui_controls/Panel.h>

#include "vgui/ILocalize.h"
#include "hud_jl_weaponselection.h"
#include "hud_numericdisplay.h"
#include "c_basehlplayer.h" // Contains info for Stamina
#include "ImageProgressBar.h"
#include "hud_status_bars.h"



// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

// #ifdef HL2_EPISODIC
DECLARE_HUDELEMENT( CHudJLWeaponSelection );
// #endif 


using namespace vgui;


//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CHudJLWeaponSelection::CHudJLWeaponSelection( const char *pElementName ) : CBaseHudWeaponSelection(pElementName), BaseClass(NULL, "HudJLWeaponSelection")
{
	vgui::Panel *pParent = g_pClientMode->GetViewport();
	SetParent( pParent );

	//if (ScreenHeight() == 1080) { Weapon_Slots = 12; }
	//else if (ScreenHeight() == 900) { Weapon_Slots = 10; }
	//else { Weapon_Slots = 8; } // Default is 8
	//if (height > Weapon_Slots * 64) { height = Weapon_Slots * 64; }
	//sizeY = height / (float)Weapon_Slots;
	//sizeX = sizeY * 2.0;

	// Size baked to 8, cause that's the right size
	if (height > 8 * 64) { height = 8 * 64; }

	sizeY = height / 8;
	sizeX = sizeY * 2.0;


	
	Medkit_Bar = new ImageProgressBar(this, "HudJLWeaponSelection");
	Medkit_Bar->SetPos(xpos, 0);
	Medkit_Bar->SetSize(100, 8);
	Medkit_Bar->SetVisible(false);
	Medkit_Bar->SetProgressDirection(ProgressBar::PROGRESS_EAST);
	Medkit_Bar->SetTopTexture("Hud/Horizontal/Bar_Green_Slim");
	Medkit_Bar->SetBottomTexture("Hud/Horizontal/Bar_Background_Slim");

	// Disabled because this causes Crashes at every 2.nd time a savegame loads!
	/*Hud_Mini_Mode = cvar->FindVar("Hud_Mini_Mode");
	Hud_Invert_Selection = cvar->FindVar("Hud_Invert_Selection");*/


	//====== Text Labels ======
	vgui::HScheme scheme = vgui::scheme()->GetScheme("ClientScheme");
	hFont = vgui::scheme()->GetIScheme(scheme)->GetFont("Mod_Font");
	charWidth = surface()->GetCharacterWidth(hFont, '0');

	hFont2 = vgui::scheme()->GetIScheme(scheme)->GetFont("HudHintTextLarge");
	charWidth2 = surface()->GetCharacterWidth(hFont2, '0');


	// Without this entry, the Weapon selection would never show up without the Hazard Suit Item
	SetHiddenBits(HIDEHUD_PLAYERDEAD); // | HIDEHUD_NEEDSUIT);
}

void CHudJLWeaponSelection::Init( void )
{	
	i_Texture_Slot = vgui::surface()->CreateNewTextureID();
	vgui::surface()->DrawSetTextureFile(i_Texture_Slot, "HUD/Weapons/Weapon_Slot", true, false);

	i_Texture_Selected = vgui::surface()->CreateNewTextureID();
	vgui::surface()->DrawSetTextureFile(i_Texture_Selected, "HUD/Weapons/Weapon_Selected", true, false);

	// Need one for each slot because otherwise it decreases Framerate by 75%!!
	i_Item_Icon_1 = vgui::surface()->CreateNewTextureID();
	i_Item_Icon_2 = vgui::surface()->CreateNewTextureID();
	i_Item_Icon_3 = vgui::surface()->CreateNewTextureID();
	i_Item_Icon_4 = vgui::surface()->CreateNewTextureID();
	i_Item_Icon_5 = vgui::surface()->CreateNewTextureID();
	i_Item_Icon_6 = vgui::surface()->CreateNewTextureID();
	i_Item_Icon_7 = vgui::surface()->CreateNewTextureID();
	i_Item_Icon_8 = vgui::surface()->CreateNewTextureID();

	i_Item_Icon_9 = vgui::surface()->CreateNewTextureID();
	i_Item_Icon_10 = vgui::surface()->CreateNewTextureID();
	i_Item_Icon_11 = vgui::surface()->CreateNewTextureID();
	i_Item_Icon_12 = vgui::surface()->CreateNewTextureID();
	i_Item_Icon_13 = vgui::surface()->CreateNewTextureID();
	i_Item_Icon_14 = vgui::surface()->CreateNewTextureID();
	i_Item_Icon_15 = vgui::surface()->CreateNewTextureID();
	i_Item_Icon_16 = vgui::surface()->CreateNewTextureID();

	// Texture for i_Item_Icon updates at runtime for Active or Inactive state of each weapon
}

//-----------------------------------------------------------------------------
// Purpose: sets up display for showing weapon pickup
//-----------------------------------------------------------------------------
void CHudJLWeaponSelection::OnWeaponPickup( C_BaseCombatWeapon *pWeapon )
{
	// add to pickup history
	/*CHudHistoryResource *pHudHR = GET_HUDELEMENT( CHudHistoryResource );
	if ( pHudHR )
	{
		pHudHR->AddToHistory( pWeapon );
	}*/
}

//-----------------------------------------------------------------------------
// Purpose: updates animation status
//-----------------------------------------------------------------------------
void CHudJLWeaponSelection::OnThink( void )
{   	
	if (Fade_Away) // Hiding all after Timeout
	{   if (gpGlobals->curtime - m_fLastChange > SELECTION_TIMEOUT_THRESHOLD)
		{
			if (gpGlobals->curtime - m_fLastChange > SELECTION_TIMEOUT_THRESHOLD + SELECTION_FADEOUT_TIME) { HideSelection(); }
			else { SetAlpha(255 - 255 * (gpGlobals->curtime - m_fLastChange - SELECTION_TIMEOUT_THRESHOLD) / SELECTION_FADEOUT_TIME); }
		}
		else if (GetAlpha() != 255) 
		{
			SetAlpha(255); // Unhide all						
		} 
	}
}

//-----------------------------------------------------------------------------
// Purpose: returns true if the panel should draw
//-----------------------------------------------------------------------------
bool CHudJLWeaponSelection::ShouldDraw()
{
	C_BasePlayer *pPlayer = C_BasePlayer::GetLocalPlayer();
	if ( !pPlayer )
	{   // Hiding weapon selection when it times out
		if (Fade_Away && IsInSelectionMode()) { HideSelection(); }
		return false;
	}

	// Checking whether Baseclass draws
	if (!CBaseHudWeaponSelection::ShouldDraw()) return false;

	// For fast swtich
    if (gpGlobals->curtime - m_fLastChange < SELECTION_TIMEOUT_THRESHOLD + SELECTION_FADEOUT_TIME) return true;

	return ( m_bSelectionVisible ) ? true : false;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudJLWeaponSelection::LevelInit()
{	CHudElement::LevelInit();
	Reset_Settings = true;
}

//-------------------------------------------------------------------------
// Purpose: draws the selection area
//-------------------------------------------------------------------------
void CHudJLWeaponSelection::Paint()
{	
	if (!ShouldDraw() || hud_fastswitch.GetInt()) return;

	
	// This sleep used to be essential, it prevented game crashes when the same Texture ID was reused multiple times!!!!!
	// If sleep value is too high you will see the game starting to lag.
	// ThreadSleep(5);

	C_BasePlayer *pPlayer = C_BasePlayer::GetLocalPlayer();
	C_BaseCombatWeapon *pSelectedWeapon = GetSelectedWeapon();
	C_BaseCombatWeapon *pActiveWeapon = pPlayer->GetActiveWeapon();
	CHud_Weapon_Bar *Weapon_Bar;

	Item_Count = 0; // Refreshing
	// This should prevent double selection of inventory!
	bool Weapon_Set = false;


	if (Reset_Settings || Weapon_Bar->Get_Changes()) { Reseting_Settings(); }

	// Hide if no Medkit is selected
	if (!pSelectedWeapon) { Medkit_Bar->SetVisible(false); }


	// First find out at which slot the very last weapon/item is located
	for (int i = MAX_WEAPONS; i > 0; i--)
	{
		C_BaseCombatWeapon *weapon = pPlayer->GetWeapon(i);
		if (weapon){ Last_Weapon_Slot = i; break; } // Exit at the slot where we find the last weapon 	
	}

	// Then count the Items in the range between 0 and Last_Weapon_Slot
	for (int i = 0; i < Last_Weapon_Slot; i++)
	{
		C_BaseCombatWeapon *weapon = pPlayer->GetWeapon(i);
		if (weapon){ Item_Count++; } // Only Draw Weapon slot if it exists!		
	}
	


	//  2.1 = + 10% away from the border
	if (Right_Sided) { xpos = ScreenWidth() - sizeY * 2.1; }
	else { xpos = sizeY / 10; }

	// Resetting for each cycle!
	if (Mini_Mode) { ypos = sizeY; }

	else // Shifting all Selection Drawing away from Mini Hud box
	{
		if (ScreenHeight() < 899)
		{
			if (Top_Sided) { StartY = 100; EndY = 0; }
			else { StartY = 0; EndY = 100; }

			Weapon_Slots = 8;
			ypos = StartY + ((ScreenHeight() - EndY - StartY) - height) / 2.0;
		}
		else if (iHud_Maximum_Tiles.GetBool())
		{			
			if (ScreenHeight() > 999) { Weapon_Slots = 12; ypos = sizeY * 2 + (sizeY / 4); } // 1080 A bit deeper
			else if (ScreenHeight() > 899) // 900 A bit higher
			{
				if (Top_Sided) { Weapon_Slots = 10; ypos = sizeY * 2; }
				else { Weapon_Slots = 10; ypos = sizeY * 2 - (sizeY / 4); }
			}
		}
	}


	int Drawn_Icons = 0;

	// IMPORTANT -1 
	// for (int i = Slot_Shift; i < Slot_Shift + Weapon_Slots; i++)

	// for (int i = Slot_Shift; i < Last_Weapon_Slot; i++)
	// for (int i = Last_Weapon_Slot; i > 0; i--)		


	for (int i = Slot_Shift; i < Slot_Shift + Last_Weapon_Slot; i++)
	{		
		C_BaseCombatWeapon *weapon = pPlayer->GetWeapon(i);
			

		if (Drawn_Icons == Weapon_Slots) { return; } // Exit when all slots were used


		if (weapon) 
		{
			// Msg("Drawing Slot %d\n", i);
			Drawn_Icons++; // Incrementing for each drawn tile


			surface()->DrawSetColor(255, 255, 255, 255); // Setting for drawings below

			s_Selection = (char*)"WeaponIcons";

			if (weapon->GetWpnData().iconInactive)
			{ 
				s_Selection = pPlayer->GetWeapon(i)->GetWpnData().iconInactive->szTextureFile; 

				// Old Check 
				/*if (Compare((char*)weapon->GetWpnData().iconInactive->szTextureFile, "-1")) { Has_Icon = false; }
				else { Has_Icon = true; }*/


				if (Compare(weapon->GetWpnData().iconInactive->szTextureFile, (char*)"WeaponIcons")) { Has_Icon = false; }
				else { Has_Icon = true; } // Returns this default string if no icon was found..
			}
			else if (!weapon->GetWpnData().iconActive) { Has_Icon = false; }
				//->GetWpnData().s_Icon_Active;
				

			//-------------------------------------------------------------------------
			// Mini Mode
			//-------------------------------------------------------------------------
			if (Mini_Mode)
			{   			
				if (weapon == pActiveWeapon)
				{		
					if (Top_Sided)
					{
						if (!Compare((char*)weapon->GetWpnData().szAmmo2, "None")							
						  && Weapon_Bar->Character_Count((char*)weapon->GetWpnData().s_Ammo_Bar_2) > 2 // If has any Weapon Bar(s)
							&& weapon->HasSecondaryAmmo()) { Mini_Ypos = ypos + 16; }

						else if (weapon->GetWpnData().bFuse_Ammo_Bars) { Mini_Ypos = ypos + 16; }

						else if (!Compare((char*)weapon->GetWpnData().szAmmo1, "None") 
							&& weapon->GetWpnData().iItemType != 6 // Ignore revolver types

						  // Disabled because this slot is given even to Weapons without any specified Ammo Bar images, shows just background for Numerics: 
						  // && Weapon_Bar->Character_Count((char*)weapon->GetWpnData().s_Ammo_Bar_1) > 2
							&& weapon->HasPrimaryAmmo()) { Mini_Ypos = ypos; }					
						else { Mini_Ypos = ypos - 16; } // Otherwise its at mediocre ypos with the average primary ammo.				  
					}				
					else if (!Top_Sided)
					{
						// Mini_Ypos = ScreenHeight() - (sizeY + Mini_Ypos);
						Mini_Ypos = ScreenHeight() - (sizeY * 2 + 15);
					    C_BaseHLPlayer *Player = (C_BaseHLPlayer *)C_BasePlayer::GetLocalPlayer();
						if (Player)
						{   // Stamina
							if (Player->m_HL2Local.m_flSuitPower < 99) { Mini_Ypos = Mini_Ypos - 16; }

							CHealth_Bar* Status_Bar;
							// Setting higher by size of an additional Bar (including + 2 margin units).
							if (Status_Bar->Get_Armor() > 0) { Mini_Ypos = Mini_Ypos - 16; }
						}  										
					}
				}
			
				if (weapon == pSelectedWeapon)
				{  	if (Show_Background) 
					{	surface()->DrawSetTexture(i_Texture_Slot);
						surface()->DrawTexturedRect(xpos, Mini_Ypos, xpos + sizeX, Mini_Ypos + sizeY);
					}

					if (!weapon->HasAnyAmmo()) { surface()->DrawSetColor(Color_Depleted); }

					if (Has_Icon) // Probably has icon, this test will show 
					{
						if (Invert_Selection && weapon->GetWpnData().iconActive)
						{   // Meaning of inactive and active is distorted.. iconInactive means the colored one and iconActive is White/unicolor
							s_Selection = weapon->GetWpnData().iconActive->szTextureFile;

							if (Compare(s_Selection, "WeaponIconsSelected") && weapon->GetWpnData().iconInactive) // If returns default value
							{   Buffer[0] = 0; // Then we append this suffix
								Weapon_Bar->Append(weapon->GetWpnData().iconInactive->szTextureFile, (char*)"_Lit", Buffer);
								s_Selection = Buffer;
							}
						} 
						// { s_Selection = (char*)pPlayer->GetWeapon(i)->GetWpnData().s_Icon_Inactive; } // Outdated


						if (!Compare(s_Selection, "WeaponIcons") && !Compare(s_Selection, "WeaponIcons_Lit") && !Compare(s_Selection, "WeaponIconsSelected")) // If makes any sense
						{   // Get_Texture_ID(i) returns a different texture for each weapon slot, cause this gives huge performance improovement.
							surface()->DrawSetTextureFile(Get_Texture_ID(i), s_Selection, true, true);
							surface()->DrawSetTexture(Get_Texture_ID(i));
							surface()->DrawTexturedRect(xpos, Mini_Ypos, xpos + sizeX, Mini_Ypos + sizeY);
						} else { Has_Icon = false; }
					}	
					
					if (!Has_Icon)
					{
						Temp_A = Get_Name(weapon); 
			

						//Msg("%d, %d, %d, %d, \n", Weapon_Bar->Get_Color_1()[0], Weapon_Bar->Get_Color_1()[1], Weapon_Bar->Get_Color_1()[2], Weapon_Bar->Get_Color_1()[3]);

						if (Weapon_Bar->Character_Count(Temp_A) < 15)
						{
							Draw_Text(xpos + 1 + sizeX / 4, Mini_Ypos + 1 + sizeY / 3, Temp_A, Color_2);
							Draw_Text(xpos + sizeX / 4, Mini_Ypos + sizeY / 3, Get_Name(weapon), Color_1);
						}
						else // Otherwise start drawing at left border for space purpose.
						{
							Draw_Text(xpos + 1 + sizeX / 10, Mini_Ypos + 1 + sizeY / 3, Temp_A, Color_2);
							Draw_Text(xpos + sizeX / 10, Mini_Ypos + sizeY / 3, Get_Name(weapon), Color_1);
						}												
					}
				} // Deactivating anything else in Mini Mode!
			}


			//-------------------------------------------------------------------------
			// Selected Weapon
			//-------------------------------------------------------------------------

			// Imp; Choosing Background Texture, CreateTime because if we compare weapon itself 2 Medkits of same type get selected otherwise!
			else if (weapon == pSelectedWeapon && weapon->HasAnyAmmo() && !Weapon_Set)
			// else if (weapon == pSelectedWeapon && weapon->HasAnyAmmo())
			{   				
				// This is supposed to re-open selection slot for weapons the player doesen't have multiple times.
				Weapon_Set = true;

				// Msg("Weight is %d\n", weapon->GetWpnData().fKilo);

				if (Show_Selector) 
				{   surface()->DrawSetColor(Color_Filter);
					Draw_Background(i_Texture_Selected);
					surface()->DrawSetColor(255, 255, 255, 255); // Clearing color filter for Images
				}
				else if (Show_Background) { Draw_Background(i_Texture_Slot); }

				// if (weapon->GetWpnData().bIsItem)
				// if (Compare((char*)weapon->GetWpnData().szPrintName, "Health Kit") || Compare((char*)weapon->GetWpnData().szPrintName, "Pain Killers"))
				if (Compare((char*)weapon->GetWpnData().szAmmo2, "Medkit") || 
					 Compare((char*)weapon->GetWpnData().szAmmo2, "Pills") || Compare((char*)weapon->GetWpnData().szAmmo2, "Antibiotics"))
				{
					// Medkit_Bar->SetPos(xpos, ypos + (sizeY / 6) * 5); Muster
					// (float)((i_Ammo_Bar_Size / i_Clip_Size_1) * i_Ammo_1) / i_Ammo_Bar_Size;	Muster
					// Mach das �ber die Armor Bar Funktion..
					int Value = weapon->Clip1();
					int Clip_Size = weapon->GetMaxClip1();

					// Medkit_Bar->SetPos(xpos + sizeX / 9, ypos + sizeY / 9);
					Medkit_Bar->SetPos(xpos + sizeX / 9, ypos + sizeY / 10);


					if (Value == 0 && weapon->m_iClip2 > 0)
					{   Medkit_Bar->SetProgress(1.0f); // Bar size is 100% cause we have Meds left
						Medkit_Bar->SetTopTexture("Hud/Horizontal/Bar_Green_Slim");
					} 					
					else 
					{   Medkit_Bar->SetProgress((float)Value / Clip_Size); 

						// Colorfull Power Bar
						if (Value * (100 / Clip_Size) < 34) { Medkit_Bar->SetTopTexture("Hud/Horizontal/Bar_Red_Slim"); }
						else if (Value * (100 / Clip_Size) < 67) { Medkit_Bar->SetTopTexture("Hud/Horizontal/Bar_Orange_Slim"); }
						else { Medkit_Bar->SetTopTexture("Hud/Horizontal/Bar_Green_Slim"); }
					}
					Medkit_Bar->SetVisible(true);
			

				} else { Medkit_Bar->SetVisible(false); }

			 	
							

				if (Has_Icon)
				{	// Utilizes TextureData/Weapon/file variable in weapon script, and actually works but the one below is more flexible
				    // surface()->DrawSetTextureFile(Get_Texture_ID(i), (char*)pPlayer->GetWeapon(i)->GetWpnData().iconInactive->szTextureFile, true, true);				
						
					if (Invert_Selection && weapon->GetWpnData().iconActive)
					{   // Meaning of inactive and active is distorted.. iconInactive means the colored one and iconActive is White/unicolor
						s_Selection = weapon->GetWpnData().iconActive->szTextureFile;

						if (Compare(s_Selection, "WeaponIconsSelected"))
						{   Buffer[0] = 0;
							sprintf(Buffer, "%s_Lit", weapon->GetWpnData().iconInactive->szTextureFile);
							s_Selection = Buffer;
						}												
					}

					if (!Compare(s_Selection, "WeaponIcons") && !Compare(s_Selection, "WeaponIcons_Lit") && !Compare(s_Selection, "WeaponIconsSelected")) // If makes any sense
					{
						surface()->DrawSetTextureFile(Get_Texture_ID(i), s_Selection, true, true);
						Draw_Icon(Get_Texture_ID(i));
						Selected = i; // used in other cycle below
				

						if (Show_Selector && Compare(Get_Name(weapon), "Menu"))
						{						
							Draw_Text(xpos + 1 + sizeX / 3, ypos + 1 + (sizeY / 7) * 4, Get_Name(weapon), Color_2);
							Draw_Text(xpos + sizeX / 3, ypos + (sizeY / 7) * 4, Get_Name(weapon), Color_1);
						}
						else if (Show_Selector) // || Show_Background) // Only if not in symbol mode
						{	

							// CHud_Weapon_Bar * Weapon_Bar;
						/*	int number = 3;
							char numberstring[(((sizeof number) * CHAR_BIT) + 2) / 3 + 2];
							sprintf(numberstring, "%d", number);*/

							/*char * Test = "Test_";
							char * Num = "03";

							Draw_Text(xpos + 1 + sizeX / 4, ypos + 1 + (sizeY / 7) * 5, Test, Color_2R, Color_2G, Color_2B, Color_2Alpha);*/					
						
							Draw_Text(xpos + 1 + sizeX / 4, ypos + 1 + (sizeY / 7) * 5, Get_Name(weapon), Color_2);
							Draw_Text(xpos + sizeX / 4, ypos + (sizeY / 7) * 5, Get_Name(weapon), Color_1);

						
							// i means here the slot of the weapon
							// Draw_Number(xpos + 1 + sizeX / 10, ypos + 1 + sizeY / 3, i, Color_2);
							// Draw_Number(xpos + sizeX / 10, ypos + sizeY / 3, i, Color_1);
						}	
					} else { Has_Icon = false; }
				}	

				if (!Has_Icon) // if(strcmp(weapon->GetWpnData().iconInactive->szTextureFile, "-1"))  252, 25, 25, 255 // Red 150, 255, 50, 255 Orange
				{   // This draws a shadow in the Background of Color 1, and provides more contrast
					Draw_Text(xpos + 1 + sizeX / 4, ypos + 1 + sizeY / 3, Get_Name(weapon), Color_2);
					Draw_Text(xpos + sizeX / 4, ypos + sizeY / 3, Get_Name(weapon), Color_1);

					// Slot test letters, they display weapon slot position in test mode 
					// Draw_Number(xpos + 1 + sizeX / 10, ypos + 1 + sizeY / 3, Weapon_Slot, Color_2);
					// Draw_Number(xpos + sizeX / 10, ypos + sizeY / 3, Weapon_Slot, Color_1);
				} // Blue
			}

			//-------------------------------------------------------------------------
			// All Unselected Weapons
			//-------------------------------------------------------------------------
			else 
			{
				Weapon_Set = false;
							
				if (iHud_Show_Dock.GetBool() && Show_Selector) // Dock Function
				{   
					if (weapon->HasAnyAmmo()) // In Dock Mode this targets the Selction Border
					{ surface()->DrawSetColor(Color_Filter); }

					else { surface()->DrawSetColor(Color_Depleted); } // 255, 60, 60, 255 Dock Red

					if (weapon == pPlayer->GetWeapon(Selected - 1) || weapon == pPlayer->GetWeapon(Selected + 1))
					{   // Actually we should define a new background, Instead I use the old one		
						if (Right_Sided) { xpos = ScreenWidth() - sizeY; }
						else { xpos = -sizeY; }
						Draw_Background(i_Texture_Selected);
					}
					else if (weapon == pPlayer->GetWeapon(Selected + 2))
					{
						if (Right_Sided) { xpos = ScreenWidth() - ((sizeY * 2) / 10) * 3; }
						else { xpos = -((sizeY * 2) / 10) * 7; }
						Draw_Background(i_Texture_Selected);
					}

					// Resetting Value for the next cycle
					if (Right_Sided) { xpos = ScreenWidth() - sizeY * 2.1; }
					else { xpos = sizeY / 10; }
				}
				else 
				{   	
					if (Has_Icon && Show_Background) { Draw_Background(i_Texture_Slot); }

					if (!weapon->HasAnyAmmo()) // A changed color is supposed to indicate that the weapon is no longer selectable
					{ surface()->DrawSetColor(Color_Dep_R, Color_Dep_G, Color_Dep_B, Color_Dep_A); }
								
				
					if (Has_Icon) // Color specified by Convar "Color_1" in hud_weapon_bars.h
					{
						if (!Invert_Selection && weapon->GetWpnData().iconActive)
						{   s_Selection = weapon->GetWpnData().iconActive->szTextureFile;

							if (Compare(s_Selection, "WeaponIconsSelected") && weapon->GetWpnData().iconInactive)
							{   Buffer[0] = 0;
								sprintf(Buffer, "%s_Lit", weapon->GetWpnData().iconInactive->szTextureFile);
								s_Selection = Buffer;
							}
						}

						if (!Compare(s_Selection, "WeaponIcons") && !Compare(s_Selection, "WeaponIcons_Lit") && !Compare(s_Selection, "WeaponIconsSelected")) // If makes any sense
						{   // Drawing all 7 remaining icons, which is expensive. This would crash the game without ThreadSleep() above!
							surface()->DrawSetTextureFile(Get_Texture_ID(i), s_Selection, true, true);
							Draw_Icon(Get_Texture_ID(i)); // Msg(weapon->GetWpnData().iconInactive->szTextureFile);
						} else { Has_Icon = false; }
					}

					// Failsafe is no texure is specified  
					if (!Has_Icon) // Color specified by Convar "Color_1" in hud_weapon_bars.h
					{   // Here we draw WITHOUT any text shadow, which is an exclsive effect to highlight the selected Slot

						if (Show_Background)
						{   Draw_Background(i_Texture_Slot);

							// Draw_Number(xpos + sizeX / 10, ypos + sizeY / 3, (int)22, Color_2);

							Draw_Text(xpos + sizeX / 10, ypos + sizeY / 3, Get_Name(weapon), Color_1);
						}
						else // If no Background is shown we use this color to mark the letters as depleted.
						{   if (weapon->HasAnyAmmo())
							{  Draw_Text(xpos + sizeX / 10, ypos + sizeY / 3, Get_Name(weapon), Color_1); }
							else
							{	// Draw_Number(xpos + sizeX / 10, ypos + sizeY / 3, (int)22, Color_Depleted);
								Draw_Text(xpos + sizeX / 10, ypos + sizeY / 3, Get_Name(weapon), Color_Depleted);
							}
						}
					}


				}				
			}	
										

			// Increasing y position for selection of the next weapon (if not in Minimal Mode)
			if (!Mini_Mode) { ypos += sizeY; }			
		}	

		/*
		if (weapon && weapon->GetWpnData().iconInactive)
		{
			float Icon_Height = weapon->GetWpnData().iconInactive->Height();
			float Icon_Width = weapon->GetWpnData().iconInactive->Width();

			float f_Ratio;
			f_Ratio = sizeX / Icon_Width;
			f_Ratio = min(f_Ratio, sizeY / Icon_Height);
			float xoff = (sizeX - Icon_Width * f_Ratio) / 2.0;
			float yoff = (sizeY - Icon_Height * f_Ratio) / 2.0;
			if (weapon->GetWpnData().iconInactive->bRenderUsingFont)
			{
				//vgui::surface()->DrawSetTextScale(fRatio,fRatio);
			}
			//weapon->GetWpnData().iconInactive->DrawSelf(xpos+xoff+5,ypos+yoff+5,sizeX-xoff*2-10,sizeY-yoff*2-10,Color(255,255,255,255));
			
			weapon->GetWpnData().iconInactive->DrawSelfCropped
				(   xpos + xoff + 5, ypos + yoff + 5, // X and Y Position
					0,0, Icon_Width / 2, Icon_Height / 2, // Crop XY + Crop Size
					32, 32, // Final Width and Height
					//sizeX-xoff*2-10,sizeY-yoff*2-10,		
					Color(255,255,255,255)
				);
		}
		*/	
	}
}

//-----------------------------------------------------------------------------
// Purpose: Draw Tile Background
//-----------------------------------------------------------------------------
void CHudJLWeaponSelection::Reseting_Settings()
{
	CHud_Weapon_Bar *Weapon_Bar;


	Mini_Mode = Weapon_Bar->Get_Mini_Mode();
	Invert_Selection = Weapon_Bar->Get_Invert_Selection();

	Top_Sided = Weapon_Bar->Get_Top_Sided();
	Right_Sided = Weapon_Bar->Get_Right_Sided();

	Show_Background = Weapon_Bar->Get_Show_Background();
	Show_Selector = Weapon_Bar->Get_Show_Selector();

	// These 3 are members of this class
	Invert_Scroll = iHud_Invert_Scroll.GetBool();
	// Show_Dock = iHud_Show_Dock.GetBool();
	Fade_Away = iHud_Fade_Away.GetBool();

//-------------------

	char Col_R[80];
	char Col_G[80];
	char Col_B[80];
	char Col_Alpha[80];

	// Extracting color values into a string array
	char* Color_1_Txt = Weapon_Bar->Get_Color_1_Value();

	char delimit = ','; // Both variants legit
	if (Compare(Color_1_Txt, ".")) { delimit = '.'; }

	Weapon_Bar->Split_Array(Color_1_Txt, Col_R, delimit, 1);
	Weapon_Bar->Split_Array(Color_1_Txt, Col_G, delimit, 2);
	Weapon_Bar->Split_Array(Color_1_Txt, Col_B, delimit, 3);
	Weapon_Bar->Split_Array(Color_1_Txt, Col_Alpha, delimit, 4);

	Color_1R = V_atof(Col_R);
	Color_1G = V_atof(Col_G);
	Color_1B = V_atof(Col_B);
	Color_1Alpha = V_atof(Col_Alpha);

	Color_1 = Color(Color_1R, Color_1G, Color_1B, Color_1Alpha);

	// 252, 25, 25, 255 // Red 
	// 255, 60, 60, 255 Dock Red
	// 150, 255, 50, 255 // Orange  252, 205, 125, 255 // Cooler Orange
	// 50, 150, 230, 255 // Dark Blue
	// 220, 220, 255, 255 // Bright Blue
	// 50, 150, 230, 255 // Blue?
	// 180, 100, 220, 255 // Purple

//-------------------

	char* Color_2_Txt = Weapon_Bar->Get_Color_2_Value();

	delimit = ','; // Both variants legit
	if (Compare(Color_2_Txt, ".")) { delimit = '.'; }

	Weapon_Bar->Split_Array(Color_2_Txt, Col_R, delimit, 1);
	Weapon_Bar->Split_Array(Color_2_Txt, Col_G, delimit, 2);
	Weapon_Bar->Split_Array(Color_2_Txt, Col_B, delimit, 3);
	Weapon_Bar->Split_Array(Color_2_Txt, Col_Alpha, delimit, 4);

	Color_2R = V_atof(Col_R);
	Color_2G = V_atof(Col_G);
	Color_2B = V_atof(Col_B);
	Color_2Alpha = V_atof(Col_Alpha);

	Color_2 = Color(Color_2R, Color_2G, Color_2B, Color_2Alpha);

//-------------------

	char* Color_Depleted_Txt = Weapon_Bar->Get_Color_Depleted_Value();

	delimit = ','; // Both variants legit
	if (Compare(Color_Depleted_Txt, ".")) { delimit = '.'; }

	Weapon_Bar->Split_Array(Color_Depleted_Txt, Col_R, delimit, 1);
	Weapon_Bar->Split_Array(Color_Depleted_Txt, Col_G, delimit, 2);
	Weapon_Bar->Split_Array(Color_Depleted_Txt, Col_B, delimit, 3);
	Weapon_Bar->Split_Array(Color_Depleted_Txt, Col_Alpha, delimit, 4);

	Color_Dep_R = V_atof(Col_R);
	Color_Dep_G = V_atof(Col_G);
	Color_Dep_B = V_atof(Col_B);
	Color_Dep_A = V_atof(Col_Alpha);

	Color_Depleted = Color(Color_Dep_R, Color_Dep_G, Color_Dep_B, Color_Dep_A);


//-------------------
	char* Tex_Color = Weapon_Bar->Get_Color_Filter_Value();

	delimit = ','; // Both variants legit
	if (Compare(Tex_Color, ".")) { delimit = '.'; }

	Weapon_Bar->Split_Array(Tex_Color, Col_R, delimit, 1);
	Weapon_Bar->Split_Array(Tex_Color, Col_G, delimit, 2);
	Weapon_Bar->Split_Array(Tex_Color, Col_B, delimit, 3);
	Weapon_Bar->Split_Array(Tex_Color, Col_Alpha, delimit, 4);

	Color_TexR = V_atof(Col_R);
	Color_TexG = V_atof(Col_G);
	Color_TexB = V_atof(Col_B);
	Color_TexAlpha = V_atof(Col_Alpha);

	Color_Filter = Color(Color_TexR, Color_TexG, Color_TexB, Color_TexAlpha);

	// They fail to pass the value, somehow..
	/*int Color_TexR = Weapon_Bar->Get_Color_Texture_R();
	int Color_TexG = Weapon_Bar->Get_Color_Texture_G();
	int Color_TexB = Weapon_Bar->Get_Color_Texture_B();
	int Color_TexAlpha = Weapon_Bar->Get_Color_Texture_A();*/

	// If inactive it will stay in responsive test mode
	Reset_Settings = false; 
}

//-----------------------------------------------------------------------------
// Purpose: Draw Tile Background
//-----------------------------------------------------------------------------
void CHudJLWeaponSelection::Draw_Background(int texture)
{	surface()->DrawSetTexture(texture);
	surface()->DrawTexturedRect(xpos, ypos, xpos + sizeX, ypos + sizeY);
}

//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
void CHudJLWeaponSelection::Draw_Icon(int texture)
{   surface()->DrawSetTexture(texture);
	surface()->DrawTexturedRect(xpos, ypos, xpos + sizeX, ypos + sizeY);
}

//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
int CHudJLWeaponSelection::Get_Texture_ID(int i)
{	
	switch (i)
	{
	case 1:
	case 17:
	case 33:
		return i_Item_Icon_1;
		break;
	case 2:
	case 18:
	case 34:
		return i_Item_Icon_2;
		break;
	case 3:
	case 19:
	case 35:
		return i_Item_Icon_3;
		break;
	case 4:
	case 20:
	case 36:
		return i_Item_Icon_4;
		break;
	case 5:
	case 21:
	case 37:
		return i_Item_Icon_5;
		break;
	case 6:
	case 22:
	case 38:
		return i_Item_Icon_6;
		break;
	case 7:
	case 23:
	case 39:
		return i_Item_Icon_7;
		break;
	case 8:
	case 24:
	case 40:
		return i_Item_Icon_8;
		break;

	case 9:
	case 25:
	case 41:
		return i_Item_Icon_9;
		break;
	case 10:
	case 26:
	case 42:
		return i_Item_Icon_10;
		break;
	case 11:
	case 27:
	case 43:
		return i_Item_Icon_11;
		break;
	case 12:
	case 28:
	case 44:
		return i_Item_Icon_12;
		break;
	case 13:
	case 29:
	case 45:
		return i_Item_Icon_13;
		break;
	case 14:
	case 30:
	case 46:
		return i_Item_Icon_14;
		break;
	case 15:
	case 31:
	case 47:
		return i_Item_Icon_15;
		break;
	case 16:
	case 32:
	case 48:
		return i_Item_Icon_16;
		break;
	}

	return i_Item_Icon_16;

	// Distributing them eqaually to 48 Item slots, this prevents crashes due to over-refreshing the art entry
	// Otherwise it would also drain the Framerate a lot!

}



//-----------------------------------------------------------------------------
// Purpose: hud scheme settings
//-----------------------------------------------------------------------------
void CHudJLWeaponSelection::ApplySchemeSettings(vgui::IScheme *pScheme)
{   BaseClass::ApplySchemeSettings(pScheme);
	SetPaintBackgroundEnabled(false);
	Reset_Settings = true;
}

//-----------------------------------------------------------------------------
// Purpose: Opens weapon selection control
//-----------------------------------------------------------------------------
void CHudJLWeaponSelection::OpenSelection( void )
{
	Assert(!IsInSelectionMode());

	C_BasePlayer *pPlayer = C_BasePlayer::GetLocalPlayer();
	if (pPlayer)
		SetSelectedWeapon( pPlayer->GetActiveWeapon() );

	m_fLastChange = gpGlobals->curtime;

	CBaseHudWeaponSelection::OpenSelection();
}

//-----------------------------------------------------------------------------
// Purpose: Closes weapon selection control immediately
//-----------------------------------------------------------------------------
void CHudJLWeaponSelection::HideSelection( void )
{ CBaseHudWeaponSelection::HideSelection(); }

//-----------------------------------------------------------------------------
// Purpose: Placed this function between the cycles to obtain more control of them
//-----------------------------------------------------------------------------
void CHudJLWeaponSelection::CycleToNextWeapon(void)
{   if (Invert_Scroll) { Cycle_Down(); }
	else { Cycle_Up(); }
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudJLWeaponSelection::CycleToPrevWeapon(void)
{	if (Invert_Scroll) { Cycle_Up(); }
	else { Cycle_Down(); }
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
int CHudJLWeaponSelection::Get_Slot_Position()
{
	C_BasePlayer *pPlayer = C_BasePlayer::GetLocalPlayer();
	if (!pPlayer) return -1;

	C_BaseCombatWeapon *activeWeapon = GetSelectedWeapon();
	if (!activeWeapon) activeWeapon = pPlayer->GetActiveWeapon();

	for (int i = 0; i < Last_Weapon_Slot; i++)
	{   if (pPlayer->GetWeapon(i) == activeWeapon)
		{ return i; }
	}

	return -1;
}

//-----------------------------------------------------------------------------
// Purpose: Moves the selection to the previous item in the menu
//-----------------------------------------------------------------------------
void CHudJLWeaponSelection::Cycle_Up(void)
{
	C_BasePlayer *pPlayer = C_BasePlayer::GetLocalPlayer();
	if (!pPlayer) { return; }

	C_BaseCombatWeapon *activeWeapon = GetSelectedWeapon();
	if (!activeWeapon){ activeWeapon = pPlayer->GetActiveWeapon(); }

	if (!IsInSelectionMode()) { OpenSelection(); }

	m_fLastChange = gpGlobals->curtime;
	int Position = Get_Slot_Position();



	if (Position > -1)
	{
		int i = Position - 1;

		
		// If Items don't longer fit into Weapon_Slots and we reached last entry		
		if (Item_Count > Weapon_Slots && i < Slot_Shift)
		{	if (Slot_Shift > 0) { Slot_Shift--; i = Slot_Shift; }
			else { Slot_Shift = Item_Count - Weapon_Slots; i = Slot_Shift + (Weapon_Slots - 1); } // -1 because of slot 0

			// Old one, this jums right back to the slot at the very bottom
			// else { i = Weapon_Slots - 1; }
		} else if (i < 0){ i = Last_Weapon_Slot -1; Slot_Shift = 0; }
		

		while (i != Position) // While position is NOT of the selected weapon
		{
			if (Item_Count < 2) { return; }

			if (pPlayer->GetWeapon(i))
			{
				// Ignoring this
				if (!hud_fastswitch.GetInt() || hud_fastswitch.GetInt() && !Compare(Get_Name(pPlayer->GetWeapon(i)), "Menu"))
				{   // if (pPlayer->GetWeapon(i)->GetWpnData().bIsItem == false) {
					SetSelectedWeapon(pPlayer->GetWeapon(i));
					// Play the "cycle to next weapon" sound
					pPlayer->EmitSound("Player.WeaponSelectionMoveSlot");

					if (pPlayer->GetWeapon(i) != pPlayer->GetWeapon(i + 1)) { return; }
					// }
				}
			}
			else if (pPlayer->GetWeapon(i - 1))
			{   SetSelectedWeapon(pPlayer->GetWeapon(i - 1));
				pPlayer->EmitSound("Player.WeaponSelectionMoveSlot");
			}
			
			i--;	


			if (Item_Count > Weapon_Slots && i < Slot_Shift) // If there are more weapons than icon slots
			{   
				if (Slot_Shift > 0) { Slot_Shift--; i = Slot_Shift; } // Decrement while possible, otherwise reset:

				else { Slot_Shift = Item_Count - Weapon_Slots; i = Slot_Shift + (Weapon_Slots - 1); } // -1 because of slot 0			
			} 
			else if (i <= 0){ i = Last_Weapon_Slot -1; Slot_Shift = 0; } // Reset! before we run into a null exception that crashes hl2
		}

		


		// Making sure there is any weapon selected within range, otherwise the player wouldn't be able to move selector!
		bool Has_Selected = false;		
		// C_BasePlayer *pPlayer = C_BasePlayer::GetLocalPlayer();
		// C_BaseCombatWeapon *pSelectedWeapon = GetSelectedWeapon();

		for (int i = 0; i < Weapon_Slots; i++)
		{			
			if (activeWeapon == pPlayer->GetWeapon(i)) // If any weapon in selection range is selected
			{ Has_Selected = true; break; } // todo break; is suspicious to exit before all item slots are printed on the ui
		}

		if (!Has_Selected)
		{   for (int i = 0; i < Weapon_Slots; i++)
			{	// Selecting the next best slot
				if (pPlayer->GetWeapon(i)) { SetSelectedWeapon(pPlayer->GetWeapon(i)); break; }
			}
		}	

	}
}

//-----------------------------------------------------------------------------
// Purpose: Moves the selection to the next item in the menu
//-----------------------------------------------------------------------------
void CHudJLWeaponSelection::Cycle_Down(void)
{
	// Get the local player.
	C_BasePlayer *pPlayer = C_BasePlayer::GetLocalPlayer();
	if (!pPlayer) { return; }

	C_BaseCombatWeapon *activeWeapon = GetSelectedWeapon();
	if (!activeWeapon){ activeWeapon = pPlayer->GetActiveWeapon(); }

	if (!IsInSelectionMode()) { OpenSelection(); }

	m_fLastChange = gpGlobals->curtime;
	int Position = Get_Slot_Position();



	if (Position > -1)
	{   int i = Position + 1;
		
		// This is the List-selection function
		if (Item_Count > Weapon_Slots && i > Slot_Shift + Weapon_Slots - 1) 
		{	// Item_Count - Weapon_Slots; because we count the drawn tiles away 
			if (Slot_Shift < Item_Count - Weapon_Slots){ Slot_Shift++; i = Slot_Shift + Weapon_Slots - 1; } // Increment by 1
			else { i = 0; Slot_Shift = 0; }
		}
		else if (i >= Last_Weapon_Slot) { i = 0; Slot_Shift = 0; } // Otherwise no List just jump to Top slot


		while (i != Position) // while (pPlayer->GetWeapon(i) != activeWeapon)
		{
			if (pPlayer->GetWeapon(i))
			{   			
				if (!hud_fastswitch.GetInt() || hud_fastswitch.GetInt() && !Compare(Get_Name(pPlayer->GetWeapon(i)), "Menu"))
				{
					// if (pPlayer->GetWeapon(i)->GetWpnData().bIsItem == false) {
					SetSelectedWeapon(pPlayer->GetWeapon(i));
					// Play the "cycle to next weapon" sound
					pPlayer->EmitSound("Player.WeaponSelectionMoveSlot");
					if (pPlayer->GetWeapon(i) != pPlayer->GetWeapon(i - 1)) { return; }
					// }
				}			
			}	
		
			i++; 		
			if (Item_Count > Weapon_Slots && i > Slot_Shift + Weapon_Slots - 1)
			{   if (Slot_Shift < Item_Count - Weapon_Slots){ Slot_Shift++; i = Slot_Shift + Weapon_Slots - 1; } 
				else { i = 0; Slot_Shift = 0; }
			} else if (i >= Last_Weapon_Slot) { i = 0; Slot_Shift = 0; }
		}
	}

}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
char* CHudJLWeaponSelection::Get_Name(C_BaseCombatWeapon * Weapon)
{
	C_BasePlayer *Player = C_BasePlayer::GetLocalPlayer();
	if (Player && Weapon)
	{			
		wchar_t* Entry = g_pVGuiLocalize->Find(Weapon->GetWpnData().szPrintName);	
		
		char Buffer[240]; // Never use MAX_WEAPON_STRING, it somehow breaks this
		Buffer[0] = 0;
		g_pVGuiLocalize->ConvertUnicodeToANSI(Entry, Buffer, 240);

		// If no Localization was found we return the straight text
		if (Compare(Buffer, "")) { return (char*)Weapon->GetWpnData().szPrintName; }	
		else { return (char*)Buffer; }		
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudJLWeaponSelection::Draw_Text(int x, int y, char* text, int R, int G, int B, int Alpha)
{   surface()->DrawSetTextFont(hFont2);
	surface()->DrawSetTextColor(R, G, B, Alpha); 			
	// surface()->DrawSetTextPos(xpos + sizeX / 2, ypos + sizeY / 4);
	surface()->DrawSetTextPos(x, y);


	// surface()->DrawSetTextScale(2.0, 2.0);
	// char* Wpn_Name = Get_Name(weapon);
	// if (Starts_With(W_Name, "#HL2_")) { Trimm_Prefix("#HL2_", W_Name); }

	/*wchar_t *ws = g_pVGuiLocalize->Find(weapon->GetWpnData().szPrintName);
	swprintf(unicode, L"%c", ws);*/

	// Q_strncpy(Wpn_Name, (char*)weapon->GetWpnData().szPrintName, MAX_WEAPON_STRING);

	mbstowcs(&unicode[0], text, MAX_WEAPON_STRING); // Max 80 characters
	surface()->DrawPrintText(unicode, wcslen(unicode));

}

void CHudJLWeaponSelection::Draw_Text(int x, int y, char* text, Color Color_Value)
{   
	surface()->DrawSetTextFont(hFont2);
	surface()->DrawSetTextColor(Color_Value);
	surface()->DrawSetTextPos(x, y);

	// surface()->DrawSetTextScale(2.0, 2.0);

	mbstowcs(&unicode[0], text, MAX_WEAPON_STRING); // Max 80 characters
	surface()->DrawPrintText(unicode, wcslen(unicode));
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudJLWeaponSelection::Draw_Number(int x, int y, int value, int R, int G, int B, int Alpha)
{   surface()->DrawSetTextFont(hFont2);
	surface()->DrawSetTextColor(R, G, B, Alpha);
	surface()->DrawSetTextPos(x, y);

	swprintf(unicode, L"%d", value);
	surface()->DrawPrintText(unicode, wcslen(unicode));
}

//-----------------------------------------------------------------------------
// Purpose: String Comparsion
//-----------------------------------------------------------------------------
bool CHudJLWeaponSelection::Compare(char* value_1, char* value_2)
{
	if (strcmp(value_1, value_2) == 0) { return true; }
	return false;
}

//-----------------------------------------------------------------------------
// Purpose: String Comparsion
//-----------------------------------------------------------------------------
// Note; We only compare until the loop passed the Size of value_2.
bool CHudJLWeaponSelection::Starts_With(char* value_1, char* value_2)
{   // Looping through all Chars in the value_2 array, if we meet any value on the way that does not match we return false

	for (int i = 0; i < sizeof(value_2); i++)
	{ if (value_1[i] != value_1[i]) return false; }

	return true;
}


//-----------------------------------------------------------------------------
// Purpose: String Comparsion
//-----------------------------------------------------------------------------
// Note; We only compare until the loop passed the Size of value_2.
// prefix to trim, source array
void CHudJLWeaponSelection::Trimm_Prefix(char* value_1, char* value_2)
{   int Filled_Slots = 0;

	for (int i = 0; i < sizeof(value_2); i++)
	{   if (i > sizeof(value_1))
		{   value_2[Filled_Slots] = value_2[i]; // Shifting ahead
			Filled_Slots++;
		}
		// else { value_2[i] = 0; }
	}
}


//-----------------------------------------------------------------------------
// Purpose: String Comparsion
//-----------------------------------------------------------------------------
// source array, suffix to trim
void CHudJLWeaponSelection::Trimm_Suffix(char* value_1, char* value_2)
{
	int Empty_Slots = sizeof(value_1);

	for (int i = sizeof(value_1) + sizeof(value_2); i == 0; i--)
	{
		if (i = sizeof(value_1) - sizeof(value_2) && Empty_Slots != 0)
		{
			value_1[Empty_Slots] = value_1[i]; // Shifting back
			Empty_Slots--;
		}
	}
}





	/*
	// Looping through all Chars in the value_2 array, if we meet any value on the way that does not match we return false
	bool Copy_All = false;
	int Filled_Slots = 0;

	for (int i = 0; i < sizeof(value_2) -1; i++)
	{
		if (i == sizeof(value_1 + 1)) 
		{   value_3[Filled_Slots] = value_2[i];
			Filled_Slots++;
		}

		
	    if (i == sizeof(value_1)) { Copy_All = true; }
			
		// Only if not matched we transcript it into the return value
		if (value_1[i] != value_2[i] || Copy_All)
		{   // This breaks as soon the last match turns false, but it still works in first loop 
			// if (Last_Match) {			
				value_3[Filled_Slots] = value_2[i];
				Filled_Slots++;
			//	Last_Match = true; // Gate opener for next cycle
			// } else { value_3[sizeof(value_2)] = '\0'; return true; } // Done
			// else if (Last_Match == false) { return true; } // Done
			// else { Last_Match = false; } 
		}
		
		
	}
	
	if (Filled_Slots == 0) return false; // No prefix found to remove

	value_3[sizeof(value_2)] = '\0'; 
	return true; 
	*/




