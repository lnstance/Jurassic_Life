//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#include "cbase.h"
#include "hud.h"
#include "hudelement.h"
#include "hud_macros.h"
#include "hud_numericdisplay.h"
#include "iclientmode.h"
#include "iclientvehicle.h"
#include <vgui_controls/AnimationController.h>
#include <vgui/ILocalize.h>
#include <vgui/ISurface.h>
#include "ihudlcd.h"

#include "hud_weapon_bars.h"


// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"


using namespace vgui;
extern ConVar iHud_Mini_Mode;

//-----------------------------------------------------------------------------
// Purpose: Displays current ammunition level
//-----------------------------------------------------------------------------
class CHudAmmo : public CHudNumericDisplay, public CHudElement
{
	DECLARE_CLASS_SIMPLE( CHudAmmo, CHudNumericDisplay );

public:
	CHudAmmo( const char *pElementName );
	void Init( void );
	void VidInit( void );
	void Reset();
	bool Compare(char* value_1, char* value_2);

	void SetAmmo(int ammo, bool playAnimation, bool Is_Battery, bool Is_Revolver);
	void SetAmmo2(int ammo2, bool playAnimation);
	virtual void Paint( void );
	bool Mini_Mode = false;
	bool Right_Sided = false;
	bool Top_Sided = false;
	bool Reset_Settings = false;

protected:
	virtual void OnThink();
	void UpdatePlayerAmmo( C_BasePlayer *player );
	void UpdateVehicleAmmo( C_BasePlayer *player, IClientVehicle *pVehicle );
	
private:
	CHandle< C_BaseCombatWeapon > m_hCurrentActiveWeapon;
	CHandle< C_BaseEntity > m_hCurrentVehicle;
	int		m_iAmmo;
	int		m_iAmmo2;
	int     m_iFull_Clip;
	CHudTexture *m_iconPrimaryAmmo;
	CHud_Weapon_Bar *Weapon_Bar;
};

DECLARE_HUDELEMENT( CHudAmmo );

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
// CAUTION; The Size of the Label is not only set by "HudAmmo" in ./Scripts/Hudlayout.res
// The Animations re-adjust Size and Position by weapon type for each weapon, all values specified in ./Scripts/HudAnimations.res !!
CHudAmmo::CHudAmmo( const char *pElementName ) : BaseClass(NULL, "HudAmmo"), CHudElement( pElementName )
{	

	SetPaintBackgroundEnabled(false);
	SetHiddenBits(HIDEHUD_HEALTH | HIDEHUD_PLAYERDEAD | HIDEHUD_WEAPONSELECTION);  //| HIDEHUD_NEEDSUIT );

	hudlcd->SetGlobalStat( "(ammo_primary)", "0" );
	hudlcd->SetGlobalStat( "(ammo_secondary)", "0" );
	hudlcd->SetGlobalStat( "(weapon_print_name)", "" );
	hudlcd->SetGlobalStat( "(weapon_name)", "" );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudAmmo::Init( void )
{
	m_iAmmo		= -1;
	m_iAmmo2	= -1;
	
	m_iconPrimaryAmmo = NULL;
	Reset_Settings = true;

	/* // Disabled the "Ammo" text for Mod
	wchar_t *tempString = g_pVGuiLocalize->Find("#Valve_Hud_AMMO");
	if (tempString)
	{
		SetLabelText(tempString);
	}
	else
	{
		SetLabelText(L"AMMO");
	}
	*/
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudAmmo::VidInit( void )
{
}

//-----------------------------------------------------------------------------
// Purpose: Resets hud after save/restore
//-----------------------------------------------------------------------------
void CHudAmmo::Reset()
{
	BaseClass::Reset();

	m_hCurrentActiveWeapon = NULL;
	m_hCurrentVehicle = NULL;
	m_iAmmo = 0;
	m_iAmmo2 = 0;
	Reset_Settings = true;

	OnThink();
}

//-----------------------------------------------------------------------------
// Purpose: called every frame to get ammo info from the weapon
//-----------------------------------------------------------------------------
void CHudAmmo::UpdatePlayerAmmo( C_BasePlayer *player )
{
	// Clear out the vehicle entity
	m_hCurrentVehicle = NULL;

	C_BaseCombatWeapon *wpn = GetActiveWeapon();

	hudlcd->SetGlobalStat( "(weapon_print_name)", wpn ? wpn->GetPrintName() : " " );
	hudlcd->SetGlobalStat( "(weapon_name)", wpn ? wpn->GetName() : " " );

	if ( !wpn || !player || !wpn->UsesPrimaryAmmo() )
	{
		hudlcd->SetGlobalStat( "(ammo_primary)", "n/a" );
        hudlcd->SetGlobalStat( "(ammo_secondary)", "n/a" );

		SetPaintEnabled(false);
		// SetPaintBackgroundEnabled(false);
		return;
	}

	SetPaintEnabled(true);
	// SetPaintBackgroundEnabled(true);

	// Get our icons for the ammo types
	m_iconPrimaryAmmo = gWR.GetAmmoIconFromWeapon( wpn->GetPrimaryAmmoType() );
	// get the ammo in our clip
	m_iFull_Clip = wpn->GetMaxClip1(); // wpn->GetMaxClip1(); wpn->GetDefaultClip1();	
	int ammo1 = wpn->Clip1();
	int ammo2;
	if (ammo1 < 0)
	{
		// we don't use clip ammo, just use the total ammo count
		ammo1 = player->GetAmmoCount(wpn->GetPrimaryAmmoType());
		ammo2 = 0;
	}
	else
	{
		// we use clip ammo, so the second ammo is the total ammo
		ammo2 = player->GetAmmoCount(wpn->GetPrimaryAmmoType());
	}

	hudlcd->SetGlobalStat( "(ammo_primary)", VarArgs( "%d", ammo1 ) );
	hudlcd->SetGlobalStat( "(ammo_secondary)", VarArgs( "%d", ammo2 ) );

				
	
		if (Compare((char*)wpn->GetWpnData().szAmmo1, "battery"))  { SetAmmo(ammo1, true, true, false); }
		else if (Compare((char*)wpn->GetWpnData().szAmmo1, "357")) // Only for weapons with Revolver Type ammo
		{	// It loads position from ./Scripts/HudAnimations.res, which overwrites ./Scripts/HudLayout.res
			
			/*
			if (!Mini_Mode)
			{   // if (Right_Sided != Weapon_Bar->Get_Right_Sided()) // Only when value switches 			
				if (Right_Sided)
				{ g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("Revolver_Top_Right"); }
				else { g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("Revolver_Top_Left"); }
													
				SetDisplayValue(ammo2); 
			}		
			else { SetAmmo(ammo2, true, false, true); }
			*/
			// this->SetPos(ScreenWidth() - 73, 87);

			// Only show if under 85% of a clip
			// if (ammo2 * (100 / m_iFull_Clip) < 86) { SetAmmo(ammo2, true, false, true); }
			SetAmmo(ammo2, true, false, true);
			
		} 
		else
		{   // same weapon, just update counts	
			// JL Imperial; I swapped ammo1 and ammo2 names, because that looks better. And disabled value 2 for our Mod.	
			SetAmmo(ammo2, true, false, false); 
			// SetAmmo2(ammo1, true);				
		}	
	
	
	if (wpn != m_hCurrentActiveWeapon)
	{
		// diferent weapon, change without triggering animation
		// SetAmmo(ammo2, true, false, false); // This distorts position for innitial setup of Revolver/Battery..
		// So we better wait for the next cycle to set this.

		// SetAmmo2(ammo1, false);

		// JL Imperial; Animations re-adjust Size and Position by weapon type for each weapon, all values specified in ./Scripts/HudAnimations.res !!		
		/*
		if (wpn->UsesClipsForAmmo1())
		{
			SetShouldDisplaySecondaryValue(false);
			g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("WeaponUsesClips");
		}
		else
		{
			g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("WeaponDoesNotUseClips");
			SetShouldDisplaySecondaryValue(false);
		}

		g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("WeaponChanged");
		*/
		m_hCurrentActiveWeapon = wpn;
	}
}

void CHudAmmo::UpdateVehicleAmmo( C_BasePlayer *player, IClientVehicle *pVehicle )
{
	m_hCurrentActiveWeapon = NULL;
	CBaseEntity *pVehicleEnt = pVehicle->GetVehicleEnt();

	if ( !pVehicleEnt || pVehicle->GetPrimaryAmmoType() < 0 )
	{
		SetPaintEnabled(false);
		// SetPaintBackgroundEnabled(false);
		return;
	}

	SetPaintEnabled(true);
	// SetPaintBackgroundEnabled(true);

	// get the ammo in our clip
	int ammo1 = pVehicle->GetPrimaryAmmoClip();
	int ammo2;
	if (ammo1 < 0)
	{
		// we don't use clip ammo, just use the total ammo count
		ammo1 = pVehicle->GetPrimaryAmmoCount();
		ammo2 = 0;
	}
	else
	{
		// we use clip ammo, so the second ammo is the total ammo
		ammo2 = pVehicle->GetPrimaryAmmoCount();
	}

	// update whether or not we show the total ammo display
	if (ammo2 == 0 || ammo1 == 0 && ammo2 == 0) { SetShouldDisplayValue(false); }
	else { SetShouldDisplayValue(true); }

	if (pVehicleEnt == m_hCurrentVehicle)
	{	
		// same weapon, just update counts
		SetAmmo(ammo2, true, false, false);
		// SetAmmo2(ammo1, true);
	}
	else
	{
		// diferent weapon, change without triggering
		SetAmmo(ammo2, true, false, false);
		// SetAmmo2(ammo1, true);

		// The Animations re-adjust Size and Position by weapon type for each weapon, all values specified in ./Scripts/HudAnimations.res !!
		if (pVehicle->PrimaryAmmoUsesClips())
		{
			SetShouldDisplaySecondaryValue(false);
			g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("WeaponUsesClips");
		}
		else
		{
			g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("WeaponDoesNotUseClips");
			SetShouldDisplaySecondaryValue(false);
		}

		g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("WeaponChanged");
		m_hCurrentVehicle = pVehicleEnt;
	}
}

//-----------------------------------------------------------------------------
// Purpose: called every frame to get ammo info from the weapon
//-----------------------------------------------------------------------------
void CHudAmmo::OnThink()
{	
	Mini_Mode = Weapon_Bar->Get_Mini_Mode();
	Right_Sided = Weapon_Bar->Get_Right_Sided();
	Top_Sided = Weapon_Bar->Get_Top_Sided();


	C_BasePlayer *player = C_BasePlayer::GetLocalPlayer();
	IClientVehicle *pVehicle = player ? player->GetVehicle() : NULL;

	if (!pVehicle)
	{
		UpdatePlayerAmmo(player);
	}
	else
	{
		UpdateVehicleAmmo(player, pVehicle);
	}
}

//-----------------------------------------------------------------------------
// Purpose: Updates ammo display
//-----------------------------------------------------------------------------
void CHudAmmo::SetAmmo(int ammo, bool playAnimation, bool Is_Battery, bool Is_Revolver)
{  // The Animations re-adjust Size and Position by weapon type for each weapon, all values specified in ./Scripts/HudAnimations.res !!
	if (ammo != m_iAmmo)
	{   // ===============  Applying Color Changer Animations =============== Modified by Imperial	

		CHud_Weapon_Bar *Weapon_Bar;

		if (Reset_Settings || Weapon_Bar->Get_Changes())
		{
			int Set_X = 111;
			int Set_Y = 46; // Set directly from this c++ file	

			// ===================== Mini Mode =====================	
			if (Mini_Mode)
			{
				if (Right_Sided)
				{	// Is_Revolver always ignored because it shows numbers beside of its Drumm				
					if (Is_Battery)
					{
						if (ammo < 100) { Set_X = ScreenWidth() - 45; }
						else { Set_X = ScreenWidth() - 39; }
					}
					else { Set_X = ScreenWidth() - 34; } // All other weapons
				}
				else if (!Right_Sided)
				{
					if (Is_Battery)
					{
						if (ammo < 100) { Set_X = 99; }
						else { Set_X = 105; }
					}
				}

				if (Top_Sided && Is_Battery) { Set_Y = 46; }
				else if (!Top_Sided){ Set_Y = ScreenHeight() - 68; }

				SetPos(Set_X, Set_Y);
			}
			// ==================================================
			else if (!Mini_Mode)
			{
				Set_X = 10;
				Set_Y = 86;

				if (Right_Sided)
				{
					if (Is_Revolver) { Set_X = ScreenWidth() - 74; }
					else if (Is_Battery)
					{
						if (ammo < 100) { Set_X = ScreenWidth() - 45; }
						else { Set_X = ScreenWidth() - 39; }
					}
					else { Set_X = ScreenWidth() - 130; } // All other weapons
				}
				else if (!Right_Sided)
				{
					if (Is_Revolver) { Set_X = 74; }
					else if (Is_Battery)
					{
						if (ammo < 100) { Set_X = 99; }
						else { Set_X = 105; }
					}
				}
				if (Top_Sided) { if (Is_Battery) { Set_Y = 106; } }
				else if (!Top_Sided)
				{
					if (Is_Battery) { Set_Y = ScreenHeight() - 69; }
					else { Set_Y = ScreenHeight() - 88; }
				}

				SetPos(Set_X, Set_Y);
			}

			// Reset_Settings = false;
		}
	
			  

		char* Animation_Type = "AmmoDecreased"; // "AmmoIncreased";
		if (ammo * (100 / m_iFull_Clip) < 34) { Animation_Type = "AmmoEmpty"; }	// If less ammo left then 1/3 display in Red	
		

		// Applying Animation_Type; Disabled all anims because they distort size for different Aspect Ratio..
		g_pClientMode->GetViewportAnimationController()->StartAnimationSequence(Animation_Type);


		// update whether or not we show the total ammo display, this must not interfere with "SetAmmo(ammo1, true, false, false);" above
		if (ammo == 0) { SetShouldDisplayValue(false); }
		else
		{   if (Is_Battery)		
			{   C_BaseCombatWeapon *wpn = GetActiveWeapon();
							
				if (Compare((char*)wpn->GetWpnData().szAmmo2, "Percent") && ammo * (100 / m_iFull_Clip) > 76) // Percentage				
				{ SetShouldDisplayValue(false); }
				else 
				{   SetShouldDisplayValue(true);
					if (ammo < 100) { SetLabelText((const wchar_t *)"%"); }
					else { SetLabelText((const wchar_t *)""); }
				}			
			}
			else 
			{   SetShouldDisplayValue(true); 
				// Removing % sign of Battery type ammo for all other weapon types
				SetLabelText((const wchar_t *)""); 
			}
		}

	
		/* // Stuff from the hudanimations.txt file was replaced by c++ entries because doesen't always distort ingame
		if (Mini_Mode)
		{
			if (Right_Sided)
			{   
				if (!Is_Battery && !Is_Revolver) { Animation_Type = "Mini_Mode_Top_Right_1"; }
				else if (Is_Battery) { Animation_Type = "Mini_Mode_Top_Right_2"; }			
			}
			else
			{   
				if (!Is_Battery && !Is_Revolver) { Animation_Type = "Mini_Mode_Top_Left_1"; }
				else if (Is_Battery) { Animation_Type = "Mini_Mode_Top_Left_2"; }				
			}
			
		    g_pClientMode->GetViewportAnimationController()->StartAnimationSequence(Animation_Type);



			// Doesent work due to timing issues!:
			//if (m_iFull_Clip - ammo != 0) { SetShouldDisplayValue(true); } // If anything was fired
			//else { SetShouldDisplayValue(false); }

			// i_Ammo_Bar_Size = 128, Processing this depends on bar size from hud_weapon_bar.cpp
			// float value = (((128 / m_iFull_Clip) * ammo) / 128);

			// if (Is_Battery || Is_Revolver || value < (float) 0.8) { SetShouldDisplayValue(true); }
			// else { SetShouldDisplayValue(false); }
		}
		else if (!Mini_Mode && !Is_Battery && !Is_Revolver)
		{ 
			if (Right_Sided) { g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("Standard_Top_Right"); }
			else { g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("Standard_Top_Left"); }
		}
		*/
			
		m_iAmmo = ammo;
	}

	SetDisplayValue(ammo);
}

//-----------------------------------------------------------------------------
// Purpose: Updates 2nd ammo display
//-----------------------------------------------------------------------------
void CHudAmmo::SetAmmo2(int ammo2, bool playAnimation)
{
	if (ammo2 != m_iAmmo2)
	{
		if (ammo2 == 0)
		{
			g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("Ammo2Empty");
		}
		else if (ammo2 < m_iAmmo2)
		{
			// ammo has decreased
			g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("Ammo2Decreased");
		}
		else
		{
			// ammunition has increased
			g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("Ammo2Increased");
		}

		m_iAmmo2 = ammo2;
	}

	SetSecondaryValue(ammo2);
}

//-----------------------------------------------------------------------------
// Purpose: We add an icon into the 
//-----------------------------------------------------------------------------
void CHudAmmo::Paint( void )
{
	// BaseClass::SetPaintBackgroundEnabled(false);

	// Disabled for Mod
	BaseClass::Paint();

	/* 
#ifndef HL2MP
	if ( m_hCurrentVehicle == NULL && m_iconPrimaryAmmo )
	{
		int nLabelHeight;
		int nLabelWidth;
		surface()->GetTextSize( m_hTextFont, m_LabelText, nLabelWidth, nLabelHeight );

		// Figure out where we're going to put this
		int x = text_xpos + ( nLabelWidth - m_iconPrimaryAmmo->Width() ) / 2;
		int y = text_ypos - ( nLabelHeight + ( m_iconPrimaryAmmo->Height() / 2 ) );
		
		m_iconPrimaryAmmo->DrawSelf( x, y, GetFgColor() );
	}
#endif // HL2MP
	*/
}


//-----------------------------------------------------------------------------
// Purpose: String Comparsion
//-----------------------------------------------------------------------------
bool CHudAmmo::Compare(char* value_1, char* value_2)
{
	if (strcmp(value_1, value_2) == 0) { return true; }
	return false;
}


//-----------------------------------------------------------------------------
// Purpose: Displays the secondary ammunition level
//-----------------------------------------------------------------------------
class CHudSecondaryAmmo : public CHudNumericDisplay, public CHudElement
{
	DECLARE_CLASS_SIMPLE( CHudSecondaryAmmo, CHudNumericDisplay );

public:
	CHudSecondaryAmmo( const char *pElementName ) : BaseClass( NULL, "HudAmmoSecondary" ), CHudElement( pElementName )
	{
		m_iAmmo = -1;
		SetPaintBackgroundEnabled(false);

		SetHiddenBits(HIDEHUD_HEALTH | HIDEHUD_WEAPONSELECTION | HIDEHUD_PLAYERDEAD); // | HIDEHUD_NEEDSUIT );
	}

	void Init( void )
	{
		/*
#ifndef HL2MP
		wchar_t *tempString = g_pVGuiLocalize->Find("#Valve_Hud_AMMO_ALT");
		if (tempString)
		{
			SetLabelText(tempString);
		}
		else
		{
			SetLabelText(L"ALT");
		}
#endif // HL2MP
	   */

	}

	void VidInit( void )
	{
	}

	// The Animations re-adjust Size and Position by weapon type for each weapon, all values specified in ./Scripts/HudAnimations.res !!
	void SetAmmo( int ammo )
	{
		if (ammo != m_iAmmo)
		{
			if (ammo == 0)
			{
				g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("AmmoSecondaryEmpty");
			}
			else if (ammo < m_iAmmo)
			{
				// ammo has decreased
				g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("AmmoSecondaryDecreased");
			}
			else
			{
				// ammunition has increased
				g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("AmmoSecondaryIncreased");
			}

			m_iAmmo = ammo;
		}
		SetDisplayValue( ammo );
	}

	void Reset()
	{
		// hud reset, update ammo state
		BaseClass::Reset();
		m_iAmmo = 0;
		m_hCurrentActiveWeapon = NULL;
		SetAlpha( 0 );
		UpdateAmmoState();
	}

	virtual void Paint( void )
	{
		BaseClass::Paint();

		/*
#ifndef HL2MP
		if ( m_iconSecondaryAmmo )
		{
			int nLabelHeight;
			int nLabelWidth;
			surface()->GetTextSize( m_hTextFont, m_LabelText, nLabelWidth, nLabelHeight );

			// Figure out where we're going to put this
			int x = text_xpos + ( nLabelWidth - m_iconSecondaryAmmo->Width() ) / 2;
			int y = text_ypos - ( nLabelHeight + ( m_iconSecondaryAmmo->Height() / 2 ) );

			m_iconSecondaryAmmo->DrawSelf( x, y, GetFgColor() );
		}
#endif // HL2MP
		*/

	}

protected:

	virtual void OnThink()
	{
		// set whether or not the panel draws based on if we have a weapon that supports secondary ammo
		C_BaseCombatWeapon *wpn = GetActiveWeapon();
		C_BasePlayer *player = C_BasePlayer::GetLocalPlayer();
		IClientVehicle *pVehicle = player ? player->GetVehicle() : NULL;
		if (!wpn || !player || pVehicle)
		{
			m_hCurrentActiveWeapon = NULL;
			SetPaintEnabled(false);
			// SetPaintBackgroundEnabled(false);
			return;
		}
		else
		{
			SetPaintEnabled(true);
			// SetPaintBackgroundEnabled(true);
		}

		UpdateAmmoState();
	}

	void UpdateAmmoState()
	{
		C_BaseCombatWeapon *wpn = GetActiveWeapon();
		C_BasePlayer *player = C_BasePlayer::GetLocalPlayer();

		if (player && wpn && wpn->UsesSecondaryAmmo())
		{   
			SetAmmo(player->GetAmmoCount(wpn->GetSecondaryAmmoType()));
			// Imperial; Disabled Here!
			if (player->GetAmmoCount(wpn->GetSecondaryAmmoType())) { SetShouldDisplayValue(false); }
			else // update whether or not we show the total ammo display			
			{ SetShouldDisplayValue(false); }
		}
		
			
		if ( m_hCurrentActiveWeapon != wpn )
		{
			if ( wpn->UsesSecondaryAmmo() )
			{
				// we've changed to a weapon that uses secondary ammo
				g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("WeaponUsesSecondaryAmmo");
			}
			else 
			{
				// we've changed away from a weapon that uses secondary ammo
				g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("WeaponDoesNotUseSecondaryAmmo");
			}
			m_hCurrentActiveWeapon = wpn;
			
			// Get the icon we should be displaying
			// m_iconSecondaryAmmo = gWR.GetAmmoIconFromWeapon( m_hCurrentActiveWeapon->GetSecondaryAmmoType() );
		}
	}
	
private:
	CHandle< C_BaseCombatWeapon > m_hCurrentActiveWeapon;
	CHudTexture *m_iconSecondaryAmmo;
	int		m_iAmmo;
};

DECLARE_HUDELEMENT( CHudSecondaryAmmo );

