#include "cbase.h"
#include "weapon_selection.h"
#include "iclientmode.h"
#include "history_resource.h"
#include "input.h"

#include "convar.h"
#include "hud_weapon_bars.h"


#define SELECTION_TIMEOUT_THRESHOLD		1.0f	// Seconds
#define SELECTION_FADEOUT_TIME			0.4f


// Switches, defined by the user settings
// Mini_Mode, Top_Sided, Right_Sided, Show_Background, Show_Selector and Invert_Selection are obtained from "hud_weapon_bar.h" by getters
// And iHud_Show_Image_Behind in Hud_Status_Bars.cpp is important too
static ConVar iHud_Show_Dock("iHud_Show_Dock", "1", FCVAR_ARCHIVE, "Renders Weapon Selection as Dock (if not in Mini_Mode)."); // , CHudJLWeaponSelection::Callback_Change_Setting);
static ConVar iHud_Maximum_Tiles("iHud_Maximum_Tiles", "1", FCVAR_ARCHIVE, "If 0 it uses 8 weapon tiles. Otherwise 8-12 tiles, depending on Resolution.");
static ConVar iHud_Fade_Away("iHud_Fade_Away", "0", FCVAR_ARCHIVE, "Should the Weapon Selection fade away?");


//-----------------------------------------------------------------------------
// Purpose: JL weapon selection hud element
//-----------------------------------------------------------------------------
class CHudJLWeaponSelection : public CBaseHudWeaponSelection, public vgui::Panel
{
	DECLARE_CLASS_SIMPLE(CHudJLWeaponSelection, vgui::Panel);

//-----------------
public:
//-----------------
	CHudJLWeaponSelection(const char *pElementName);

	int		i_Texture_Slot;
	int		i_Texture_Selected;
	int     i_Item_Icon_1, i_Item_Icon_2, i_Item_Icon_3, i_Item_Icon_4;
	int		i_Item_Icon_5, i_Item_Icon_6, i_Item_Icon_7, i_Item_Icon_8;

	int     i_Item_Icon_9, i_Item_Icon_10, i_Item_Icon_11, i_Item_Icon_12;
	int     i_Item_Icon_13, i_Item_Icon_14, i_Item_Icon_15, i_Item_Icon_16;

	int Color_1R, Color_1G, Color_1B, Color_1Alpha = 255;
	int Color_2R, Color_2G, Color_2B, Color_2Alpha = 255;
	int Color_Dep_R, Color_Dep_G, Color_Dep_B, Color_Dep_A = 255;
	int Color_TexR, Color_TexG, Color_TexB, Color_TexAlpha = 255;

	
    int StartY = 100; // From top
    int EndY = 0; // From bottom
	int Selected = 0;
	int xpos, ypos;
	int Mini_Ypos = ypos;
	float sizeX, sizeY;
	float	m_fLastChange;
	int height = ScreenHeight() - EndY - StartY;
	int Item_Count = 0;
	int Last_Weapon_Slot = 0;
	int Weapon_Slots = 8; // Amount of Tile slots to be rendered!
	int Slot_Shift = 0;

	// Causes Crash on every 2nd Savegame Load!
	// ConVar* Hud_Mini_Mode = nullptr;

	bool Has_Icon = false;
	bool Reset_Settings = false;

	// Switches, defined by the user settings
	bool   Mini_Mode = false;
	bool   Invert_Selection = false;

	bool   Top_Sided = false;
	bool   Right_Sided = false;
	bool   Show_Background = false;
	bool   Show_Selector = false;

	bool   Invert_Scroll = false;
	bool   Fade_Away = false;
	// bool   Show_Dock = false;

	char* s_Selection = null;
	char* Temp_A;
	char Buffer[512]; 
	
	int Get_Slot_Position();
	bool Compare(char* value_1, char* value_2);
	bool Starts_With(char* value_1, char* value_2);

	// prefix to trim, source array
	void Trimm_Prefix(char* value_1, char* value_2);
	// source array, suffix to trim
	void Trimm_Suffix(char* value_1, char* value_2);

	void Draw_Text(int x, int y, char* text, int R, int G, int B, int Alpha);
	void Draw_Text(int x, int y, char* text, Color Color_Value = Color(255, 255, 255, 255)); // Defaults as White

	void Draw_Number(int x, int y, int value, int R, int G, int B, int Alpha);

	int Get_Texture_ID(int i);
	char* Get_Selection() { return s_Selection; }


	ImageProgressBar *Medkit_Bar;

	//====== Text Labels ======
	vgui::HFont hFont;
	vgui::HFont hFont2;
	int			charWidth;
	int			charWidth2;
	wchar_t		unicode[80];
	//=========================

	Color Color_1;
	Color Color_2;
	Color Color_Depleted;
	Color Color_Filter;


	void Reseting_Settings();
	virtual void LevelInit();
	
	virtual bool ShouldDraw();
	virtual void OnWeaponPickup(C_BaseCombatWeapon *pWeapon);

	virtual void Cycle_Up(void);
	virtual void Cycle_Down(void);

	virtual void CycleToNextWeapon(void);
	virtual void CycleToPrevWeapon(void);

	virtual void OpenSelection(void);
	virtual void HideSelection(void);

	virtual void SelectSlot(int iSlot) { SwitchWeaponSlot(iSlot - 1); }

	virtual C_BaseCombatWeapon *GetSelectedWeapon(void) { return m_hSelectedWeapon; }

	C_BaseCombatWeapon *GetWeapon(int slot)
	{   C_BasePlayer *pPlayer = C_BasePlayer::GetLocalPlayer();
		if (!pPlayer) { return NULL; }
		return pPlayer->GetWeapon(slot);
	}

	void SwitchWeaponSlot(int slot)
	{   C_BaseCombatWeapon *weapon = GetWeapon(slot);
		if (weapon)
		{   
			::input->MakeWeaponSelection(weapon);
			/*if (weapon->GetWpnData().bIsItem == false) { ::input->MakeWeaponSelection(weapon); }
			else if (IsInSelectionMode())
			{   char temp2[32];
				Q_snprintf(temp2, sizeof(temp2), "primaryattack %d", slot + 1);
				engine->ClientCmd(temp2);
			} else { OpenSelection(); }*/
		}
	}


	char* Get_Name(C_BaseCombatWeapon * Weapon);

	// void Reset_UI() { Reset_Settings = true; };
	static void Callback_Change_Setting(IConVar *var, const char *pOldValue, float flOldValue)
	{   Msg("ConVar %s was changed from %s to %s\n", var->GetName(), pOldValue, ((ConVar*)var)->GetString());
		// Doesen't work..
		/*CHudJLWeaponSelection * Selection;
		Selection->Reseting_Settings();
		Selection->Reset_Settings = true;
		Selection->Reset_UI();	*/
	};


//-----------------
protected:
//-----------------

	virtual void Init(void);
	virtual void OnThink();
	virtual void Paint();
	virtual void ApplySchemeSettings(vgui::IScheme *pScheme);

	virtual void Draw_Icon(int texture);
	virtual void Draw_Background(int texture);

	virtual bool IsWeaponSelectable()
	{   if (IsInSelectionMode()) { return true; }
		return false;
	}

	virtual void SetWeaponSelected()
	{   if (m_hSelectedWeapon) // && m_hSelectedWeapon->GetWpnData().bIsItem == false)
		{ CBaseHudWeaponSelection::SetWeaponSelected(); }		
	}


//-----------------
private:
//-----------------

	C_BaseCombatWeapon *FindNextWeaponInWeaponSelection(int iCurrentSlot, int iCurrentPosition);
	C_BaseCombatWeapon *FindPrevWeaponInWeaponSelection(int iCurrentSlot, int iCurrentPosition);

	virtual C_BaseCombatWeapon *GetWeaponInSlot(int iSlot, int iSlotPos) { return NULL; }

	virtual void SelectWeaponSlot(int iSlot) { m_hSelectedWeapon = GetWeapon(iSlot - 1); }

	void FastWeaponSwitch(int iWeaponSlot);

	virtual	void SetSelectedWeapon(C_BaseCombatWeapon *pWeapon) { m_hSelectedWeapon = pWeapon; }

};


