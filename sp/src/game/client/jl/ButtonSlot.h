//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: Creates a HTML control
//
// $NoKeywords: $
//=============================================================================//

#ifndef BUTTONSLOT_H
#define BUTTONSLOT_H

#include "cbase.h"
#include <vgui_controls/Button.h>
#include "cdll_util.h" // Get ScreenHeight();


class C_BaseCombatWeapon;

namespace vgui
{

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
class ButtonSlot : public Button
{
	DECLARE_CLASS_SIMPLE( ButtonSlot, Button );

public:
	// You can optionally pass in the panel to send the click message to and the name of the command to send to that panel.
	ButtonSlot(Panel *parent, const char *panelName, const char *text, Panel *pActionSignalTarget=NULL, const char *pCmd=NULL);
	ButtonSlot(Panel *parent, const char *panelName, const wchar_t *text, Panel *pActionSignalTarget=NULL, const char *pCmd=NULL);

	bool LinkSlot(int slot, bool playerWeapon = true, bool Hide_Slot = false);
	bool Give_Item(ButtonSlot * Other_Slot);

	int GetSlot();
	int Last_Delta = 0;
	int Mouse_Wheeled_Slots = 0;
	int Current_Mouse_Wheel = 0;

	int Object_Scrollbar_Size = 0;
	void Set_Wheel(int i = 0) { Mouse_Wheeled_Slots = i; };
	int Get_Wheel_Delta(void) { return Mouse_Wheeled_Slots; };
	int Current_Wheel_Value(void) { return Current_Mouse_Wheel; };

	bool IsPlayerWeapon();	
	C_BaseCombatWeapon* Get_Weapon_Slot() const;

	virtual void ApplySettings( KeyValues *inResourceData );
	virtual void ApplySchemeSettings(IScheme *pScheme);

	virtual void Paint(void);

	virtual bool IsDragEnabled() const;
	virtual bool IsDroppable( CUtlVector< KeyValues * >& msglist );

	virtual void OnPanelDropped( CUtlVector< KeyValues * >& msglist );
	virtual void OnDroppablePanelPaint( CUtlVector< KeyValues * >& msglist, CUtlVector< Panel * >& dragPanels );
	virtual void OnDraggablePanelPaint();


	bool Show_Selector = false;
	bool Show_Background = false;
	bool Invert_Selection = false;
	bool Reset_Settings = false;
	char Buffer[512];
	char * s_Img_Selection;


	int Item_Type = 0;
	char * Image_All = "Hud/Weapons_Hl2/Machinegun";
	char * Image_Map = "Hud/Weapons/Machete";
	char * Image_Note = "Hud/Weapons/Medkit";
	char * Image_Item = "Hud/Weapons/Grenade";
	char * Image_Objective = "Hud/Weapons/Radio";

	void Set_Item_Icon(int Item_Type);
	void Set_Item_Icon_Inverted(int Item_Type);
	

	bool Show_Slot = true;


private:	
	void Init();	
	void Switch(ButtonSlot *slot);

	void OnMouseWheeled(int delta);
	void DisplayDragWeapon();
	void Draw_Icon(int texture);
	void Draw_Large_Icon(int texture);
	void Draw_Preview(int texture);
	int Character_Count(char* value_1);

	//====== Text Labels ======
	vgui::HFont hFont;
	vgui::HFont hFont2;
	int			charWidth;
	int			charWidth2;
	wchar_t		unicode[80];
	void Draw_Text(int x, int y, char* text, int R, int G, int B, int Alpha);
	//=========================

	void Resetting_Settings();
	int Color_1R, Color_1G, Color_1B, Color_1Alpha = 255;
	int Color_2R, Color_2G, Color_2B, Color_2Alpha = 255;
	int Color_Texture_R, Color_Texture_G, Color_Texture_B, Color_Texture_A = 255;

	int StartY = 20; // From top
	int EndY = 80; // From bottom
	int Range = ScreenHeight() - EndY - StartY;

	int Xpos = 0;
	int Ypos = 0; 
	int Width = 0;
	int Height = 0;

	int Texture_Slot = 0; 
	int Texture_Selected = 0;
	int Weapon_Icon = 0;
	int Weapon_Tile = 0;

protected:
	bool m_bPlayerWeapon;
	int m_iSlot;
	int m_iItemType;

	//vgui::IImage* m_pEmptyTexture;
	int m_iEmptyTexture;
	float m_fEmptyTextureUV[4];
};

} // namespace vgui

#endif // BUTTONSLOT_H
