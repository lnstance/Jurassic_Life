#include "hud.h"
#include "cbase.h"

#include "iclientmode.h"
#include "hud_macros.h"
#include "vgui_controls/controls.h"
#include "vgui/ISurface.h"

#include "fmtstr.h"
#include "vgui/ILocalize.h"
#include "hud_weapon_bars.h"
#include "ImageProgressBar.h"
#include "weapon_selection.h"
#include "hud_numericdisplay.h"
#include <vgui_controls/AnimationController.h>

#include "KeyValues.h"
#include <string>
#include "IGameUIFuncs.h"

#include "tier0/memdbgon.h"

using namespace vgui;

DECLARE_HUDELEMENT(CHud_Weapon_Bar);


// ConVar iHud_Mini_Mode("iHud_Mini_Mode", "0", FCVAR_ARCHIVE, "Toggles HUD MiniMode.");


CHud_Weapon_Bar::CHud_Weapon_Bar(const char *pElementName) : CHudElement(pElementName), BaseClass(NULL, "Hud_Weapon_Bars")
{

	Panel *pParent = g_pClientMode->GetViewport();
	SetParent(pParent);

	// SetAlpha(255);
	// SetVisible(true);
	// SetPaintBorderEnabled(false);

	// Texture for i_Weapon_Icon updates at runtime for Active or Inactive state of each weapon
	i_Weapon_Icon = vgui::surface()->CreateNewTextureID();

	i_Texture_Slot = vgui::surface()->CreateNewTextureID();
	vgui::surface()->DrawSetTextureFile(i_Texture_Slot, "HUD/Weapons/Weapon_Bar_Slot", true, false);

	i_Texture_Selected = vgui::surface()->CreateNewTextureID();  // Not Used
	vgui::surface()->DrawSetTextureFile(i_Texture_Selected, "HUD/Weapons/Weapon_Bar_Selected", true, false);

	i_Revolver_Icon = vgui::surface()->CreateNewTextureID();
	i_Revolver_Drumm = vgui::surface()->CreateNewTextureID();

	i_Grenade_Icon = vgui::surface()->CreateNewTextureID();
	vgui::surface()->DrawSetTextureFile(i_Grenade_Icon, "HUD/Weapons/Grenade_Mini", true, false);

	i_Grenade_Icon_Inactive = vgui::surface()->CreateNewTextureID();
	vgui::surface()->DrawSetTextureFile(i_Grenade_Icon_Inactive, "HUD/Weapons/Grenade_Mini_Inactive", true, false);
	
	// This defines size of the Weapon Icon to be same as weaponselection.cpp
	range = ScreenHeight() - EndY - StartY;
	if (range > 9 * 64) { range = 9 * 64; } // == 8*64 = 512
	height = range / (float)9; // = 512 scaled down
	width = height * 2.0; // = 1024 scaled down



	// ================= Defining Ammo Bars  ================= Rendered first because they're under the other one
	if (Get_Right_Sided_Ammo() == -1) { Ammo_Orientation = '0'; }
	else if (Get_Right_Sided_Ammo()) { Ammo_Orientation = 'W'; } // W-est = From Right to Left

	Primary_Ammo_Bar = Create_Status_Bar(Background_Bar, xpos, 64, true, i_Ammo_Bar_Size, 14, Ammo_Orientation, false);
	Secondary_Ammo_Bar = Create_Status_Bar(Background_Bar, xpos, 80, true, i_Ammo_Bar_Size, 14, Ammo_Orientation, false);


	// Swapping orientation because the used ammo will be drawn in tinted blue from the other side
	if (Ammo_Orientation == 'W') { Ammo_Orientation = 'E'; }
	else if (Ammo_Orientation == 'E') { Ammo_Orientation = 'W'; }

	// 0 Means there is no Background Texture, so it stays translucent and we can see the Status Bar under it.
	Primary_Ammo_Bar_Empty = Create_Status_Bar(0, xpos, 64, true, i_Ammo_Bar_Size, 14, Ammo_Orientation, true);
	Secondary_Ammo_Bar_Empty = Create_Status_Bar(0, xpos, 80, true, i_Ammo_Bar_Size, 14, Ammo_Orientation, true);


	Reset_Settings = true;
	Tick = gpGlobals->curtime; // Important or Tick will never Raise over -1


	//====== Text Labels ======
	vgui::HScheme scheme = vgui::scheme()->GetScheme("ClientScheme");
	hFont = vgui::scheme()->GetIScheme(scheme)->GetFont("Mod_Font");
	charWidth = surface()->GetCharacterWidth(hFont, '0');

	hFont2 = vgui::scheme()->GetIScheme(scheme)->GetFont("HudHintTextLarge");
	charWidth2 = surface()->GetCharacterWidth(hFont2, '0');


	SetHiddenBits(HIDEHUD_PLAYERDEAD); // | HIDEHUD_NEEDSUIT);
}



//-----------------------------------------------------------------------------
// Purpose: Getters
//-----------------------------------------------------------------------------

// All of these values are Valve ConVars, and set by the ingame console
bool CHud_Weapon_Bar::Get_Mini_Mode() { return iHud_Mini_Mode.GetBool(); };
bool CHud_Weapon_Bar::Get_Invert_Selection() { return iHud_Invert_Selection.GetBool(); };

bool CHud_Weapon_Bar::Get_Top_Sided() { return iHud_Top_Sided.GetBool(); };
bool CHud_Weapon_Bar::Get_Right_Sided() { return iHud_Right_Sided.GetBool(); };

int  CHud_Weapon_Bar::Get_Right_Sided_Ammo() { return iHud_Right_Sided_Ammo.GetInt(); };
bool CHud_Weapon_Bar::Get_Changes() { return iHud_Show_Changes.GetBool(); };

bool CHud_Weapon_Bar::Get_Show_Background() { return iHud_Show_Background.GetBool(); };
bool CHud_Weapon_Bar::Get_Show_Selector() { return iHud_Show_Selector.GetBool(); };


char* CHud_Weapon_Bar::Get_Color_1_Value() { return (char*)iHud_Color_1.GetString(); };
char* CHud_Weapon_Bar::Get_Color_2_Value() { return (char*)iHud_Color_2.GetString(); };
char* CHud_Weapon_Bar::Get_Color_Filter_Value() { return (char*)iHud_Color_Texture_Filter.GetString(); };
char* CHud_Weapon_Bar::Get_Color_Depleted_Value(){ return (char*)iHud_Color_Depleted.GetString(); };

// They fail to pass the value here, unless it's directly set...
//int CHud_Weapon_Bar::Get_Color_Texture_R() { return Color_Texture_R; };
//int CHud_Weapon_Bar::Get_Color_Texture_G() { return Color_Texture_G; };
//int CHud_Weapon_Bar::Get_Color_Texture_B() { return Color_Texture_B; };
//int CHud_Weapon_Bar::Get_Color_Texture_A() { return Color_Texture_A; };


//-----------------------------------------------------------------------------
// Purpose: hud scheme settings
//-----------------------------------------------------------------------------
void CHud_Weapon_Bar::ApplySchemeSettings(vgui::IScheme *pScheme)
{
	BaseClass::ApplySchemeSettings(pScheme);
	SetPaintBackgroundEnabled(false);
	Reset_Settings = true;
}

//-----------------------------------------------------------------------------
// Purpose: Think handles percentage bars for ammo
//-----------------------------------------------------------------------------
void CHud_Weapon_Bar::OnThink()
{
	C_BasePlayer *Player = C_BasePlayer::GetLocalPlayer();
	C_BaseCombatWeapon *Weapon = GetActiveWeapon();

	bool Show_Ammo_Bar_1 = false;
	bool Show_Ammo_Bar_2 = false;

	if (Player)
	{   if (Weapon) //Get Ammo of player	
		{ 		

			b_Fuse_Ammo_Bars = Weapon->GetWpnData().bFuse_Ammo_Bars;	
			
			if (Weapon->HasPrimaryAmmo() && Weapon->GetWpnData().iItemType != 6) // Revolvers use a own UI!
			{
				i_Ammo_1 = Weapon->Clip1(); // Loaded Ammo
			    // i_Ammo = Player->GetAmmoCount(Weapon->m_iPrimaryAmmoType); // Remaining Ammo
				i_Clip_Size_1 = Weapon->GetWpnData().iMaxClip1;
			
				// && !Compare((char*)Weapon->GetWpnData().s_Ammo_Bar_1, "-1"))
				if (Character_Count((char*)Weapon->GetWpnData().s_Ammo_Bar_1) > 2)
				{   Primary_Ammo_Bar->SetTopTexture((char*)Weapon->GetWpnData().s_Ammo_Bar_1); Show_Ammo_Bar_1 = true;
					Primary_Ammo_Bar_Empty->SetTopTexture((char*)Weapon->GetWpnData().s_Ammo_Bar_1); 
				}
				// Showing just the empty Background bar as Backdrop for Ammo numbers.
				else if (Mini_Mode){ Primary_Ammo_Bar->SetProgress(0); Primary_Ammo_Bar_Empty->SetProgress(0); Show_Ammo_Bar_1 = true; }
														

				// if (Character_Count((char*)Weapon->GetWpnData().szAmmo1) > 2) 
				if (!Mini_Mode && Compare((char*)Weapon->GetWpnData().szAmmo1, "-1")) { Show_Ammo_Bar_1 = false; }
					

				if (Compare((char*)Weapon->GetWpnData().szAmmo1, "grenade"))
				{   i_Clip_Size_1 = 5; // Grenades are a bit different
					
					if (Player->GetAmmoCount(Weapon->GetPrimaryAmmoType())) // Preventing crash
					{ i_Ammo_1 = Player->GetAmmoCount(Weapon->GetPrimaryAmmoType()); }
				}					
				// Used for JL Crattleprod, Radio and Flashlight, "Percent" replaces the 2 batteries in the bar with first ammo bar
				else if (Compare((char*)Weapon->GetWpnData().szAmmo1, "battery") && !Compare((char*)Weapon->GetWpnData().szAmmo2, "Percent"))
				{	
					i_Clip_Size_1 = 3; // 2 Batteries and the 3rd slot is used for percentage info		
					if (i_Ammo_1 != 0 && i_Ammo_1 < 51) { i_Ammo_1 = 1; }
					else if (i_Ammo_1 > 50) { i_Ammo_1 = 2; }							
				} 
						
								
				// If progress value was set to 0 above we want to keep that for weapons with no specified ammo bars
				if (Character_Count((char*)Weapon->GetWpnData().s_Ammo_Bar_1) > 2)
				{
					if (b_Fuse_Ammo_Bars) { Primary_Ammo_Bar->SetSize(i_Ammo_Bar_Size, 30); 
					Primary_Ammo_Bar_Empty->SetSize(i_Ammo_Bar_Size, 30); } // Double size, we fuse both status bar slots!
					else { Primary_Ammo_Bar->SetSize(i_Ammo_Bar_Size, 14); Primary_Ammo_Bar_Empty->SetSize(i_Ammo_Bar_Size, 14); } // Back to normal size
					

					/*if (Compare((char*)Weapon->GetWpnData().szAmmo2, "Medkit"))
					{ Primary_Ammo_Bar->SetProgress((float)((100 / i_Clip_Size_1) * i_Ammo_1) / 100); }
					else if (Compare(Get_Name(Weapon), "Pulse Rifle"))  
					{
						Primary_Ammo_Bar->SetProgress((float)((128 / 30) * i_Ammo_1) / 128);		
					}
					else if (Compare(Get_Name(Weapon), "SMG"))
					{
						Primary_Ammo_Bar->SetProgress((float)((100 / i_Clip_Size_1) * i_Ammo_1) / 100);
					}*/

					/*if (Compare((char*)Weapon->GetWpnData().szAmmo2, "Percent"))
					{
						Primary_Ammo_Bar->SetProgress((float)((100 / i_Clip_Size_1) * i_Ammo_1) / 100);
					}
					else */
				

					/*if (Compare((char*)Weapon->GetWpnData().szAmmo2, "Medkit"))
					{
						int Started_Medpack = 0;
						if (Weapon->GetWpnData().iMaxClip2 > 0) { Started_Medpack = 1; }

						Primary_Ammo_Bar->SetProgress((float)((100 / i_Clip_Size_1) * i_Ammo_1 + Started_Medpack) / 100);
					}*/


					// * i_Ammo multiplicates by remaining rounds that should render, / i_Ammo_Bar_Size because its supposed to be float 0-1;
					float Value = (float)((i_Ammo_Bar_Size / i_Clip_Size_1) * i_Ammo_1) / i_Ammo_Bar_Size;
					// Flashlight, Tazer, Radio, Medkit
					if (i_Clip_Size_1 > 20) { Value = (float)((100 / i_Clip_Size_1) * i_Ammo_1) / 100; }
					
					Primary_Ammo_Bar->SetProgress(Value);
					Primary_Ammo_Bar_Empty->SetProgress(1.0f - Value);				
				}
			}
					
			//-----------------------------------------------------------------------------
			// Secondary Ammo
			//-----------------------------------------------------------------------------	
			// If supposed to fuse both bar slots and not overwritten by the bar above and space for this bar is not already engaged	
			if (b_Fuse_Ammo_Bars && Character_Count((char*)Weapon->GetWpnData().s_Ammo_Bar_1) < 3) // Nothing
			{  
				if (Character_Count((char*)Weapon->GetWpnData().s_Ammo_Bar_2) > 2) // Anything
				{
					Show_Ammo_Bar_1 = true;					
					Primary_Ammo_Bar->SetSize(i_Ammo_Bar_Size, 30);
					Primary_Ammo_Bar_Empty->SetSize(i_Ammo_Bar_Size, 30);

					// We still work with primary ammo bar because it has the correct position...
					Primary_Ammo_Bar->SetTopTexture((char*)Weapon->GetWpnData().s_Ammo_Bar_2);
					Primary_Ammo_Bar_Empty->SetTopTexture((char*)Weapon->GetWpnData().s_Ammo_Bar_2);

					float Value = (float)((i_Ammo_Bar_Size / i_Clip_Size_1) * i_Ammo_1) / i_Ammo_Bar_Size;

					Primary_Ammo_Bar->SetProgress(Value);
					Primary_Ammo_Bar_Empty->SetProgress(1.0f - Value);

					// This shows remaining Ammo
					// Secondary_Ammo_Bar->SetProgress((float)((i_Ammo_Bar_Size / i_Clip_Size_1) * i_Ammo_2) / i_Ammo_Bar_Size);
				}
			}

			else if (!b_Fuse_Ammo_Bars && Weapon->HasSecondaryAmmo())
			{ 							
				i_Ammo_2 = Weapon->Clip2();
				// GetDefaultClip2(); Clip2(); // Ammo and Ammo Salves are differnt...
				i_Ammo_2_Salves = Player->GetAmmoCount(Weapon->GetSecondaryAmmoType());
				i_Clip_Size_2 = Weapon->GetWpnData().iMaxClip2;
				
				if (i_Ammo_2_Salves == -1) // If nothing specificated
				{ i_Ammo_2_Salves = 3; } // if (i_Ammo_2 > 1) { i_Clip_Size_2 = i_Ammo_2; }
				if (i_Ammo_2 == -1) { i_Ammo_2 = i_Ammo_2_Salves; }
					

				if (Character_Count((char*)Weapon->GetWpnData().s_Ammo_Bar_2) > 2)
				{	Secondary_Ammo_Bar->SetTopTexture((char*)Weapon->GetWpnData().s_Ammo_Bar_2); Show_Ammo_Bar_2 = true; 
					Secondary_Ammo_Bar_Empty->SetTopTexture((char*)Weapon->GetWpnData().s_Ammo_Bar_2);
				}
			
				// If has no entry for available Ammo // if (Compare((char*)Weapon->GetWpnData().szAmmo2, "-1"))
				// if (Character_Count((char*)Weapon->GetWpnData().szAmmo2) > 2) 
				if (!Mini_Mode && Compare((char*)Weapon->GetWpnData().szAmmo2, "-1")) { Show_Ammo_Bar_2 = false; }


				float Value = (float)((i_Ammo_Bar_Size / i_Clip_Size_2) * i_Ammo_2) / i_Ammo_Bar_Size;

				Secondary_Ammo_Bar->SetProgress(Value);
				Secondary_Ammo_Bar_Empty->SetProgress(1.0f - Value);
			}		
		}	
				
		Primary_Ammo_Bar->SetVisible(Show_Ammo_Bar_1);
		Primary_Ammo_Bar_Empty->SetVisible(Show_Ammo_Bar_1);

		Secondary_Ammo_Bar->SetVisible(Show_Ammo_Bar_2);
		Secondary_Ammo_Bar_Empty->SetVisible(Show_Ammo_Bar_2);
	}
}

//-----------------------------------------------------------------------------
// Purpose: Paint handles Weapon Icons, their background tile, their colors, and weapon name letters
//-----------------------------------------------------------------------------
void CHud_Weapon_Bar::Paint()
{		
	C_BasePlayer *pPlayer = C_BasePlayer::GetLocalPlayer();
	if (!pPlayer) return;

	// C_BaseCombatWeapon *pSelectedWeapon = CBaseHudWeaponSelection::GetSelectedWeapon();
	C_BaseCombatWeapon *Active_Weapon = pPlayer->GetActiveWeapon();
	if (!Active_Weapon) return;


	if (Reset_Settings || Get_Changes()) { Resetting_Settings(); }

	// Setting for all drawings in this class, except for colored weapons while selectin is NOT inverted!
	// surface()->DrawSetColor(V_atoi(Col_TexR), V_atoi(Col_TexG), V_atoi(Col_TexB), V_atoi(Col_TexA));
	surface()->DrawSetColor(Color_Texture_R, Color_Texture_G, Color_Texture_B, Color_Texture_A);
	
	
	
	// if (Right_Sided) { SetPos(ScreenWidth() - 136, 48); } // -136 or -140
	// else { SetPos(8, 48); } // Set by this file
	/*if (Right_Sided) { g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("Weapon_Bars_Top_Right"); }
	else { g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("Weapon_Bars_Top_Left"); }*/
	

	int set_x = 8;
	int set_y = 48; // Set directly from this c++ file	
	if (Right_Sided) { set_x = ScreenWidth() - 136; }

	if (Mini_Mode)
	{	if (!Top_Sided) { set_y = ScreenHeight() - 65; } 
		SetPos(set_x, set_y);
		Primary_Ammo_Bar->SetPos(xpos, 2);
		Primary_Ammo_Bar_Empty->SetPos(xpos, 2);

		Secondary_Ammo_Bar->SetPos(xpos, 18);	
		Secondary_Ammo_Bar_Empty->SetPos(xpos, 18);
	}
	else if (!Mini_Mode)
	{   
		if (!Top_Sided) { set_y = ScreenHeight() - 128; }
		SetPos(set_x, set_y);
		Primary_Ammo_Bar->SetPos(xpos, 63);
		Primary_Ammo_Bar_Empty->SetPos(xpos, 63);

		Secondary_Ammo_Bar->SetPos(xpos, 79);
		Secondary_Ammo_Bar_Empty->SetPos(xpos, 79);
		
		// Inverted Selection means contrast to darker Background, otherwise we want a brighter panel texture to work with darker icons!	
		if (Invert_Selection && Active_Weapon->GetWpnData().iconActive)
		{   s_Img_Selection = Active_Weapon->GetWpnData().iconActive->szTextureFile;
			
			if (Compare(s_Img_Selection, "WeaponIconsSelected") && Active_Weapon->GetWpnData().iconInactive) // If returns default there is no texture
			{   char Buffer[512]; Buffer[0] = 0; // And we try appending the suffix _Lit
				sprintf(Buffer, "%s_Lit", Active_Weapon->GetWpnData().iconInactive->szTextureFile);
				s_Img_Selection = Buffer;
			}

			if (Show_Background) { Draw_Icon(i_Texture_Slot ); }
		}
		else if (!Invert_Selection && Active_Weapon->GetWpnData().iconInactive)
		{
			// Meaning of inactive and active is distorted.. iconInactive means the colored one and iconActive is White/unicolor
			s_Img_Selection = (char*)Active_Weapon->GetWpnData().iconInactive->szTextureFile; 
			if (Show_Background) { Draw_Icon(i_Texture_Selected); }

			// Because for normal selection the texture color filter above should not be applied
			surface()->DrawSetColor(255, 255, 255, 255);
		}



		// if (s_Img_Selection != null && Character_Count((char*)Active_Weapon->GetWpnData().iconInactive->szTextureFile) > 2)
		if (s_Img_Selection != null && Active_Weapon->GetWpnData().iconInactive) 
		{
			if (!Compare(Active_Weapon->GetWpnData().iconInactive->szTextureFile, (char*)"WeaponIcons")) // Returns this default string if no icon was found..
			{   surface()->DrawSetTextureFile(i_Weapon_Icon, s_Img_Selection, true, true);
				Draw_Icon(i_Weapon_Icon);
			}
		}
		else if (Active_Weapon->GetWpnData().iItemType != 6) // Exception cause Revolvers draw their Ammo over the Text..
		{
			char* Temp_A = Get_Name(Active_Weapon);

			if (Character_Count(Temp_A) < 15)
			{  
				/*IGameUIFuncs *Game_UI;
				int Res_X, Res_Y = 0;		
				Game_UI->GetDesktopResolution(Res_X, Res_Y);*/		
				//Draw_Text(xpos + 1 + width / 4, ypos + 1 + height / 3, Temp_A, Color_2R, Color_2G, Color_2B, Color_2Alpha);
			
				Draw_Text(xpos + 1 + width / 4, ypos + 1 + height / 3, Temp_A, Color_2);
				// Shadow Text will fail to Draw unless it is drawn from a second Get_Name(), don't ask me why...
				Draw_Text(xpos + width / 4, ypos + height / 3, Get_Name(Active_Weapon), Color_1);
				
			}
			else // Otherwise start drawing at left border for space purpose.
			{   Draw_Text(xpos + 1 + width / 10, ypos + 1 + height / 3, Temp_A, Color_2);
				Draw_Text(xpos + width / 10, ypos + height / 3, Get_Name(Active_Weapon), Color_1);
			}
		}
	}

	// Revolvers use a own UI!
	// if (Compare((char*)Active_Weapon->GetWpnData().szAmmo1, "357"))
	if (Active_Weapon->GetWpnData().iItemType == 6)
	{
		if (!gpGlobals->curtime > Tick) return; // Drawing only while ticking
		
		surface()->DrawSetColor(255, 255, 255, 255);
		i_Clip_Size_1 = Active_Weapon->GetWpnData().iMaxClip1;

		if (Mini_Mode)
		{   // Unfortunately we can't draw at bottom because there is no space left within boundries of this class
			// if (!Top_Sided) { return; }

			if(Show_Background) {Draw_Icon(i_Texture_Selected);}	
		} // if (Compare((char*)Active_Weapon->GetWpnData().iconInactive->szTextureFile, "-1")) { return; }
		else if (!Mini_Mode && Get_Right_Sided_Ammo() != -1) // Only Bullets with no Drum in Mini Mode, no real reason for that; just for fun ;)
		{		
			surface()->DrawSetTextureFile(i_Revolver_Drumm, CFmtStr("HUD/Weapons/Revolver_%d/Revolver_Drum", i_Clip_Size_1), true, true);
			surface()->DrawSetTexture(i_Revolver_Drumm);
			// surface()->DrawTexturedRect(xpos + 1, ypos + 1, xpos + (height - 1), ypos + (height - 1)); 
			surface()->DrawTexturedRect(xpos, ypos, xpos + height, ypos + height); // about 60x60
		}

		// Format to get Max Clip and current Ammo as string for the right Texture file
		CFmtStr Selection = CFmtStr("HUD/Weapons/Revolver_%d/Rounds_%d", i_Clip_Size_1, Active_Weapon->Clip1()); // Loaded Ammo

		surface()->DrawSetTextureFile(i_Revolver_Icon, Selection, true, true);
		surface()->DrawTexturedRect(xpos + 1, ypos + 1, xpos + (height - 1), ypos + (height - 1));;
		Tick = gpGlobals->curtime + 8.0f;
	}

}

//-----------------------------------------------------------------------------
// Purpose: Ressetting Settings
//-----------------------------------------------------------------------------
void CHud_Weapon_Bar::Resetting_Settings()
{   // Loading values from Convars
	Mini_Mode = iHud_Mini_Mode.GetBool();
	Invert_Selection = iHud_Invert_Selection.GetBool();
	Top_Sided = iHud_Top_Sided.GetBool();
	Right_Sided = iHud_Right_Sided.GetBool();
	Show_Background = iHud_Show_Background.GetBool();
	Show_Selector = iHud_Show_Selector.GetBool();


	// ================= Ammo Bar Allignment  ================= 
	if (Get_Right_Sided_Ammo() == -1) // Disabling
	{   Primary_Ammo_Bar->SetBottomTexture(NULL);
		Secondary_Ammo_Bar->SetBottomTexture(NULL);
	} 
	else if (Get_Right_Sided_Ammo()) // From Right to Left
	{   Primary_Ammo_Bar->SetBottomTexture(Background_Bar);
		Secondary_Ammo_Bar->SetBottomTexture(Background_Bar);

		Primary_Ammo_Bar->SetProgressDirection(ProgressBar::PROGRESS_WEST);
		Secondary_Ammo_Bar->SetProgressDirection(ProgressBar::PROGRESS_WEST);

		Primary_Ammo_Bar_Empty->SetProgressDirection(ProgressBar::PROGRESS_EAST);
		Secondary_Ammo_Bar_Empty->SetProgressDirection(ProgressBar::PROGRESS_EAST);
	} 
	else // From Left to Right
	{   Primary_Ammo_Bar->SetBottomTexture(Background_Bar);
		Secondary_Ammo_Bar->SetBottomTexture(Background_Bar);

		Primary_Ammo_Bar->SetProgressDirection(ProgressBar::PROGRESS_EAST);
		Secondary_Ammo_Bar->SetProgressDirection(ProgressBar::PROGRESS_EAST);

		Primary_Ammo_Bar_Empty->SetProgressDirection(ProgressBar::PROGRESS_WEST);
		Secondary_Ammo_Bar_Empty->SetProgressDirection(ProgressBar::PROGRESS_WEST);
	}


	// Loading color values from Convar	
	//------------------- Texture Color Filter
	char* Color_for_Textures = (char*)iHud_Color_Texture_Filter.GetString();
	// char* Color_1 = (char*)iHud_Color_1.GetString();

	// Don't make smaller then 80 or it will crash the Game!
	char Col_R[80];
	char Col_G[80];
	char Col_B[80];
	char Col_A[80];


	char delimiter = ','; // Both variants legit
	if (Compare(Color_for_Textures, ".")) { delimiter = '.'; }

	// Splitting and extracting color values into a string array
	Split_Array(Color_for_Textures, Col_R, delimiter, 1);
	Split_Array(Color_for_Textures, Col_G, delimiter, 2);
	Split_Array(Color_for_Textures, Col_B, delimiter, 3);
	Split_Array(Color_for_Textures, Col_A, delimiter, 4);

	Color_Texture_R = V_atof(Col_R);
	Color_Texture_G = V_atof(Col_G);
	Color_Texture_B = V_atof(Col_B);
	Color_Texture_A = V_atof(Col_A);

	Color_Filter = Color(Color_Texture_R, Color_Texture_G, Color_Texture_B, Color_Texture_A);
	// DevMsg("Color Filter is %d %d %d %d\n", Color_Texture_R, Color_Texture_G, Color_Texture_B, Color_Texture_A);


	//------------------- Texture 1

	
	// Extracting color values into a string array
	char* Color_1_Txt = Get_Color_1_Value();

	delimiter = ','; // Both variants legit
	if (Compare(Color_1_Txt, ".")) { delimiter = '.'; }

	Split_Array(Color_1_Txt, Col_R, delimiter, 1);
	Split_Array(Color_1_Txt, Col_G, delimiter, 2);
	Split_Array(Color_1_Txt, Col_B, delimiter, 3);
	Split_Array(Color_1_Txt, Col_A, delimiter, 4);

	Color_1R = V_atoi(Col_R);
	Color_1G = V_atoi(Col_G);
	Color_1B = V_atoi(Col_B);
	Color_1A = V_atoi(Col_A);

	Color_1 = Color(Color_1R, Color_1G, Color_1B, Color_1A);
	// DevMsg("Color 1 is %d %d %d %d\n", Color_1R, Color_1G, Color_1B, Color_1A);



	//------------------- Texture 2
	char* Color_2_Txt = Get_Color_2_Value();

	delimiter = ','; // Both variants legit
	if (Compare(Color_2_Txt, ".")) { delimiter = '.'; }

	Split_Array(Color_2_Txt, Col_R, delimiter, 1);
	Split_Array(Color_2_Txt, Col_G, delimiter, 2);
	Split_Array(Color_2_Txt, Col_B, delimiter, 3);
	Split_Array(Color_2_Txt, Col_A, delimiter, 4);

	Color_2R = V_atoi(Col_R);
	Color_2G = V_atoi(Col_G);
	Color_2B = V_atoi(Col_B);
	Color_2A = V_atoi(Col_A);

	Color_2 = Color(Color_2R, Color_2G, Color_2B, Color_2A);
	// DevMsg("Color 2 is %d %d %d %d\n", Color_2R, Color_2G, Color_2B, Color_2A);

	//-------------------
	// If inactive it will stay in responsive test mode
	Reset_Settings = false;

}


//-----------------------------------------------------------------------------
// Purpose: Applying Settings at each level start
//-----------------------------------------------------------------------------
void CHud_Weapon_Bar::LevelInit()
{   CHudElement::LevelInit();
	Reset_Settings = true;
}


//-----------------------------------------------------------------------------
// Purpose: Draw Tile Background
//-----------------------------------------------------------------------------
void CHud_Weapon_Bar::Draw_Icon(int texture)
{
	surface()->DrawSetTexture(texture);
	surface()->DrawTexturedRect(xpos, ypos, xpos + width, ypos + height);
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
char* CHud_Weapon_Bar::Get_Name(C_BaseCombatWeapon * Weapon)
{
	C_BasePlayer *Player = C_BasePlayer::GetLocalPlayer();
	if (Player && Weapon)
	{
		wchar_t* Entry = g_pVGuiLocalize->Find(Weapon->GetWpnData().szPrintName);

		char Buffer[240]; // Never use MAX_WEAPON_STRING, it somehow breaks this
		Buffer[0] = 0;
		g_pVGuiLocalize->ConvertUnicodeToANSI(Entry, Buffer, 240);

		// If no Localization was found we return the straight text
		if (Compare(Buffer, "")) { return (char*)Weapon->GetWpnData().szPrintName; }
		else { return (char*)Buffer; }
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHud_Weapon_Bar::Draw_Text(int x, int y, char* text, int R, int G, int B, int Alpha)
{
	surface()->DrawSetTextFont(hFont2);
	surface()->DrawSetTextColor(R, G, B, Alpha);
	// surface()->DrawSetTextPos(xpos + sizeX / 2, ypos + sizeY / 4);
	surface()->DrawSetTextPos(x, y);


	// surface()->DrawSetTextScale(2.0, 2.0);

	mbstowcs(&unicode[0], text, MAX_WEAPON_STRING); // Max 80 characters
	surface()->DrawPrintText(unicode, wcslen(unicode));
}

void CHud_Weapon_Bar::Draw_Text(int x, int y, char* text, Color Color_Value)
{
	surface()->DrawSetTextFont(hFont2);
	surface()->DrawSetTextColor(Color_Value);
	// surface()->DrawSetTextPos(xpos + sizeX / 2, ypos + sizeY / 4);
	surface()->DrawSetTextPos(x, y);


	// surface()->DrawSetTextScale(2.0, 2.0);

	mbstowcs(&unicode[0], text, MAX_WEAPON_STRING); // Max 80 characters
	surface()->DrawPrintText(unicode, wcslen(unicode));
}

//-----------------------------------------------------------------------------
// Purpose: String Comparsion
//-----------------------------------------------------------------------------
bool CHud_Weapon_Bar::Compare(char* value_1, char* value_2)
{
	if (strcmp(value_1, value_2) == 0) { return true; }
	return false;
}


//-----------------------------------------------------------------------------
// Purpose: String Comparsion
//-----------------------------------------------------------------------------
void  CHud_Weapon_Bar::Append(char* Value, char* Suffix, char* Buffer)
{   
	// char s_Texture_Name[80]; s_Texture_Name[0] = 0;
	sprintf(Buffer, "%s%s", Value, Suffix);
	// Value = Buffer;
}


//-----------------------------------------------------------------------------
// Purpose: Count Characters (testing whether char arrays are empty)
//-----------------------------------------------------------------------------
// ((i >= 'a' &&  i <= 'z') || (i >= 'A' && i <= 'Z'))
int CHud_Weapon_Bar::Character_Count(char* value_1)
{   // Looping through all Chars in the value_1 array
	int Counter = 0;

	for (int i = 0; i < sizeof(value_1); i++)
	{
		if (value_1[i] == '\0') { return Counter; }
		Counter++;
	}
	return Counter;
}


//-----------------------------------------------------------------------------
// Purpose: Append To Char
//-----------------------------------------------------------------------------
char* CHud_Weapon_Bar::Append_To_Char(char* value_1, char* value_2)
{

	bool Start_Writing = false;
	int Charater_Slot = 0;

	for (int i = 0; i < 80; i++) // 80 is used in most cases
	{
		if (value_1[i] == '\0') { Start_Writing = true; }

		if (Start_Writing)
		{
			// If all chars were transfered from value_2 to value_1 we end the array
			if (Charater_Slot >= CHud_Weapon_Bar::Character_Count(value_2)) { value_1[i] = '\0'; return value_1; }

			else // Transfer the next value
			{
				value_1[i] = value_2[Charater_Slot];
				Charater_Slot++;
			}
		}
	}

	// If nothing was transfered we return an empty array
	// value_2[0] = '\0'; return value_2;
}

//-----------------------------------------------------------------------------
// Simpler Array Split Version
//-----------------------------------------------------------------------------
// Loop until the delimiter char sign is found ',' then return the first value in value_2
/*char* CHud_Weapon_Bar::Split_Array(char* value_1, char* value_2, char delimiter)
{
	for (int i = 0; i < 80; i++) // 80 is used in most cases
	{
		if (value_1[i] == delimiter) { value_2[i] = '\0'; return value_2; }
		// We transfere values over to the new char array
		value_2[i] = value_1[i];
	}

	return null; 
}*/

//-----------------------------------------------------------------------------
// Loop until the delimiter char sign is found ',' then return the first value in value_2
//-----------------------------------------------------------------------------
char* CHud_Weapon_Bar::Split_Array(char* value_1, char* value_2, char delimiter, int word_number)
{
	int Word_Count = 0;
	int Charater_Slot = 0;

	for (int i = 0; i < 80; i++) // 80 is used in most cases
	{
		if (value_1[i] == delimiter)
		{
			Word_Count++; // We raise wordcount after each delimiter, if count matches desired word we return it
			if (Word_Count == word_number) { value_2[Charater_Slot] = '\0'; return value_2; }
			else { Charater_Slot = 0; } // Otherwise we start overwriting by the next word/value
		}
		else if(value_1[i] != ' ') // Ignoring emptyspaces
		{   // We transfere values over to the new char array
			value_2[Charater_Slot] = value_1[i];
			Charater_Slot++;
		}
	}
	return null;
}


//-----------------------------------------------------------------------------
// BROKEN
//-----------------------------------------------------------------------------
void CHud_Weapon_Bar::Grab_Color(static ConVar Entry, int Result[])
{	
	//char Col_R[80];
	//char Col_G[80];
	//char Col_B[80];
	//char Col_Alpha[80];

	//// Extracting color values into a string array
	//char* Color_1 = (char*)Entry.GetString();

	//char delimit = ','; // Both variants legit
	//if (Compare(Color_1, ".")) { delimit = '.'; }

	//Split_Array(Color_1, Col_R, delimit, 1);
	//Split_Array(Color_1, Col_G, delimit, 2);
	//Split_Array(Color_1, Col_B, delimit, 3);
	//Split_Array(Color_1, Col_Alpha, delimit, 4);

	//Result[0] = V_atof(Col_R);
	//Result[1] = V_atof(Col_G);
	//Result[2] = V_atof(Col_B);
	//Result[3] = V_atof(Col_Alpha);
}



//-----------------------------------------------------------------------------
// Purpose: Create a new Status Bar
//-----------------------------------------------------------------------------
ImageProgressBar* CHud_Weapon_Bar::Create_Status_Bar(const char* Background_Texture, int x = 0, int y = 0, bool Visible = true, int width = 140, int height = 14, char direction = 'E', bool Colorize = false)
{
	Bar = new ImageProgressBar(this, "Hud_Weapon_Bar");
	Bar->SetPos(x, y);
	Bar->SetSize(width, height);
	// And Color will be different, as needed to darken out the Armor Bar
	if (Colorize) { Bar->Set_RGBA(90, 100, 90, 255); }
	// if (Bar_Texture){ Bar->SetTopTexture(Bar_Texture); } // Obsolete

	Bar->SetProgress(10.0f); // Innitial Setup
	Bar->SetVisible(Visible);


	if (Background_Texture){ Bar->SetBottomTexture(Background_Texture); }
	// If innitialized with 0 as Top Texture it will also leave BG blank and define TopTexture at runtime.
	else if (!Background_Texture) { Bar->SetBottomTexture(NULL); }


	if (direction == '0') // Disabled Background textures
	{   Bar->SetProgressDirection(ProgressBar::PROGRESS_EAST);
		Bar->SetBottomTexture(NULL);
	}
	else if (direction == 'E') // its Horizontal
	{	Bar->SetProgressDirection(ProgressBar::PROGRESS_EAST); }
	else if (direction == 'W') // its Horizontal
	{	Bar->SetProgressDirection(ProgressBar::PROGRESS_WEST); }
	else if (direction == 'N') // its Vertical
	{	Bar->SetProgressDirection(ProgressBar::PROGRESS_NORTH); }
	else if (direction == 'S')
	{   Bar->SetProgressDirection(ProgressBar::PROGRESS_SOUTH); }
	

	return Bar;
}




//-----------------------------------------------------------------------------
// Purpose: Numeric Display for amount of Grenades in Weapon Selection mode
//-----------------------------------------------------------------------------
class CHud_Grenades : public CHudElement, public CHudNumericDisplay
{
	DECLARE_CLASS_SIMPLE(CHud_Grenades, CHudNumericDisplay);

public:
	CHud_Grenades(const char *pElementName);
	virtual void OnThink();
	virtual void Paint();
	bool Compare(char* value_1, char* value_2);
	bool Should_Draw = false;
	bool Should_Paint = false;
	bool Reset_Settings = true;
	int i_Grenades = 0;
	int i_Grenades_New = 0;

	int i_Grenade_Icon;
	int i_Grenade_Icon_Inactive;
	int Weapon_Count = 0;

};

DECLARE_HUDELEMENT(CHud_Grenades);

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CHud_Grenades::CHud_Grenades(const char *pElementName) : CHudElement(pElementName), CHudNumericDisplay(NULL, "Hud_Grenade")
{
	CHud_Grenades::SetPaintBackgroundEnabled(false);
	// SetZPos(3); // Specified in HudLayout.res

	i_Grenade_Icon = vgui::surface()->CreateNewTextureID();
	vgui::surface()->DrawSetTextureFile(i_Grenade_Icon, "HUD/Weapons/Grenade_Mini", true, false);

	i_Grenade_Icon_Inactive = vgui::surface()->CreateNewTextureID();
	vgui::surface()->DrawSetTextureFile(i_Grenade_Icon_Inactive, "HUD/Weapons/Grenade_Mini_Lit", true, false);

	Reset_Settings = true;

	SetHiddenBits(HIDEHUD_HEALTH | HIDEHUD_PLAYERDEAD); // | HIDEHUD_NEEDSUIT );
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHud_Grenades::OnThink()
{
	C_BasePlayer *Player = C_BasePlayer::GetLocalPlayer();
	C_BaseCombatWeapon *Weapon = GetActiveWeapon();
	

	if (Player)
	{   Weapon_Count = 0;

		for (int i = 0; i < MAX_WEAPONS; i++)
		{   C_BaseCombatWeapon *Selected_Weapon = Player->GetWeapon(i);	
		
			if (Selected_Weapon)
			{   Weapon_Count++;

				if (Compare((char*)Selected_Weapon->GetWpnData().szAmmo1, "grenade"))
				{   Should_Draw = true;
					// If any Slot contains Grenades	
					i_Grenades_New = Player->GetAmmoCount(Selected_Weapon->GetPrimaryAmmoType());

					// Draw to indicate the grenade when other weapons are selected, but not if Grenades are selected.
					if (Selected_Weapon == Weapon) { Should_Draw = false; return; }
				}
			}									
		}

		if (i_Grenades_New < 1 || Weapon_Count < 1) { Should_Draw = false; } // Should not draw if no Grenades were found

		/*if (Has_Grenades) { SetLabelText((const wchar_t *)"+"); }
		else { SetLabelText((const wchar_t *)""); }*/
	}


	// Only update the fade if we've changed anything, also it hides percentage if over 80%
	if (i_Grenades_New == i_Grenades) return;

	i_Grenades = i_Grenades_New;
	
}

//-----------------------------------------------------------------------------
// Purpose: Paint
//-----------------------------------------------------------------------------
void CHud_Grenades::Paint()
{
	CHud_Weapon_Bar *Weapon_Bar;

	if (Reset_Settings || Weapon_Bar->Get_Changes())
	{
		int set_x = 144;
		int set_y = 32;

		if (Weapon_Bar->Get_Right_Sided()) { set_x = ScreenWidth() - 159; }

		if (!Weapon_Bar->Get_Top_Sided())
		{
			if (Weapon_Bar->Get_Mini_Mode()) { set_y = ScreenHeight() - 81; }
			else { set_y = ScreenHeight() - 144; }
		}

		SetPos(set_x, set_y);
		Reset_Settings = false;
	}


	// surface()->DrawSetColor( Color( m_AuxPowerColor[0], m_AuxPowerColor[1], m_AuxPowerColor[2], m_iAuxPowerDisabledAlpha ) );


	if (!Should_Draw) { SetShouldDisplayValue(false); }
	else
	{   surface()->DrawSetColor(255, 255, 255, 255);
		if (Weapon_Bar->Get_Invert_Selection()) { surface()->DrawSetTexture(i_Grenade_Icon_Inactive); }
		else { surface()->DrawSetTexture(i_Grenade_Icon); }
		surface()->DrawTexturedRect(0, 0, this->GetWide(), this->GetTall()); 

		SetShouldDisplayValue(true); 
		SetDisplayValue(i_Grenades);
	}

	// Comment this out to get rid of the Text
	if (Weapon_Bar->Get_Invert_Selection()) { BaseClass::Paint();}
}

//-----------------------------------------------------------------------------
// Purpose: String Comparsion
//-----------------------------------------------------------------------------
bool CHud_Grenades::Compare(char* value_1, char* value_2)
{
	if (strcmp(value_1, value_2) == 0) { return true; }
	return false;
}




