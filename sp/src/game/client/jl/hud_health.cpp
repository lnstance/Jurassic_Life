//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//
//=============================================================================//
//
// Health.cpp
//
// implementation of CHudHealth class
//
#include "cbase.h"
#include "hud.h"
#include "hud_macros.h"
#include "view.h"

#include "iclientmode.h"
#include "hud_weapon_bars.h"
#include "vgui/ISurface.h" // For drawing Med Indicator

#include <KeyValues.h>
#include <vgui/ISurface.h>
#include <vgui/ISystem.h>
#include <vgui_controls/AnimationController.h>

#include <vgui/ILocalize.h>

using namespace vgui;

#include "hudelement.h"
#include "hud_numericdisplay.h"

#include "convar.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

#define INIT_HEALTH -1

//-----------------------------------------------------------------------------
// Purpose: Health panel
//-----------------------------------------------------------------------------
class CHudHealth : public CHudElement, public CHudNumericDisplay
{
	DECLARE_CLASS_SIMPLE( CHudHealth, CHudNumericDisplay );

public:
	CHudHealth( const char *pElementName );
	virtual void Init( void );
	virtual void VidInit( void );
	virtual void Reset( void );
	virtual void OnThink();
	virtual void Paint(void);
			void MsgFunc_Damage( bf_read &msg );
	bool Compare(char* value_1, char* value_2);
	bool Has_Medpack = false;
	bool Reset_Settings = false;

private:
	// old variables
	int		m_iHealth;	
	int		m_bitsDamage;
};	

DECLARE_HUDELEMENT( CHudHealth );
DECLARE_HUD_MESSAGE( CHudHealth, Damage );

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CHudHealth::CHudHealth( const char *pElementName ) : CHudElement( pElementName ), CHudNumericDisplay(NULL, "HudHealth")
{
	CHudHealth::SetPaintBackgroundEnabled(false);
	// SetZPos(3); // Specified in HudLayout.res

	SetHiddenBits(HIDEHUD_HEALTH | HIDEHUD_PLAYERDEAD); // | HIDEHUD_NEEDSUIT );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudHealth::Init()
{
	HOOK_HUD_MESSAGE( CHudHealth, Damage );
    Reset(); 
	Reset_Settings = true;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudHealth::Reset()
{   m_iHealth		= INIT_HEALTH;
	m_bitsDamage	= 0;
	Reset_Settings = true;

	// Imperial; Disabled text entry to stay minimal
	/*
	wchar_t *tempString = g_pVGuiLocalize->Find("#Valve_Hud_HEALTH");

	if (tempString)
	{
		SetLabelText(tempString);
	}
	else
	{
		SetLabelText(L"HEALTH");
	}
	SetDisplayValue(m_iHealth);
	*/
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudHealth::VidInit()
{
	 Reset();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudHealth::OnThink()
{
	int newHealth = 0;
	C_BasePlayer *Player = C_BasePlayer::GetLocalPlayer();


	if (Player)
	{	// Never below zero
		newHealth = MAX(Player->GetHealth(), 0);

		// Old Indicator drawn by text
		// if (Has_Medpack) { SetLabelText((const wchar_t *)"+"); }
		// else {  SetLabelText((const wchar_t *)""); }
	}


	CHud_Weapon_Bar *Weapon_Bar;

	if (Reset_Settings || Weapon_Bar->Get_Changes())
	{
		int set_x = 115;
		int set_y = 32; // Set directly from this c++ file	 16 for Armor Bar	


		if (Weapon_Bar->Get_Right_Sided()) { set_x = ScreenWidth() - 30; }
		if (!Weapon_Bar->Get_Top_Sided())
		{
			if (Weapon_Bar->Get_Mini_Mode()){ set_y = ScreenHeight() - 81; }
			else { set_y = ScreenHeight() - 144; }
		}
		SetPos(set_x, set_y);
		Reset_Settings = false;
	}

	
	// Position Changers by Animation.txt Disabled because they distort when Aspect Ratio changes!
	// if (Weapon_Bar->Get_Right_Sided()) { g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("Health_Top_Right"); }
	// else { g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("Health_Top_Left"); }


	// Only update the fade if we've changed health, also it hides percentage if over 80%
	if (newHealth == m_iHealth) return;	
	m_iHealth = newHealth;  

	if (m_iHealth > 85){ SetShouldDisplayValue(false); }
	else { SetShouldDisplayValue(true); }

	if ( m_iHealth >= 10 ) // 33% because 33.33 is 1/3 of 100%
	{   // Anim specifications are in .\scripts\hudanimations.txt
		g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("Health_Increased");
	}
	else if ( m_iHealth > 0 )
	{
		g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("Health_Decreased");
		g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("Health_Low");
	}
	

	SetDisplayValue(m_iHealth);
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudHealth::Paint()
{   
	BaseClass::Paint();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudHealth::MsgFunc_Damage( bf_read &msg )
{	// int armor = msg.ReadByte();
	int damageTaken = msg.ReadByte();  // health
	long bitsDamage = msg.ReadLong(); // damage bits
	bitsDamage; // variable still sent but not used

	Vector vecFrom;

	vecFrom.x = msg.ReadBitCoord();
	vecFrom.y = msg.ReadBitCoord();
	vecFrom.z = msg.ReadBitCoord();

	// Actually took damage?
	if (damageTaken > 0 || bitsDamage > 0)
	{	if ( damageTaken > 0 )
		{
			// start the animation
			g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("HealthDamageTaken");
		}
	}
}

//-----------------------------------------------------------------------------
// Purpose: String Comparsion
//-----------------------------------------------------------------------------
bool CHudHealth::Compare(char* value_1, char* value_2)
{
	if (strcmp(value_1, value_2) == 0) { return true; }
	return false;
}






//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
class CHealth_Indicator : public CHudElement, public vgui::Panel
{
	DECLARE_CLASS_SIMPLE(CHealth_Indicator, vgui::Panel);

	int i_Med_Indicator;
	int Tick = 0;
	bool Has_Medpack = false;
	
public:
	CHealth_Indicator(const char *pElementName);
	
	virtual void OnThink();
	virtual void Paint();
	bool Compare(char* value_1, char* value_2);
	bool Reset_Settings = true;
};

DECLARE_HUDELEMENT(CHealth_Indicator);

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------

CHealth_Indicator::CHealth_Indicator(const char *pElementName) : CHudElement(pElementName), BaseClass(NULL, "Health_Indicator")
{
	Panel *pParent = g_pClientMode->GetViewport();
	SetParent(pParent);

	CHealth_Indicator::SetPaintBackgroundEnabled(false);
	
	i_Med_Indicator = vgui::surface()->CreateNewTextureID();
	vgui::surface()->DrawSetTextureFile(i_Med_Indicator, "HUD/Horizontal/Med_Indicator", true, false);


	SetHiddenBits(HIDEHUD_HEALTH | HIDEHUD_PLAYERDEAD); // | HIDEHUD_NEEDSUIT );
}

//-----------------------------------------------------------------------------
// Purpose: Think
//-----------------------------------------------------------------------------
void CHealth_Indicator::OnThink()
{
	if (!gpGlobals->curtime > Tick) return; // Drawing only while ticking

	C_BasePlayer *Player = C_BasePlayer::GetLocalPlayer();
	if (Player)
	{
		C_BaseCombatWeapon *Selected_Weapon = GetActiveWeapon();
		CHud_Weapon_Bar *Weapon_Bar;

		if (Reset_Settings || Weapon_Bar->Get_Changes())
		{
			int set_x = 12;
			int set_y = 35;

			if (Weapon_Bar->Get_Right_Sided()) { set_x = ScreenWidth() - 132; }
			if (!Weapon_Bar->Get_Top_Sided())
			{
				if (Weapon_Bar->Get_Mini_Mode()){ set_y = ScreenHeight() - 78; }
				else { set_y = ScreenHeight() - 141; }
			}

			SetPos(set_x, set_y);
			Reset_Settings = false;
		}


		for (int i = 0; i < MAX_WEAPONS; i++)
		{
			C_BaseCombatWeapon *Weapon = Player->GetWeapon(i);

			if (Weapon)
			{   


				if (Weapon_Bar->Compare((char*)Weapon->GetWpnData().szAmmo2, "Medkit")
					|| Weapon_Bar->Compare((char*)Weapon->GetWpnData().szAmmo2, "Pills")
					|| Weapon_Bar->Compare((char*)Weapon->GetWpnData().szAmmo2, "Antibiotics"))
				{
					if (!Weapon_Bar->Compare((char*)Selected_Weapon->GetWpnData().szAmmo2, "Medkit")
						&& !Weapon_Bar->Compare((char*)Selected_Weapon->GetWpnData().szAmmo2, "Pills")
						&& !Weapon_Bar->Compare((char*)Selected_Weapon->GetWpnData().szAmmo2, "Antibiotics"))					
					{
						Has_Medpack = true; break;
					}
					else { Has_Medpack = false; } // If any Slot contains Meds
				}
				else { Has_Medpack = false; }
			}
			else { Has_Medpack = false; }
		}
	}

	Tick = gpGlobals->curtime + 8.0f;
}

//-----------------------------------------------------------------------------
// Purpose: Paint
//-----------------------------------------------------------------------------
void CHealth_Indicator::Paint()
{   if (Has_Medpack)
	{	surface()->DrawSetTexture(i_Med_Indicator);		
		surface()->DrawSetColor(255, 255, 255, 255);
		surface()->DrawTexturedRect(0, 0, 12, 12);
	}
}

//-----------------------------------------------------------------------------
// Purpose: String Comparsion
//-----------------------------------------------------------------------------
bool CHealth_Indicator::Compare(char* value_1, char* value_2)
{
	if (strcmp(value_1, value_2) == 0) { return true; }
	return false;
}


