﻿//========= Copyright © 1996-2004, Valve LLC, All rights reserved. ============
//
// Purpose: Simple HUD element
//
//=============================================================================

#include "stdlib.h"
#include "cbase.h"
#include "c_basehlplayer.h"
#include "hudelement.h"
#include "hud_macros.h"
#include "iclientmode.h"
#include "view.h"

#include "hud_weapon_bars.h"
#include "IInventory.h"


using namespace vgui;


#include <vgui_controls/Label.h>
#include <vgui_controls/RichText.h>

#include <vgui_controls/Panel.h>
#include <vgui_controls/Frame.h>
#include <vgui/IScheme.h>
#include <vgui/ISurface.h>
#include <vgui/ILocalize.h>


//-----------------------------------------------------------------------------
//  Note Info
//-----------------------------------------------------------------------------
class CHUDNoteInfo : public CHudElement, public vgui::Panel
{
private:
	DECLARE_CLASS_SIMPLE( CHUDNoteInfo, vgui::Panel );
	CHud_Weapon_Bar* Weapon_Bar;
	int		iMail_ID;

public:
	CHUDNoteInfo( const char *pElementName ) : CHudElement( pElementName ), vgui::Panel( NULL, "Hud_Note_Info" ) 
	{
		vgui::Panel *pParent = g_pClientMode->GetViewport();
		SetParent( pParent );

		SetPaintBorderEnabled( false );
		SetPaintBackgroundEnabled( false );
		
		SetHiddenBits(HIDEHUD_HEALTH | HIDEHUD_PLAYERDEAD | HIDEHUD_WEAPONSELECTION);  //| HIDEHUD_NEEDSUIT );
	
		// Objective_Field = new RichText(this, "Hud_Objective_Info");
	};

	// RichText* Objective_Field; 

	void	Init( void );
	void	Paint( void );	
};

DECLARE_HUDELEMENT( CHUDNoteInfo );


void CHUDNoteInfo::Init( void )
{
	iMail_ID = vgui::surface()->CreateNewTextureID();
	char* Mail_Texture = Weapon_Bar->Localize_Text("Note_Texture");

	if (Weapon_Bar->Compare(Mail_Texture, "")){ Mail_Texture = "HUD/Inventory/Themes/01/Note_Info"; } // Failsafe
	PrecacheMaterial(Mail_Texture);
	vgui::surface()->DrawSetTextureFile(iMail_ID, Mail_Texture, true, false);

}


//-----------------------------------------------------------------------------
// Paint
//-----------------------------------------------------------------------------
void CHUDNoteInfo::Paint( void )
{
	C_BasePlayer *player = C_BasePlayer::GetLocalPlayer();	
	
	//  Draw HUD Bottom Left image (Health)
	if (player && player->HasNewNote())
	{

		if (Weapon_Bar->Get_Right_Sided()) { SetPos(5, 70); }
		else { SetPos(ScreenWidth() - GetWide(), 70); }

		surface()->DrawSetTexture(iMail_ID);
		surface()->DrawSetColor(255, 255, 255, 255);
		surface()->DrawTexturedRect(0, 0, GetWide(), GetTall());	


		
		// Alternative for free drawing on the object
		//surface()->DrawSetTexture(iMail_ID);
		//surface()->DrawSetColor(255, 255, 255, 255);

		//int Icon_Size = 64; // 64x64 Printing Mail Symbol
		//if (Weapon_Bar->Get_Right_Sided()) 
		//{ surface()->DrawTexturedRect(5, 100, Icon_Size + 5, 100 + Icon_Size); }
		//else { surface()->DrawTexturedRect(ScreenWidth() - (Icon_Size + 5), 100, ScreenWidth(), 100 + Icon_Size); }


		// Weapon_Bar->Draw_Text(xpos + 1 + width / 10, ypos + 1 + height / 3, Temp_A, Color_2R, Color_2G, Color_2B, Color_2Alpha);
			
	}
}




//-----------------------------------------------------------------------------
// Objective Info As Rich Text
//-----------------------------------------------------------------------------

//class CObjective_Info : public CHudElement, public vgui::RichText
//{
//private:
//	DECLARE_CLASS_SIMPLE(CObjective_Info, vgui::RichText);
//	CHud_Weapon_Bar* Weapon_Bar;
//
//public:
//
//	CObjective_Info(const char *pElementName) : CHudElement(pElementName), vgui::RichText(NULL, "Hud_Objective_Info")
//	{
//		vgui::Panel *pParent = g_pClientMode->GetViewport();
//		SetParent(pParent);
//
//		/*SetPaintBorderEnabled(false);
//		SetPaintBackgroundEnabled(false);*/
//
//		SetHiddenBits(HIDEHUD_HEALTH | HIDEHUD_PLAYERDEAD | HIDEHUD_WEAPONSELECTION);  //| HIDEHUD_NEEDSUIT );
//
//		// Objective_Field = new RichText(this, "Hud_Objective_Info");
//	};
//
//	// RichText* Objective_Field; 
//
//	// virtual void	Init(void);
//	virtual void	Paint(void);
//
//private:
//	int		iMail_ID, iObjective_ID;
//};
//
//DECLARE_HUDELEMENT(CObjective_Info);
//
//
//
//void CObjective_Info::Paint()
//{ 
//	
//	SetText("This is an Test");
//	SetBgColor(Color(60, 60, 70, 255));
//	SetFgColor(Color(0, 0, 0, 255));
//
//	//	// Objective_Field->SetBgColor(Color(60, 60, 70, 255));
//	//	Objective_Field->SetFgColor(Color(0, 0, 0, 255));
//	//	Objective_Field->SetVisible(false);
//
//	BaseClass::Paint(); 
//};


//-----------------------------------------------------------------------------
// Objective Info as Label (has no Text Wrapper and Scrollbar)
//-----------------------------------------------------------------------------
class CObjective_Info : public CHudElement, public vgui::Label
{
private:
	DECLARE_CLASS_SIMPLE(CObjective_Info, vgui::Label);
	CHud_Weapon_Bar* Weapon_Bar;
	CInventory * Inventory;
	int			Small_Objective, Large_Objective, Alpha;
	float		m_fLastChange = 0;
	Color		Text_Color;
	Color		Background_Color;

public:
	CObjective_Info(const char *pElementName);
	// RichText* Objective_Field = new RichText(this, "Hud_Objective_Info") 

	char* Screen_Text = "";
	int Old_Objective = 0;

	virtual void	Paint(void);
};

DECLARE_HUDELEMENT(CObjective_Info);


//-----------------------------------------------------------------------------
// Constructor
//-----------------------------------------------------------------------------
CObjective_Info::CObjective_Info(const char *pElementName) : CHudElement(pElementName), vgui::Label(NULL, "Hud_Objective_Info", "")
{
	vgui::Panel *pParent = g_pClientMode->GetViewport();
	SetParent(pParent);

	// SetPaintBorderEnabled(false);
	SetPaintBackgroundEnabled(false);

	SetHiddenBits(HIDEHUD_HEALTH | HIDEHUD_PLAYERDEAD | HIDEHUD_WEAPONSELECTION);  //| HIDEHUD_NEEDSUIT );


	Small_Objective = vgui::surface()->CreateNewTextureID();
	char* Texture_Small = Weapon_Bar->Localize_Text("Objective_Small");

	if (Weapon_Bar->Compare(Texture_Small, "")){ Texture_Small = "HUD/Inventory/Themes/01/Objective_Texture_Small"; }
	PrecacheMaterial(Texture_Small);
	vgui::surface()->DrawSetTextureFile(Small_Objective, Texture_Small, true, false);


	Large_Objective = vgui::surface()->CreateNewTextureID();
	char* Texture_Large = Weapon_Bar->Localize_Text("Objective_Large");

	if (Weapon_Bar->Compare(Texture_Large, "")){ Texture_Large = "HUD/Inventory/Themes/01/Objective_Texture_Large"; }
	PrecacheMaterial(Texture_Large);
	vgui::surface()->DrawSetTextureFile(Large_Objective, Texture_Large, true, false);

	SetTextInset(12, 2); // Margins
	SetAlpha(0);
};



//-----------------------------------------------------------------------------
// Paint
//-----------------------------------------------------------------------------
void CObjective_Info::Paint()
{
	C_BasePlayer *Player = C_BasePlayer::GetLocalPlayer();
	int Active_Objective = Player->Get_Newest_Objective();
	int Status = Player->Get_Paper_Number(Active_Objective, "Doc_Status");
	

	if (Player && Active_Objective && Status)
	{   // DevMsg("Active_Objective is %d and Status is %d\n", Active_Objective, Status);

		char Buffer[1024]; Buffer[0] = 0;
		g_pVGuiLocalize->ConvertUnicodeToANSI(Player->Get_Localized_Text(Active_Objective, "Objective"), Buffer, 1024);
		Screen_Text = Buffer;

		SetText(Screen_Text);
		
		// char Entry[24]; Entry[0] = 0;
		// Q_snprintf(Entry, sizeof(Entry), "Doc_Objective_%d", Active_Objective);
		// Screen_Text = Weapon_Bar->Localize_Text(Entry);	



		if (!m_fLastChange || Active_Objective != Old_Objective)  // New Entry  
		{	m_fLastChange = gpGlobals->curtime;		
			Alpha = 255; 
			SetAlpha(255);
			SetVisible(true);
			Old_Objective = Active_Objective;


			// int Page_ID = Player->Get_Paper_Number(Active_Objective, "ID"); // DevMsg("%d\n", Page_ID);
			Text_Color = Inventory->Get_Color(Active_Objective, "Color_1");
			Background_Color = Inventory->Get_Color(Active_Objective, "Color_2");
		
			// DevMsg("Text_Color is %d %d %d %d\n", Text_Color[0], Text_Color[1], Text_Color[2], Text_Color[3]);
			// DevMsg("Background_Color is %d %d %d %d\n", Background_Color[0], Background_Color[1], Background_Color[2], Background_Color[3]);

			if (Text_Color == Color(0, 0, 0, 0)) { Text_Color = Color(255, 255, 255, 255); } // White
							
			if (Background_Color == Color(255, 255, 255, 255) || Background_Color == Color(0, 0, 0, 0))
			{   Background_Color = Color(120, 120, 130, 220);
				// if (Weapon_Bar->Get_Show_Background()) { Background_Color = Color(120, 120, 130, 220); }// Brighter Black
				// else { Background_Color = Color(60, 60, 70, 230); } // Darker Black
				// char* BG_Color = Weapon_Bar->Localize_Text("Objective_Background");			
			}		

			SetFgColor(Text_Color);
			/*if (!Weapon_Bar->Get_Show_Background())
			{   SetBgColor(Background_Color);
				SetPaintBackgroundEnabled(true); // Stays true forever...
			}*/

			if (Weapon_Bar->Get_Right_Sided()) { SetPos(0, 0); }
			else { SetPos(ScreenWidth() - GetWide(), 0); }		
		} 
		else if (Alpha < 1) // Exit
		{ 	Alpha = 0; 
			SetVisible(false); 
			m_fLastChange = 0;
			Player->Clear_Notification(); 
			SetPaintBackgroundEnabled(false);
			DevMsg("Cleared Objective Info\n"); 
		} 
		else if (gpGlobals->curtime - m_fLastChange > 10.0f && Alpha > 0) { SetAlpha(Alpha--); } // Countdown
		

		// if (Weapon_Bar->Get_Show_Background()) {
		    // SetPaintBackgroundEnabled(false);
			surface()->DrawSetColor(Background_Color);
			surface()->DrawSetTexture(Large_Objective);
			surface()->DrawTexturedRect(0, 0, GetWide(), GetTall());
		// }

	
			

		// char temp[20];
		// Q_snprintf(temp, sizeof(temp), "%d", Character_Count(Screen_Text) * 2);
		// Then we need more space and expand the texture background accordingly
		/*if (Weapon_Bar->Character_Count(Screen_Text) > 300)
		{ SetSize(Weapon_Bar->Character_Count(Screen_Text), 128); }
		else { SetSize(600, 64); }*/


		//// Printing Objective Texture Background
		//if (Weapon_Bar->Character_Count(Screen_Text) > 300) // Then we need more space and expand the texture background accordingly
		//{   surface()->DrawSetTexture(Large_Objective);
		//	surface()->DrawTexturedRect(0, 0, GetWide(), GetTall()); // Double the trouble
		//}
		//else
		//{   surface()->DrawSetTexture(Small_Objective);
		//	surface()->DrawTexturedRect(0, 0, GetWide(), GetTall() / 2);
		//}

		BaseClass::Paint();

	}

	
};

