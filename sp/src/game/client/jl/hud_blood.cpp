//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#include "cbase.h"
#include "hud.h"
#include "hud_macros.h"
#include "iclientmode.h"
// #include "clienteffectprecachesystem.h"
#include "c_te_effect_dispatch.h"


#include "hud_weapon_bars.h"
#include "util_shared.h"  // Random generators
#include "fmtstr.h"

using namespace vgui;

#include "hudelement.h"
#include "hud_numericdisplay.h"
#include "vgui/ISurface.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"


// This function seems only to specify from which height blood shall splat when player is Hit
// The actual Blood splat effect is done in client/fx_blood.cpp
void BloodSplatCallback( const CEffectData & data )
{
/*[JL re-enable the effect
	Msg("SPLAT!\n");

	int		x,y;

	// Find our screen position to start from
	x = XRES(320);
	y = YRES(240);

	// Draw the ammo label
	CHudTexture	*pSplat = gHUD.GetIcon( "hud_blood1" );
	
  // FIXME:  This can only occur during vgui::Paint() stuff
	pSplat->DrawSelf( x, y, gHUD.m_clrNormal);
//JL] */
}

DECLARE_CLIENT_EFFECT( "HudBloodSplat", BloodSplatCallback );





//-----------------------------------------------------------------------------
// Blood Drop(s)
//-----------------------------------------------------------------------------

class C_Hud_Blood_Drop : public Panel
{
	DECLARE_CLASS_SIMPLE(C_Hud_Blood_Drop, Panel);

public:
	C_Hud_Blood_Drop(Panel *parent, const char *panelName);
	C_Hud_Blood_Drop(Panel *parent, const char *panelName, int posx, int posy, int size);
	~C_Hud_Blood_Drop();
	void OnDeletePanel();
	void DeletePanel(); //VPANEL vguiPanel);
	virtual void Paint();
	virtual void OnThink();

private:
	// C_Hud_Blood_Drop * This_Drop;
	int xpos = 0;
	int ypos = 0;
	int Drop_Texture = 0;
	int Drop_Size = 20;

	int Tick = -1;
	int Kamikaze_Time = -1;
	int Red_Value = -1;
	int Color_Values = -1;
	int Alpha = -1;
};

DECLARE_BUILD_FACTORY(C_Hud_Blood_Drop);

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
C_Hud_Blood_Drop::C_Hud_Blood_Drop(Panel *parent, const char *panelName) : Panel(parent, panelName)
{	
	SetParent(parent);
	SetPos(xpos, ypos);
	SetSize(Drop_Size, Drop_Size);
	C_Hud_Blood_Drop::SetPaintBackgroundEnabled(false);

	Red_Value = SharedRandomInt((char *)"", 120, 255, (int)panelName);
	Color_Values = SharedRandomInt((char *)"", 40, 120, (int)panelName);
	Alpha = SharedRandomInt((char *)"", 180, 255, (int)panelName);

}


C_Hud_Blood_Drop::C_Hud_Blood_Drop(Panel *parent, const char *panelName, int posx, int posy, int size) : Panel(parent, panelName)
{
	// SetPaintBackgroundEnabled(false);
	Drop_Size = size;

	SetParent(parent);
	SetPos(posx, posy);
	SetSize(Drop_Size, Drop_Size);
	C_Hud_Blood_Drop::SetPaintBackgroundEnabled(false);

	Kamikaze_Time = gpGlobals->curtime + 4.0f; // Make Harakiri in this many seconds

	// We seed panelName into a random value
	Red_Value = SharedRandomInt((char *)"", 120, 255, (int)panelName);
	Color_Values = SharedRandomInt((char *)"", 40, 120, (int)panelName);


	Alpha = 255; // Some of them will NOT be translucent
	if (SharedRandomInt((char *)"", 0, 1, (int)panelName))
	{ Alpha = SharedRandomInt((char *)"", 180, 255, (int)panelName); }


	Drop_Texture = vgui::surface()->CreateNewTextureID();
	// When this HUD is Created it is named to something like "HUD/Blood/Drop_02", the number changes randomly; thus we use the panel name as Texture!
	vgui::surface()->DrawSetTextureFile(Drop_Texture, panelName, true, false);
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------

C_Hud_Blood_Drop::~C_Hud_Blood_Drop()
{

}


void C_Hud_Blood_Drop::DeletePanel() 
{
	C_Hud_Blood_Drop * This_Drop;

	if (This_Drop)
	{
		This_Drop->SetParent((vgui::Panel *)NULL);
		delete This_Drop;
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void C_Hud_Blood_Drop::OnThink()
{
	if (gpGlobals->curtime > Kamikaze_Time + 10) // Then it's finally time for Harakikri XD
	{
		DeletePanel();
		// BaseClass::DeletePanel(Panel_Name);
		// C_Hud_Blood_Drop::~C_Hud_Blood_Drop();
		// this->DeletePanel();
		// OnDeletePanel();

		// SetAlpha(255 - 255 * (gpGlobals->curtime - Tick));

		// if (!SharedRandomInt((char *)"", 0, 3, gpGlobals->curtime)) { C_Hud_Blood_Drop::~C_Hud_Blood_Drop(); }
		
	}

	else if (gpGlobals->curtime > Tick) // Setting next Tick
	{
		Tick = gpGlobals->curtime + 8.0f; // 0.15f;
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void C_Hud_Blood_Drop::Paint()
{   // if (!Should_Draw) return;	
	
	surface()->DrawSetColor(Red_Value, Color_Values, Color_Values, Alpha);
	surface()->DrawSetTexture(Drop_Texture);
	surface()->DrawTexturedRect(xpos, ypos, xpos + Drop_Size, ypos + Drop_Size);
}







//-----------------------------------------------------------------------------
// Purpose: Container for Blood Drops
//-----------------------------------------------------------------------------

class C_Hud_Blood : public CHudElement, public Panel
{
	DECLARE_CLASS_SIMPLE(C_Hud_Blood, vgui::Panel);

public:
	C_Hud_Blood(const char *pElementName);
	// C_Hud_Blood(Panel *parent, const char *pElementName);
	virtual void OnThink();
	int Get_Texture_ID(int i);
	void MsgFunc_Damage(bf_read &msg);
	void ApplySchemeSettings(vgui::IScheme *pScheme);	

private:
	virtual void Paint();
	bool Should_Draw = false;

	int xpos = 0;
	int ypos = 0;
	int width = 1200;
	int height = 800;

	CFmtStr Texture_Name;
	int Drop_Size = 20;
	int Patch = 0;

	int Drop_01, Drop_02, Drop_03, Drop_04, Drop_05, Drop_06, Drop_07, Drop_08, Drop_09, Drop_10,
		Drop_11, Drop_12, Drop_13, Drop_14, Drop_15, Drop_16, Drop_17, Drop_18, Drop_19, Drop_20 = 0;

	int Tick = 0;
	int Kamikaze_Time = -1;
	int Red_Value = -1;
	int Color_Values = -1;
	int Alpha = -1;


	int	i_Health = 0;
	int	i_Damage = 0;
};

DECLARE_HUDELEMENT(C_Hud_Blood);
DECLARE_HUD_MESSAGE(C_Hud_Blood, Damage);

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------

// C_Hud_Blood::C_Hud_Blood(Panel *parent, const char *pElementName) : CHudElement(pElementName), BaseClass(NULL, "Hud_Blood")
C_Hud_Blood::C_Hud_Blood(const char *pElementName) : CHudElement(pElementName), BaseClass(NULL, "Hud_Blood")
{   
	C_Hud_Blood::SetPaintBackgroundEnabled(false);
	// SetZPos(3); // Specified in HudLayout.res

	Panel *pParent = g_pClientMode->GetViewport();
	SetParent(pParent);


	Patch = vgui::surface()->CreateNewTextureID();
	vgui::surface()->DrawSetTextureFile(Patch, "HUD/Blood/Patch", true, false);


	Drop_01 = vgui::surface()->CreateNewTextureID();
	Drop_02 = vgui::surface()->CreateNewTextureID();
	Drop_03 = vgui::surface()->CreateNewTextureID();
	Drop_04 = vgui::surface()->CreateNewTextureID();
	Drop_05 = vgui::surface()->CreateNewTextureID();
	Drop_06 = vgui::surface()->CreateNewTextureID();
	Drop_07 = vgui::surface()->CreateNewTextureID();
	Drop_08 = vgui::surface()->CreateNewTextureID();
	Drop_09 = vgui::surface()->CreateNewTextureID();
	Drop_10 = vgui::surface()->CreateNewTextureID();

	Drop_11 = vgui::surface()->CreateNewTextureID();
	Drop_12 = vgui::surface()->CreateNewTextureID();
	Drop_13 = vgui::surface()->CreateNewTextureID();
	Drop_14 = vgui::surface()->CreateNewTextureID();
	Drop_15 = vgui::surface()->CreateNewTextureID();
	Drop_16 = vgui::surface()->CreateNewTextureID();
	Drop_17 = vgui::surface()->CreateNewTextureID();
	Drop_18 = vgui::surface()->CreateNewTextureID();
	Drop_19 = vgui::surface()->CreateNewTextureID();
	Drop_20 = vgui::surface()->CreateNewTextureID();

	surface()->DrawSetTextureFile(Drop_01, "HUD/Blood/Drop_01", true, false);
	surface()->DrawSetTextureFile(Drop_02, "HUD/Blood/Drop_02", true, false);
	surface()->DrawSetTextureFile(Drop_03, "HUD/Blood/Drop_03", true, false);
	surface()->DrawSetTextureFile(Drop_04, "HUD/Blood/Drop_04", true, false);
	surface()->DrawSetTextureFile(Drop_05, "HUD/Blood/Drop_05", true, false);
	surface()->DrawSetTextureFile(Drop_06, "HUD/Blood/Drop_06", true, false);
	surface()->DrawSetTextureFile(Drop_07, "HUD/Blood/Drop_07", true, false);
	surface()->DrawSetTextureFile(Drop_08, "HUD/Blood/Drop_08", true, false);
	surface()->DrawSetTextureFile(Drop_09, "HUD/Blood/Drop_09", true, false);
	surface()->DrawSetTextureFile(Drop_10, "HUD/Blood/Drop_10", true, false);

	surface()->DrawSetTextureFile(Drop_11, "HUD/Blood/Drop_11", true, false);
	surface()->DrawSetTextureFile(Drop_12, "HUD/Blood/Drop_12", true, false);
	surface()->DrawSetTextureFile(Drop_13, "HUD/Blood/Drop_13", true, false);
	surface()->DrawSetTextureFile(Drop_14, "HUD/Blood/Drop_14", true, false);
	surface()->DrawSetTextureFile(Drop_15, "HUD/Blood/Drop_15", true, false);
	surface()->DrawSetTextureFile(Drop_16, "HUD/Blood/Drop_16", true, false);
	surface()->DrawSetTextureFile(Drop_17, "HUD/Blood/Drop_17", true, false);
	surface()->DrawSetTextureFile(Drop_18, "HUD/Blood/Drop_18", true, false);
	surface()->DrawSetTextureFile(Drop_19, "HUD/Blood/Drop_19", true, false);
	surface()->DrawSetTextureFile(Drop_20, "HUD/Blood/Drop_20", true, false);


    SetHiddenBits(HIDEHUD_HEALTH | HIDEHUD_PLAYERDEAD); // | HIDEHUD_NEEDSUIT );
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void C_Hud_Blood::OnThink()
{
	C_BasePlayer *Player = C_BasePlayer::GetLocalPlayer();
	
	if (Player)
	{	if (MAX(Player->GetHealth(), 0) < 34) { Should_Draw = true; }
		else { Should_Draw = false; }
	
		Red_Value = SharedRandomInt((char *)"", 120, 255, gpGlobals->curtime);
		Color_Values = SharedRandomInt((char *)"", 40, 120, gpGlobals->curtime);	
		Alpha = 255;

		if (SharedRandomInt((char *)"", 0, 1, gpGlobals->curtime))
		{ Alpha = SharedRandomInt((char *)"", 180, 255, gpGlobals->curtime); }

		xpos = SharedRandomInt((char *)"", 0, ScreenWidth(), gpGlobals->curtime);
		ypos = SharedRandomInt((char *)"", 0, ScreenHeight(), gpGlobals->curtime);
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void C_Hud_Blood::Paint()
{	 
	return;
	if (!gpGlobals->curtime > Tick) 
	{ SetAlpha(255 - 255 * (gpGlobals->curtime - Tick - 1.0f) / 0.4f); return; }
	else SetAlpha(255);
	

	surface()->DrawSetColor(Red_Value, Color_Values, Color_Values, Alpha);
	
	
	int Apply = 0;
	float posx = 0;
	float posy = 0;


	for (int i = 2; i < SharedRandomInt((char *)"", 2, 6, gpGlobals->curtime); i++)
	// for (int i = 0; i < i_Damage * 10; i++)
	{   //if (!SharedRandomInt((char *)"", 0, 3, gpGlobals->curtime)) continue;
		Drop_Size = SharedRandomInt((char *)"", 0, 50, i);

		// Only perhaps being applied, that prevents the value from a scaling pattern
		Apply = 1; //  SharedRandomInt((char *)"", 0, 1, i * 3);
		if (Apply != 0) { posx = SharedRandomFloat((char *)"", 0, width, gpGlobals->curtime + i); }

		// Apply = SharedRandomInt((char *)"", 0, 1, i / 2);
		if (Apply != 0) { posy = SharedRandomFloat((char *)"", 0, height, gpGlobals->curtime - i); }

		// Format to append a random Int between parameter 3 and 4
		// Texture_Name = CFmtStr("HUD/Blood/Drop_0%d", SharedRandomInt((char *)"", 2, 6, gpGlobals->curtime - i));
		// surface()->DrawSetTextureFile(Get_Texture_ID(i), Texture_Name, true, false);

		surface()->DrawSetTexture(Get_Texture_ID(i));
		surface()->DrawTexturedRect(posx, posy, posx + Drop_Size, posy + Drop_Size);
	}

	Tick = gpGlobals->curtime + 30.0;
	
}


//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
int C_Hud_Blood::Get_Texture_ID(int i)
{
	switch (i)
	{
	case 1:
		return Drop_01;
		break;
	case 2:
		return Drop_02;
		break;
	case 3:
		return Drop_03;
		break;
	case 4:
		return Drop_04;
		break;
	case 5:
		return Drop_05;
		break;
	case 6:
		return Drop_06;
		break;
	case 7:
		return Drop_07;
		break;
	case 8:
		return Drop_08;
		break;
	case 9:
		return Drop_09;
		break;
	case 10:
		return Drop_10;
		break;

	case 11:
		return Drop_11;
		break;
	case 12:
		return Drop_12;
		break;
	case 13:
		return Drop_13;
		break;
	case 14:
		return Drop_14;
		break;
	case 15:
		return Drop_15;
		break;
	case 16:
		return Drop_16;
		break;
	case 17:
		return Drop_17;
		break;
	case 18:
		return Drop_18;
		break;
	case 19:
		return Drop_19;
		break;
	case 20:
		return Drop_20;
		break;
	}

	return Drop_20;
}

//-----------------------------------------------------------------------------
// Purpose: hud scheme settings
//-----------------------------------------------------------------------------
void C_Hud_Blood::ApplySchemeSettings(vgui::IScheme *pScheme)
{
	// if (!Should_Draw) return;
	
	//C_BasePlayer *Player = C_BasePlayer::GetLocalPlayer();

	//xpos = SharedRandomInt((char *)"", 0, ScreenWidth(), gpGlobals->curtime);
	//ypos = SharedRandomInt((char *)"", 0, ScreenHeight(), gpGlobals->curtime);


	//int Apply = 0;
	//float posx = 0;
	//float posy = 0;


	//for (int i = 0; i < SharedRandomInt((char *)"", 0, 14, gpGlobals->curtime); i++)
	//// for (int i = 0; i < i_Damage * 10; i++)
	//{		
	//	//if (!SharedRandomInt((char *)"", 0, 3, gpGlobals->curtime)) continue;

	//	Drop_Size = SharedRandomInt((char *)"", 0, 50, i);

	//
	//	// Only perhaps being applied, that prevents the value from a scaling pattern
	//    Apply = SharedRandomInt((char *)"", 0, 1, i * 3);
	//	if (Apply != 0) { posx = SharedRandomFloat((char *)"", 0, width, gpGlobals->curtime + i); }

	//	Apply = SharedRandomInt((char *)"", 0, 1, i /2);
	//	if (Apply != 0) { posy = SharedRandomFloat((char *)"", 0, height, gpGlobals->curtime - i); }

	//	
	//	// Format to append a random Int between parameter 3 and 4
	//	new C_Hud_Blood_Drop(this, CFmtStr("HUD/Blood/Drop_0%d", SharedRandomInt((char *)"", 2, 6, gpGlobals->curtime - i))
	//		, posx, posy, Drop_Size); // parent, name, xpos, ypos, size

	//	/*int number = 3;
	//	char numberstring[(((sizeof number) * CHAR_BIT) + 2) / 3 + 2];
	//	sprintf(numberstring, "%d", number);


	//	/*if (Apply != 0) { Dro->~C_Hud_Blood_Drop(); }
	//	else return;*/
	//}
	

	C_Hud_Blood::SetPaintBackgroundEnabled(false);
	BaseClass::ApplySchemeSettings(pScheme);	
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void C_Hud_Blood::MsgFunc_Damage(bf_read &msg)
{ i_Damage = msg.ReadByte(); }





