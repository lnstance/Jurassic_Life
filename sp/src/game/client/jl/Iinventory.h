
using namespace vgui;
#include <vgui/IVGui.h>
#include <vgui_controls/Frame.h>
#include <vgui_controls/imagepanel.h>
#include <vgui/ISurface.h>
#include <vgui/IInput.h>
#include <vgui_controls/Controls.h>
#include <vgui_controls/Frame.h>
#include <vgui_controls/Label.h>
#include <vgui_controls/ListPanel.h>
#include <vgui_controls/Button.h>


//#include "cdll_client_int.h"
//#include "networkstringtabledefs.h"
//#include "networkstringtable_clientdll.h"

//#include "gamerules.h"
//#include "usermessages.h"

#include "vgui/ILocalize.h"



// IInventory.h
class IInventory
{
public:
	virtual void		Create( vgui::VPANEL parent ) = 0;
	virtual void		Destroy( void ) = 0;
};

extern IInventory* inventory;

class CClass3DModelZone : public vgui::ImagePanel
{
    public:
        typedef vgui::ImagePanel BaseClass;

		CClass3DModelZone( vgui::Panel *pParent, const char *pName );
        virtual ~CClass3DModelZone();		
        virtual void ApplySettings( KeyValues *inResourceData );
		virtual void Paint();
};



class CInventory : public vgui::Frame
{
	DECLARE_CLASS_SIMPLE(CInventory, vgui::Frame); 
	//CInventory : This Class / vgui::Frame : BaseClass

public:
	CInventory(vgui::VPANEL parent); 	// Constructor
	~CInventory();	// Destructor
	
	void	PostRenderVGui();

	bool	Full_Screen = false;
	int		Selected_Slot = 0;
	void	SetSelecteditem(int i);

	void	Check_Available_Items();
	void	PrintPosition();
	void	Print_Command(char* value_1, int value_2);

	char* Doc_Texture = "";
	char* Doc_Model = "";

	Color Text_Color;
	Color Background_Color;
	Color	Get_Color(int i, char* Color_Name);
	bool Compare(char* value_1, char* value_2);
	char* Split_Array(char* value_1, char* value_2, char delimiter, int word_number);
	void Split_Into(char* Source, char Delimiter, char* P1 = "", char* P2 = "", char* P3 = "", char* P4 = "", char* P5 = "", char* P6 = "", char* P7 = "", char* P8 = "");


	bool Does_Player_Have(int Selcted_ID);

	// Called from a Command when the "Button_Previous_Page" and "Button_Next_Page" are pressed	
	void Get_Page_ID(bool Should_Increment);

protected:
	//VGUI overrides:
	virtual void OnTick();
	virtual void OnCommand(const char* pcCommand);
	void	Paint( void );

	virtual void LevelInit();
	virtual void Precache(void);
	void	ApplySchemeSettings(vgui::IScheme *pScheme);
	void	ApplySettings(KeyValues *ResourceData);
	Panel	*CreateControlByName(const char *controlName);
	//void	PaintBuildOverlay();

	void	UseItem(int Button, int Slot_Shift);
	void    Toggle_Inventory_Text(bool Is_On = true);
	void	Set_Controls();
	void	Update_Documents();
	void	Clear_Screen();
	int		Character_Count(char* value_1);
	bool	Makes_Sense(char* value_1, int Expected_Count);


	// UI Features added by Imperial
	virtual void OnMouseWheeled(int delta);
	void	OnCursorEntered();
	void	OnCursorExited();
	void	OnCursorMoved(int x, int y);
	void	OnMousePressed(vgui::MouseCode code);
	void	OnMouseDoublePressed(vgui::MouseCode code);
	void	OnMouseReleased(vgui::MouseCode code);
	void	OnScreenSizeChanged(int iOldWide, int iOldTall) { Set_Controls(); };

	int Tick, Type_Count, Current_Document = 0;
	float Tack = 0;
	int Current_Tab = 1;
	int Player_Item_Slots = 10;
	int Current_Button_Index = 0;
	int Resource_X, Resource_Y, Resource_Width, Resource_Height = 0;
	
	char Temporal_A[1024];
	char Temporal_B[1024];

	int	Original_X, Original_Y, Original_X_Axis, Original_Y_Axis, Original_Z_Axis = 0;
	int Mouse_X, Mouse_Y, Delta_X, Delta_Y, Text_Width, Text_Height = 0;
	bool Left_Click, Right_Click, Rotating = false;
	bool First_Load, Allow_Rotating = true;

	int Item_Count, Print_X, Print_Y, Start_Print_X, Start_Print_Y, Original_Print_X, Original_Print_Y = 0;
	float Print_Width, Print_Height;
		

	void Interpolate_Image_Location(int Slot, bool Use_Window_Position = false);
	float Image_X, Image_Y = 0;


	int Frame_X_Delta = 232;
	int Frame_Y_Delta = 70; 

	// Arrays that store data of the Pictures inside of Documents
	int Doc_Target_X[11];
	int Doc_Target_Y[11];
	int Target_Zoom_Width[11]; // Parameter 3
	int Target_Zoom_Height[11]; // Parameter 3

	// 0 = Innitiate Zoomed and stay Zoomed in; every other parameter collapses when any other Pic is clicked.
	// 1 = Zoomed and colored by the Background Color of the Paper
	// 2 = Map Icon, 3 = "i" InfoPoint Helping Hint for the Player
	int Doc_Target_Mode[11];
	char Doc_Target_Pic[11][240]; 
	char Doc_Image[11][240];

	// Draw_Target_Nr, only one Target is supposed to be zoomed at a time unless "Pic_Should_Collapse" specifies by a other flag.
	int Target_Nr = 0; 
	int Target_Width = 30;
	int Target_Height = 30;
	int Play_Button_Size = 24;


	void Center()
	{   int x,w,h; 
		GetBounds(x,x,w,h);
		SetPos(0.5f*(ScreenWidth()-w),0.5f*(ScreenHeight()-h));
	}

	void Get_Selected_Document();

	char* Get_Name(C_BaseCombatWeapon * Weapon);
	// Called by Menu_List's Key value entries when clicked by the User.
	MESSAGE_FUNC(OnItemSelected, "ItemSelected") { Get_Selected_Document(); }

	char Buffer_Text[4096];
	char* Note_Text = "";	
	// char Video_Name[1024];


private:
	void UpdateInventory();

	//====== Text Labels ======
	vgui::HFont hFont;
	vgui::HFont hFont2;
	int			charWidth;
	int			charWidth2;
	wchar_t		unicode[4096];
	void Draw_Text(int x, int y, char* text, int R, int G, int B, int Alpha);
	//=========================

	// C_BasePlayer *Player;
	vgui::ListPanel *List;
	vgui::Label *Screen_Text;
	vgui::Label *Render_Zone;
	vgui::ImageList *m_pImageList;
	ScrollBar * Scrollbar;

	int Previous_Page, Next_Page, Current_Page = 0;

	//Other used VGUI control Elements:
	const model_t *pModel;
	C_BaseAnimating *pAnimModel;
	CClass3DModelZone *m_p3DModelZone;

	int m_TCase;

	float m_fZoom;
	float m_fLeft;
	float m_fTop;
	float m_fx;
	float m_fy;
	float m_fz;

	int selecteditem;

	int X_Ratio = 1; // Means the Background Paper
	int Y_Ratio = 1;

	// Means the % difference we remove from full texture size, because Valve requires power of 2 textures like 2048x1024
	int Full_Ratio = 1;
	int Chopped_Ratio = 1;

	int Last_Tab = 1;
	int Doc_Status = 1;
	// char Buffer[1024];
	char Weapon_Info[1024];

	int Texture, Note_Background, Objective_Target, Objective_Info, Objective_Image, View_Button, Play_Button;
	int Picture_1, Picture_2, Picture_3, Picture_4, Picture_5, Picture_6, Picture_7, Picture_8, Picture_9, Picture_10;
	void Refresh_Proportions();
	void Get_Target_Images();
	int Get_Texture_ID(int i);


	void Localize_Entry(char* Entry, char* Result);
	char* Localize_Text(char* Entry, const int Buffer_Size = 1024);
	char* Localize_Document(int Document_ID, char* Entry);


	bool Allow_Zooming = true;
	bool m_bZoomMore;
	bool m_bZoomLess;
	bool m_bMoveUp;
	bool m_bMoveDown;
	bool m_bMoveLeft;
	bool m_bMoveRight;
};

extern CUtlVector<CInventory*> g_ClassInventory;



