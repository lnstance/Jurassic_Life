// The following include files are necessary to allow your MyPanel.cpp to compile.
#include "cbase.h"
#include "HUD_Macros.h"
#include "Icase_panel.h"

using namespace vgui;
#include <vgui/IVGui.h>
#include <vgui/ISurface.h>
#include <vgui/IInput.h>
#include <vgui_controls/Frame.h>
#include <vgui_controls/Label.h>
#include <vgui_controls/Scrollbar.h>
#include "vgui/ILocalize.h"
#include <KeyValues.h> 
#include "IGameUIFuncs.h" // for key bindings 
#include <igameresources.h>
#include "iclientmode.h"
//#include "iinventory.h"
#include "ButtonSlot.h"
#include "c_item_case.h"


extern IGameUIFuncs *gameuifuncs; // for key binding details 



//CMyPanel class: Tutorial example class
class CCasePanel : public vgui::Frame
{
	DECLARE_CLASS_SIMPLE(CCasePanel, vgui::Frame); 
	//CMyPanel : This Class / vgui::Frame : BaseClass

	CCasePanel(vgui::VPANEL parent); // Constructor
	~CCasePanel(){}; // Destructor

	int Mouse_X, Mouse_Y;
	virtual void OnMouseWheeled(int delta);

protected:
	virtual void OnTick();
	virtual void OnCommand(const char* pcCommand);
	void	CCasePanel::OnKeyCodePressed(KeyCode code);
	void	Paint( void );	
	
	void Center()
	{
		int x,w,h; 
		GetBounds(x,x,w,h);
		SetPos(0.5f*(ScreenWidth()-w),0.5f*(ScreenHeight()-h));
	}

	Panel	*CreateControlByName(const char *controlName);
	vgui::ScrollBar* Scrollbar;
	vgui::Label* ScrollBar_Value;
	vgui::ScrollBar * Player_Scrollbar;

	int m_iQuit;
	int selecteditem;
	int m_iState, Selected_Slot;
	int Case_Slots = 0;
	int Player_Item_Slots = 0;

	wchar_t	unicode[6];
	vgui::HFont hFont ;

private:
	void Update_ScrollBar_Value();
	int Object_Scrollbar_Size = 8;
	float Tack = 0;
	int Current_Button_Index = 0;
	void UpdateCasePanel();
	void UseItem(int Button, int Slot_Shift);
	void Scroll_Slots(vgui::ScrollBar* Scrollbar, vgui::ButtonSlot *m_pSlot, int Location);	
	char Slot[16];

};


//-----------------------------------------------------------------------------
// Class Constructor: Initializes the Panel
//-----------------------------------------------------------------------------
CCasePanel::CCasePanel(vgui::VPANEL parent): BaseClass(NULL, "CasePanel")
{   SetParent( parent );	
	SetKeyBoardInputEnabled( true );
	SetMouseInputEnabled( true );
	SetProportional( false );
	SetCloseButtonVisible( false );

	SetPaintBackgroundEnabled(false);
	SetPaintBorderEnabled(false);
	SetTitleBarVisible(false);
	SetMinimizeButtonVisible(false);
	SetMaximizeButtonVisible(false);
	SetSizeable(false);
	SetMoveable(true);

	SetVisible( true );
	SetSize(640,512);
	SetPos(ScreenWidth()/2-640/2,ScreenHeight()/2-512/2);

	
	// test for menu background
	this->FlashWindowStop();
	//this->GetParent()->GetParent()

	SetScheme(vgui::scheme()->LoadSchemeFromFile("resource/SourceScheme.res", "SourceScheme"));
	LoadControlSettings("resource/UI/CasePanel.res");

	vgui::HScheme scheme = vgui::scheme()->GetScheme("ClientScheme");
	hFont = vgui::scheme()->GetIScheme(scheme)->GetFont("HudInfoSmall");

	vgui::ivgui()->AddTickSignal( GetVPanel(), 100 );


	ScrollBar_Value = dynamic_cast<vgui::Label*>(FindChildByName("ScrollBar_Value"));

	Scrollbar = dynamic_cast<vgui::ScrollBar*>(FindChildByName("Scrollbar"));
	if (Scrollbar)
	{   // CAUTION, if 8 Slots x Range surpasses amount of MAX_JL_ITEMS the Weapons will be dropped into the Nirvana and are gone.
		Scrollbar->SetRange(1, Object_Scrollbar_Size + 1);
		Scrollbar->SetRangeWindow(1);
		Scrollbar->SetTabPosition(1);
		Scrollbar->SetButtonPressedScrollValue(1);
		Scrollbar->SetValue(1);

		Update_ScrollBar_Value();
	}



	Player_Scrollbar = dynamic_cast<vgui::ScrollBar*>(FindChildByName("Player_ScrollBar"));
	if (Player_Scrollbar)
	{   // CAUTION, 8 Slots x Range might surpass amount of MAX_JL_ITEMS 
		Player_Scrollbar->SetRange(1, Player_Scrollbar->GetTabPosition() + 1);
		Player_Scrollbar->SetRangeWindow(1);
		Player_Scrollbar->SetTabPosition(1);
		Player_Scrollbar->SetButtonPressedScrollValue(1);
		Player_Scrollbar->SetValue(1);
		// 600 is Window Height and 20 the top border
		// Player_Scrollbar->SetSize(12, 600 - 20); // Bad idea, let the res file handle it
	}
	

	Panel * Background = dynamic_cast<vgui::Panel*>(FindChildByName("Background"));
	if (Background) // Must not take over the control focus from the Window so it stays movable
	{   Background->SetKeyBoardInputEnabled(false);
		Background->SetMouseInputEnabled(false);
	}


	vgui::ButtonSlot *m_pSlot;
	for (int i = 0; i < 12; i++)
	{
		Q_snprintf(Slot, sizeof(Slot), "Slot_%d", i + 1);
		m_pSlot = dynamic_cast<vgui::ButtonSlot*>(FindChildByName(Slot));
		if (m_pSlot) { Player_Item_Slots++; } // Increment for each found slot


		Q_snprintf(Slot, sizeof(Slot), "Case_%d", i + 1);
		m_pSlot = dynamic_cast<vgui::ButtonSlot*>(FindChildByName(Slot));
		if (m_pSlot) { Case_Slots++; } // Increment for each found slot		
	}

	// DevMsg("Found %d slots.\n", Player_Item_Slots);

	// DevMsg("CasePanel has been constructed\n");
	// Replaced by a background entity in "resource/UI/CasePanel.res"
	//m_TCase = vgui::surface()->CreateNewTextureID();
	//vgui::surface()->DrawSetTextureFile(m_TCase,"hud/case",true,false);
}


//-----------------------------------------------------------------------------
// CCasePanelInterface
//-----------------------------------------------------------------------------
class CCasePanelInterface : public ICasePanel
{
private:
	CCasePanel *CasePanel;
public:
	CCasePanelInterface() { CasePanel = NULL; }

	void Create(vgui::VPANEL parent) { CasePanel = new CCasePanel(parent); }	
	void Destroy()
	{   if (CasePanel) {   CasePanel->SetParent( (vgui::Panel *)NULL); delete CasePanel; }
	}
};


static CCasePanelInterface g_CasePanel;
ICasePanel* casepanel = (ICasePanel*)&g_CasePanel;

ConVar jl_showcasepanel("jl_showcasepanel", "0", FCVAR_CLIENTDLL, "Sets the state of casepanel : <state>");


//-----------------------------------------------------------------------------
// OnTick
//-----------------------------------------------------------------------------
void CCasePanel::OnTick()
{
	BaseClass::OnTick();


	if (jl_showcasepanel.GetBool() == false && IsVisible())  // Disable Window
	{   SetVisible(false);
		engine->ClientCmd("crosshair 1\n");
		engine->ClientCmd("host_timescale 1.0\n"); // Quit Bullettime	
	} 


	else if (jl_showcasepanel.GetBool()) // Enable Window
	{   
		UpdateCasePanel(); // this needs to update constantly!

		if (!IsVisible())
		{	Center();
			SetVisible(true);
			engine->ClientCmd("crosshair 0\n"); // Hide Crosshair
			engine->ClientCmd("host_timescale 0.02\n"); // Enter Bullettime
		}
	}



	// int key = gameuifuncs->GetEngineKeyCodeForBind( "slot1" );	
	//KeyValues *data = new KeyValues("CasePanel");
	//DevMsg("%d \n",data->GetInt("Flags"));
	//data->deleteThis();
}

CON_COMMAND(ToggleCasePanel, "Toggles Case Panel on or off")
{ jl_showcasepanel.SetValue(!jl_showcasepanel.GetBool()); };



//-----------------------------------------------------------------------------
// Appeal Functions: Update Case
//-----------------------------------------------------------------------------
void CCasePanel::Scroll_Slots(vgui::ScrollBar* Scrollbar, vgui::ButtonSlot *m_pSlot, int Location)
{
	if (Scrollbar && m_pSlot && Current_Button_Index && gpGlobals->curtime > Tack)
	{
		Location = Scrollbar->GetValue();
		Location += Current_Button_Index;

		int Min = 0; // Resetting
		int Max = 0;
		Scrollbar->GetRange(Min, Max);
		if (Location < Min) { Location = Min; }
		else if (Location > Max) { Location = Max; }

		Scrollbar->SetValue(Location);
		Tack = gpGlobals->curtime + 0.006f; // Once every while, as we're in Bullet time	
		Current_Button_Index = 0;
		m_pSlot->Set_Wheel(); // Reset Delta to 0
	}
}


//-----------------------------------------------------------------------------
// Appeal Functions: Update Case
//-----------------------------------------------------------------------------
void CCasePanel::Update_ScrollBar_Value()
{   if (ScrollBar_Value)
	{   if (Object_Scrollbar_Size == 1) { ScrollBar_Value->SetText(""); }
		else
		{   // int Min, Max;
			// Scrollbar->GetRange(Min, Max);

			int Range = Object_Scrollbar_Size;
			// 0 means use the tabPosition value specified in the .res file 
			if (!Object_Scrollbar_Size) { Range = 10; } // { Range = Scrollbar->GetTabPosition() + 1; }

			Q_snprintf(Slot, sizeof(Slot), "%d/%d", Scrollbar->GetValue(), Range);

			ScrollBar_Value->SetText(Slot);
			ScrollBar_Value->SetFgColor(Color(200, 220, 220, 255)); // Light Blue
		}
	}
}


//-----------------------------------------------------------------------------
// Appeal Functions: Update Case
//-----------------------------------------------------------------------------
void CCasePanel::UpdateCasePanel()
{
	Slot[0] = 0;
	int Slot_Shift = 0;
	// int Empty_Slots = 0;

	int Position = 0;
	int Player_Scroll_Location = 0;
	vgui::ButtonSlot *m_pSlot;
	C_BasePlayer *Player = C_BasePlayer::GetLocalPlayer();





	bool Has_Visible_Weapons = false;

	// This check prevents the game from crashing  
	// Because the while state below would crash the game if there are only hidden weapons in the inventory
	for (int i = 0; i < Player_Item_Slots; i++)
	{
		if (!Player->GetWeapon(i)) continue;
		if (Player->GetWeapon(i)->GetWpnData().iItemType != 3) { Has_Visible_Weapons = true; break; }
	}
	



	if (Player_Scrollbar)
	{   Player_Scrollbar->SetVisible(true);
		Player_Scroll_Location = Player_Scrollbar->GetValue();
	}

	Position = Player_Item_Slots * (Player_Scroll_Location - 1);


	for (int i = 0; i < Player_Item_Slots; i++) // Looping through all player weapon slots
	{	
		// int Linked_Slot = -1;

		if (Player->GetWeapon(Position + i + Slot_Shift)) 
		{   
			if (Has_Visible_Weapons) 
			{   while (Player->GetWeapon(Position + i + Slot_Shift)->GetWpnData().iItemType == 3) // While weapon of type 3 = "skip"
				{   // Hard coded weapon names, + Slot_Shift because it needs to concider all previous cycles.
					Slot_Shift++;
				}
			}


			// If weapon doesen't exist this stays -1 (empty)
			// Linked_Slot = Position + i + Slot_Shift;
		} 
	



		Q_snprintf(Slot, sizeof(Slot), "Slot_%d", i + 1);
		m_pSlot = dynamic_cast<vgui::ButtonSlot*>(FindChildByName(Slot));		

		if (m_pSlot) // && Position == m_pSlot->GetSlot()) // Needs to match 
		{   // Empty_Slots = 0; // Reset

			// Indirect setting through Current_Button_Index works better then directly setting Get_Wheel_Delta() !
			Current_Button_Index = m_pSlot->Get_Wheel_Delta(); 
			Scroll_Slots(Player_Scrollbar, m_pSlot, Player_Scroll_Location);


			//if (The_Case->m_nCaseItems[ Position + i + Slot_Shift]) {   
				// This executes the "IsDroppable()" function in ButtonSlot.cpp, which compares "item_type" variable in the Weaponscript 
				// with the "item_type" entry of the Button in "resource/UI/CasePanel.res". 
				// It decides if a item belongs in Small slot (ammo), Medium (Handguns) or Large slot (Rifles).
				
			
				m_pSlot->LinkSlot(Position + i + Slot_Shift, true);  
				

		

				// Document_Type:
				// 0 = ID Cards and Keys, 1 = Notes, 2 = Objectives, 3 = Maps;

				// Document_Status:
				// Notes + Maps: 1 = Unreaded, 2 = Readed
				// Objectives: 0 = resolved, 1-3 = Priority stages, 4 = Hidden
				// Items, Keys and ID Cards: 0 = Used (can be dropped), 4-1 = Access level, the lower the number the greater access

				// Player->AddNote(1, 1, 2); return;



				//  + m_pSlot->Mouse_Wheeled_Value()); // Hehe this makes me scroll through the items :)
			//}
		}
		// else { Empty_Slots++; }

		// if (Empty_Slots > 2) { /*Msg("breaking\n");*/ break; } // Stop once the last Slot was assigned
	}



	//------------------------
	// Case Slots
	//------------------------

	int Scroll_Location = 0;
	if (Scrollbar) 
	{ 
		Scroll_Location = Scrollbar->GetValue();
	
	

		if (Player && Player->Get_Active_Case()) // Re-adjust size of its Scrollbar
		{
			Object_Scrollbar_Size = (reinterpret_cast<C_Item_Case*>(Player->Get_Active_Case()))->Get_Scrollbar_Size();
			// DevMsg("Scrollbar Size is %d\n", Object_Scrollbar_Size);
		
			if (Object_Scrollbar_Size == 1) 
			{	Scrollbar->SetVisible(false); 
				Scrollbar->SetRange(1, 1); 
			}
			else 
			{   Scrollbar->SetVisible(true);

				if (Object_Scrollbar_Size < 1) { Scrollbar->SetRange(1, 10 + 1); } // 10 is default size for Player crates			
				else { Scrollbar->SetRange(1, Object_Scrollbar_Size + 1); } // +1 cause of 0 slot..
			}
		}	
	}




	// No usage of the variable "Slot_Shift" for the Crate items, because they are not supposed to hide hide anything.
	for (int i = 0; i < Case_Slots; i++) // Case_Slots = 12
	{
		Slot[0] = 0;
		Q_snprintf(Slot, sizeof(Slot), "Case_%d", i + 1); // Append i to Slot_Type turns it into Slot
		m_pSlot = dynamic_cast<vgui::ButtonSlot*>(FindChildByName(Slot));
		// DevMsg("Loading %s\n", Slot);

		
		if (m_pSlot)
		{   
			Current_Button_Index = m_pSlot->Get_Wheel_Delta();
			Scroll_Slots(Scrollbar, m_pSlot, Scroll_Location);		
			Update_ScrollBar_Value();


			// CAUTION, if 8 Slots x Scroll_Location surpasses amount of MAX_JL_ITEMS the Weapons will be dropped into the Nirvana and are gone.
			// Case_Slots * (Scroll_Location - 1) gets us to the right area of slots
			m_pSlot->LinkSlot(Case_Slots * (Scroll_Location - 1) + i, false);
		}
	}

}


//-----------------------------------------------------------------------------
// Use Item
//-----------------------------------------------------------------------------
void CCasePanel::UseItem(int Button, int Slot_Shift)
{
	/*
	DevMsg("Use Item : %d\n", Button);
	char Slot_Name[16];
	// "Slot_%d" instead of "Case_%d" because only stuff the player already has can be equipped/used
	Q_snprintf(Slot_Name, sizeof(Slot_Name), "Slot_%d", Button);

	char temp[32];
	char type[8];

	vgui::Label *m_pSlot;
	m_pSlot = dynamic_cast<vgui::Label*>(FindChildByName(Slot_Name));
	//DevMsg("Slot : %s \n",tempslot);
	m_pSlot->GetText(temp, sizeof(temp));
	nexttoken(type, temp, '_');
	Msg("Selected : %s\n", type);
	*/

	char command[32];
	C_BasePlayer *player = C_BasePlayer::GetLocalPlayer();

	if (player->GetWeapon(Button - 1 + Slot_Shift)->GetWpnData().iItemType == 5) // 5 means usable Item
	{
		//DevMsg("Use Item\n");
		/*C_BaseCombatWeapon *weapon = player->GetWeapon(Button-1);
		weapon->PrimaryAttack();*/
		Q_snprintf(command, sizeof(command), "primaryattack %d", Button + Slot_Shift);
		engine->ClientCmd(command);
	}
	else
	{   //DevMsg("Is Not Item\n");
		//DevMsg("Weapon Switch : %s\n",temp);
		Q_snprintf(command, sizeof(command), "switchweapon %d", Button + Slot_Shift);
		engine->ClientCmd(command);
	}
}

//-----------------------------------------------------------------------------
// Commands
//-----------------------------------------------------------------------------
void CCasePanel::OnCommand(const char* pcCommand)
{
	
	// ===================== Select Weapons and Items ===================== 
	// If the command "Slot_%d" was sent by the button in .\Resource\UI\Inventory.res
	for (int i = 0; i < Player_Item_Slots; i++)
	{	Slot[0] = 0;
		Q_snprintf(Slot, sizeof(Slot), "Slot_%d", i + 1);

		if (!Q_stricmp(pcCommand, Slot))
		{
			vgui::ButtonSlot *m_pSlot;
			C_BasePlayer *Player = C_BasePlayer::GetLocalPlayer();

			m_pSlot = dynamic_cast<vgui::ButtonSlot*>(FindChildByName(Slot));
			int Position = 0;

			// Msg("Weapon is %d\n", i);
			if (m_pSlot)
			{
				Position = m_pSlot->GetSlot();

				if (Selected_Slot == Position && Player->GetWeapon(Position))
				{   // Msg("Slot is %d\n", m_pSlot->GetSlot());
					UseItem(Position + 1, 0); 
				}
			}
			Selected_Slot = Position; // Pre selection before 2nd click	

			// char command[16]; // Q_snprintf(command, sizeof(command), "switchweapon %d", i); // engine->ClientCmd(command);
			return;
		}		
	}


	/*
	int val = (dynamic_cast<vgui::ScrollBar*>( FindChildByName( "Scrollbar" ) ))->GetValue();
	//casetoinv
	for (int i=1; i<=4;i++)
	{
		Slot[0] = 0;
		Q_snprintf(Slot, sizeof(Slot), "Slot_Large_%d", i);
		if (!Q_stricmp(pcCommand, Slot))
		{
			char command[16];
			Q_snprintf(command,sizeof(command),"casetoinv %d", Case_Slots *(val-1)+i-1);	
			DevMsg("Case : %s\n",command);
			engine->ClientCmd(command);
			break;
		}
	}


	for (int i=1; i<=3;i++)
	{
		Slot[0] = 0;
		Q_snprintf(Slot, sizeof(Slot), "Slot_Small_%d", i);
		if (!Q_stricmp(pcCommand, Slot))
		{
				char command[16];
				Q_snprintf(command,sizeof(command),"casetoinv %d", Case_Slots *(val-1)+4+i-1);			
				engine->ClientCmd(command);
				break;
		}
	}*/

	if (!Q_stricmp(pcCommand, "turnoff")) { jl_showcasepanel.SetValue(0); }
	BaseClass::OnCommand( pcCommand );
}



//-----------------------------------------------------------------------------
// Scrolling the Mouse Wheel - Imperial
//-----------------------------------------------------------------------------
void CCasePanel::OnMouseWheeled(int delta) 
{
	// Mouse wheeling on the window here but NOT the button entries
	if (Scrollbar && Object_Scrollbar_Size != 1) // only 1 slot means there is no scrolling
	{
		int Location = Scrollbar->GetValue();
		Location += -delta;

		int Min = 0; // Resetting
		int Max = 0;
		Scrollbar->GetRange(Min, Max);
		if (Location < Min) { Location = Min; }
		else if (Location > Max) { Location = Max; }

		Scrollbar->SetValue(Location);
		Update_ScrollBar_Value();
	}
	
	//if (Mouse_X > Image_X && Mouse_X < Image_X + Box_Width // If is in Range on X Axis
	//	&& Mouse_Y > Image_Y && Mouse_Y < Image_Y + Box_Height) TODO

	/*
	int Win_Pos_X, Win_Pos_Y = 0;
	GetPos(Win_Pos_X, Win_Pos_Y);

	input()->GetCursorPos(Mouse_X, Mouse_Y);

	// 600 is the Window Height
	if (Win_Pos_X > Mouse_X && Mouse_X < Win_Pos_X + 130 // If is in Range on X Axis
     && Win_Pos_Y > Mouse_Y && Mouse_Y < Win_Pos_Y + 600) // and in Range on Y Axis, then we have a Click 
	{
		Msg("Delta is %d", delta);
		if (Player_Scrollbar)  
		{   // Adjust to ScrollDelta
			Player_Scrollbar->SetValue(Player_Scrollbar->GetValue() + -delta * 20);		
		}
	}

	Msg("Delta is %d\n", delta);
	*/
}

//-----------------------------------------------------------------------------
// Paint
//-----------------------------------------------------------------------------
void CCasePanel::Paint( void )
{
	//Case
	/*surface()->DrawSetColor(255,255,255,255);
	surface()->DrawSetTexture(m_TCase);
	surface()->DrawTexturedRect(0,0,512,512);*/

	//Shotgun
	/*if (jl_caseflags.GetInt() & SF_CASE_SHOTGUN)
	{
		surface()->DrawSetColor(255,255,255,255);
		surface()->DrawSetTexture(m_TCaseShotgun);
		surface()->DrawTexturedRect(426,0,426+128,512);
	}

	if (jl_caseflags.GetInt() & SF_CASE_MEDUSA)
	{
		surface()->DrawSetColor(255,255,255,255);
		surface()->DrawSetTexture(m_TCaseMedusa);
		surface()->DrawTexturedRect(600,0,600+196,196);
	}*/
	

	//Draw the item name of player inventory
	// C_BasePlayer *player = C_BasePlayer::GetLocalPlayer();

	//Case items
	//int val = (dynamic_cast<vgui::ScrollBar*>( FindChildByName( "Scrollbar" ) ))->GetValue();
	//DevMsg("Scroll : %d\n",val);

	// Print out all Weapon names as Label text
	/*
	if (player)
	{
		vgui::Label *m_pSlot;
		Slot[0] = 0;

		for (int i=0;i<3;i++)
		{
			Q_snprintf(Slot,sizeof(Slot),"Slot_Small_%d",i+1);
			m_pSlot = dynamic_cast<vgui::Label*>( FindChildByName( Slot ) );
			if (player->GetCaseItem(Case_Slots *(val-1)+4+i))
				m_pSlot->SetText(player->GetCaseItem(Case_Slots *(val-1)+4+i)->GetClassname());
			else
				m_pSlot->SetText("");
			//DevMsg("CaseItem L: %d : %s\n", Case_Slots*(val-1)+4+i,player->GetCaseItem(Case_Slots*(val-1)+4+i));
		}

		for (int i=0;i<4;i++)
		{
			Q_snprintf(Slot,sizeof(Slot),"Slot_Large_%d",i+1);
			m_pSlot = dynamic_cast<vgui::Label*>( FindChildByName( Slot ) );
			if (player->GetCaseItem(Case_Slots*(val-1)+i))
				m_pSlot->SetText(player->GetCaseItem(Case_Slots*(val-1)+i)->GetClassname());
			else
				m_pSlot->SetText("");
			//DevMsg("CaseItem P: %d\n",Case_Slots*(val-1)+i);
		}*/
	
	
		//Inventory Item
	
		/*vgui::Label *m_pSlot;
		Slot[0] = 0;
		for (int i=0;i<8;i++)
		{
			Q_snprintf(Slot,sizeof(Slot),"Slot_%d",i+1);
			m_pSlot = dynamic_cast<vgui::Label*>( FindChildByName( Slot ) );
			//m_pSlot->SetText(player->GetItem(i)->GetClassname());
			if (player->GetWeapon(i)!=NULL)
				m_pSlot->SetText(player->GetWeapon(i)->GetClassname());
			else
				m_pSlot->SetText("");
			
			if (player->GetWeapon(i))
			{
				const FileWeaponInfo_t &weaponInfo = player->GetWeapon(i)->GetWpnData();	
				wchar_t text[128];
				wchar_t *tempString = g_pVGuiLocalize->Find(weaponInfo.szPrintName);
				// setup our localized string
				if ( tempString )
				{
					_snwprintf(text, sizeof(text)/sizeof(wchar_t) - 1, L"%s", tempString);
					text[sizeof(text)/sizeof(wchar_t) - 1] = 0;
				}
				else
				{
					// string wasn't found by g_pVGuiLocalize->Find()
					g_pVGuiLocalize->ConvertANSIToUnicode(weaponInfo.szPrintName, text, sizeof(text));
				}								
			}			
		}
	}*/
}

//-----------------------------------------------------------------------------
// On Key Pressed
//-----------------------------------------------------------------------------
void CCasePanel::OnKeyCodePressed(KeyCode code)
{
	//int lastPressedEngineKey = engine->GetLastPressedEngineKey();
	/*m_iSlot1 = gameuifuncs->GetEngineKeyCodeForBind( "slot1" );
	m_iSlot2 = gameuifuncs->GetEngineKeyCodeForBind( "slot2" );
	m_iSlot3 = gameuifuncs->GetEngineKeyCodeForBind( "slot3" );
	m_iSlot4 = gameuifuncs->GetEngineKeyCodeForBind( "slot4" );
	m_iSlot5 = gameuifuncs->GetEngineKeyCodeForBind( "slot5" );
	m_iSlot6 = gameuifuncs->GetEngineKeyCodeForBind( "slot6" );*/


	/*if( lastPressedEngineKey == m_iSlot1 &&  jl_caseflags.GetInt() & SF_CASE_SHOTGUN )
	{
		engine->ClientCmd("jl_case_getshotgun");
	}
	if( lastPressedEngineKey == m_iSlot2 &&  jl_caseflags.GetInt() & SF_CASE_MEDUSA )
	{
		engine->ClientCmd("jl_case_getmedusa");
	}
	else*/

	
	if (input()->IsKeyDown(gameuifuncs->GetButtonCodeForBind("+use"))) { jl_showcasepanel.SetValue(false); }
	else { BaseClass::OnKeyCodePressed( code ); }
}

//-----------------------------------------------------------------------------
// On Key Pressed
//-----------------------------------------------------------------------------
Panel *CCasePanel::CreateControlByName(const char *controlName)
{   /*if ( Q_stricmp( controlName, "ButtonSlot" ) == 0 )
    {   DevMsg("Create ButtonSlot\n");
        return new ButtonSlot( this, controlName, controlName );
    }*/

    return BaseClass::CreateControlByName( controlName );
}