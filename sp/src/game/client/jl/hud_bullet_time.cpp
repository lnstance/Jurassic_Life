//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#include "cbase.h"
#include "hud.h"
#include "hud_macros.h"
#include "iclientmode.h"
// #include "clienteffectprecachesystem.h"
#include "c_te_effect_dispatch.h"


#include "hud_weapon_bars.h"
#include "util_shared.h"  // Random generators
#include "fmtstr.h"

using namespace vgui;

#include "hudelement.h"
#include "hud_numericdisplay.h"
#include "vgui/ISurface.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"



//-----------------------------------------------------------------------------
// Purpose: Container for Bullet Time
//-----------------------------------------------------------------------------

class C_Bullet_Time : public CHudElement, public Panel
{
	DECLARE_CLASS_SIMPLE(C_Bullet_Time, vgui::Panel);

public:
	C_Bullet_Time(const char *pElementName);
	// C_Bullet_Time(Panel *parent, const char *pElementName);
	virtual void OnThink();
	int Get_Texture(int i);
	void Draw_Icon(int texture);
	void ApplySchemeSettings(vgui::IScheme *pScheme);	


	//====== Text Labels ======
	vgui::HFont hFont;
	vgui::HFont hFont2;
	int			charWidth;
	int			charWidth2;
	wchar_t		unicode[80];
	//=========================
	void Draw_Number(int x, int y, int value, int R, int G, int B, int Alpha);

private:
	virtual void Paint();
	bool Should_Draw = true;

	const int StartY = 20; // From top
	const int EndY = 80; // From bottom
	int range = ScreenHeight() - EndY - StartY;
	float Ticking = 0;
	int Slomo = 25;

	int xpos = 0;
	int ypos = 0;
	int width = 0;
	int height = 0;

	int glass_width = 0;
	int glass_height = 0;

	int Glass_1, Glass_2, Glass_3, Glass_4, Glass_5, Glass_6, Glass_7, Glass_8, Glass_9;
	int Texture_Background;

	ImageProgressBar *Hourglass_Top;
	ImageProgressBar *Hourglass_Bottom;
	ImageProgressBar *Create_Hour_Bar(const char* Background_Texture, int x, int y, bool Visible, int width, int height, char direction, bool Colorize);

};

DECLARE_HUDELEMENT(C_Bullet_Time);


ConVar Bullet_Time_Active("Bullet_Time_Active", "0", FCVAR_CLIENTDLL, "Toggles Bullet Time on or off");
ConVar Bullet_Time_Duration("Bullet_Time_Duration", "25", FCVAR_CLIENTDLL, "Duration in Seconds");
ConVar Bullet_Time_Factor("Bullet_Time_Factor", "0.2", FCVAR_CLIENTDLL, "1.0f is normal time, this describes how slow it goes like for example 0.2");

CON_COMMAND(Bullet_Time, "Toggles Bullet Time on or off")
{
	if (Bullet_Time_Active.GetBool()) // Dactivating
	{
		engine->ClientCmd("host_timescale 1.0\n");
		engine->ClientCmd("phys_timescale 1.0\n"); // General floatyness 
		engine->ClientCmd("cl_phys_timescale 1.0\n"); // Ragdoll floatyness 

		Bullet_Time_Active.SetValue(0);
	}
	else // Activating Bullet Time
	{  
		Bullet_Time_Active.SetValue(1);
		engine->ClientCmd("phys_timescale 0.3\n"); 
		engine->ClientCmd("cl_phys_timescale 0.3\n"); 
		engine->ClientCmd(CFmtStr("host_timescale %f \n", Bullet_Time_Factor.GetFloat()));

		/*color32 white = { 255, 255, 255, 255 };
		UTIL_ScreenFade(this, white, 1.5, 0.0, FFADE_IN);*/
	}

};


//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------

// C_Bullet_Time::C_Bullet_Time(Panel *parent, const char *pElementName) : CHudElement(pElementName), BaseClass(NULL, "Hud_Bullet_Time")
C_Bullet_Time::C_Bullet_Time(const char *pElementName) : CHudElement(pElementName), BaseClass(NULL, "Hud_Bullet_Time")
{   
	C_Bullet_Time::SetPaintBackgroundEnabled(false);

	Panel *pParent = g_pClientMode->GetViewport();
	SetParent(pParent);

	Texture_Background = vgui::surface()->CreateNewTextureID();
	vgui::surface()->DrawSetTextureFile(Texture_Background, "HUD/Inventory/Bullet_Time/Hourglass_Rim", true, false);

	Glass_8 = vgui::surface()->CreateNewTextureID();
	vgui::surface()->DrawSetTextureFile(Glass_8, "HUD/Inventory/Bullet_Time/Time_1", true, false);


	// This defines size of the Weapon Icon to be same as weaponselection.cpp
	// if (range > 9 * 64) { range = 9 * 64; } // == 8*64 = 512
	height = range / 9; // 9 works
	width = (height / 20) * 14; // Keeping Proportions of 20/14

	xpos = height / 10;
	ypos = height / 4;


	glass_height = (height / 3) * 2;
	glass_width = (glass_height / 10) * 6;
	// works  glass_width = (glass_height / 10) * 6;  

	//  2.1 = + 10% away from the border
	//if (Right_Sided) { xpos = ScreenWidth() - sizeY * 2.1; }
	//else { xpos = sizeY / 10; }


	//CHud_Weapon_Bar *Weapon_Bar;
	//// Parameter 1 as 0 means Translucent Background
	//Hourglass_Top = Create_Hour_Bar(0, xpos *2, ypos + 20, true, glass_width, 20, 'N', true);
	//Hourglass_Bottom = Create_Hour_Bar(0, xpos * 2, ypos + 60, true, glass_width, 20, 'N', false);


	Hourglass_Top = new ImageProgressBar(this, "Hud_Bullet_Time");
	Hourglass_Top->SetPos(xpos * 2, ypos + 20);
	Hourglass_Top->SetSize(glass_width, 25);
	// Hourglass_Top->SetVisible(false);
	Hourglass_Top->SetProgressDirection(ProgressBar::PROGRESS_NORTH);
	Hourglass_Top->SetTopTexture("HUD/Inventory/Bullet_Time/Hourglass_Top");
	Hourglass_Top->SetBottomTexture(NULL);


	Hourglass_Bottom = new ImageProgressBar(this, "Hud_Bullet_Time");
	Hourglass_Bottom->SetPos(xpos * 2, ypos + 50);
	Hourglass_Bottom->SetSize(glass_width, 25);
	// Hourglass_Bottom->SetVisible(false);
	Hourglass_Bottom->SetProgressDirection(ProgressBar::PROGRESS_NORTH);
	Hourglass_Bottom->SetTopTexture("HUD/Inventory/Bullet_Time/Hourglass_Bottom");
	Hourglass_Bottom->SetBottomTexture(NULL);
	

    SetHiddenBits(HIDEHUD_HEALTH | HIDEHUD_PLAYERDEAD); // | HIDEHUD_NEEDSUIT );
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void C_Bullet_Time::OnThink()
{
	// "HUD/Inventory/Bullet_Time/Hourglass_Bottom"

}


void C_Bullet_Time::Draw_Number(int x, int y, int value, int R, int G, int B, int Alpha)
{
	surface()->DrawSetTextFont(hFont2);
	surface()->DrawSetTextColor(R, G, B, Alpha);
	surface()->DrawSetTextPos(x, y);

	swprintf(unicode, L"%d", value);
	surface()->DrawPrintText(unicode, wcslen(unicode));
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void C_Bullet_Time::Paint()
{
	// if (!Should_Draw) return;
	return; // Disabled Dev Mode

	surface()->DrawSetColor(255, 255, 255, 255);

	/*vgui::surface()->DrawSetTextureFile(Glass_8, CFmtStr("HUD/Inventory/Bullet_Time/Time_%d", Get_Texture(Slomo)), true, false);
	Draw_Icon(Glass_8);*/

	surface()->DrawSetTexture(Texture_Background);
	surface()->DrawTexturedRect(xpos, ypos, xpos + width, ypos + height);



	C_BasePlayer *Player = C_BasePlayer::GetLocalPlayer();
	if (!Player) { return; }
	C_BaseCombatWeapon *Weapon = Player->GetActiveWeapon();

	if (Bullet_Time_Active.GetBool()) // This needs to check fast and often 
	{
		if (Weapon && !Weapon->Clip1()) { engine->ClientCmd("Bullet_Time\n"); } // Aborting Slomo when we run out of Ammo
	}

	if (gpGlobals->curtime > Ticking) // Once every Tick
	{
		if (Bullet_Time_Active.GetBool())
		{
			if (Slomo < 1){ engine->ClientCmd("Bullet_Time\n"); } // Exit Bullettime
			else
			{
				Slomo--;
			} // Should_Draw = true;	

			Ticking = gpGlobals->curtime + Bullet_Time_Factor.GetFloat(); // Needs to be SAME as BulletTime!
		}
		else if (Slomo < Bullet_Time_Duration.GetInt())
		{   // Replenishing Slomo Time once every Tick
			Slomo++;
			// Should_Draw = false;
			Ticking = gpGlobals->curtime + 1.0; // + Bullet_Time_Factor.GetFloat(); // 5.0; // 1.0 in Normal time
		}

		float Value = (float)((25 / 25) * Slomo) / 25;;
		// float Value = (float)(Slomo / 25);
		Hourglass_Top->SetProgress(Value); // We drain remaining Slomo down to bottom of Hourglass
		Hourglass_Bottom->SetProgress(1.0f - Value);
	}

}

//-----------------------------------------------------------------------------
// Purpose: hud scheme settings
//-----------------------------------------------------------------------------
void C_Bullet_Time::ApplySchemeSettings(vgui::IScheme *pScheme)
{	
	//xpos = SharedRandomInt((char *)"", 0, ScreenWidth() - width, gpGlobals->curtime);
	//ypos = SharedRandomInt((char *)"", 0, ScreenHeight() - height, gpGlobals->curtime);
	// C_BasePlayer *Player = C_BasePlayer::GetLocalPlayer();

	C_Bullet_Time::SetPaintBackgroundEnabled(false);
	BaseClass::ApplySchemeSettings(pScheme);	
}

//-----------------------------------------------------------------------------
// Purpose: Create a new Status Bar
//-----------------------------------------------------------------------------
/*
ImageProgressBar* C_Bullet_Time::Create_Hour_Bar(const char* Background_Texture, int x = 0, int y = 0, bool Visible = true, int width = 140, int height = 14, char direction = 'E', bool Colorize = false)
{
	Hourglass_Bottom = new ImageProgressBar(this, "Hud_Bullet_Time");
	Hourglass_Bottom->SetPos(x, y);
	Hourglass_Bottom->SetSize(width, height);
	// And Color will be different, as needed to darken out the Armor Bar
	if (Colorize) { Hourglass_Bottom->Set_RGBA(200, 60, 120, 255); }
	// if (Bar_Texture){ Bar->SetTopTexture(Bar_Texture); } // Obsolete

	Hourglass_Bottom->SetProgress(10.0f); // Innitial Setup
	Hourglass_Bottom->SetVisible(Visible);


	if (Background_Texture){ Hourglass_Bottom->SetBottomTexture(Background_Texture); }
	// If innitialized with 0 as Top Texture it will also leave BG blank and define TopTexture at runtime.
	else if (!Background_Texture) { Hourglass_Bottom->SetBottomTexture(NULL); }


	if (direction == '0') // Disabled Background textures
	{
		Hourglass_Bottom->SetProgressDirection(ProgressBar::PROGRESS_EAST);
		Hourglass_Bottom->SetBottomTexture(NULL);
	}
	else if (direction == 'E') // its Horizontal
	{
		Hourglass_Bottom->SetProgressDirection(ProgressBar::PROGRESS_EAST);
	}
	else if (direction == 'W') // its Horizontal
	{
		Hourglass_Bottom->SetProgressDirection(ProgressBar::PROGRESS_WEST);
	}
	else if (direction == 'N') // its Vertical
	{
		Hourglass_Bottom->SetProgressDirection(ProgressBar::PROGRESS_NORTH);
	}
	else if (direction == 'S')
	{
		Hourglass_Bottom->SetProgressDirection(ProgressBar::PROGRESS_SOUTH);
	}


	return Hourglass_Bottom;
}
*/

//-----------------------------------------------------------------------------
// Purpose: Draw Tile Background
//-----------------------------------------------------------------------------
void C_Bullet_Time::Draw_Icon(int texture)
{   surface()->DrawSetTexture(texture);

	int Distance = (glass_width / 5) * 3;
	surface()->DrawTexturedRect(Distance, glass_width, Distance + glass_width, glass_width + glass_height);
	// works surface()->DrawTexturedRect(Distance, glass_width, Distance + glass_width, glass_width + glass_height);
	// surface()->DrawTexturedRect(Distance, glass_height, Distance + glass_width, glass_height + glass_height);
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
int C_Bullet_Time::Get_Texture(int i)
{   switch (i)
	{   case 1:
		case 2:
		case 3:
			return 1;
			break;
		case 4:
		case 5:
		case 6:
			return 2;
			break;
		case 7:
		case 8:
		case 9:
			return 3;
			break;
		case 10:
		case 11:
		case 12:
			return 4;
			break;
		case 13:
		case 14:
		case 15:
			return 5;
			break;
		case 16:
		case 17:
		case 18:
			return 6;
			break;
		case 19:
		case 20:
		case 21:
			return 7;
			break;
		case 22:
		case 23:
		case 24:
			return 8;
			break;
	}

	return 1;
}



//switch (i)
//{
//case 1:
//case 9:
//	return 1;
//	break;
//case 2:
//case 10:
//	return 2;
//	break;
//case 3:
//case 11:
//	return 3;
//	break;
//case 4:
//case 12:
//	return 4;
//	break;
//
//
//case 5:
//case 13:
//	return 5;
//	break;
//case 6:
//case 14:
//	return 6;
//	break;
//case 7:
//case 15:
//	return 7;
//	break;
//case 8:
//case 16:
//	return 8;
//	break;
//}


