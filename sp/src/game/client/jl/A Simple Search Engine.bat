
@echo off

:Begin
Title New Search
cls
:: Prompting the User for a Search Word:
color 0B
set /p Searchword="Please enter a search word:  "

:: Searching for the Variable in all of the Filetypes in the Folder this File is in, and optionally printing it into at txt file or displaing it.
findstr /m /s /i %Searchword% *.* > %UserProfile%\Desktop\Searchresults.txt


if %errorlevel%==0 (
cls
echo Found something and logged files into Searchresults.txt on your Desktop.
echo.
type %UserProfile%\Desktop\Searchresults.txt 
echo.
echo You can scroll up/down to view the Content.
echo Please type any Key to start a new Search or quit the Program with the X Button. 
) else (
cls 
title ERROR
color 0C 
echo Sorry, no matches found
echo Please type any Key to start a new Search or quit the Program with the X Button.
echo.
)

pause

goto Begin


:END
