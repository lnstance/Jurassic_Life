//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//
//=============================================================================//
//
// battery.cpp 
//
// implementation of CHudBattery class
//
#include "cbase.h"
#include "hud.h"
#include "hudelement.h"
#include "hud_macros.h"
#include "hud_numericdisplay.h"
#include "iclientmode.h"
#include "vgui_controls/AnimationController.h"
#include "vgui/ILocalize.h"

#include "hud_weapon_bars.h"


// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

#define INIT_BAT	-1

//-----------------------------------------------------------------------------
// Purpose: Displays suit power (armor) on hud
//-----------------------------------------------------------------------------
class CHudBattery : public CHudNumericDisplay, public CHudElement
{
	DECLARE_CLASS_SIMPLE( CHudBattery, CHudNumericDisplay );

public:
	CHudBattery( const char *pElementName );
	void Init( void );
	void Reset( void );
	void VidInit( void );
	void OnThink( void );
	void MsgFunc_Battery(bf_read &msg );
	bool ShouldDraw();
	bool Reset_Settings = false;
	
private:
	int		Armor;	
	int		New_Armor;
};

DECLARE_HUDELEMENT( CHudBattery );
DECLARE_HUD_MESSAGE( CHudBattery, Battery );

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CHudBattery::CHudBattery( const char *pElementName ) : BaseClass(NULL, "HudSuit"), CHudElement( pElementName )
{
	CHudBattery::SetPaintBackgroundEnabled(false);
	SetHiddenBits(HIDEHUD_HEALTH); // | HIDEHUD_NEEDSUIT );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudBattery::Init( void )
{
	HOOK_HUD_MESSAGE( CHudBattery, Battery);
	Reset();
	Armor = INIT_BAT;
	New_Armor = 0;
	Reset_Settings = true;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudBattery::Reset( void )
{   // SetLabelText(g_pVGuiLocalize->Find("#Valve_Hud_SUIT"));

	SetDisplayValue(Armor);
	Reset_Settings = true;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudBattery::VidInit( void )
{
	Reset();
}

//-----------------------------------------------------------------------------
// Purpose: Save CPU cycles by letting the HUD system early cull
// costly traversal.  Called per frame, return true if thinking and 
// painting need to occur.
//-----------------------------------------------------------------------------
bool CHudBattery::ShouldDraw( void )
{
	bool bNeedsDraw = (Armor != New_Armor) || (GetAlpha() > 0);

	return ( bNeedsDraw && CHudElement::ShouldDraw() );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudBattery::OnThink( void )
{   
	// Adjusting location of this, dependant on ConVar Settings in hud_weapon_bars.h
	CHud_Weapon_Bar *Weapon_Bar;

	if (Reset_Settings || Weapon_Bar->Get_Changes())
	{
		int set_x = 115;
		int set_y = 13; // Set directly from this c++ file	 16 for Armor Bar	
		if (Weapon_Bar->Get_Right_Sided()) { set_x = ScreenWidth() - 30; }
		if (!Weapon_Bar->Get_Top_Sided())
		{
			if (Weapon_Bar->Get_Mini_Mode()) { set_y = ScreenHeight() - 100; }
			else { set_y = ScreenHeight() - 163; }
		}

		SetPos(set_x, set_y);
		Reset_Settings = false;
	}



	if (Armor == New_Armor)
		return;

	// Not showing the full bar
	if (New_Armor == 0 || New_Armor > 85){ SetShouldDisplayValue(false); return; }
	else { SetShouldDisplayValue(true); }

	if (!New_Armor)
	{
	 	g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("SuitPowerZero");
	}
	else if (New_Armor < Armor)
	{
		// battery power has decreased, so play the damaged animation
		g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("SuitDamageTaken");

		// play an extra animation if we're super low
		if (New_Armor < 20)
		{
			g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("SuitArmorLow");
		}
	}
	else
	{
		// battery power has increased (if we had no previous armor, or if we just loaded the game, don't use alert state)
		if (Armor == INIT_BAT || Armor == 0 || New_Armor >= 20)
		{
			g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("SuitPowerIncreasedAbove20");
		}
		else
		{
			g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("SuitPowerIncreasedBelow20");
		}
	}


	Armor = New_Armor;

	SetDisplayValue(Armor);
}

//-----------------------------------------------------------------------------
// Purpose: (Imperial) Updating Armor value to new value, transmitted by this message
//-----------------------------------------------------------------------------
void CHudBattery::MsgFunc_Battery( bf_read &msg )
{
	New_Armor = msg.ReadShort();
}

