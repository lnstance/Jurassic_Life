//=============================================================================//
//
// Purpose: 
//
//=============================================================================//
#if !defined( C_IMPERIALWARE )
#define C_IMPERIALWARE
#ifdef _WIN32
#pragma once
#endif


//#if defined( CLIENT_DLL )
// #define CJL_Item_Case C_Item_Case
//#endif


#include "hudelement.h"
#include <vgui_controls/Panel.h>
#include "vgui_controls/controls.h"

using namespace vgui;
#include "vgui/ISurface.h"

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
class CImperial : public CHudElement, public vgui::Panel
{
// protected:
private:
	DECLARE_CLASS_SIMPLE(CImperial, vgui::Panel);
	
	int xpos = 0;
	int ypos = 0;
	int width = 256;
	int height = 128;


public:
	int		Color_1R, Color_1G, Color_1B, Color_1Alpha = 255;

	int     i_Item_Icon_1, i_Item_Icon_2, i_Item_Icon_3, i_Item_Icon_4;
	int		i_Item_Icon_5, i_Item_Icon_6, i_Item_Icon_7, i_Item_Icon_8;

	int     i_Item_Icon_9, i_Item_Icon_10, i_Item_Icon_11, i_Item_Icon_12;
	int     i_Item_Icon_13, i_Item_Icon_14, i_Item_Icon_15, i_Item_Icon_16;

	//====== Text Labels ======
	vgui::HFont hFont1;
	vgui::HFont hFont2;
	int			charWidth1;
	int			charWidth2;
	wchar_t		unicode[80];

	void Draw_Text(int x, int y, char* text, int R, int G, int B, int Alpha);
	void Draw_Text(int x, int y, char* text, Color Color_Value = Color(255, 255, 255, 255)); // Defaults as White

	void Draw_Number(int x, int y, int value, int R, int G, int B, int Alpha);
	void Draw_Number(int x, int y, int value, Color Color_Value = Color(255, 255, 255, 255));
	//=========================


	bool Match(char* value_1, char* value_2) // String Comparsion
	{
		if (strcmp(value_1, value_2) == 0) { return true; }
		return false;
	}
	
	int Get_Texture_ID(int i);
	Color CImperial::Text_To_Color(char* Color_1_Txt); // Untested
	int Character_Count(char* value_1);
	void Append(char* Value, char* Suffix, char* Buffer);	
	void Trimm_Prefix(char* value_1, char* value_2); // Untested
	void Trimm_Suffix(char* value_1, char* value_2); // Untested
	char* Split_Array(char* value_1, char* value_2, char delimiter, int word_number);
	// Caution! Raise the 1024 signs to either 2048 or 4096 if necessary
	char* Localize_Text(char* Entry, int Buffer_Size = 1024);
	
	
	//=========================
	void Draw_Icon(int texture)
	{   surface()->DrawSetTexture(texture);
		surface()->DrawTexturedRect(xpos, ypos, xpos + width, ypos + height);
	}

	void Draw_Icon(int xpos, int ypos, int texture, int width, int height)
	{   surface()->DrawSetTexture(texture);
		surface()->DrawTexturedRect(xpos, ypos, xpos + width, ypos + height);
	}

	void Draw_Background(int texture)
	{   surface()->DrawSetTexture(texture);
	    surface()->DrawTexturedRect(xpos, ypos, xpos + width, ypos + height);
	}


};








#endif // C_IMPERIALWARE