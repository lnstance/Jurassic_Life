//The following include files are necessary to allow your MyPanel.cpp to compile.
#include "cbase.h"
#include "IInventory.h"
#include "ButtonSlot.h"


using namespace vgui;

//#include <vgui/IVGui.h>
//#include <vgui_controls/Frame.h>
//#include <vgui_controls/imagepanel.h>
//#include <vgui/ISurface.h>
//#include <vgui/IInput.h>
//#include <vgui_controls/Controls.h>
//#include <vgui_controls/Frame.h>
//#include <vgui_controls/Label.h>
#include <vgui_controls/Scrollbar.h>
#include <vgui_controls/Panel.h>
#include <vgui_controls/ImageList.h>
#include "vgui/ILocalize.h"
#include "c_basehlplayer.h"
#include "c_baseentity.h"
#include <game/client/iviewport.h>
#include "view_shared.h"
#include "view.h"
#include "ivrenderview.h"
#include "model_types.h"
#include "IGameUIFuncs.h" // for key bindings 
#include "in_buttons.h"
#include "ivrenderview.h"
#include "r_efx.h"
#include "dlight.h"




#include "iclientmode.h" // To get Parent of HUD Screen



//#include "c_basecombatcharacter.h"
#include "tier0/memdbgon.h"
//struct model_t;
//#include "math_base.h"



extern IGameUIFuncs *gameuifuncs; // for key binding details 

CUtlVector<CInventory*> g_ClassInventory;
int Window_X, Window_Y, Model_X, Model_Y, Model_Width, Model_Height;

extern IVEfx *effects;

//CInventory class: 

CInventory::~CInventory()
{
	g_ClassInventory.FindAndRemove( this );
	if (m_pImageList)
	{
		delete m_pImageList;
	}

};		



//-----------------------------------------------------------------------------
// Constructor: Initializes the Panel
//-----------------------------------------------------------------------------
CInventory::CInventory(vgui::VPANEL parent): BaseClass(NULL, "Inventory")
{	
	C_BasePlayer *Player = C_BasePlayer::GetLocalPlayer();

	SetParent( parent );
	SetVisible(true);
	SetSizeable(true);
	SetMoveable(true);
	SetPaintBorderEnabled(false);
	SetPaintBackgroundEnabled(false);
	// BaseClass::SetPaintBackgroundEnabled(true);

	SetKeyBoardInputEnabled(true);
	SetMouseInputEnabled(true);

	SetMinimizeButtonVisible(false);
	SetMaximizeButtonVisible(false);
	SetCloseButtonVisible(false);
	SetTitleBarVisible(false);
	SetProportional(false);


	SetScheme(vgui::scheme()->LoadSchemeFromFile("resource/SourceScheme.res", "SourceScheme"));
	LoadControlSettings("resource/UI/Inventory.res");

	vgui::ivgui()->AddTickSignal( GetVPanel(), 100 );


	m_bZoomMore = false;
	m_bZoomLess = false;
	m_bMoveUp = false;
	m_bMoveDown = false;
	m_bMoveLeft = false;
	m_bMoveRight = false;

	//m_TCase = vgui::surface()->CreateNewTextureID();
	//vgui::surface()->DrawSetTextureFile(m_TCase,"hud/case",true,false);


	Note_Background = vgui::surface()->CreateNewTextureID(); // Textures are set dynamically

	Objective_Image = vgui::surface()->CreateNewTextureID(); // Is set in the Paint function

	// Need all those cause the game crashes if I reuse the same Texture ID too often per second!!
	Picture_1 = vgui::surface()->CreateNewTextureID();
	Picture_2 = vgui::surface()->CreateNewTextureID();
	Picture_3 = vgui::surface()->CreateNewTextureID();
	Picture_4 = vgui::surface()->CreateNewTextureID();
	Picture_5 = vgui::surface()->CreateNewTextureID();
	Picture_6 = vgui::surface()->CreateNewTextureID();
	Picture_7 = vgui::surface()->CreateNewTextureID();
	Picture_8 = vgui::surface()->CreateNewTextureID();
	Picture_9 = vgui::surface()->CreateNewTextureID();
	Picture_10 = vgui::surface()->CreateNewTextureID();


	// =========== Getting Global variables from the Localize.txt file ===========

	char* Buffer_Image = Localize_Text("Inventory_Background");
	if (Compare(Buffer_Image, "")){ Buffer_Image = "HUD/Inventory/Themes/01/Background"; } // Failsafe
	PrecacheMaterial(Buffer_Image);
	Texture = vgui::surface()->CreateNewTextureID();
	vgui::surface()->DrawSetTextureFile(Texture, Buffer_Image, true, false);


	Buffer_Image = Localize_Text("Weapon_Bar_Selected");
	if (Compare(Buffer_Image, "")){ Buffer_Image = "HUD/Weapons/Weapon_Bar_Selected"; }
	View_Button = vgui::surface()->CreateNewTextureID();
	vgui::surface()->DrawSetTextureFile(View_Button, Buffer_Image, true, false);
	
	
	Buffer_Image = Localize_Text("Objective_Target_Texture");
	if (Compare(Buffer_Image, "")){ Buffer_Image = "HUD/Inventory/Maps/Target"; } // Failsafe
	Objective_Target = vgui::surface()->CreateNewTextureID();
	vgui::surface()->DrawSetTextureFile(Objective_Target, Buffer_Image, true, false);


	Buffer_Image = Localize_Text("Objective_Info_Texture");
	if (Compare(Buffer_Image, "")){ Buffer_Image = "HUD/Inventory/Maps/Target_Info"; }
	Objective_Info = vgui::surface()->CreateNewTextureID();
	vgui::surface()->DrawSetTextureFile(Objective_Info, Buffer_Image, true, false);

	Buffer_Image = Localize_Text("Button_Play_Texture");
	if (Compare(Buffer_Image, "")){ Buffer_Image = "HUD/Inventory/Themes/01/Button_Play"; }
	Play_Button = vgui::surface()->CreateNewTextureID();
	vgui::surface()->DrawSetTextureFile(Play_Button, Buffer_Image, true, false);
	

	DevMsg("Map Target Texture is %s\n", Buffer_Image);
	DevMsg("Inventory has been constructed\n");
	
	

	// =========== Getting Crop Ratio of Paper Format ===========
	char* Crop_Ratio = Localize_Text("Crop_Text_Ratio");
	char Uncropped[80]; Uncropped[0] = 0;
	char Cropped[80]; Cropped[0] = 0;
	Split_Array(Crop_Ratio, Uncropped, ',', 1);
	Split_Array(Crop_Ratio, Cropped, ',', 2);

	Full_Ratio = atoi(Uncropped);
	Chopped_Ratio = atoi(Cropped);




	GetPos(Window_X, Window_Y);
	Print_X = Window_X;
	Print_Y = Window_Y;

	/*Print_X = Original_Print_X + Delta_X + Window_X;
	Print_Y = Original_Print_Y + Delta_Y + Window_Y;*/

	//====== Text Labels ======
	vgui::HScheme scheme = vgui::scheme()->GetScheme("ClientScheme");
	hFont = vgui::scheme()->GetIScheme(scheme)->GetFont("Mod_Font");
	charWidth = surface()->GetCharacterWidth(hFont, '0');

	hFont2 = vgui::scheme()->GetIScheme(scheme)->GetFont("HudHintTextLarge");
	charWidth2 = surface()->GetCharacterWidth(hFont2, '0');
	//========================


	g_ClassInventory.AddToTail( this );

	// vgui::Label *test = dynamic_cast<vgui::Label*>( FindChildByName( "ScrollBar" ) );
	// test->SetRange(0,100);
	// list = new vgui::z(this,"Menu_List");

	// Test Button
	/*
	vgui::Label *test = new vgui::Button(this,"Test","test");
	test->SetPos(10,10);
	test->SetSize(100,50);	
	if (list)
	{
		list->SetPos(207,52);
		list->SetSize(450,285);
		list->SetPaintBorderEnabled(false);
		list->SetPaintBackgroundEnabled(false);

		list->AddColumnHeader(0,"Title","",200,0,200);
		list->AddColumnHeader(1,"Texture","",10,0,200);
		list->AddColumnHeader(2,"Message","",10,0,200);
		list->SetColumnVisible(1,false);
		list->SetColumnVisible(2,false);
		list->SetFgColor(Color(255,0,0));

		list->SetColumnHeaderHeight(0);
		list->SetColumnVisible(0,false);

		list->SetDragEnabled(true);
		list->SetDropEnabled(true);


		KeyValues *Data = new KeyValues("data");
		Data->SetString("Title","Premier titre");
		Data->SetString("Texture","texture du message");
		Data->SetString("Message","premier message");
		
		list->AddItem(Data,0,0,0);
		Data->SetString("Title","Deuxi�me titre");
		list->AddItem(Data,0,0,0);
		Data->deleteThis();
		list->SetVisible(false);
	} */
	

	m_pImageList = new vgui::ImageList(false);
	if (m_pImageList)
	{	// This points into .\materials\vgui\ and grabs this .vtf name:
		m_pImageList->AddImage(vgui::scheme()->GetImage("NoteUnread",false));		// Is called by Value->SetInt("Read", 1);
		m_pImageList->AddImage(vgui::scheme()->GetImage("NoteRead",false));			// Called by 2

		m_pImageList->AddImage(vgui::scheme()->GetImage("Objective_Inactive", false));	// Called by 3 etc...
		m_pImageList->AddImage(vgui::scheme()->GetImage("Objective_Red", false));		// 4

		m_pImageList->AddImage(vgui::scheme()->GetImage("Objective_Yellow", false));	// 5
		m_pImageList->AddImage(vgui::scheme()->GetImage("Objective_Blue", false));		// 6	
		m_pImageList->AddImage(vgui::scheme()->GetImage("Objective_Green", false));		// 7
	}


	List = dynamic_cast<vgui::ListPanel*>(FindChildByName("Menu_List"));
	if (List)
	{
		//List->SetPaintBorderEnabled(false);
		//List->SetPaintBackgroundEnabled(false);		
		//g_pVGuiLocalize->GetNameByIndex(g_pVGuiLocalize->FindIndex("JL_Note"))

		//list->AddColumnToSection( m_iSectionId, "avatar", "", SectionedListPanel::COLUMN_IMAGE | SectionedListPanel::COLUMN_RIGHT, m_iAvatarWidth );
		List->AddColumnHeader(0, "Read", "", 20, 20, 20, ListPanel::COLUMN_IMAGE);
		List->AddColumnHeader(1, "Title", "  Notes", 180); // Was "#JL_Note"
		//List->AddColumnHeader(1,"Message","",10,0,200);
		//List->AddColumnHeader(2,"Texture","",10,0,200);
		//List->SetColumnVisible(1,false);
		//List->SetColumnVisible(2,false);

		List->SetMultiselectEnabled(false);
		List->SetColumnSortable(0, false);

		List->SetImageList(m_pImageList, false);
	}

	//SetDragEnabled(true);
	/*SetDropEnabled(true);

	vgui::Label* m_pSlot;
	char temp[16];
	for (int i = 0;i < Item_Count; i++)
	{
		Q_snprintf(temp,sizeof(temp),"Slot_%d",i+1);
		m_pSlot = dynamic_cast<vgui::Label*>( FindChildByName( temp ) );
		
		m_pSlot->SetDragEnabled(true);
		m_pSlot->SetDropEnabled(true);
	}*/

	vgui::ButtonSlot *m_pSlot; // Counting slots
	Player_Item_Slots = 1;

	for (int i = 0; i < 16; i++)
	{
		char temp[16]; // Slot1+0
		Q_snprintf(temp, sizeof(temp), "Slot_%d", i + 1);
		m_pSlot = dynamic_cast<vgui::ButtonSlot*>(FindChildByName(temp));

		if (m_pSlot) { Player_Item_Slots++; }
	}
	if (Player_Item_Slots < 2) { Player_Item_Slots = 2; }



	//PropertySheet* sheet = new PropertySheet();
	pAnimModel = NULL;
	m_p3DModelZone = dynamic_cast<CClass3DModelZone*>( FindChildByName( "3DModel" ) );
	if (m_p3DModelZone)
	{
		m_p3DModelZone->SetPaintBackgroundEnabled(false);
		/*m_p3DModelZone->SetKeyBoardInputEnabled(false);
		m_p3DModelZone->SetMouseInputEnabled(false);*/
	}


	Scrollbar = dynamic_cast<vgui::ScrollBar*>(FindChildByName("Scrollbar"));
	if (Scrollbar)
	{ 
		// CAUTION, if the Slots x Range surpasses amount of MAX_JL_ITEMS the Weapons will be dropped into the Nirvana and are gone.
		Scrollbar->SetRange(1, Scrollbar->GetTabPosition() + 1); // +1 cause of 0 slot..
		Scrollbar->SetRangeWindow(1);
		Scrollbar->SetTabPosition(1);
		Scrollbar->SetButtonPressedScrollValue(1);
		Scrollbar->SetValue(1);
	}



	// vgui::ImagePanel *Wallpaper = dynamic_cast<vgui::ImagePanel*>(FindChildByName("Background"));

	Render_Zone = dynamic_cast<vgui::Label*>(FindChildByName("Render_Zone"));
	if (Render_Zone) // Important otherwise this draws Mouse Controls away from MainFrame 
	{   Render_Zone->SetKeyBoardInputEnabled(false);
		Render_Zone->SetMouseInputEnabled(false);
	}

	Screen_Text = dynamic_cast<vgui::Label*>(FindChildByName("Screen_Text"));
	if (Screen_Text)// This Class draws over Inventar Panel, so we need to disable its Mouse controls, otherwise it blocks the ParentForm from receiving input!!	
	{	Screen_Text->SetMouseInputEnabled(false);
		Screen_Text->SetKeyBoardInputEnabled(false);
	}


	vgui::Panel * The_Button = FindChildByName("Button_Previous_Page");
	if (The_Button) { The_Button->SetVisible(false); } // Auto Hiding

	The_Button = FindChildByName("Button_Next_Page");
	if (The_Button) { The_Button->SetVisible(false); }

	The_Button = FindChildByName("List_Background");
	if (The_Button) 
	{   The_Button->SetVisible(false); 
		The_Button->SetKeyBoardInputEnabled(false);
		The_Button->SetMouseInputEnabled(false);
	}

	



	// === Setting Inventary Button Visability ===
	/*vgui::Panel *Tab;
	char Slot_Name[6];

	for (int i = 0; i < 5; i++)
	{
		Q_snprintf(Slot_Name, sizeof(Slot_Name), "Tab_%d", i + 1);
		Tab = dynamic_cast<vgui::Panel*>(FindChildByName(Slot_Name));

		if (Tab)
		{
			SetParent(parent);
			Tab->SetPaintBackgroundEnabled(false);
		}
	}*/

}


//-----------------------------------------------------------------------------
// Hud Scheme Settings
//-----------------------------------------------------------------------------
void CInventory::ApplySchemeSettings(vgui::IScheme *pScheme)
{
	 BaseClass::ApplySchemeSettings(pScheme);
	 // LoadControlSettings("resource/UI/Inventory.res");

	 First_Load = true;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CInventory::LevelInit()
{
	First_Load = true;

}

//-----------------------------------------------------------------------------
// Apply Scheme Settings
//-----------------------------------------------------------------------------
void CInventory::ApplySettings(KeyValues *ResourceData)
{
	// Size and Position are defined in the .res file!
	Resource_X = ResourceData->GetInt("xpos");
	Resource_Y = ResourceData->GetInt("ypos");
	Resource_Width = ResourceData->GetInt("wide");
	Resource_Height = ResourceData->GetInt("tall");

	BaseClass::ApplySettings(ResourceData);
}



//-----------------------------------------------------------------------------
// Purpose: Used for construction.
//-----------------------------------------------------------------------------

class CInventoryInterface : public IInventory
{
private:
	CInventory *Inventory;
public:
	CInventoryInterface() { Inventory = NULL; }
	
	void Create(vgui::VPANEL parent) { Inventory = new CInventory(parent); }

	void Destroy()
	{   if (Inventory)
		{   Inventory->SetParent( (vgui::Panel *)NULL);
			delete Inventory;
		}
	}

	CInventory* GetInventory() { return Inventory; }
};
static CInventoryInterface g_Inventory;
IInventory* inventory = (IInventory*)&g_Inventory;

ConVar jl_showinventory("jl_showinventory", "0", FCVAR_CLIENTDLL, "Sets the state of Inventory <state>");
extern ConVar jl_post_blur_fade_enable;



//-----------------------------------------------------------------------------
// Precache: This needs to be placed under the declaration of "jl_showinventory"
//-----------------------------------------------------------------------------
void CInventory::Precache(void)
{
	// There is a bug: If inventory was never shown and if the player picks up any item it won't be added to his inventory.
	// Auto showing on first load resolves this.
	//SetVisible(true); // Also, first time needs to auto reveal in order to trigger precache code
	//jl_showinventory.SetValue(1);

}

//-----------------------------------------------------------------------------
// OnTick: Activates and Deactivates the Inventory System!
//-----------------------------------------------------------------------------
void CInventory::OnTick()
{
	BaseClass::OnTick();

	// Permanent Rotation for Weapon and Item Models
	if (!Left_Click && !Right_Click && Rotating && Allow_Rotating) // && Current_Tab < 3) // This used to limit outside of doc area
	{ m_fy = m_fy - 0.3f; }
	

	if (jl_showinventory.GetBool() == false && IsVisible()) // Disable Window
	{	//jl_post_blur_fade_enable.SetValue(0); // disabled by -red

		SetVisible(false);
		Clear_Screen(); 

		engine->ClientCmd("crosshair 1\n"); 
		engine->ClientCmd("host_timescale 1.0\n"); // Quit Bullettime		
	}

	if (jl_showinventory.GetBool() && !IsVisible()) // Enable Window
	{   // Delete Me Show_Item();
		SetVisible(true);

		// DON'T place this into if (First_Load)! because it needs to run 1x after each map load
		Check_Available_Items();

		if (Render_Zone && Current_Tab < 3) { Render_Zone->SetText(g_pVGuiLocalize->Find("JL_Info_Null")); }

		// Center position first time the Window loads
		if (First_Load) { Center(); First_Load = false; }
		Update_Documents(); // Updates the Status of Objectives 
			
		engine->ClientCmd("crosshair 0\n"); // Hide Crosshair
		engine->ClientCmd("host_timescale 0.02\n"); // Enter Bullettime
		//UpdateInventory();
	}




	if (jl_showinventory.GetBool() /*&& HasFocus()*/ && !engine->IsPaused()) // Update Window
	{
		if (Current_Tab < 2) { UpdateInventory(); } // Only for Weapons/Items	
		else { Scrollbar->SetVisible(false); }
	
		//if (input()->WasKeyPressed(gameuifuncs->GetButtonCodeForBind("inventory"))/* && code!=BUTTON_CODE_NONE*/)
		//if (input()->IsKeyDown(gameuifuncs->GetButtonCodeForBind("inventory"))/* && code!=BUTTON_CODE_NONE*/)
		if (input()->WasKeyTyped(gameuifuncs->GetButtonCodeForBind("ToggleInventory"))) { jl_showinventory.SetValue(false); } // Show Window		
		// if (input()->IsKeyDown(gameuifuncs->GetButtonCodeForBind("+use"))) { jl_showinventory.SetValue(false); }
	
	
		// Zoom
		if (input()->IsKeyDown(gameuifuncs->GetButtonCodeForBind("+reload")) || m_bZoomMore) m_fZoom--;
	    if (input()->IsKeyDown(gameuifuncs->GetButtonCodeForBind("+use")) || m_bZoomLess) m_fZoom++;
		// Making sure model doesen't clip into the screen (too large) or zooms away (too small)
		/*if (m_fZoom>100) m_fZoom = 100; // Better prevent it below so there is no flickering
		if (m_fZoom<10) m_fZoom = 10;*/

		// Move Top / Bottom or  Right / Left 
		if (input()->IsKeyDown(gameuifuncs->GetButtonCodeForBind("+lookup"))) m_fTop += 0.1;
		if (input()->IsKeyDown(gameuifuncs->GetButtonCodeForBind("+lookdown"))) m_fTop -= 0.1;
		if (input()->IsKeyDown(gameuifuncs->GetButtonCodeForBind("+right"))) m_fLeft += 0.25;
		if (input()->IsKeyDown(gameuifuncs->GetButtonCodeForBind("+left"))) m_fLeft -= 0.25;
		

		// Rotate on X, Y, Z Axis	
		if (input()->IsKeyDown(gameuifuncs->GetButtonCodeForBind("forward")) || m_bMoveUp) m_fx++;
		if (input()->IsKeyDown(gameuifuncs->GetButtonCodeForBind("back")) || m_bMoveDown) m_fx--;		
		if (input()->IsKeyDown(gameuifuncs->GetButtonCodeForBind("moveright")) || m_bMoveRight) m_fy++;
		if (input()->IsKeyDown(gameuifuncs->GetButtonCodeForBind("moveleft")) || m_bMoveLeft) m_fy--;
		if (input()->IsKeyDown(gameuifuncs->GetButtonCodeForBind("walk")) || m_bMoveUp) m_fz++;
		if (input()->IsKeyDown(gameuifuncs->GetButtonCodeForBind("duck")) || m_bMoveDown) m_fz--;			
	}

}



//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CInventory::OnCursorEntered()
{	
	Rotating = false;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CInventory::OnCursorExited()
{   // Allow Model Rotation but block Movement outside of Borders for Tab 1 and 2, this stops rotation
	// if (Current_Tab > 2) { 
	Left_Click = false; 

	Right_Click = false;	
	Rotating = true;
}


//-----------------------------------------------------------------------------
// Scrolling the Mouse Wheel - Imperial
//-----------------------------------------------------------------------------
void CInventory::OnMouseWheeled(int delta)
{	/* // This Snap function centers the model
	float Old_Zoom = m_fZoom;
	input()->GetCursorPos(Mouse_X, Mouse_Y);

	int x = Mouse_X - m_fLeft;    // Where location of the mouse in the pictureframe
	int y = Mouse_Y - m_fTop;

	int Old_Image_X = (int)(x / Old_Zoom);  // Where in the IMAGE is now
	int Old_Image_Y = (int)(y / Old_Zoom);

	int New_Image_X = (int)(x / m_fZoom);     // Where in the IMAGE will it be when the new zoom i made
	int New_Image_Y = (int)(y / m_fZoom);

	m_fLeft = New_Image_X - Old_Image_X;  // Where to move image to keep focus on one point
	m_fTop = New_Image_Y - Old_Image_Y;
	*/

	// if (Current_Tab < 3) { // Allow model movement in other tabs


		int Max_Zoom = 120;
		if (Current_Tab == 1) { Max_Zoom = 200; } // Exception for Items, they are allowed to zoom larger


	    m_fZoom += -delta * 2; // *2 to double Zoom Speed
		// Making sure model doesen't clip into the screen (too large) or zooms away (too small)
		if (m_fZoom > Max_Zoom) { m_fZoom = Max_Zoom; }
		if (m_fZoom < 20) { m_fZoom = 20; }


		/* // Wont work because it lacks ScaleTransform and a Interpolation mode
		float Old_Zoom = Print_Height;
		Print_Height += -delta * 20; // New Zoom

		input()->GetCursorPos(Mouse_X, Mouse_Y);

		int x = Mouse_X - Print_X;   // Where location of the mouse in the pictureframe
		int y = Mouse_Y - Print_Y;

		int Old_Image_X = (int)(x / Old_Zoom);  // Where in the IMAGE is now
		int Old_Image_Y = (int)(y / Old_Zoom);

		int New_Image_X = (int)(x / Print_Height);     // Where in the IMAGE will it be when the new zoom i made
		int New_Image_Y = (int)(y / Print_Height);


		Print_X = New_Image_X - Old_Image_X + Print_X;  // Where to move image to keep focus on one point
		Print_Y = New_Image_Y - Old_Image_Y + Print_X;
		*/
	// } else 

	if (Allow_Zooming) // && Current_Tab > 2) 
	{	
		if (X_Ratio > Y_Ratio)
		{
			if ((Print_Width / X_Ratio) * Y_Ratio > ScreenHeight() / 10 * 9) // Resetting
			{
				int Win_X, Win_Y;
				GetSize(Win_X, Win_Y);
				Print_Width = Win_X - Frame_X_Delta;
				Print_Height = Win_Y - Frame_Y_Delta;

				Target_Width = 30; // Resetting Objective Target Size
				Target_Height = 30;
			}
			else // Just Zoom
			{
				Print_Width += -delta * 20; 
				Print_Height += -delta * 20;
				Target_Width += -delta; // Monotonous for all Images
				Target_Height += -delta;
				// if (Print_Width < 70) { Print_Width = 70; }

				// Must not shrink more then its largest image
				int Largest_Image = 70;
				for (int i = 1; i < 11; i++) 
				{   if (Largest_Image < Target_Zoom_Width[i]){ Largest_Image = Target_Zoom_Width[i]; } // New largest
					if (Print_Width < Largest_Image) { Print_Height = Largest_Image; Print_Width = Largest_Image; }
					
					/*Target_Zoom_Width[i] += -delta * 10;
					Target_Zoom_Height[i] += -delta * 10;
					
					if (Target_Zoom_Width[i] > Print_Width)
					{ Target_Zoom_Width[i] = Print_Width - 40; Target_Zoom_Height[i] = Print_Height - 40; }*/

					// Disabled all location scrolling of Sub-Images here, because instead scrolling happens passively like this:
					// Doc_Target_X[i] and Doc_Target_Y[i] are percent values (0-100) that state where in the document the image top left corner is palaced.
					// Then the Print() function below setts their X and Y location according to the scale of either Print_Width or Target_Height
					/*Doc_Target_X[i] += -delta * 6;
					Doc_Target_Y[i] += -delta * 6;*/
				}

					
				if (Print_Width < 70)
				{ Print_Width = 70; Target_Height = 20; Target_Width = 20; } // Doc_Target_X[Target_Nr] = 20, Doc_Target_Y[Target_Nr] = 20; }
				if (Target_Width < 20) { Target_Height = 20; Target_Width = 20; } // Reset
				else if (Target_Width > 50){ Target_Height = 50; Target_Width = 50; }
			}
		}
		else // if (Y_Ratio > X_Ratio) 
		{   // This is the Zoom Value for Documents  		
			if ((Print_Height / Y_Ratio) * X_Ratio > ScreenWidth() / 5 * 4) // Resetting when Max Zoom was reached
			{
				int Win_X, Win_Y;
				GetSize(Win_X, Win_Y);
				Print_Width = Win_X - Frame_X_Delta;
				Print_Height = Win_Y - Frame_Y_Delta;

				Target_Width = 30;
				Target_Height = 30;
			}
			else
			{   Print_Height += -delta * 20; // New Zoom
				Print_Width += -delta * 20;
				Target_Width += -delta; // Monotonous for all Images
				Target_Height += -delta;
				// if (Print_Height < 70) { Print_Height = 70; }// Assuring Minimal Zoom	
				
				int Largest_Image = 70;
				for (int i = 1; i < 11; i++)
				{   if (Largest_Image < Target_Zoom_Height[i]){ Largest_Image = Target_Zoom_Height[i]; } // New largest
					if (Print_Height < Largest_Image) { Print_Height = Largest_Image; Print_Width = Largest_Image; }

					/*Target_Zoom_Width[i] += -delta * 10;
					Target_Zoom_Height[i] += -delta * 10;

					if (Target_Zoom_Height[i] > Print_Height)
					{
						Target_Zoom_Height[i] = Print_Width - 40; Target_Zoom_Height[i] = Print_Height - 40;
					}*/
				}
		
				if (Print_Height < 70) // Assuring Minimal Zoom
				{ Print_Height = 70; Target_Height = 20; Target_Width = 20; } 

				if (Target_Height < 20) { Target_Height = 20; Target_Width = 20; }
				else if (Target_Height > 50){ Target_Height = 50; Target_Width = 50; }
			}
		}


		// DevMsg("Width is %d and Height is %d, DocX is %d DocY is %d\n", Print_Width, Print_Height, Doc_Target_X, Doc_Target_Y);
		// DevMsg("Width is %d and Height is %d\n", Print_Width, Print_Height);
	}

}

void CInventory::OnMousePressed(vgui::MouseCode code)
{	input()->GetCursorPos(Mouse_X, Mouse_Y);
	Original_Print_X = Print_X;
	Original_Print_Y = Print_Y;

	if (code == MOUSE_LEFT && !Left_Click)
	{	Left_Click = true;
		// DevMsg("Left Click\n");

		Original_X_Axis = m_fx; 
		Original_Y_Axis = m_fy; 
		Original_Z_Axis = m_fz;
	} 
	if (code == MOUSE_RIGHT && !Right_Click)
	{   Right_Click = true;
		// DevMsg("Right Click\n");
		
		Original_X = m_fLeft;
		Original_Y = m_fTop;			
	}	
} 

void CInventory::OnMouseDoublePressed(vgui::MouseCode code)
{
	OnMouseReleased(code);
}


void CInventory::Interpolate_Image_Location(int Slot, bool Use_Window_Position) //, float Image_X, float Image_Y)
{	
	// Preparing all used Variables
	int Win_Pos_X, Win_Pos_Y = 0;

	// Target_Nr isn't supposed to concider window position, contrary to all other items.
	if (Use_Window_Position)
	{   GetPos(Window_X, Window_Y);
		Win_Pos_X = Window_X;
		Win_Pos_Y = Window_Y;
	}


	float Shorter_X = (Doc_Target_X[Slot] / 2); // Normal measures
	float Shorter_Y = (Doc_Target_Y[Slot] / 2);

	if (X_Ratio / Y_Ratio == 2) // If it's seems to be an A4 paper, then it's indeed shorter
	{
		Shorter_X = (float)(Doc_Target_X[Slot] / Full_Ratio) * Chopped_Ratio;
		Shorter_Y = (float)(Doc_Target_Y[Slot] / Full_Ratio) * Chopped_Ratio;
	}


	Image_X = Win_Pos_X + Print_X + ((Print_Height / 100) * (Doc_Target_X[Slot] / 2)) - (Target_Zoom_Width[Slot] / 2);
	Image_Y = Win_Pos_Y + Print_Y + ((Print_Height / 100) * Shorter_Y) - (Target_Zoom_Height[Slot] / 2);

	if (X_Ratio > Y_Ratio) // Shifting Ratio
	{   Image_X = Win_Pos_X + Print_X + ((Print_Width / 100) * Shorter_X) - (Target_Zoom_Width[Slot] / 2);
		Image_Y = Win_Pos_Y + Print_Y + ((Print_Width / 100) * (Doc_Target_Y[Slot] / 2)) - (Target_Zoom_Height[Slot] / 2);
	}
}


void CInventory::OnMouseReleased(vgui::MouseCode code)
{	// input()->GetCursorPos(Mouse_X, Mouse_Y);

	if (Left_Click || Right_Click)
	{   if (Current_Tab > 1) // Only for Documents, not for Weapons or Items
		{
					
			// DevMsg("%s\n", Video_Name);
			// if (Makes_Sense(Video_Name, 5))
			/* // Outdated and replaced by the loop below
			if (Character_Count(Video_Name) > 2) // Play a Video
			{
				GetPos(Window_X, Window_Y);
				Image_X = Window_X + Print_X + (Text_Width / 2);
				Image_Y = Window_Y + Print_Y + (Text_Height / 2);
			
				if (Mouse_X > Image_X - Play_Button_Size && Mouse_X < Image_X + Play_Button_Size // If is in Range on X Axis
					&& Mouse_Y > Image_Y - Play_Button_Size && Mouse_Y < Image_Y + Play_Button_Size) // and in Range on Y Axis, then we have a Click  38
				{
					char szClientCmd[256];
					Q_snprintf(szClientCmd, sizeof(szClientCmd),
						"playvideo_exitcommand %s ent_fire %s __MovieFinished\n", Video_Name, this->GetName());

					if (!engine->IsPaused()) { engine->ClientCmd("pause\n"); }
					engine->ClientCmd(szClientCmd); // Send command string from above

					if (Right_Click) { Right_Click = false; }
					if (Left_Click) { Left_Click = false; }
					return; // Ignoring the code below
				}
			}
			*/
		

			int Box_Width, Box_Height;
			
			for (int i = 1; i < 11; i++)
			{  
				// Updating Location into Image_X and Image_Y
				// The boolean value just specifies that we need to concider Window X and Y position.
				Interpolate_Image_Location(i, true);


				if (Compare(Doc_Target_Pic[i], "Audio") || Compare(Doc_Target_Pic[i], "audio"))
				{
					if (Mouse_X > Image_X && Mouse_X < Image_X + Target_Zoom_Width[i]
						&& Mouse_Y > Image_Y && Mouse_Y < Image_Y + Target_Zoom_Width[i])
					{
						C_BasePlayer *Player = C_BasePlayer::GetLocalPlayer();
						Player->EmitSound(Doc_Image[i]); // Doc_Image[] is actually a audio file name link in this case.
					}
				}

				else if (Compare(Doc_Target_Pic[i], "Video") || Compare(Doc_Target_Pic[i], "video"))
				{   // Doc_Image[i] targets Paramter 5, which is supposed to be just the NAME of a video in .\Moddir\media WITHOUT suffix									
					if (Mouse_X > Image_X && Mouse_X < Image_X + Target_Zoom_Width[i] 
						&& Mouse_Y > Image_Y && Mouse_Y < Image_Y + Target_Zoom_Width[i]) 
					{									
						char szClientCmd[256];
						Q_snprintf(szClientCmd, sizeof(szClientCmd),
							"playvideo_exitcommand %s ent_fire %s __MovieFinished\n", Doc_Image[i], this->GetName());

						if (!engine->IsPaused()) { engine->ClientCmd("pause\n"); }
						engine->ClientCmd(szClientCmd);
					}
				}					
				else if (!Compare(Doc_Image[i], "") && Doc_Target_Mode[i]) // If has any Picture and is not in permanent Mode
				{		
										
					if (Target_Nr == i) // Switching Previw image Size
					{   Box_Width = Target_Zoom_Width[i];
						Box_Height = Target_Zoom_Height[i];
					}
					else
					{   Box_Width = Target_Width;
						Box_Height = Target_Height;
					}


					if (Mouse_X > Image_X && Mouse_X < Image_X + Box_Width // If is in Range on X Axis
						&& Mouse_Y > Image_Y && Mouse_Y < Image_Y + Box_Height) // and in Range on Y Axis, then we have a Click
					{
						// Drawn below in the Paint function.
						if (Target_Nr == i) // Toggle Draw_Target_Nr
						{							
							Doc_Target_X[i] += Target_Zoom_Width[i] / 11; // Centering was / 4
							Doc_Target_Y[i] += Target_Zoom_Height[i] / 11; // Was / 8
							Target_Nr = 0; // Freed now for the next entity the player clicks
						}
						else
						{	// Revert Changes to former Target_Nr												
							Doc_Target_X[Target_Nr] += Target_Zoom_Width[Target_Nr] / 11; // UnCentering 
							Doc_Target_Y[Target_Nr] += Target_Zoom_Height[Target_Nr] / 11;

							Target_Nr = i; // Only set this as selected if Mode is not set to permanent mode (0)												
							Doc_Target_X[i] -= Target_Zoom_Width[i] / 11; // Centering the new One
							Doc_Target_Y[i] -= Target_Zoom_Height[i] / 11;							
						}

						// DevMsg("Clicked Slot %d \n", i);
					}				
				}
			}

			// DevMsg("Mouse is %d %d and Target is %d %d\n", Mouse_X, Mouse_Y, Start_X, Start_Y);
			// Msg("Located at %d %d ", Print_X, Print_Y); 
		}	
	} 

	if (Right_Click) { Right_Click = false; }
	if (Left_Click) { Left_Click = false; }
	
	/*DevMsg("Mouse Up\n");
	DevMsg("Mouse at %d_%d\n", Mouse_X, Mouse_Y);*/
}

void CInventory::OnCursorMoved(int x, int y)
{
	// the distance the mouse was moved since mouse was pressed
	Delta_X = x - Mouse_X;
	Delta_Y = y - Mouse_Y;

	//if (Current_Tab < 2) { // Moving/Rotating Models
	
		if (Left_Click) // Rotating 
		{	// That X axis is kind of useless for the View of weapons so I ignored it
			// m_fx = Original_X_Axis + (Delta_Y + Window_Y) / (m_fZoom / 5 * 3);

			m_fy = Original_Y_Axis + ((Delta_X + Window_X) / 2); // This is actually the X Axis.
			m_fz = Original_Z_Axis + ((Delta_Y + Window_Y) / 2); // Some models have this axis inverted..	
		}

		if (Right_Click) // Moving the Model
		{
			int Speed_Boost = 2;
			// This inverts the scroll limitation 
			int Scroll_Delta = 100 - m_fZoom;
			if (Scroll_Delta < 34)
			{
				Speed_Boost = 1;
				if (Scroll_Delta < 11) { Scroll_Delta = 10; }
			}
			else if (Scroll_Delta > 66) { Speed_Boost = 1; }

			m_fLeft = (Original_X + (Delta_X + Window_X) / ((m_fZoom / 100) * Scroll_Delta) * Speed_Boost);
			m_fTop = (Original_Y - (Delta_Y + Window_Y) / ((m_fZoom / 100) * Scroll_Delta) * Speed_Boost);
		}
	// } else
		
    if (Current_Tab > 1) // Moving/Rotating Documents, Objectives, Maps
	{	if (Left_Click || Right_Click) 
		{	GetPos(Window_X, Window_Y);

			Print_X = Original_Print_X + Delta_X + Window_X;
			Print_Y = Original_Print_Y + Delta_Y + Window_Y;

			// This prevents Documents from colliding with the border.
			// Disabled collision check on Y axis on request of Robert
			// if (Print_X < Start_Print_X)
			if (Print_X < -(Print_Width * 2)) { Print_X = Start_Print_X; } // If too far on the outside
			// if (Print_Y < Start_Print_Y) { Print_Y = Start_Print_Y; } // Can't allow moving up because it would draw over the Tab buttons.
		}
	}

	
}


//-----------------------------------------------------------------------------
// Console Commands
//-----------------------------------------------------------------------------
CON_COMMAND(ToggleInventory, "Toggles CasePanel on or off")
{
	jl_showinventory.SetValue(!jl_showinventory.GetBool());
	if (jl_showinventory.GetBool())
	{
	    //GetVPanel()
		if (g_Inventory.GetInventory())
			vgui::ivgui()->AddTickSignal( g_Inventory.GetInventory()->GetVPanel(), 10 );
	}
	else
	{
		if (g_Inventory.GetInventory())
			vgui::ivgui()->AddTickSignal( g_Inventory.GetInventory()->GetVPanel(), 250 );
	}
};

CON_COMMAND(show_weapon_preview_position, "show inventory weapon preview position")
{
	if (g_Inventory.GetInventory())
	{
		g_Inventory.GetInventory()->PrintPosition();
	}
};



//-----------------------------------------------------------------------------
// Appeal Functions: UpdateInventory
//-----------------------------------------------------------------------------
void CInventory::UpdateInventory()
{
	
	C_BasePlayer *pPlayer = C_BasePlayer::GetLocalPlayer();
	vgui::ButtonSlot *m_pSlot;
	char temp[16]; // Slot1+0

	int Position = 0;
	int Slot_Shift = 0;
	int Empty_Slots = 0;
	int Linked_Slot = -1;
	int Scroll_Location = 0;

	if (Scrollbar) 
	{   Scrollbar->SetVisible(true);
		Scroll_Location = Scrollbar->GetValue();	
	}
	

	Position = Player_Item_Slots * (Scroll_Location - 1);


	bool Has_Visible_Weapons = false;


	// This check prevents the game from crashing  
	// Because the while state below would crash the game if there are only hidden weapons in the inventory
	for (int i = 0; i < Player_Item_Slots; i++)
	{
		if (!pPlayer->GetWeapon(i)) continue;	
		if (pPlayer->GetWeapon(i)->GetWpnData().iItemType != 3) { Has_Visible_Weapons = true; break; }		
	}
	if (!Has_Visible_Weapons) { return; }





	for (int i = 0; i < Player_Item_Slots; i++)
	{
		if (pPlayer->GetWeapon(Position + i + Slot_Shift))
		{  	//if (Compare(Get_Name(pPlayer->GetWeapon(i + Slot_Shift)), "Hands")) { Slot_Shift++; Case_Slots--; }
			//if (Compare(Get_Name(pPlayer->GetWeapon(i + Slot_Shift)), "Menu")) { Slot_Shift++; Case_Slots--; } // Ignore	
			//if (Compare(Get_Name(pPlayer->GetWeapon(i + Slot_Shift)), "Card")) { Slot_Shift++; Case_Slots--; }

		
			while (pPlayer->GetWeapon(Position + i + Slot_Shift)->GetWpnData().iItemType == 3) // While weapon of type 3 = "skip"
			{   // Hard coded weapon names, + Slot_Shift because it needs to concider all previous cycles.
				Slot_Shift++;
			}
		

		
			Linked_Slot = Position + i + Slot_Shift;


			/* // Dropped feature: Sorting between Items and Weapons for different tabs
			for (int Slot = 0; Slot < Case_Slots; Slot++)
			{
			if (!pPlayer->GetWeapon(i + Slot_Shift)) { continue; } // Failsafe
			else if (Current_Tab == 1 && pPlayer->GetWeapon(i + Slot_Shift)->GetWpnData().iItemType) { Slot_Shift++; } // In Weapon Tab hide all Items
			else if (Current_Tab == 2 && !pPlayer->GetWeapon(i + Slot_Shift)->GetWpnData().iItemType) { Slot_Shift++; }// In Item Tab hide all Weapons
			else break;
			}
			*/
		} else { Linked_Slot = -1; } // If weapon doesen't exist this stays -1 (empty)
			

		Q_snprintf(temp, sizeof(temp), "Slot_%d", i + 1);
		m_pSlot = dynamic_cast<vgui::ButtonSlot*>(FindChildByName(temp));

		if (m_pSlot)
		{   Empty_Slots = 0; // reset

			// Indirect setting through Current_Button_Index works better then directly setting Get_Wheel_Delta() !
			Current_Button_Index = m_pSlot->Get_Wheel_Delta();
			// DevMsg("Value is %d\n", Current_Button_Index);


			if (Scrollbar && m_pSlot && Current_Button_Index && gpGlobals->curtime > Tack)
			{
				Scroll_Location = Scrollbar->GetValue();
				Scroll_Location += Current_Button_Index;

				int Min = 0; // Resetting
				int Max = 0;
				Scrollbar->GetRange(Min, Max);
				if (Scroll_Location < Min) { Scroll_Location = Min; }
				else if (Scroll_Location > Max) { Scroll_Location = Max; }

				Scrollbar->SetValue(Scroll_Location);
				Tack = gpGlobals->curtime + 0.006f; // Once every while, as we're in Bullet time	
				Current_Button_Index = 0;
				m_pSlot->Set_Wheel(); // Reset Delta to 0
			}



			m_pSlot->LinkSlot(Position + i + Slot_Shift, true); //  + m_pSlot->Mouse_Wheeled_Value()); // Hehe this makes me scroll through the items :)
			//else { m_pSlot->LinkSlot(Player_Item_Slots * (Scroll_Location - 1) + i + Slot_Shift, false, true); Used_Slots++; } // This jumps over Slots
			// if (i + 1 - Slot_Shift > Player_Item_Slots) { return; } // Cause there are only 11 slots we can assign.			
		}
		else { Empty_Slots++; }

		if (Empty_Slots > 3) { /*Msg("breaking\n");*/ break; } // Stop once the last Slot was assigned			
	}
		

}



//-----------------------------------------------------------------------------
// Use Item
//-----------------------------------------------------------------------------
void CInventory::UseItem(int Button, int Slot_Shift)
{ 	
	
	C_BasePlayer *player = C_BasePlayer::GetLocalPlayer();
	C_BaseCombatWeapon *weapon = player->GetWeapon(Button - 1 + Slot_Shift);

	if (!player || !weapon) { return; } // Prevent Crash if no item/weapon is available

	else if (player->GetWeapon(Button - 1 + Slot_Shift)->GetWpnData().iItemType == 5) // 5 means usable Item
	{
		char command[32];

		//DevMsg("Use Item\n");
		/*C_BaseCombatWeapon *weapon = player->GetWeapon(Button-1);
		weapon->PrimaryAttack();*/
		if (pAnimModel)
		{
			pAnimModel->Remove();
			pAnimModel = NULL;
		}
		Q_snprintf(command, sizeof(command), "primaryattack %d", Button + Slot_Shift);
		engine->ClientCmd(command);
	}
	else
	{  
		char command[32];

		//DevMsg("Is Not Item\n");
		//DevMsg("Weapon Switch : %s\n",temp);
		Q_snprintf(command, sizeof(command), "switchweapon %d", Button + Slot_Shift);
		engine->ClientCmd(command);
	}
}


//-----------------------------------------------------------------------------
// On Command
//-----------------------------------------------------------------------------
void CInventory::Toggle_Inventory_Text(bool Is_On)
{	
	// Compare last Weapon_Info, if first char is not 0   if (Weapon_Info[0]) 
	if (!Is_On) { Render_Zone->SetText(""); return; } // Toggeling to empty string		


	C_BasePlayer *player = C_BasePlayer::GetLocalPlayer();

	char Collumn_1[2048];
	char Collumn_2[2048];

	if (Current_Tab == 1) // Weapons
	{
		Collumn_1[0] = 0;
		// Q_snprintf(info, sizeof(info), "%s_Info", Get_Name(player->GetWeapon(Selected_Slot))); // Use Localized Text
		Q_snprintf(Collumn_1, sizeof(Collumn_1), "%s_Info", player->GetWeapon(Selected_Slot)->GetWpnData().szPrintName); // Use Unlocalized Text
		// Render_Zone->SetText(Weapon_Info); return; // Just displaying feedback

		Collumn_2[0] = 0;
		Localize_Entry(Collumn_1, Collumn_2); // Localizing WeaponName_Info into Collumn_2

		// Then localizing Weapon Name into Collumn_1
		Localize_Entry((char*)player->GetWeapon(Selected_Slot)->GetWpnData().szPrintName, Collumn_1);


		Weapon_Info[0] = 0; // Assembling Info by Name Header and Info stored in Collumn 2
		Q_snprintf(Weapon_Info, sizeof(Weapon_Info), "%s \n%s", Collumn_1, Collumn_2);
	}
	// For Items/ Documents the value of Weapon_Info is set below in Get_Selected_Document(); and Get_Page_ID(); 
	// else if (Current_Tab > 1) {}



	Collumn_1[0] = 0; // Recycling Variables
	// Turn it into a string with single slot float precision %.1f	
	Q_snprintf(Collumn_1, sizeof(Collumn_1), "Zoom: %.1f     X Position: %.1f     Y Position: %.1f", m_fZoom, m_fTop, m_fLeft);

	Collumn_2[0] = 0;
	Q_snprintf(Collumn_2, sizeof(Collumn_2), "%s\nX Axis: %.1f     Y Axis: %.1f     Z Axis: %.1f", Collumn_1, m_fx, m_fy, m_fz);


	Collumn_1[0] = 0; // Appending Weapon_Info into Collumn_1
	Q_snprintf(Collumn_1, sizeof(Collumn_1), "%s \n\n%s", Collumn_2, Weapon_Info);
	// Q_snprintf(Collumn_1, sizeof(Collumn_1), "%s \n\n", Weapon_Info);


	if (Render_Zone && Collumn_1[0])
	{
		Render_Zone->SetFgColor(Color(200, 220, 220, 255));
		Render_Zone->SetText(Collumn_1);
	}

	/*
	if (Screen_Text && Collumn_1[0])
	{
	Screen_Text->SetFgColor(Color(200, 220, 220, 255));
	Screen_Text->SetText(Collumn_1);
	}*/
	
}

//-----------------------------------------------------------------------------
// On Command
//-----------------------------------------------------------------------------
void CInventory::OnCommand(const char* pcCommand)
{
	C_BasePlayer *player = C_BasePlayer::GetLocalPlayer();


	if(!Q_stricmp(pcCommand, "drop"))
	{
		if (selecteditem != -1 && player->GetWeapon(selecteditem))
		{		
			DevMsg("Weapon Drop : %s\n", player->GetWeapon(selecteditem));
			// if (Q_stricmp(player->GetWeapon(selecteditem - 1)->GetClassname(), "weapon_hands") && Q_stricmp(GetActiveWeapon()->GetClassname(), "weapon_menu")) // Ignoring those

			// not defined = 0 = large, 1 = medium (handgun), 2 = small (ammo), 3 = inventory_hidden, 4 = no_discard, 5 round revolver, 6 round revolver, 7 = item 
			if (player->GetWeapon(selecteditem - 1)->GetWpnData().iItemType != 3 && player->GetWeapon(selecteditem - 1)->GetWpnData().iItemType != 4) // Ignore all weapons of type 3 and 4
			{  // Print_Command("dropitem", selecteditem);
			
				char Command[16];
				Q_snprintf(Command, sizeof(Command), "dropitem %d", selecteditem);
				engine->ClientCmd(Command); // Drop item

				selecteditem = -1;
		    }
		}
	}


	char Slot_Name[16];


	// Looping through all Tab button slots in .\Resource\UI\Inventory.res
	for (int i = 1; i < 6; i++)
	{   Q_snprintf(Slot_Name, sizeof(Slot_Name), "Tab_%d", i);

		// Setting the current Tab if command was matched then Showing or Hiding the effected controls on the Form
		// Controls also trigger to reset if player presses the Large Button(s) by "Set_Controls" below
		if (!Q_stricmp(pcCommand, Slot_Name)) { Current_Tab = i; Set_Controls(); }
	}
	




	// ===================== Select Weapons and Items ===================== 
	// If the command "Slot_%d" was sent by the button in .\Resource\UI\Inventory.res
	for (int i = 0; i < Player_Item_Slots; i++)
	{
		char Slot[256]; Slot[0] = 0;
		Q_snprintf(Slot, sizeof(Slot), "Slot_%d", i + 1);

		if (!Q_stricmp(pcCommand, Slot))
		{			
			vgui::ButtonSlot *m_pSlot;
			C_BasePlayer *Player = C_BasePlayer::GetLocalPlayer();

			m_pSlot = dynamic_cast<vgui::ButtonSlot*>(FindChildByName(Slot));
			int Position = 0;

			// Msg("Weapon is %d\n", i);
			if (m_pSlot)
			{   Position = m_pSlot->GetSlot();
				
				if (selecteditem == Position && Player->GetWeapon(Position))
				{   // Msg("Slot is %d\n", m_pSlot->GetSlot());
					UseItem(Position + 1, 0); return;
				}
			}
			selecteditem = Position; // Pre selection before 2nd click



			if (Player->GetWeapon(Position))
			{	
				const FileWeaponInfo_t &weaponInfo = player->GetWeapon(Position)->GetWpnData();
				if (pAnimModel) pAnimModel->Remove(); // Remove old Entry

				m_fZoom = weaponInfo.fPosZoom;
				m_fLeft = weaponInfo.fPosLeft;
				m_fTop = weaponInfo.fPosTop;
				m_fx = weaponInfo.fPosX;
				m_fy = weaponInfo.fPosY;
				m_fz = weaponInfo.fPosZ;

				pAnimModel = new C_BaseAnimatingOverlay;
				//if (weaponInfo)
				pAnimModel->InitializeAsClientEntity(weaponInfo.szWorldModel, RENDER_GROUP_OPAQUE_ENTITY);
				pAnimModel->AddEffects(EF_NODRAW); // don't let the renderer draw the model normally


				// Need this when "Press_Rectangle" button is hit, then it will toggle onscreen info 
				if (Position > 0) { Selected_Slot = Position; }
				else { Selected_Slot = 0; }

				if (Render_Zone) { Render_Zone->SetText(""); } // Clearing		

				// Show_Slot = false;
				Rotating = false; // It draggs around like crazy...	
				return;
			}
		}
	}



	
	// Depending on "tabPosition" value of each single entry in .\Resource\UI\Inventory.res this function causes them to show or hide, if they match current_tab
	if (!Q_stricmp(pcCommand, "Set_Controls")) { Set_Controls(); }



	else if (!Q_stricmp(pcCommand, "Use_Item")) // Sent by the large button in item tab
	{
		if (player)
		{			
			// Use only items from Items Tab  Todo; Check if this works properly	
			if (Current_Tab == 2 && selecteditem) { UseItem(selecteditem, 0); } //return;		
		}
	}

	else if (!Q_stricmp(pcCommand, "Drop_Document")) // Sent by the large button in item tab
	{   if (player)
		{   // Call Drop_Document(int pos) in c_baseplayer.h which sends the command "Drop_Document"
			// To have player.cpp on the server side set all 3 slots of this document to 0: 
			// m_iNotes, m_iDoc_Type, m_iDoc_Status   and also decrement m_iNoteCount--; 
	
			player->Drop_Document(Current_Document); // WIP, not fully working yet,  todo
			Current_Document = 0; // Reset
			Update_Documents();
		}
	}

	else if (!Q_stricmp(pcCommand, "Cycle_Document")) 
	{   if (List)
		{   //if (Current_Document < 0) { Current_Document = Type_Count - 1; } // The other way around
			//List->SetSingleSelectedItem(Current_Document);
			//Current_Document--;

			if (Current_Document >= Type_Count) { Current_Document = 0; } // Reset

			List->SetSingleSelectedItem(Current_Document);
			Current_Document++;
		}
	}

	

	else if (!Q_stricmp(pcCommand, "Previous_Page") && Previous_Page > 0) { Get_Page_ID(false); } // Decremnent 

	
	else if(!Q_stricmp(pcCommand, "Next_Page")) 
	{
		if (Current_Tab > 2 && Next_Page > 0) { Get_Page_ID(true); } // Just Incremnent the Pagenumber			

		else if(!Allow_Rotating) // For Weapons/Items
		{   Allow_Rotating = true;

			vgui::Panel *Button = FindChildByName("Button_Triangle");
			if (Button) { Button->SetVisible(true); } // Switching Buttons
					
			Button = FindChildByName("Button_Next_Page");
			if (Button) { Button->SetPos(690, 17); Button->SetVisible(false); }

			Toggle_Inventory_Text(false);
		}
	}


	else if (!Q_stricmp(pcCommand, "Press_Triangle"))
	{   if (Allow_Rotating) 
		{   // Toggeling automatic Rotation
			Allow_Rotating = false;

			vgui::Panel *Button = FindChildByName("Button_Next_Page");
			if (Button) { Button->SetPos(652, 17); Button->SetVisible(true); }
			
			Button = FindChildByName("Button_Triangle"); // Exchanging Buttons
			if (Button) { Button->SetVisible(false); }	

		    Toggle_Inventory_Text(true); // TODO Toggle_Inventory_Text() for Items?
		}	


	    if (Current_Tab < 2) { UpdateInventory(); } // Only for Weapons		
	}

	else if (!Q_stricmp(pcCommand, "Press_Circle")) // Settings
	{	


		/*DevMsg("Texture is %s\n", Doc_Texture);
		DevMsg("Text is %s\n", Note_Text);
		DevMsg("X is %d and Y is %d", X_Ratio, Y_Ratio);*/
	}

	else if (!Q_stricmp(pcCommand, "Press_Rectangle") && Render_Zone)
	{					
		if (Full_Screen)
		{   Full_Screen = false;
			SetSize(Resource_Width, Resource_Height);

			// -20 Is the Border at the Top
			// if (Scrollbar) { Scrollbar->SetSize(12, Resource_Height - 20); } // Bad idea, let the res file handle it
			Refresh_Proportions();
		}
		else // Toggle
		{   Full_Screen = true;
			// -80 and -70 simply because it looks good with these measures
			SetSize(ScreenWidth() - 80, ScreenHeight() - 70);

			// -90 cause of 20 border and the 70 above
			if (Scrollbar) { Scrollbar->SetSize(12, ScreenHeight() - 90); }

			// Refreshing only the Item list but not Print_Width and Print_Height in Full_Screen mode.
			int Win_Y, Item_X, Item_Y;
			Win_Y = GetTall();
			List->GetSize(Item_X, Item_Y);
			if (Win_Y > 0 && Item_Y != Print_Height) { List->SetSize(Item_X, Win_Y - 98); }
		}

		Center();								
	}

	// cancelselect in order to remove Model which would cause gamecrash when player loads a new Savegame! || !Q_stricmp(pcCommand, "cancelselect")
	if (!Q_stricmp(pcCommand, "Press_Cross")) 
	{ jl_showinventory.SetValue(0); }




	/* // These Buttons were Disabled
	if(!Q_stricmp(pcCommand, "+zoommore")) { m_bZoomMore = true; } 
	else if(!Q_stricmp(pcCommand, "-zoommore")) { m_bZoomMore = false; }

	if(!Q_stricmp(pcCommand, "+zoomless")) { m_bZoomLess = true; } 
	else if(!Q_stricmp(pcCommand, "-zoomless")) { m_bZoomLess = false; }

	if(!Q_stricmp(pcCommand, "+moveup")) { m_bMoveUp = true; } 
	else if(!Q_stricmp(pcCommand, "-moveup")) { m_bMoveUp = false; }

	if(!Q_stricmp(pcCommand, "+movedown")) { m_bMoveDown = true; } 
	else if(!Q_stricmp(pcCommand, "-movedown")) { m_bMoveDown = false; }
	
	if(!Q_stricmp(pcCommand, "+moveleft")) { m_bMoveLeft = true; } 
	else if(!Q_stricmp(pcCommand, "-moveleft")) { m_bMoveLeft = false; }
	
	if(!Q_stricmp(pcCommand, "+moveright")) { m_bMoveRight = true; }
	else if(!Q_stricmp(pcCommand, "-moveright")) { m_bMoveRight = false; }
	*/

}


//-----------------------------------------------------------------------------
// Appeal Functions: Paint
//-----------------------------------------------------------------------------
void CInventory::Paint( void )
{

	surface()->DrawSetColor(255, 255, 255, 255);
	surface()->DrawSetTexture(Texture); // Background Image
	surface()->DrawTexturedRect(0,0, 1920, 1080); // 1920x1080 coz needs to be highest resolution.

	// SetPaintBackgroundEnabled(false);

	

	if (Current_Tab > 1) // Ignore Tab 1 & 2 which are for Weapons and Items. 
	{ 
		// surface()->DrawSetTexture(View_Button); // Background of large Button, replaced by a button
		// surface()->DrawTexturedRect(18, 20, 146, 84);	
			
		if (Render_Zone)
		{						
			if (!Compare(Doc_Texture, "")) // The Background of the Document
			{
				surface()->DrawSetColor(Background_Color);
				
				vgui::surface()->DrawSetTextureFile(Note_Background, Doc_Texture, true, false);
				surface()->DrawSetTexture(Note_Background); // European A4 Paper has Ratio of 10 to 0.7

				// Update_Documents(0) grabbs image sizes from localization.txt and stores them into KeyValue entries of the "Menu_List" entity,
				// Then when the player clicks at any List Entry Get_Selected_Document() extracts the stored values and sets X_Ratio and Y_Ratio from the current selection.					
				
				// Y_Ratio having Priority over X_Ratio for scale, because quadratic images are supposed to exactly fit screen height which is shorter then screen width.
				int Paper_Width = Print_X + (Print_Height / Y_Ratio) * X_Ratio;
				int Paper_Height = Print_Y + Print_Height;
			
				if (X_Ratio > Y_Ratio)
				{   // X_Ratio comes first because the else statement will trigger when X_Ratio == Y_Ratio, adjusting to Y which is smaller then X for the Inventory screen Ingame.
					Paper_Width = Print_X + Print_Width;
					Paper_Height = Print_Y + (Print_Width / X_Ratio) * Y_Ratio;
				}

				surface()->DrawTexturedRect(Print_X, Print_Y, Paper_Width, Paper_Height);
				surface()->DrawSetColor(255, 255, 255, 255); // Reset
			}
				
			   	


			//==================== Document Images ==================== 

			// Must not be out of range
			//if (Print_X + Doc_Target_X + Target_Width < End_X && Print_Y + Doc_Target_Y + Target_Height < End_Y) {
		    // "Print_X + Doc_Target_X" places this image at the same coordinates as the Printed Background under it!
											
			if (Target_Nr) // If any Entry is selected it is getting painted here instead of the loop below 
			{
				if (Compare(Doc_Image[Target_Nr], "")){ Target_Nr = 0; }
				else
				{   // Updating Location into Image_X and Image_Y 
					Interpolate_Image_Location(Target_Nr);				

					surface()->DrawSetTexture(Objective_Image);
					vgui::surface()->DrawSetTextureFile(Objective_Image, Doc_Image[Target_Nr], true, false);

					surface()->DrawTexturedRect(Image_X, Image_Y, Image_X + Target_Zoom_Width[Target_Nr], Image_Y + Target_Zoom_Height[Target_Nr]);
				}
			}


			//========== Loop through all Document Images ========== 
			for (int i = 1; i < 11; i++)
			{	
				// Updating Location into Image_X and Image_Y 
				Interpolate_Image_Location(i);

				// DevMsg("Start_X = %d, Start_Y = %d, Short_X = %d, Short_Y = %d\n", Start_X, Start_Y, Shorter_X, Shorter_Y);

				if (Doc_Target_Mode[i] == 1) // Means overwrite by Background Color
				{ surface()->DrawSetColor(Background_Color); }

			
				// Video and Audio Mode needs to be OVER parameter 4 textures
				if (Compare(Doc_Target_Pic[i], "Video") || Compare(Doc_Target_Pic[i], "video")
				 || Compare(Doc_Target_Pic[i], "Audio") || Compare(Doc_Target_Pic[i], "audio"))											
				{ 
					surface()->DrawSetTexture(Play_Button);
					surface()->DrawTexturedRect(Image_X, Image_Y, Image_X + Target_Zoom_Width[i], Image_Y + Target_Zoom_Height[i]);
					surface()->DrawSetColor(255, 255, 255, 255); 
					continue;
				}


				if (!Doc_Target_Mode[i] || Doc_Target_Mode[i] == 1) // !Doc_Target_Mode[i] Means show always Zoomed				
				{
					surface()->DrawSetTexture(Get_Texture_ID(i));
					vgui::surface()->DrawSetTextureFile(Get_Texture_ID(i), Doc_Image[i], true, false);
					surface()->DrawTexturedRect(Image_X, Image_Y, Image_X + Target_Zoom_Width[i], Image_Y + Target_Zoom_Height[i]);

					surface()->DrawSetColor(255, 255, 255, 255);
				}

				else if (Target_Nr != i)
				{   // if (Doc_Target_X[i] || Doc_Target_Y[i]) // If any Coordinates are present
					if (!Compare(Doc_Image[i], "")) // If has any Picture
					{   // This draws only the map marker, OnMouseReleased() above sets Draw_Target_Nr for the zoomed in Image.
						
						surface()->DrawSetColor(Text_Color);
						if (Doc_Target_Mode[i] == 2) { surface()->DrawSetTexture(Objective_Target); }
						else if (Doc_Target_Mode[i] == 3) { surface()->DrawSetTexture(Objective_Info); } // That flag specifies which Image we use
											
						else if (Doc_Target_Mode[i] == 4) // Then we use Doc_Target_Pic[i], which is the parameter 4 itself 
						{   surface()->DrawSetTexture(Get_Texture_ID(i));
							vgui::surface()->DrawSetTextureFile(Get_Texture_ID(i), Doc_Target_Pic[i], true, false);													
						}
				

						surface()->DrawTexturedRect(Image_X, Image_Y, Image_X + Target_Width, Image_Y + Target_Height);

						surface()->DrawSetColor(255, 255, 255, 255); // Resetting for next brush
					}
				}
			
			}
						


			// Located here because its used by the code under this statement too
			Text_Width = (Print_Height / Y_Ratio) * X_Ratio;
			Text_Height = (Print_Width / X_Ratio) * Y_Ratio;


			//========== Render Text in the Text Class (1 layer over CInventory) ========== 
			if (!Compare(Note_Text, "") && Screen_Text) // Text
			{   							
				// Msg("%d %d\n", X_Ratio, Y_Ratio);

				int Margin_X = Text_Width / 10;
				int Margin_Y = Text_Height / 10;

				if (X_Ratio > Y_Ratio)
				{   // Decrase by 30% to get the European A4 Letter format
					// This complicaton happens due to Valve's 2048x1024 or 1024x512 automatic interal texture sizing! 
					// You can replace Text_Width and Text_Height below back with Print_Width + Print_Height
					// And use non power of 2 Textures instead.. but those woun't export back to proper 

					if (X_Ratio / Y_Ratio == 2) { Text_Width = (Print_Width / Full_Ratio) * Chopped_Ratio; }
					Margin_X = Text_Width / 30; // Updating to the change above
				

					Screen_Text->SetSize(Text_Width - Margin_X, Text_Height - Margin_Y);
					Screen_Text->SetPos(Print_X + Margin_X / 2, Print_Y + Margin_Y / 2);
				}
				else // if(Y_Ratio > X_Ratio) 
				{
					// "== 2" because this triggers if Ratio is 2 to 1
					if (Y_Ratio / X_Ratio == 2) { Text_Height = (Print_Height / Full_Ratio) * Chopped_Ratio; }
					Margin_Y = Text_Height / 30;

					Screen_Text->SetSize(Text_Width - Margin_X, Text_Height - Margin_Y); // -40 Are the Margins
					Screen_Text->SetPos(Print_X + Margin_X / 2, Print_Y + Margin_Y / 2); // +20 is Distance from edge to inscription margin
				}
					

				Screen_Text->SetFgColor(Text_Color); 					
				Screen_Text->SetText(Note_Text);
						
				// Method of direct printing without involvement of any Label
				// Draw_Text(Print_X + 1, Print_Y + 1, Note_Text, 255, 255, 255, 255);
				// Draw_Text(Print_X, Print_Y, Note_Text, 255, 60, 60, 255);
			}


				
			/* // Outdated!
			C_BasePlayer *player = C_BasePlayer::GetLocalPlayer();	
			// DevMsg("%s\n", Video_Name);
			// if (Makes_Sense(Video_Name, 5))
			if (Character_Count(Video_Name) > 2) // Draw the Videoplayer Buton 
			{
				surface()->DrawSetColor(255, 255, 255, 255);
				surface()->DrawSetTexture(Play_Button);
			
				int Draw_X = Print_X + (Text_Width / 2);
				int Draw_Y = Print_Y + (Text_Height / 2);

				surface()->DrawTexturedRect(Draw_X - Play_Button_Size, Draw_Y - Play_Button_Size, Draw_X + Play_Button_Size, Draw_Y + Play_Button_Size); // 2x19 = 38 being Button Size
			}
			*/


		}		
	}


	
	if (gpGlobals->curtime > Tick) // Update
	{
		// Assuring normal size and position if model size is too small or too large
		GetSize(Model_Width, Model_Height);
		if (Model_Width < Resource_Width || Model_Height < Resource_Height
			|| Model_Width > ScreenWidth() || Model_Height > ScreenHeight())
		{ SetSize(Resource_Width, Resource_Height); Center(); }
	
		C_BasePlayer *player = C_BasePlayer::GetLocalPlayer();
		if (player == NULL) return;


		Item_Count = 0; // Refreshing
		for (int i = 0; i < MAX_WEAPONS; i++)
		{
			C_BaseCombatWeapon *weapon = player->GetWeapon(i);
			if (weapon){ Item_Count++; } // Only Draw Weapon slot if it exists!		
		}

		Tick = gpGlobals->curtime + 0.1f; // Only 0.1f because Bullettime slows the Timescale a lot
	}
		

	//surface()->DrawSetColor(0,96,0,64);
	//surface()->DrawFilledRect( 205,52,205+453,52+286);




	/*vgui::Label *m_pSlot;
	char temp[16];
	
	for (int i = 0;i< Item_Count ;i++)
	{
		Q_snprintf(temp,sizeof(temp),"Slot_%d",i+1);
		m_pSlot = dynamic_cast<vgui::Label*>( FindChildByName( temp ) );

		if (player->GetWeapon(i))
			m_pSlot->SetText(player->GetWeapon(i)->GetClassname());
		else
			m_pSlot->SetText("");
		
		if (player->GetWeapon(i))
		{
			const FileWeaponInfo_t &weaponInfo = player->GetWeapon(i)->GetWpnData();	
			wchar_t text[128];
			wchar_t *tempString = g_pVGuiLocalize->Find(weaponInfo.szPrintName);
			// setup our localized string
			if ( tempString )
			{
				_snwprintf(text, sizeof(text)/sizeof(wchar_t) - 1, L"%s", tempString);
				text[sizeof(text)/sizeof(wchar_t) - 1] = 0;
			}
			else
			{
				// string wasn't found by g_pVGuiLocalize->Find()
				g_pVGuiLocalize->ConvertANSIToUnicode(weaponInfo.szPrintName, text, sizeof(text));
			}			
		}			
	}

	if (selecteditem!=-1)
	{
		char tempslot[16];
		Q_snprintf(tempslot,sizeof(tempslot),"Slot_%d",selecteditem);
		vgui::Label *m_pSlot;
		m_pSlot = dynamic_cast<vgui::Label*>( FindChildByName( tempslot ) );
		int x, y, width, height;
		m_pSlot->GetSize(width, height);
		m_pSlot->GetPos(x, y);
		surface()->DrawSetColor(255,0,0,255);
		surface()->DrawOutlinedRect(x,y,x+width,y+height);
		surface()->DrawOutlinedRect(x+1,y+1,x+width+1,y+height+1);
		surface()->DrawOutlinedRect(x-1,y-1,x+width-1,y+height-1);
	}*/
}

//-----------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------
int CInventory::Get_Texture_ID(int i)
{
	if (i > 10){ return Picture_10; }

	switch (i)
	{
	case 1:
		return Picture_1;
		break;
	case 2:
		return Picture_2;
		break;
	case 3:
		return Picture_3;
		break;
	case 4:
		return Picture_4;
		break;
	case 5:
		return Picture_5;
		break;
	case 6:
		return Picture_6;
		break;
	case 7:
		return Picture_7;
		break;
	case 8:
		return Picture_8;
		break;
	case 9:
		return Picture_9;
		break;
	case 10:
		return Picture_10;
		break;		
	}

	// Distributing them eqaually to 48 Item slots, this prevents crashes due to over-refreshing the art entry
	// Otherwise it would also drain the Framerate a lot!

}


//-----------------------------------------------------------------------------
// Appeal Functions: Post VGUI - Render the Model
//-----------------------------------------------------------------------------
void CInventory::PostRenderVGui()
{
	// Doesen't work..
	//char* Overwrite_Color = Localize_Document(Current_Page, "Color_2");
	//Msg(Overwrite_Color);

	//// Background_Color is set in Get_Selected_Document() or Get_Page_ID()
	//if (!Compare(Overwrite_Color, "")) { surface()->DrawSetColor(Background_Color); }
	//else { surface()->DrawSetColor(255, 255, 255, 255); } // Making sure it doesen't tint the model with any other color from before.

	
	//DevMsg("Post Renderer VGUI\n");
	// Draw the model
	bool visible = true;
	if (m_p3DModelZone) { visible = m_p3DModelZone->IsVisible(); }

	if (pAnimModel && visible)
	{	
		C_BasePlayer *player = C_BasePlayer::GetLocalPlayer();
		trace_t tr;
		Vector vecDir, vecForward, vecRight, vecUp;

		// get the angles
		//AngleVectors( player->EyeAngles(), &vecDir );
		AngleVectors( player->EyeAngles(), &vecForward, &vecRight, &vecUp );

		//DevMsg( "Dir %f %f %f\n",vecDir.x,vecDir.y,vecDir.z);
		Vector origin = player->EyePosition();

			Vector lightOrigin = origin;
		    // find a spot inside the world for the dlight's origin, or it won't illuminate the model
			Vector testPos( origin.x - 100, origin.y, origin.z + 100 );
			//trace_t tr;
			UTIL_TraceLine( origin, testPos, MASK_OPAQUE, player, COLLISION_GROUP_NONE, &tr );
			if ( tr.fraction == 1.0f )
			{
				lightOrigin = tr.endpos;
			}
			else
			{   // Now move the model away so we get the correct illumination
				lightOrigin = tr.endpos + Vector( 1, 0, -1 );	// pull out from the solid
				Vector start = lightOrigin;
				Vector end = lightOrigin + Vector( 100, 0, -100 );
				UTIL_TraceLine( start, end, MASK_OPAQUE, player, COLLISION_GROUP_NONE, &tr );
				origin = tr.endpos;
			}

			float ambient = engine->GetLightForPoint( origin, true ).Length();

			// Make a light so the model is well lit.
			// use a non-zero number so we cannibalize ourselves next frame
			dlight_t *dl = effects->CL_AllocDlight( LIGHT_INDEX_TE_DYNAMIC+1 );

			dl->flags = DLIGHT_NO_WORLD_ILLUMINATION;
			dl->origin = lightOrigin;
			// Go away immediately so it doesn't light the world too.
			dl->die = gpGlobals->curtime + 0.1f;

			dl->color.r = dl->color.g = dl->color.b = 250;
			if ( ambient < 1.0f )
			{
				dl->color.exponent = 1 + (1 - ambient) * 2;
			}
			dl->radius	= 400;
		

		// Set XY Position of the Model
		pAnimModel->SetAbsOrigin( origin + vecForward * m_fZoom + vecRight * m_fLeft + vecUp * m_fTop );
		//DevMsg("Top : %d\n",m_fTop);
		//pAnimModel->SetModelWidthScale(5);
		
		// Set Model Rotation
		pAnimModel->SetAbsAngles(  player->EyeAngles() + QAngle(m_fx, m_fy, m_fz) );//m_fx,y,0.

		Quaternion quat1,quat2,quat3;
		QAngle angle;
		AngleQuaternion(player->EyeAngles(),quat1);
		AngleQuaternion( QAngle( m_fx, m_fy, m_fz), quat2);
		QuaternionMult(quat1,quat2,quat3);
		QuaternionAngles(quat3,angle);

		pAnimModel->SetAbsAngles(angle);
		
		CViewSetup view;
		// setup the views location, size and fov (amongst others)
		// This includes position of Inventory Window
		
	
		GetPos(Window_X, Window_Y);
		GetSize(Model_Width, Model_Height);

		// These define a Window where the model is rendered.
		view.x = Window_X + 152; // 18 emptyspace + 128 Weapon Slot + 6 emptyspace
		view.y = Window_Y + 50; // 20 emptyspace from top + 30 Tab height	
		view.width = Model_Width - 152; 
		view.height = Model_Height - 50; 


		view.m_bOrtho = false;
		view.fov = 54;

		view.origin = origin ;
		
		// make sure that we see all of the player model
		Vector vMins, vMaxs;
		pAnimModel->C_BaseAnimating::GetRenderBounds( vMins, vMaxs );
		view.origin.z += ( vMins.z + vMaxs.z ) * 0.55f;

		view.angles.Init();
		//view.m_vUnreflectedOrigin = view.origin;
		view.zNear = VIEW_NEARZ;
		view.zFar = 1000;
		
		view.angles = player->EyeAngles();

		// render it out to the new CViewSetup area
		// it's possible that ViewSetup3D will be replaced in future code releases
		Frustum dummyFrustum;
		//VPlane dummyFrustum;
		
		//render->ViewSetup3D( &view, 0, dummyFrustum );
		//render->SetMainView(view.origin,view.angles);
		
		//render->Push3DView( view, 16384, /*false,*/ NULL, dummyFrustum ); //DF_CLEARCOLOR=16384
		render->Push3DView( view, 0, NULL, dummyFrustum ); //DF_CLEARCOLOR=16384
		//render->Push3DView( view, VIEW_CLEAR_DEPTH | VIEW_CLEAR_COLOR, NULL, dummyFrustum );

			pAnimModel->DrawModel( STUDIO_RENDER );	
			
		render->PopView( dummyFrustum ); 
	}
}


//-----------------------------------------------------------------------------
// Create Control By Name
//-----------------------------------------------------------------------------

Panel *CInventory::CreateControlByName(const char *controlName)
{
    if ( Q_stricmp( controlName, "3DModel" ) == 0 )
    {
		DevMsg("Create 3D Model\n");
        return new CClass3DModelZone( this, controlName );
    }

	/*if ( Q_stricmp( controlName, "ButtonSlot" ) == 0 )
    {
		DevMsg("Create ButtonSlot\n");
        return new ButtonSlot( this, controlName, controlName );
    }*/	

    return BaseClass::CreateControlByName( controlName );
}






//------------------------------------------------------------------------------------------------------------------------
// CLASSES FOR MODEL RENDERING
//------------------------------------------------------------------------------------------------------------------------

CClass3DModelZone::CClass3DModelZone( vgui::Panel *pParent, const char *pName ) : vgui::ImagePanel( pParent, pName )
{
}

CClass3DModelZone::~CClass3DModelZone()
{
}


//-----------------------------------------------------------------------------
// Apply Scheme Settings
//-----------------------------------------------------------------------------
void CClass3DModelZone::ApplySettings( KeyValues *inResourceData )
{
	// Size and Position are defined in the .res file!
	Window_X = inResourceData->GetInt("xpos");
	Window_Y = inResourceData->GetInt("ypos");
	Model_Width = inResourceData->GetInt("wide");
	Model_Height = inResourceData->GetInt("tall");


	CClass3DModelZone::SetPaintBackgroundEnabled(false);
	SetPaintBorderEnabled(false);
	SetMouseInputEnabled(false); // This Class draws over Inventar Panel, so we need this to leave it the Mouse controls.
	SetKeyBoardInputEnabled(false);

	// Current Parent is the Inventory Panel

    BaseClass::ApplySettings( inResourceData );
}

//-----------------------------------------------------------------------------
// Paint
//-----------------------------------------------------------------------------
void CClass3DModelZone::Paint()
{
	CInventory *Inventory;
	// surface()->DrawSetColor(255, 255, 255, 255); // Overwriting Color of other Classes in the RAM

	// if (!Inventory->Compare(Inventory->Doc_Model, "")) { surface()->DrawSetColor(Inventory->Background_Color); }	

    BaseClass::Paint();
	surface()->DrawFilledRect(0, 0, GetWide(), GetTall());
}


//-----------------------------------------------------------------------------
//  Setting Control Visability, this hides everything outside of the active Tab
//-----------------------------------------------------------------------------
void CInventory::Set_Controls()
{	// Weapons and Items have no own Button, instead they use the space as SlotButton
	const char *Inventory_Buttons[] = 
	{	// "3DModel", // Used to print text for Tab 1-2
		// "Render_Zone", 
		// "Screen_Text", // Used to print text for Tab 3-5
		"Items",
		"Notes",
		"Objectives",	
		"Maps",
		
		// Playstation Triangle and Circle Buttons are replaced by Prev and Next Page buttons:		
	};

	for (int i = 0; i < ARRAYSIZE(Inventory_Buttons); ++i) // For each string in the Array
	{
		vgui::Panel *iPanel = FindChildByName(Inventory_Buttons[i]); 
		if (iPanel)
		{
			// DevMsg("Current Tab = %d, position = %s\n", Current_Tab, Inventory_Buttons[i]);			
			// Show if matches the Current Tab, hide if not
			if (iPanel->GetTabPosition() == Current_Tab) { iPanel->SetVisible(true); }
			else { iPanel->SetVisible(false); }
		}	
	}

	
	// === Setting Inventary Button Visability ===
	// Disabled because we don't longer need to hide it, the "List_Background" is just drawn over it so its not visible when unused.
	vgui::ButtonSlot *m_pSlot;
	char Slot_Name[16];

	for (int i = 0; i < 11; i++)
	{	Q_snprintf(Slot_Name, sizeof(Slot_Name), "Slot_%d", i + 1);
		m_pSlot = dynamic_cast<vgui::ButtonSlot*>(FindChildByName(Slot_Name));

		if (m_pSlot)
		{   if (Current_Tab < 2) { m_pSlot->SetVisible(true); }
			else { m_pSlot->SetVisible(false); }
		} 			
	}


	
	//m_pSlot = dynamic_cast<vgui::ButtonSlot*>(FindChildByName("Background_Button"));
	//if (m_pSlot)	
	//{	

	
	if (Current_Tab > 1) // Ignore Tab 1 & 2 which are for Weapons and Items. 
	{
		// Resetting Print Position for Image and Text 
		Print_X = Start_Print_X;
		Print_Y = Start_Print_Y;

		if (Screen_Text) { Screen_Text->SetPos(Print_X + 20, Print_Y + 20); }

		// Show for Documents of type "Note", "Objective", and "Map" aka Tab 3, 4, 5
		
		if (List) // List is a class member
		{   List->SetVisible(true);
			if (List->FindColumn("Title"))
			{ 	// Adjusting meaning of List Title depending on selected Tab
				if (Current_Tab == 2) { List->SetColumnHeaderText(List->FindColumn("Title"), (const char *)"#List_Items"); }
				else if (Current_Tab == 3) { List->SetColumnHeaderText(List->FindColumn("Title"), (const char *)"#List_Notes"); }
				else if (Current_Tab == 4) { List->SetColumnHeaderText(List->FindColumn("Title"), (const char *)"#List_Objectives"); }
				else if (Current_Tab == 5) { List->SetColumnHeaderText(List->FindColumn("Title"), (const char *)"#List_Maps"); }
			}
		}


		// Show only if we found any page
		/*vgui::Panel *Button = FindChildByName("Button_Previous_Page");	 
		if (Button && Previous_Page) { Button->SetVisible(true); }
		
		Button = FindChildByName("Button_Next_Page");
		if (Button && Next_Page) { Button->SetVisible(true); }*/

		// Clear_Screen(); // Moved below 
		Update_Documents();		
	} 
	else // if (Current_Tab < 2) // < 3 if Items are supposed to show weapons
	{ 
		if (List) 
		{   List->SetVisible(false); // Hiding for Tab 1, 2
			List->DeleteAllItems();  // Clear to prevent arrow buttons to swipe through items in weapon mode
		}
		
		if (Screen_Text){ Screen_Text->SetText(""); } // Clearing
		if (Render_Zone){ Render_Zone->SetText(""); }
	}
	


	// ================= On Tabchange ===================
	if (Last_Tab != Current_Tab) // Preventing Changes within the same Tab
	{		
		Last_Tab = Current_Tab;



		vgui::Panel * The_Button = FindChildByName("Button_Circle");
		if (The_Button) // Not dealed above becuse this button is a special case
		{
			// if (The_Button->GetTabPosition() == Current_Tab) 
			if (Current_Tab < 3) { The_Button->SetVisible(true); }
			else { The_Button->SetVisible(false); }
		}
	
	    The_Button = FindChildByName("Button_Triangle");
		if (The_Button) // Not dealed above becuse this button is a special case
		{   
			if (Current_Tab < 3 && Allow_Rotating) { The_Button->SetVisible(true); }
			else { The_Button->SetVisible(false); }
		}

		The_Button = FindChildByName("Button_Previous_Page");
		if (The_Button) { The_Button->SetVisible(false); }

		The_Button = FindChildByName("Button_Next_Page");
		if (The_Button)
		{   if (Current_Tab < 3) // Inventory
			{   if (Allow_Rotating) { The_Button->SetPos(690, 17); The_Button->SetVisible(false); } // Original Position
				else if (!Allow_Rotating) { The_Button->SetPos(652, 17); The_Button->SetVisible(true); }
			}
			else { The_Button->SetPos(690, 16); The_Button->SetVisible(false); }
		}

		The_Button = FindChildByName("List_Background");
		if (The_Button)
		{   if (Current_Tab < 2) { The_Button->SetVisible(false); }
			else 
			{   The_Button->SetVisible(true);
				// Leaving controls to the mainwindow and its events that control model movement!
				The_Button->SetKeyBoardInputEnabled(false); 
				The_Button->SetMouseInputEnabled(false);
			}
		}

		The_Button = FindChildByName("Button_Background");
		if (The_Button)
		{   if (Current_Tab < 2) { The_Button->SetVisible(false); }
			else { The_Button->SetVisible(true); }
		}

		Clear_Screen();
	}
	

	/* // Removes Model between Tab selection, not that cool..
	if (Current_Tab > 2) // No Models being displayed outside of Weapon and Inventory Tab
	{   selecteditem = -1;
	if (pAnimModel)
	{
	pAnimModel->Remove();
	pAnimModel = NULL;
	}
	}*/


	// Localize this string and print its content
	if (selecteditem == -1 && Current_Tab < 3) // Clearing Info Text
	{   
		wchar_t *Temp = g_pVGuiLocalize->Find("JL_Info_Null"); // That Nulltext just gives general hints.
		// m_pInfo->SetText(Temp); // Would Work instead of the stuff below	
	
		char Buffer[1024]; Buffer[0] = 0; // Erasing Buffer
		g_pVGuiLocalize->ConvertUnicodeToANSI(Temp, Buffer, 1024);
		Render_Zone->SetText(Buffer);
	}

}



//-----------------------------------------------------------------------------
//  Update Notes: This Formats a KeyValues *Data Entry for a Massege,
//  which is accessed by MESSAGE_FUNC( OnItemSelected, "ItemSelected" ) in this IInventory.h
//-----------------------------------------------------------------------------
void CInventory::Update_Documents() // This funcion is called with Set_Controls() each time a Tab or big button was pressed
{
	// if (Caller_Code == 1) return;

	C_BasePlayer *player = C_BasePlayer::GetLocalPlayer();

	if (List) // <- Global Variable
	{  
		
		Refresh_Proportions(); 
		Type_Count = 0;

		// DevMsg("Client note count %d\n", player->Get_Paper_Number(0, "Note_Count")); 
		List->DeleteAllItems(); // Refresh
		

		if (player)
		{	//for (int i=1;i<=player->GetNoteCount();i++)
			for (int i = player->Get_Paper_Number(0, "Note_Count"); i > 0; i--)
			{   				
				// -2 Because Maps is Tab 5 but Hammer Option 3 + 0 slot, Objectives is Tab 4 but Hammer Option 2..
				//  0 = ID Cards and Keys, 1 = Notes, 2 = Objectives, 3 = Maps;  if wrong type we don't list it

				if (player->Get_Paper_Number(i, "Doc_Type") != Current_Tab - 2 || 
				 // "Doc_Status" 4 means a Objective is Hidden so we skip this entry, 
				 // Which isn't supposed to exclude Items, Keys and ID Cards in slot 0
				 player->Get_Paper_Number(i, "Doc_Type") == 2 && player->Get_Paper_Number(i, "Doc_Status") == 4) { continue; }
				
	


				Type_Count++;
							
				char Buffer_Image[1024];				
				Buffer_Image[0] = 0;

				// Unused
				// Buffer_Text[0] = 0;
				// g_pVGuiLocalize->ConvertUnicodeToANSI(player->Get_Localized_Text(i, "Text"), Buffer_Text, 4096);
				// DevMsg("Note ID is %d\n", player->Get_Paper_Number(i, "ID"));



				// Q_snprintf(buffer_X_Ratio, sizeof(buffer_X_Ratio), "%d", player->Get_X_Ratio(i));
				// Q_snprintf(buffer_Y_Ratio, sizeof(buffer_Y_Ratio), "%d", player->Get_Y_Ratio(i));
				// Original Code by CTH:
				// g_pVGuiLocalize->ConvertUnicodeToANSI(player->GetNoteTitle(i), bufferTitle, 2048);
				// g_pVGuiLocalize->ConvertUnicodeToANSI(player->GetNote(i), bufferText, 2048);
				// DevMsg("Note id %d\n", player->GetNoteId(i));
				// List->AddItem(Data, player->GetNoteId(i), false, false);

				// buffer_X_Ratio[0] = 0;
				// buffer_Y_Ratio[0] = 0;
				// Q_snprintf(buffer_X_Ratio, sizeof(buffer_X_Ratio), "%d", player->Get_Paper_Number(i, "X_Ratio"));
				// Q_snprintf(buffer_Y_Ratio, sizeof(buffer_Y_Ratio), "%d", player->Get_Paper_Number(i, "Y_Ratio"));
				// Data->SetInt("Type", player->Get_Paper_Number(i, "Doc_Type"));
				// Data->SetInt("Status", player->Get_Paper_Number(i, "Doc_Status"));

			
		

				// ======== Storing Key Value ========
				
				KeyValues *Data = new KeyValues("data");
				/*Data->SetUseGrowableStringTable(true); // Doesen't work..
				Data = new KeyValues("data");*/
				Data->SetInt("Note_Slot", i);
				// Data->SetInt("Note_ID", player->Get_Paper_Number(i, "ID"));


				// For Items/Documents Doc_Status has a different meaning then Objectives and Notes:
				// Notes + Maps: 1 = Unreaded, 2 = Readed
				// Objectives: 0 = resolved, 1-3 = Priority stages, 4 = Hidden
				// Items, Keys and ID Cards: 0 = Used (can be dropped), 4-1 = Access level, the lower the number the greater access
				int Status = player->Get_Paper_Number(i, "Doc_Status");



				if (player->Get_Paper_Number(i, "Doc_Type") == 0) // ID Cards and Keys render their models
				{
					if (Status == 0) { Data->SetInt("Read", 7); } // Objective Completed Green
					else // if (Status == 1) 
					{ Data->SetInt("Read", 4); } // Objective Active in Red = Priority 1				
				}
				// Objectives are managed by Input_Activate_Objective and Input_Change_Status in item_note.cpp, by Hammer IO 
				else if (player->Get_Paper_Number(i, "Doc_Type") == 2) 
				{
					if (Status == 0) { Data->SetInt("Read", 3); } // Objective Completed
					else if (Status == 1) { Data->SetInt("Read", 4); } // Objective Active in Red = Priority 1
					
					else if (Status == 2) { Data->SetInt("Read", 5); } // Yellow = Priority 2
					else if (Status == 3) { Data->SetInt("Read", 6); } // Blue = Priority 3
					// Status == 4 means hidden so its invisible!
					else if (Status == 5) { Data->SetInt("Read", 7); } // Green = currently unassigned
				} 			
				else // Show Unreaded Enverlope Icon in Listclass for Notes and Maps as soon the player views them for first time.
				{   Data->SetInt("Read", player->Get_Paper_Number(i, "Doc_Status")); // Just use the original value					
				} 
			


				Data->SetString("Title", Localize_Document(i, "Title"));
				Data->SetString("Scroll", Localize_Document(i, "Scroll"));
				Data->SetString("Background", Localize_Document(i, "Doc_Background"));
				// Data->SetString("Message", Buffer_Text);
							
				// Set the Keyvalue "Color_1" to the Color we extract from the Localize text with the same name.
				Data->SetColor("Color_1", Get_Color(i, "Color_1"));
				Data->SetColor("Color_2", Get_Color(i, "Color_2"));
				


			
				char Buffer_Width[1024];
				char Buffer_Heigth[1024];
				Buffer_Width[0] = 0;
				Buffer_Heigth[0] = 0;

				// We grab a Value from Localize function, that is either Pixelsize for X and Y or a Ratio, both possabilities are fine
				g_pVGuiLocalize->ConvertUnicodeToANSI(player->Get_Localized_Text(i, "X_Width"), Buffer_Width, 1024);
				g_pVGuiLocalize->ConvertUnicodeToANSI(player->Get_Localized_Text(i, "Y_Heigth"), Buffer_Heigth, 1024);



				// Failsafe: set Ratio as 1x1 if no entry exists
				if (Compare(Buffer_Width, "")) { Data->SetInt("X_Ratio", 1); }
				if (Compare(Buffer_Heigth, "")) { Data->SetInt("Y_Ratio", 1); }
				
				if (!Compare(Buffer_Width, "") || !Compare(Buffer_Heigth, ""))
				{   float X_Width = atoi(Buffer_Width);
					float Y_Heigth = atoi(Buffer_Heigth);

					if (X_Width > Y_Heigth)
					{
						Y_Heigth = (Y_Heigth / X_Width) * 10;

						Data->SetInt("X_Ratio", 10);
						Data->SetInt("Y_Ratio", Y_Heigth);
					}

					else // if (Y_Heigth > X_Width)
					{
						X_Width = (X_Width / Y_Heigth) * 10;

						Data->SetInt("X_Ratio", X_Width);
						Data->SetInt("Y_Ratio", 10);
					}

					DevMsg("Width is %s and Height is %s\n", Buffer_Width, Buffer_Heigth);
				}

				//Data->SetString("Texture","texture message");			
				List->AddItem(Data, player->Get_Paper_Number(i, "ID"), false, false);
			}


			// Preparing an invisible, empty, temporal entry for the Get_Page_ID(int i) function below and appending it at the bottoom.
			// Last slot is always reserved for temporal entry, its supposed to stay invisible!
			KeyValues *Temporal_Entry = new KeyValues("Temporal_Entry");
			// Temporal_Entry->SetString("Title", (char*) "Temporal"); // Just for Test visualization, the rest is empty	
			List->AddItem(Temporal_Entry, 0, false, false);	
			int Temporal_Slot = List->GetItem("Temporal_Entry");			
			List->SetItemVisible(Temporal_Slot, false); 
			// Current_Document = Type_Count -1;


		}

		Current_Page = 0; // reset

		//if (Render_Zone) // Render_Zone is actually a Label object
		//{ // Render_Zone->SetText(bufferText); 
		//}
	}

}

//-----------------------------------------------------------------------------
// Creating a list of all available Notes/Items for the Impulse 111 cheat
//-----------------------------------------------------------------------------
void CInventory::Check_Available_Items()
{
	// Update_Available_Notes	
	char Buffer_A[512];

	// int Item_Type = 1;
	int Slot = 0;
	int Skipped = 0;
	int Note_ID = 0;



	// This registers all available notes from .\modname\resource\modname_english.txt through Update_Available_Note()
	for (int i = 1; i < 600 + 1; ++i)
	{		
		Buffer_A[0] = 0; // clear
		Q_snprintf(Buffer_A, sizeof(Buffer_A), "#Doc_Title_%d", i);
		char* Buffer_B = Localize_Text(Buffer_A);

		if (Buffer_B == null || Compare(Buffer_B, "")) { Skipped++; continue; }
		Slot++;
		Note_ID = i;


		// Placing a -1 seperator between the different Item types
		if (Skipped > 15) { Note_ID = -1; Skipped = 0; }


		// Precache all legitimate models
		Buffer_A[0] = 0; Buffer_B[0] = 0;
		Q_snprintf(Buffer_A, sizeof(Buffer_A), "#Doc_Model_%d", i);
		Buffer_B = Localize_Text(Buffer_A);
		
		if (!Compare(Buffer_B, "") && !Compare(Buffer_B, "0") && Buffer_B != null)
		{
			Buffer_A[0] = 0;
			Q_snprintf(Buffer_A, sizeof(Buffer_A), "precache_model %s", Buffer_B);
			engine->ClientCmd(Buffer_A);
			// DevMsg("Precaching %s\n", Buffer_B); // Obsolete duplicate
		}

		

		char Command[24]; // Caution; limited to 23 signs!
		Q_snprintf(Command, sizeof(Command), "AddInfo %d %d", Slot, Note_ID);
		engine->ClientCmd(Command); // Drop item

		// DevMsg("Inserting %d as: %s\n", i, Command);


		/* // Won't work.. delete this
		char Temporal_X[1024]; Temporal_X[0] = 0;
		Q_snprintf(Temporal_X, sizeof(Temporal_X), "#Doc_Model_%d", Note_ID);

		char AlternateModel[1024]; AlternateModel[0] = 0;
		g_pVGuiLocalize->ConvertUnicodeToANSI(g_pVGuiLocalize->Find(Temporal_X), AlternateModel, 1024);


		char Command_2[24]; // Caution; limited to 23 signs!
		Q_snprintf(Command_2, sizeof(Command_2), "PrecacheIt %s", AlternateModel);
		engine->ClientCmd(Command_2); // Drop item
		*/

	}

}

//-----------------------------------------------------------------------------
// Get Selected Note
// Beware if the CORRECT .\Modname\Resources\LocalizeModname_Language.txt is not found the note will fail to show up
//-----------------------------------------------------------------------------
void CInventory::Get_Selected_Document()
{
	if (List)
	{   int selCount = List->GetSelectedItemsCount();
		if ( selCount <= 0 ) { return; }

		C_BasePlayer *player = C_BasePlayer::GetLocalPlayer();
	

		if (player)
		{
			int Item_Id = List->GetSelectedItem(0);		
			KeyValues *Value = List->GetItem(Item_Id);

			// Msg("%d Count is %d\n", itemId, Type_Count);

			// int noteid = List->GetItemUserData(itemId);
			// DevMsg("Selected note %d %d %s\n",Item_Id,noteid,player->GetNote(noteid));
			// Render_Zone->SetText(player->GetNote(noteid));

			int Current_Slot = Value->GetInt("Note_Slot", 0); // reset
			

			
			if (Value && Item_Id != List->GetItem("Temporal_Entry") // Excluding temporary Entry at slot 0
				// Because Status 4 means "Hide it from the Inventory"
				|| player->Get_Paper_Number(Current_Slot, "Doc_Type") == 2 && player->Get_Paper_Number(Current_Slot, "Doc_Status") < 4)
			{		
				


				// For Items Doc_Status has a different meaning then Objectives and Notes:
				// Notes + Maps: 1 = Unreaded, 2 = Readed
				// Objectives: 0 = resolved, 1-3 = Priority stages, 4 = Hidden
				// Items, Keys and ID Cards: 0 = Used (can be dropped), 4-1 = Access level, the lower the number the greater access


				// Commented out because it seems obsolete in this area:
				/*
				int Status = player->Get_Paper_Number(Current_Slot, "Doc_Status");

				if (player->Get_Paper_Number(Current_Slot, "Doc_Type") == 0) // ID Cards and Keys
				{
					if (Status == 0) { Value->SetInt("Read", 7); } // Objective Completed Green
					else { Value->SetInt("Read", 4); } // Objective Active in Red = Priority 1	

					DevMsg("ID #%d has Status %d\n", player->Get_Paper_Number(Current_Slot, "ID"), Status);
				}

				// If is Objective Type	
				else if (player->Get_Paper_Number(Current_Slot, "Doc_Type") == 2)
				{	// if (Value->GetInt("Status") == 0) { Value->SetInt("Read", 3); } Use the line above instead because this is stored in value and doesent update in Realtime!
								
					if (Status == 0) { Value->SetInt("Read", 3); } // Objective Completed
					else if (Status == 1) { Value->SetInt("Read", 4); } // Objective Active in Red = Priority 1

					else if (Status == 2) { Value->SetInt("Read", 5); } // Yellow
					else if (Status == 3) { Value->SetInt("Read", 6); } // Blue
					// Status == 4 means hidden so its invisible!
					else if (Status == 5) { Value->SetInt("Read", 7); } // Green
					
				}	
				*/

				if (player->Get_Paper_Number(Current_Slot, "Doc_Type") != 0 && player->Get_Paper_Number(Current_Slot, "Doc_Type") != 2) // Show Readed Enverlope for non Objective types: Notes (1) and Maps (3) 
				{   player->SetNoteRead(Current_Slot);
					Value->SetInt("Read", 2); 					
				}			
				
				


				// ======== Getting Previous and Next Page ========
				char Buffer[1024];
				Buffer[0] = 0;
				g_pVGuiLocalize->ConvertUnicodeToANSI(player->Get_Localized_Text(Current_Slot, "Previous_Page"), Buffer, 1024);
				

				Current_Page = player->Get_Paper_Number(Current_Slot, "ID");

				// If the keyword "Next" is used we decrement by -1
				if (Compare(Buffer, "Next") || Compare(Buffer, "next")) { Previous_Page = Current_Page - 1; }
				else if (!Compare(Buffer, "") && !Compare(Buffer, "0")) { Previous_Page = atoi(Buffer); } // If any number was found
				else { Previous_Page = 0; } // This indirectly disables the Button

				vgui::Panel * The_Button = FindChildByName("Button_Previous_Page");
				if (The_Button && Previous_Page && Does_Player_Have(Previous_Page)) { The_Button->SetVisible(true); }
				else { The_Button->SetVisible(false); }
			

				Buffer[0] = 0;
				g_pVGuiLocalize->ConvertUnicodeToANSI(player->Get_Localized_Text(Current_Slot, "Next_Page"), Buffer, 1024);

				// If the keyword is used we increment by 1
				if (Compare(Buffer, "Next") || Compare(Buffer, "next")) { Next_Page = Current_Page + 1; }
				else if (!Compare(Buffer, "") && !Compare(Buffer, "0")) { Next_Page = atoi(Buffer); } // If any number was found
				else { Next_Page = 0; } // This indirectly disables the Button


				The_Button = FindChildByName("Button_Next_Page");
				// Ignoring this for Weapons and Items
				if (Current_Tab < 3 && !Allow_Rotating || The_Button && Next_Page && Does_Player_Have(Next_Page)) { The_Button->SetVisible(true); }
				else { The_Button->SetVisible(false); }
			

				// ================================================


				Weapon_Info[0] = 0;
				// Printing item name into Weapon info, in case that item has a model that can be viewed
				Q_snprintf(Weapon_Info, sizeof(Weapon_Info), "%s", (char*)Value->GetString("Title"));
				

			
				char Temporal_X[1024]; Temporal_X[0] = 0;
				Q_snprintf(Temporal_X, sizeof(Temporal_X), "#Doc_Model_%d", Current_Page);

				char AlternateModel[1024]; AlternateModel[0] = 0;
				g_pVGuiLocalize->ConvertUnicodeToANSI(g_pVGuiLocalize->Find(Temporal_X), AlternateModel, 1024);



				Doc_Model = Localize_Document(Current_Slot, "Model"); // Or Current_Page
				DevMsg("Model Path is %s\n", Doc_Model);
				


				if (!Compare(Doc_Model, "")) // If there is any specified model
				{		

					Temporal_A[0] = 0;
					Q_snprintf(Temporal_A, sizeof(Temporal_A), "#Doc_Model_Position_%d", Current_Page);
					char* Model_Position = Localize_Text(Temporal_A);	

					// DevMsg("Model Position is %s\n", Model_Position);

					if (!Compare(Model_Position, ""))
					{
						char Position_X[80];
						char Position_Y[80];
						char Model_Scale[80];
						char Rotation_X[80];
						char Rotation_Y[80];
						char Rotation_Z[80];

						// Doesen't work..
						// Split_Into(Model_Position, ',', Position_X, Position_Y, Model_Scale, Rotation_X, Rotation_Y, Rotation_Z);


						Split_Array(Model_Position, Position_X, ',', 1);
						Split_Array(Model_Position, Position_Y, ',', 2);
						Split_Array(Model_Position, Model_Scale, ',', 3);

						Split_Array(Model_Position, Rotation_X, ',', 4);
						Split_Array(Model_Position, Rotation_Y, ',', 5);
						Split_Array(Model_Position, Rotation_Z, ',', 6);

						// Overwriting with new values:
						m_fLeft = V_atof(Position_X);
						m_fTop = V_atof(Position_Y);
						m_fZoom = V_atof(Model_Scale);

						m_fx = V_atof(Rotation_X);
						m_fy = V_atof(Rotation_Y);
						m_fz = V_atof(Rotation_Z);
					}
					else // Reset model position to default values
					{					
						m_fLeft = 0;
						m_fTop = 0;
						m_fZoom = 34;

						m_fx = 0;
						m_fy = 0;
						m_fz = 0;
					}
						

				
					// if (pAnimModel) { pAnimModel->Remove(); } // Remove old 					
					pAnimModel = new C_BaseAnimatingOverlay;
					// DevMsg("Below Model is %s\n", AlternateModel);
					pAnimModel->InitializeAsClientEntity(AlternateModel, RENDER_GROUP_OPAQUE_ENTITY);

					pAnimModel->AddEffects(EF_NODRAW); // don't let the renderer draw the model normally


					Doc_Texture = ""; // Clearing, this will also prevent the Paint() function from drawing any BG texture.
				}

				// Render_Zone->SetText(Value->GetString("Message")); // This directly prints text in the "Render_Zone" label
				// Image and Text are printing out in the "Paint" function above			
				else 
				{
					Doc_Texture = (char*)Value->GetString("Background");

					if (pAnimModel)
					{   pAnimModel->Remove();
						pAnimModel = NULL;
					}
				}
				// Note_Text = (char*)Value->GetString("Message");

			
				
				
				Buffer_Text[0] = 0;
				g_pVGuiLocalize->ConvertUnicodeToANSI(player->Get_Localized_Text(Current_Slot, "Text"), Buffer_Text, 4096);
				Note_Text = Buffer_Text;


				// Video_Name[0] = 0; // Alternate Variant
				// g_pVGuiLocalize->ConvertUnicodeToANSI(player->Get_Localized_Text(Current_Slot, "Video"), Video_Name, 1024);		
				
			
				Get_Target_Images();
						
				if (Compare((char*)Value->GetString("Scroll"), "")) { Allow_Zooming = true; } // Not specified, so allowd by default
				else { Allow_Zooming = Value->GetBool("Scroll"); } // Depends on Document entry

				Text_Color = Value->GetColor("Color_1"); 	
				Background_Color = Value->GetColor("Color_2");

				X_Ratio = Value->GetInt("X_Ratio");
				Y_Ratio = Value->GetInt("Y_Ratio");


				/* // Doesent work
				if (pAnimModel)
				{   pAnimModel->SetRenderColorR((byte)Background_Color[0]);
					pAnimModel->SetRenderColorG((byte)Background_Color[1]);
					pAnimModel->SetRenderColorB((byte)Background_Color[2]);
					pAnimModel->SetRenderColorA((byte)Background_Color[3]);
				}*/

			} else { Note_Text = ""; Doc_Texture = ""; }

				
			Refresh_Proportions();

			Current_Document = List->GetSelectedItem(0) + 1; 
			
			if (Screen_Text) { Screen_Text->SetText(""); } // Clearing from last usage

			Print_X = Start_Print_X;
			Print_Y = Start_Print_Y;
		}
	}
}


//-----------------------------------------------------------------------------
// Only if the player posesses items with a certain Note_ID, this will return true
//-----------------------------------------------------------------------------
bool CInventory::Does_Player_Have(int Selcted_ID)
{
	C_BasePlayer *player = C_BasePlayer::GetLocalPlayer();
	if (!player) { return false; }


	// Cycle down, through the index of all items the player posesses
	for (int i = player->Get_Paper_Number(0, "Note_Count"); i > 0; i--)
	{
		if (player->Get_Paper_Number(i, "Doc_Type") != Current_Tab - 2 ||
			// "Doc_Status" 4 means a Objective is Hidden so we skip this entry, 
			// Which isn't supposed to exclude Items, Keys and ID Cards in slot 0
			player->Get_Paper_Number(i, "Doc_Type") == 2 && player->Get_Paper_Number(i, "Doc_Status") == 4) {
			continue;
		}

		// If one of all entries match the Selected_ID we're looking for, then we know the player got it!
		if (Selcted_ID == player->Get_Paper_Number(i, "ID")) { return true; }
	}

	return false;
}

//-----------------------------------------------------------------------------
// Called from a Command above when the "Button_Previous_Page" and "Button_Next_Page" are pressed in the Inventory.
//-----------------------------------------------------------------------------

void CInventory::Get_Page_ID(bool Should_Increment)
{

	if (Previous_Page == null && Next_Page == null || Previous_Page == 0 && Next_Page == 0) { return; } // Failsafe

	int Selection = 0;
	if (Should_Increment) { Selection = Next_Page; } // Msg("Next Page is %d.\n", i); }
	else { Selection = Previous_Page; } // Msg("Previous Page is %d.\n", i); }
	if (Selection < 0) { Msg("No Page found."); return; }



	C_BasePlayer *player = C_BasePlayer::GetLocalPlayer();

	if (player && List)
	{	
		/* // Doesen't work
		int Position = player->Get_Paper_Number(i, "Position");
		if (Position) // If available in player inventory we call the Document from there
		{   Msg("Position is %d\n", Position);
			List->SetSingleSelectedItem(Position + 1); return;
		}


		// Cycle down, through the index of all items the player posesses
		for (int i = List->GetItemCount() -1; i > 0; i--)
		{
			KeyValues *Value = List->GetItem(i);


			// If title matches the one of the document we expect
			// if (Value->GetInt("Note_ID") == Selection);
			if ((char*)Value->GetString("Title") == Localize_Document(Selection, "Title"));			
			{   Msg("Position is %d\n", Selection);
				List->SetSingleSelectedItem(i);
				return;
			}
		}
		*/
		


		int Temporal_Slot = List->GetItem("Temporal_Entry");					
		KeyValues *Value = List->GetItem(Temporal_Slot);
		// DevMsg("Entry is %d\n", List->GetItem("Temporal_Entry"));


		if (Value) // Will einfach nicht �ber 10 rendern...
		{		
			Clear_Screen(); 

			char Buffer[1024];
			Buffer[0] = 0;
			Current_Page = Selection; // Resetting for the "Next" entries below

			Q_snprintf(Buffer, sizeof(Buffer), "#Doc_Prev_Page_%d", Selection);
			// This disables the Buttons once the chain of "next" entries is broken by a 0 entry
			char* Page = Localize_Text(Buffer);

			// If the keyword "Next" is used we decrement by -1
			if (Compare(Page, "Next") || Compare(Page, "next")) { Previous_Page = Current_Page - 1; }
			else if (!Compare(Page, "") && !Compare(Page, "0")) { Previous_Page = atoi(Page); } // If any number was found
			else { Previous_Page = 0; } // This indirectly disables the Button
		

			vgui::Panel * The_Button = FindChildByName("Button_Previous_Page");					 
			if (The_Button && Previous_Page && Does_Player_Have(Previous_Page)) { The_Button->SetVisible(true); }
			else { The_Button->SetVisible(false); }
			


			Buffer[0] = 0;
			Q_snprintf(Buffer, sizeof(Buffer), "#Doc_Next_Page_%d", Selection);
			Page = Localize_Text(Buffer);
		
			if (Compare(Page, "Next") || Compare(Page, "next")) { Next_Page = Current_Page + 1; }
			else if (!Compare(Page, "") && !Compare(Page, "0")) { Next_Page = atoi(Page); } 
			else { Next_Page = 0; } 

			The_Button = FindChildByName("Button_Next_Page");
			if (The_Button && Next_Page && Does_Player_Have(Next_Page)) { The_Button->SetVisible(true); }
			else { The_Button->SetVisible(false); }
	


			// Need to store all of this as Value otherwise it's getting deleted with the temporary buffer variables
			// Value->SetString("Background", Localize_Document(i, "Doc_Background"));

			// Please don't confuse Localize_Text() with Localize_Document(), which is used by Update_Documents() and Get_Selected_Document() above!
			// This is more direct then the Localize_Document() method. 
			Buffer[0] = 0;
			Q_snprintf(Buffer, sizeof(Buffer), "#Doc_Background_%d", Selection);
			Value->SetString("Background", Localize_Text(Buffer));
			Doc_Texture = (char*)Value->GetString("Background");	
		
			// Msg("Title is %s and Texture is %s\n", (char*)Value->GetString("Title"), Doc_Texture);

			Weapon_Info[0] = 0;
			// Printing item name into Weapon info, in case that item has a model that can be viewed
			Q_snprintf(Weapon_Info, sizeof(Weapon_Info), "%s", (char*)Value->GetString("Title"));


			// Not using Localize_Text() here cause its 1024 Buffer is not large enough		
			Buffer_Text[0] = 0;
			Q_snprintf(Buffer_Text, sizeof(Buffer_Text), "#Doc_Text_%d", Selection);
			Value->SetString("Message", Localize_Text(Buffer_Text, 4096));
			Note_Text = (char*)Value->GetString("Message");
			

			
			// DevMsg("%s\n", Note_Text); 
			
	

			// Set the Keyvalue "Color_1" to the Color we extract from the Localize text with the same name.
			Value->SetColor("Color_1", Get_Color(Selection, "Color_1"));
			Text_Color = Value->GetColor("Color_1");

			// Bool Value disables Objective color checks
			Value->SetColor("Color_2", Get_Color(Selection, "Color_2"));
			Background_Color = Value->GetColor("Color_2");


			// =============== Getting Ratio ==============
	
			Q_snprintf(Buffer, sizeof(Buffer), "#Doc_Width_%d", Selection);
			float X_Width = atof(Localize_Text(Buffer));
	
			Q_snprintf(Buffer, sizeof(Buffer), "#Doc_Height_%d", Selection);
			float Y_Heigth = atof(Localize_Text(Buffer));
			
			if (X_Width > Y_Heigth)
			{   Y_Heigth = (Y_Heigth / X_Width) * 10;

				Value->SetInt("X_Ratio", 10);
				Value->SetInt("Y_Ratio", Y_Heigth);
			}
			else // if (Y_Heigth > X_Width)
			{   X_Width = (X_Width / Y_Heigth) * 10;

				Value->SetInt("X_Ratio", X_Width);
				Value->SetInt("Y_Ratio", 10);
			}

			if (X_Width == 0 && Y_Heigth == 0) // Failsafe
			{   Value->SetInt("X_Ratio", 1);
				Value->SetInt("Y_Ratio", 1);
			}

			X_Ratio = Value->GetInt("X_Ratio");
			Y_Ratio = Value->GetInt("Y_Ratio");

		
			Refresh_Proportions();
			// DevMsg("X is %d, Y is %d\n", X_Ratio, Y_Ratio);
			// ================================================

			Get_Target_Images();


			Q_snprintf(Buffer, sizeof(Buffer), "#Doc_Can_Scroll_%d", Selection);
			Value->SetString("Scroll", Localize_Text(Buffer));
			if (Compare((char*)Value->GetString("Scroll"), "")) { Allow_Zooming = true; } // Not specified, so allowd by default
			else { Allow_Zooming = Value->GetBool("Scroll"); }	
	
		}
	}
}



//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
Color CInventory::Get_Color(int i, char* Color_Name)
{
	C_BasePlayer *Player = C_BasePlayer::GetLocalPlayer();

	int ID = i;
	// If the player has this Document we get it from his Inventory Array,
	if (Player->Get_Paper_Number(i, "ID")) { ID = Player->Get_Paper_Number(i, "ID"); }
	else // Then we need to find out whether the player got this Document in his Inventory Array the other way around:
	{   // Set i if we can find any position 
		if (Player->Get_Paper_Number(i, "Position")) { i = Player->Get_Paper_Number(i, "Position"); }
	}


	if (Player)
	{
		char Entry[32];  Entry[0] = 0;
		if (Compare(Color_Name, "Color_1")){ Q_snprintf(Entry, sizeof(Entry), "#Doc_Text_Color_%d", ID); }
		else if (Compare(Color_Name, "Color_2")){ Q_snprintf(Entry, sizeof(Entry), "#Doc_Paper_Color_%d", ID); }

		char* Color_Entry = Localize_Text(Entry); // For all Notes and Maps
		// DevMsg("Color is %s\n", Color_Entry);



		// If there is no "Doc_Text_Color_" specified in Localization.txt this overwrites the Global Colors for Objectives
		if (Compare(Color_Entry, "")
			&& Player->Get_Paper_Number(i, "Doc_Type") == 2 && !Compare(Color_Name, "Color_2")) // And if entry i of Objective type
		{
			if (Player->Get_Paper_Number(i, "Doc_Status") == 0) { Color_Entry = Localize_Text("Objective_Color_Resolved"); }
			else
			{
				Entry[0] = 0; // Append Status Number to this String, which returns Objective Color from Localize.txt file
				Q_snprintf(Entry, sizeof(Entry), "Objective_Color_Pririty_%d", Player->Get_Paper_Number(i, "Doc_Status"));
				Color_Entry = Localize_Text(Entry);
			}
		}

		if (Compare(Color_Entry, "")) // Failsafe
		{
			if (Compare(Color_Name, "Color_2")) { Color_Entry = "255, 255, 255, 255"; }
			// If empty we override with gray, which is visible for both: dark and bright surfaces.
			else
			{
				Color_Entry = Localize_Text("Color_Neutral"); // This affects Objectives the player has not activated yet too.
			}
		}



		char Col_R[80];
		char Col_G[80];
		char Col_B[80];
		char Col_Alpha[80];
		int Color_1R, Color_1G, Color_1B, Color_1Alpha;

		Split_Array(Color_Entry, Col_R, ',', 1);
		Split_Array(Color_Entry, Col_G, ',', 2);
		Split_Array(Color_Entry, Col_B, ',', 3);
		Split_Array(Color_Entry, Col_Alpha, ',', 4);

		Color_1R = V_atof(Col_R);
		Color_1G = V_atof(Col_G);
		Color_1B = V_atof(Col_B);
		Color_1Alpha = V_atof(Col_Alpha);

		// DevMsg("Text Color is %s\n", Color_Entry);
		return Color(Color_1R, Color_1G, Color_1B, Color_1Alpha);

	}

}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CInventory::Get_Target_Images()
{
	C_BasePlayer *player = C_BasePlayer::GetLocalPlayer();
	// DevMsg("Current Page is %d\n", Current_Page);
	if (!Current_Page) { return; }


	if (player)
	{
		char Buffer_Image[1024];
		// int Empty_Slots = 0;

		for (int i = 1; i < 11; i++)
		{

			if (!Compare(Doc_Image[i], "")) // If has any Picture
			{	// We're clearing everything from last Document
				Doc_Target_X[i] = 0;
				Doc_Target_Y[i] = 0;
				Doc_Target_Mode[i] = 0;

				Target_Zoom_Width[i] = 0;
				Target_Zoom_Height[i] = 0;

				Doc_Image[i][0] = 0;
				Doc_Target_Pic[i][0] = 0;
			}
			// else { Empty_Slots++; }



			char Adress[48]; Adress[0] = 0;
			Q_snprintf(Adress, sizeof(Adress), "#Doc_%d_Image_%d", i, Current_Page);
			char* The_Image = Localize_Text(Adress);


			// Too many entries to store them above through  "Data->SetString("Target", xxx);" so we fetch them Live for each Document.
			// (char*)Value->GetString("Target");
			// DevMsg("Map Target Coordinates are %s\n", Coordinates);

			if (Compare(The_Image, "")) { continue; }
			else // Otherwise grabbing the 4 Values it contains
			{
				char Tar_X[80];
				char Tar_Y[80];
				char Tar_Scale[80];
				char Tar_Mode[80];

				Tar_X[0] = 0;
				Tar_Y[0] = 0;
				Tar_Scale[0] = 0;
				Tar_Mode[0] = 0;

				Doc_Image[i][0] = 0; // Clearing Entry on first level
				Doc_Target_Pic[i][0] = 0;

				Split_Array(The_Image, Tar_X, ',', 1);
				Split_Array(The_Image, Tar_Y, ',', 2);
				Split_Array(The_Image, Tar_Scale, ',', 3);


				Split_Array(The_Image, Tar_Mode, ',', 4);
				if (!Compare(Tar_Mode, ""))
				{
					if (Character_Count(Tar_Mode) < 4) { Doc_Target_Mode[i] = V_atof(Tar_Mode); } // Then it is a Mode Flag
					else // 4 Means parse the Tar_Mode text itself as Texturepath
					{
						Doc_Target_Mode[i] = 4;

						Split_Array(The_Image, Doc_Target_Pic[i], ',', 4);
					}
				}



				Split_Array(The_Image, Doc_Image[i], ',', 5); // Getting Image Picture into second array level variable

				// If these two are not 0 the paint function above will print the Target Icon over our Background Image
				Doc_Target_X[i] = V_atof(Tar_X);
				Doc_Target_Y[i] = V_atof(Tar_Y);

				Target_Zoom_Width[i] = V_atof(Tar_Scale);
				Target_Zoom_Height[i] = Target_Zoom_Width[i];



				//DevMsg("Map Target Position is %d + %d, Size is %d and Imagename is %s\n", 
				// Doc_Target_X, Doc_Target_Y, Target_Zoom_Width, Doc_Image);
			}


			// Resetting to normal size
			Target_Height = 30;
			Target_Width = 30;

		}

		/*
		int itemId = List->GetSelectedItem(0);
		KeyValues *Value = List->GetItem(itemId);

		if (Value)
		{
		// Only if this Document has NONE of its image Slots assigned it is allowed to Zoom
		// Otherwise it would just look like a mess because I never completed Zoom for child images
		// if (Empty_Slots == 10)
		// {	// Only allow zooming for Documents that are supposed to do it.
		if (Compare((char*)Value->GetString("Scroll"), "")) { Allow_Zooming = true; } // Not specified, so allowd by default
		else { Allow_Zooming = Value->GetBool("Scroll"); } // Depends on Document entry
		// } else { Allow_Zooming = false; }
		}*/
	}
}



//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CInventory::Localize_Entry(char* Entry, char* Result)
{   /*char Buffer[1024]; Buffer[0] = 0;*/
	g_pVGuiLocalize->ConvertUnicodeToANSI(g_pVGuiLocalize->Find(Entry), Result, 1024);
	// Result* = (char*)&Buffer;
}


// ---------------------------------------------------------------------------- -
// Purpose: Test
//-----------------------------------------------------------------------------
char* CInventory::Localize_Document(int Document_ID, char* Entry)
{
	char Buffer[1024];
	Buffer[0] = 0;

	C_BasePlayer *player = C_BasePlayer::GetLocalPlayer();

	if (player) { g_pVGuiLocalize->ConvertUnicodeToANSI(player->Get_Localized_Text(Document_ID, Entry), Buffer, 1024); }
	return (char*)Buffer;
}



// ---------------------------------------------------------------------------- -
// Purpose: Localizes an entry from Language.txt
//-----------------------------------------------------------------------------
char* CInventory::Localize_Text(char* Entry, int Buffer_Size)
{
	if (Buffer_Size >= 4096)
	{ 
		char Buffer[4096];	
		Buffer[0] = 0;
		g_pVGuiLocalize->ConvertUnicodeToANSI(g_pVGuiLocalize->Find(Entry), Buffer, 4096);
		return (char*)Buffer;
	}
	else if (Buffer_Size >= 2048)
	{
		char Buffer[2048];
		Buffer[0] = 0;
		g_pVGuiLocalize->ConvertUnicodeToANSI(g_pVGuiLocalize->Find(Entry), Buffer, 2048);
		return (char*)Buffer;
	}
	else 
	{
		char Buffer[1024];
		Buffer[0] = 0;
		g_pVGuiLocalize->ConvertUnicodeToANSI(g_pVGuiLocalize->Find(Entry), Buffer, 1024);
		return (char*)Buffer;

	} 
		
}



//-----------------------------------------------------------------------------
// Purpose: Count Characters (testing whether char arrays are empty)
//-----------------------------------------------------------------------------
int CInventory::Character_Count(char* value_1)
{   // Looping through all Chars in the value_1 array
	int Counter = 0;

	for (int i = 0; i < sizeof(value_1); i++)
	{
		if (value_1[i] == '\0') { return Counter; }
		Counter++;
	}
	return Counter;
}

//-----------------------------------------------------------------------------
// Purpose: Count Characters (testing whether char arrays are empty)
//-----------------------------------------------------------------------------
bool CInventory::Makes_Sense(char* value_1, int Expected_Count)
{
	int Counter = 0;

	for (int i = 0; i < sizeof(value_1); i++) 
	{	// If more then 33% of the characters in this array are letters or the Expected_Count was reached I guess it makes sense XD
		if (Counter > (sizeof(value_1) / 3) || Counter > Expected_Count - 1) { return true; }
		else if ((value_1[i] >= 'a' &&  value_1[i] <= 'z') 
			  || (value_1[i] >= 'A' && value_1[i] <= 'Z') || value_1[i] == '_') { Counter++; }
	}

	return false;
}


//bool CInventory::Makes_Sense(char* value_1, int Expected_Count)
//{
//	int Counter = 0;
//
//	for (int i = 0; i < sizeof(value_1); i++)
//	{	// If more then 33% of the characters in this array are letters or the Expected_Count was reached I guess it makes sense XD
//		if (Counter >(sizeof(value_1) / 3) || Counter > Expected_Count - 1) { return true; }
//		else if ((i >= 'a' &&  i <= 'z') || (i >= 'A' && i <= 'Z')) { Counter++; }
//	}
//
//	return false;
//}


//-----------------------------------------------------------------------------
// Purpose: Refreshing Size 
//-----------------------------------------------------------------------------
void CInventory::Clear_Screen()
{
	Note_Text = "";
	
	Doc_Texture = "";
	if (Screen_Text){ Screen_Text->SetText(""); }
	if (Render_Zone){ Render_Zone->SetText(""); }

	if (pAnimModel)
	{   Doc_Model = "";
		pAnimModel->Remove();
		pAnimModel = NULL;
	}

	for (int i = 1; i < 11; i++) // 11 cause there are Max 10 slots
	{
		// We're clearing everything from last Document		
		Doc_Target_X[i] = 0;
		Doc_Target_Y[i] = 0;
		Doc_Target_Mode[i] = 0;

		Target_Zoom_Width[i] = 0;
		Target_Zoom_Height[i] = 0;

		Doc_Image[i][0] = 0;
		Doc_Target_Pic[i][0] = 0;		
	}
}

//-----------------------------------------------------------------------------
// Purpose: Refreshing Size 
//-----------------------------------------------------------------------------
void CInventory::Refresh_Proportions()
{
	int Win_X, Win_Y, Item_X, Item_Y;
	GetSize(Win_X, Win_Y); // Win_Y = GetTall();
	List->GetSize(Item_X, Item_Y); // Adjust Note Controls to the Widnow Size


	// Incrase by 30% to get the European A4 Letter format	
	if (X_Ratio / Y_Ratio == 2 || Y_Ratio / X_Ratio == 2)
	{
		Print_Width = ((Win_X - Frame_X_Delta) / Chopped_Ratio) * Full_Ratio;
		Print_Height = ((Win_Y - Frame_Y_Delta) / Chopped_Ratio) * Full_Ratio;
	}
	else
	{	// If no Scroll is allowed we stick with the Size parameters specified in .\Resource\UI\Inventory.res\Inventory
		if (Allow_Zooming)
		{
			Print_Width = Win_X - Frame_X_Delta;
			Print_Height = Win_Y - Frame_Y_Delta;
		}
		else
		{
			Print_Width = Resource_Width - Frame_X_Delta;
			Print_Height = Resource_Height - Frame_Y_Delta;
		}
	}

	if (X_Ratio > Y_Ratio && Full_Screen) // Perventing it from being too large
	{  // End_X /= 3;  End_Y /= 3; // Funny Wobbling effect XD
		Print_Width = (Print_Width / 6) * 5;
		Print_Height = (Print_Height / 6) * 5;
	}


	if (List)
	{   List->GetSize(Item_X, Item_Y); // Adjust Note Controls to the Widnow Size
		// Additionally -30 because the Render_Zone position is by 30 lower then the "List"
		// 98 because of the -70 + additional 28
		if (Win_Y > 0 && Item_Y != Print_Height) { List->SetSize(Item_X, Win_Y - 98); }
	}

	if (Render_Zone)
	{   Render_Zone->GetSize(Item_X, Item_Y);
		Render_Zone->GetPos(Start_Print_X, Start_Print_Y);
		if (Win_Y > 0 && Item_Y != Print_Height) { Render_Zone->SetSize(Print_Width, Print_Height); }
	}

}



//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
char* CInventory::Get_Name(C_BaseCombatWeapon * Weapon)
{
	C_BasePlayer *Player = C_BasePlayer::GetLocalPlayer();
	if (Player && Weapon)
	{
		wchar_t* Entry = g_pVGuiLocalize->Find(Weapon->GetWpnData().szPrintName);

		char Buffer[240]; // Never use MAX_WEAPON_STRING, it somehow breaks this
		Buffer[0] = 0;
		g_pVGuiLocalize->ConvertUnicodeToANSI(Entry, Buffer, 240);

		// If no Localization was found we return the straight text
		if (Compare(Buffer, "")) { return (char*)Weapon->GetWpnData().szPrintName; }
		else { return (char*)Buffer; }
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CInventory::Draw_Text(int x, int y, char* text, int R, int G, int B, int Alpha)
{
	surface()->DrawSetTextFont(hFont2);
	surface()->DrawSetTextColor(R, G, B, Alpha);
	surface()->DrawSetTextPos(x, y);

	mbstowcs(&unicode[0], text, 4096); // Max 4096 characters
	surface()->DrawPrintText(unicode, wcslen(unicode));
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CInventory::Split_Into(char* Source, char Delimiter, char* P1, char* P2, char* P3, char* P4, char* P5, char* P6, char* P7, char* P8)
{
	// They're defaultiing to "", if any char* was passed as argument it will get the parameter from its position
	if (P1[0] > 0) { Split_Array(Source, P1, Delimiter, 1); }
	if (P2[0] > 0) { Split_Array(Source, P2, Delimiter, 2); }
	if (P3[0] > 0) { Split_Array(Source, P3, Delimiter, 3); }
	if (P4[0] > 0) { Split_Array(Source, P4, Delimiter, 4); }

	if (P5[0] > 0) { Split_Array(Source, P5, Delimiter, 5); }
	if (P6[0] > 0) { Split_Array(Source, P6, Delimiter, 6); }
	if (P7[0] > 0) { Split_Array(Source, P7, Delimiter, 7); }
	if (P8[0] > 0) { Split_Array(Source, P8, Delimiter, 8); }
}

//-----------------------------------------------------------------------------
// Loop until the delimiter char sign is found ',' then return the first value in value_2
//-----------------------------------------------------------------------------
char* CInventory::Split_Array(char* value_1, char* value_2, char delimiter, int word_number)
{
	int Word_Count = 0;
	int Charater_Slot = 0;

	for (int i = 0; i < 80; i++) // 80 is used in most cases
	{
		if (value_1[i] == delimiter)
		{
			Word_Count++; // We raise wordcount after each delimiter, if count matches desired word we return it
			if (Word_Count == word_number) { value_2[Charater_Slot] = '\0'; return value_2; }
			else { Charater_Slot = 0; } // Otherwise we start overwriting by the next word/value
		}
		else if (value_1[i] != ' ') // Ignoring emptyspaces
		{   // We transfere values over to the new char array
			value_2[Charater_Slot] = value_1[i];
			Charater_Slot++;
		}
	}
	return null;
}

//-----------------------------------------------------------------------------
// Set Selected item
//-----------------------------------------------------------------------------
void CInventory::SetSelecteditem(int i)
{
	C_BasePlayer *player = C_BasePlayer::GetLocalPlayer();
	selecteditem = i;
	
	
	if (player->GetWeapon(i-1))
	{
		if ( selecteditem == i ) UseItem(i, 0); // Slot Shift is 0 here

		selecteditem = i;

		// Resetting Model Zoom/Position/Rotation
		m_fZoom = 34;
		m_fLeft = 0;
		m_fTop = 0;
		m_fx = 0;
		m_fy = 0;
		m_fz = 0;
		m_fLeft = 0;

		const FileWeaponInfo_t &weaponInfo = player->GetWeapon(i-1)->GetWpnData();
		if ( pAnimModel ) pAnimModel->Remove(); // Removing old values

		pAnimModel = new C_BaseAnimatingOverlay;
		pAnimModel->InitializeAsClientEntity( weaponInfo.szWorldModel, RENDER_GROUP_OPAQUE_ENTITY );
		pAnimModel->AddEffects( EF_NODRAW ); // don't let the renderer draw the model normally

	
		/* // Outdated
		char temp[32];
		strcpy(temp,player->GetWeapon(i-1)->GetClassname());
		item = nexttoken( temp2, temp, '_' );				
		}*/
	}
}

//-----------------------------------------------------------------------------
// Purpose: String Comparsion
//-----------------------------------------------------------------------------
bool CInventory::Compare(char* value_1, char* value_2)
{
	if (strcmp(value_1, value_2) == 0) { return true; }
	return false;
}


//-----------------------------------------------------------------------------
// Purpose: Run command and append an int
//-----------------------------------------------------------------------------
void CInventory::Print_Command(char* value_1, int value_2)
{   char Command[48];
	Q_snprintf(Command, sizeof(Command), value_1, " %d"); // Appending a digit.
	Q_snprintf(Command, sizeof(Command), Command, value_2); 
	engine->ClientCmd(Command); 
}


//-----------------------------------------------------------------------------
// Print Position
//-----------------------------------------------------------------------------
void CInventory::PrintPosition()
{
	Msg("-----------\nWeapon Preview Position\nZoom : %f\nX : %f\nY : %f\nZ : %f\nTop : %f\nLeft : %f\n-----------\n",m_fZoom,m_fx,m_fy,m_fz,m_fTop,m_fLeft);
}
