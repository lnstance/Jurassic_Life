#include "hudelement.h"
#include "ImageProgressBar.h"


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
class CHealth_Bar : public CHudElement, public vgui::Panel
{
	DECLARE_CLASS_SIMPLE(CHealth_Bar, vgui::Panel);

public:

	ImageProgressBar* Bar; // Temporary bar used in Create_Status_Bar();

	int i_Bar_X_Offset = 8;
	int i_Bar_Length = 128; // 140
	int Bar_Position_1 = 2;
	int Bar_Position_2 = 18;
	int Bar_Position_3 = 34;

	ImageProgressBar *Stamina_Bar;
	int Stamina = 0;
	int New_Stamina = 0;

	ImageProgressBar *Armor_Bar;
	int Armor = 0;
	int New_Armor = 0;

	ImageProgressBar *Health_Bar;
	int Health = 0;
	int New_Health = 0;

	bool Has_Weapon = false;
	bool Reset_Settings = false;

	CHealth_Bar(const char *pElementName);
	void ApplySchemeSettings(vgui::IScheme *pScheme);
	void Init();
	void MsgFunc_Battery(bf_read &msg);
	int Get_Armor();

protected:
	// virtual void OnTick();
	virtual void Think();
	virtual void Paint();
	int i_Texture_Slot;
	ImageProgressBar* Create_Status_Bar(const char* Bar_Texture, int x, int y, bool Visible, int width, int height, char direction);

private:
	void	Draw_Icon(int texture);
};