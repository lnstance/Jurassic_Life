//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: Basic button control
//
// $NoKeywords: $
//=============================================================================//

#include "cbase.h"

#include <vgui/IVGui.h>
#include <vgui/IInput.h>
#include <vgui/IScheme.h>
#include <vgui/ISurface.h>
#include <vgui/ISystem.h>
#include <vgui/IVGui.h>
#include <vgui/MouseCode.h>
#include <vgui/KeyCode.h>
#include <KeyValues.h>

#include "ButtonSlot.h"
#include "hud_weapon_bars.h"
#include <vgui_controls/Scrollbar.h>

#include "c_item_case.h"



// #include "hud_jl_weaponselection.h"
//#include </*../../game/shared/*/basecombatweapon_shared.h>


// memdbgon must be the last include file in a .cpp file!!!
#include <tier0/memdbgon.h>

using namespace vgui;

DECLARE_BUILD_FACTORY_DEFAULT_TEXT( ButtonSlot, ButtonSlot );

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
ButtonSlot::ButtonSlot(Panel *parent, const char *panelName, const char *text, Panel *pActionSignalTarget, const char *pCmd)
: Button(parent,panelName,text,pActionSignalTarget,pCmd)
{
	Init();
}

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
ButtonSlot::ButtonSlot(Panel *parent, const char *panelName, const wchar_t *text, Panel *pActionSignalTarget, const char *pCmd)
: Button(parent,panelName,text,pActionSignalTarget,pCmd)
{
	Init();
}

void ButtonSlot::Init()
{
	SetDragEnabled(true);
	SetDropEnabled(true,0.5);
	//LinkWeaponSlot(NULL);
	LinkSlot(-1,true);
	m_iItemType = -1;
	
	SetPaintBackgroundEnabled( false );

	m_iEmptyTexture = 0;
	m_fEmptyTextureUV[0] = 0.f;
	m_fEmptyTextureUV[1] = 0.f;
	m_fEmptyTextureUV[2] = 1.f;
	m_fEmptyTextureUV[3] = 1.f;


	// This defines size of the Weapon Icon to be same as weaponselection.cpp
	if (Range > 9 * 64) { Range = 9 * 64; } // == 8*64 = 512
	Height = Range / (float)9; // = 512 scaled down
	Width = Height * 2.0; // = 1024 scaled down


	//====== Text Labels ======
	vgui::HScheme scheme = vgui::scheme()->GetScheme("ClientScheme");
	hFont = vgui::scheme()->GetIScheme(scheme)->GetFont("Mod_Font");
	charWidth = surface()->GetCharacterWidth(hFont, '0');

	hFont2 = vgui::scheme()->GetIScheme(scheme)->GetFont("HudHintTextLarge");
	charWidth2 = surface()->GetCharacterWidth(hFont2, '0');

	Reset_Settings = true;

	// Texture for i_Weapon_Icon updates at runtime for Active or Inactive state of each weapon
	Weapon_Icon = vgui::surface()->CreateNewTextureID();
	Weapon_Tile = vgui::surface()->CreateNewTextureID();



	CHud_Weapon_Bar *Weapon_Bar;

	char* Buffer_Image = Weapon_Bar->Localize_Text("Weapon_Bar_Selected");
	if (Weapon_Bar->Compare(Buffer_Image, "")){ Buffer_Image = "HUD/Weapons/Weapon_Bar_Selected"; } // Failsafe
	Texture_Slot = vgui::surface()->CreateNewTextureID();
	vgui::surface()->DrawSetTextureFile(Texture_Slot, Buffer_Image, true, false);

	Buffer_Image = Weapon_Bar->Localize_Text("Weapon_Bar_Slot");
	if (Weapon_Bar->Compare(Buffer_Image, "")){ Buffer_Image = "HUD/Weapons/Weapon_Bar_Slot"; }
	Texture_Selected = vgui::surface()->CreateNewTextureID();  
	vgui::surface()->DrawSetTextureFile(Texture_Selected, Buffer_Image, true, false);
}

/*void ButtonSlot::LinkWeaponSlot(C_BaseCombatWeapon** slot)
{
	m_pWeaponSlot=slot;
	if (m_pWeaponSlot && (*m_pWeaponSlot))
	{
		SetText((*m_pWeaponSlot)->GetClassname());
	}else{
		SetText("");
	}
}

C_BaseCombatWeapon** ButtonSlot::GetWeaponSlot()
{
	return m_pWeaponSlot;
}*/


//-----------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------
bool ButtonSlot::LinkSlot(int slot, bool playerWeapon, bool Hide_Slot)
{
	
	m_bPlayerWeapon = playerWeapon;
	m_iSlot = slot;

	if (Hide_Slot) { Show_Slot = false; }
	else { Show_Slot = true; }

	C_BasePlayer *Player = C_BasePlayer::GetLocalPlayer();
	C_BaseCombatWeapon* Weapon = NULL;


	if (m_iSlot != -1 && Player)
	{
		if (m_bPlayerWeapon) { Weapon = Player->GetWeapon(m_iSlot); }
		// Stored in player.cpp, this wires different crates together
		// Object_Scrollbar_Size < 1 means this Crate is a universal one stored in the player instead of the crate object
		else if (Object_Scrollbar_Size < 1) { Weapon = Player->Get_Player_Case(m_iSlot); }

		else if (Player->Get_Active_Case()) // stored in the active crate
		{ Weapon = reinterpret_cast<C_Item_Case*>(Player->Get_Active_Case())->m_nCaseItems[m_iSlot]; }	
	}


	if (Weapon) 
	{ 
		SetText(Weapon->GetClassname());

		/*
		if (strcmp(Weapon->GetClassname(), "weapon_shotgun") == 0)
		{
			Msg("Weapon is: %s\n", Weapon->GetClassname());


			// Document_Type:
			// 0 = ID Cards and Keys, 1 = Notes, 2 = Objectives, 3 = Maps;

			// Document_Status:
			// Notes + Maps: 1 = Unreaded, 2 = Readed
			// Objectives: 0 = resolved, 1-3 = Priority stages, 4 = Hidden
			// Items, Keys and ID Cards: 0 = Used (can be dropped), 4-1 = Access level, the lower the number the greater access

			// Player->AddNote(1, 1, 2);


			Player->Give_Note(2); 
			return true;
		} */
		
		
	}
	else { SetText(""); }

	return false;
}


//-----------------------------------------------------------------------------
// Stumb to detect weapons on client side and give the according notes/items to the player, this function was dropped though
//-----------------------------------------------------------------------------
bool ButtonSlot::Give_Item(ButtonSlot * Other_Slot)
{
	char temp[32];

	char* Required_Item = "weapon_shotgun";
	C_BasePlayer *Player = C_BasePlayer::GetLocalPlayer();
	C_BaseCombatWeapon *Weapon_1 = NULL;
	C_BaseCombatWeapon *Weapon_2 = NULL;

	if (Other_Slot->m_bPlayerWeapon) { Weapon_1 = Player->GetWeapon(Other_Slot->m_iSlot); }
	else if (Object_Scrollbar_Size < 1) { Weapon_1 = Player->Get_Player_Case(Other_Slot->m_iSlot); }
	else if (Player->Get_Active_Case()) { Weapon_1 = reinterpret_cast<C_Item_Case*>(Player->Get_Active_Case())->m_nCaseItems[Other_Slot->m_iSlot]; }


	if (m_bPlayerWeapon) { Weapon_2 = Player->GetWeapon(m_iSlot); }
	else if (Object_Scrollbar_Size < 1) { Weapon_2 = Player->Get_Player_Case(m_iSlot); }
	else if (Player->Get_Active_Case()) { Weapon_2 = reinterpret_cast<C_Item_Case*>(Player->Get_Active_Case())->m_nCaseItems[m_iSlot]; }


	if (Weapon_1)
	{
		if (strcmp(Weapon_1->GetClassname(), Required_Item) == 0)
		{
			Msg("Weapon is: %s\n", Required_Item);


			// Document_Type:
			// 0 = ID Cards and Keys, 1 = Notes, 2 = Objectives, 3 = Maps;

			// Document_Status:
			// Notes + Maps: 1 = Unreaded, 2 = Readed
			// Objectives: 0 = resolved, 1-3 = Priority stages, 4 = Hidden
			// Items, Keys and ID Cards: 0 = Used (can be dropped), 4-1 = Access level, the lower the number the greater access

			// Player->AddNote(1, 1, 2);
			Player->Give_Note(2);


			
			// If moving from the case towards player inventory
			if (m_bPlayerWeapon) 
			{	Q_snprintf(temp, sizeof(temp), "switchCaseToInv %d %d", Other_Slot->m_iSlot, -1); // -1 means wipe that slot!

				// Sending the command to the player entity to delete the weapon
				engine->ClientCmd(temp); // Switch item
			}
					

			return true;
		}
	}

	/*
	if (Weapon_2)
	{
		if (strcmp(Weapon_2->GetClassname(), Required_Item) == 0)
		{
			Msg("Weapon is: %s\n", Required_Item);


			Player->Give_Note(2);



			// If moving from the case towards player inventory
			if (m_bPlayerWeapon)
			{
				Q_snprintf(temp, sizeof(temp), "switchCaseToInv %d %d", -1, m_iSlot); // -1 means wipe that slot!

				// Sending the command to the player entity to delete the weapon
				engine->ClientCmd(temp); // Switch item
			}

			return true;
		}
	}
	*/


	return false;
}


//-----------------------------------------------------------------------------
// Imperial: Cases/Crates/Boxes for inventory work like this; 
// Player has a m_hActive_Case array, and each Case entity has a individual one too in item_case.cpp and networked into c_item_case.cpp.

// If the pointer to a case entity on the server is passed through a m_hActive_Case to the client, there the ButtonSlot.cpp can do this:
// C_BaseCombatWeapon* Weapon = reinterpret_cast<C_Item_Case*>(Player->Get_Active_Case())->m_nCaseItems[m_iSlot];   --> to grab the individual content arrays.
// Then it links the combatweapon it extracts from that crate array or player array to a player item slot and vice versa (item swapping)
// Inventory.cpp and item_case.cpp only deliver .\resource\ui\Inventory.res and CasePanel.res that contain information for the buttons that instanciate from the ButtonSlot.cpp class.
// item_case.cpp also offers some control over m_nCaseItems[] through Hammer to allow loading of starting inventory items right into them.
//-----------------------------------------------------------------------------


int ButtonSlot::GetSlot() { return m_iSlot; }

bool ButtonSlot::IsPlayerWeapon() { return m_bPlayerWeapon; }



C_BaseCombatWeapon* ButtonSlot::Get_Weapon_Slot() const
{	
	C_BasePlayer *Player = C_BasePlayer::GetLocalPlayer();
	C_BaseCombatWeapon* Weapon = NULL;

	if (m_iSlot != -1 && Player)
	{   if (m_bPlayerWeapon) { Weapon = Player->GetWeapon(m_iSlot); }
		else if (Object_Scrollbar_Size < 1) { Weapon = Player->Get_Player_Case(m_iSlot); } // Infinite crate type
		else if (Player->Get_Active_Case())
		{	// Need to reinterpret fromm C_Baseentity to C_Item_Case because #include the case.h file breaks c_baseplacer.h
			Weapon = reinterpret_cast<C_Item_Case*>(Player->Get_Active_Case())->m_nCaseItems[m_iSlot];		
		}
	}
	return Weapon;
}


//-----------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------
void ButtonSlot::Switch(ButtonSlot * Other_Slot)
{
	if (Other_Slot) // && m_iSlot > -1)
	{   C_BasePlayer *Player = C_BasePlayer::GetLocalPlayer();
		if (Player)
		{

			// Suggestion: You could use code from Give_Item() inside of here to get Weapon_1 and 2.
			
			/*
			if (Other_Slot->m_bPlayerWeapon)
			{
				Player->SetWeapon(Other_Slot->m_iSlot, weapon2);
			}else{
				Player->Set_Player_Case(Other_Slot->m_iSlot, weapon2);
			}

			if (m_bPlayerWeapon)
			{
				Player->SetWeapon(m_iSlot,weapon1);
			}else{
				Player->Set_Player_Case(m_iSlot,weapon1);
			}*/




			char temp[32];
			
			if (Other_Slot->m_bPlayerWeapon)
			{
				// DevMsg("Sending Player %d\n", m_iSlot);
				if (m_bPlayerWeapon) { Q_snprintf(temp, sizeof(temp), "switchInv %d %d", Other_Slot->m_iSlot, m_iSlot); }
				else { Q_snprintf(temp, sizeof(temp), "switchInvToCase %d %d", Other_Slot->m_iSlot, m_iSlot); }
			}
			else // Crate Weapon
			{
				// DevMsg("Sending %d to %d\n", slot->m_iSlot, m_iSlot);
				if (m_bPlayerWeapon) { Q_snprintf(temp, sizeof(temp), "switchCaseToInv %d %d", Other_Slot->m_iSlot, m_iSlot); }
				else { Q_snprintf(temp, sizeof(temp), "switchCase %d %d", Other_Slot->m_iSlot, m_iSlot); }
			}

			// Sending the command to the player entity to switch parameter 2 and parameter 3
			engine->ClientCmd(temp); // Switch item


			// Swap button slots, obsolete because they will be linked automatically 
			/*Other_Slot->LinkSlot(Other_Slot->m_iSlot, Other_Slot->m_bPlayerWeapon);
			LinkSlot(m_iSlot, m_bPlayerWeapon);*/
		}
	}
}


//-----------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------
void ButtonSlot::ApplySettings( KeyValues *inResourceData )
{
	Reset_Settings = true;

	BaseClass::ApplySettings( inResourceData );

	const char *emptyTexture =	inResourceData->GetString( "emptyTexture", NULL );
	if (emptyTexture)
	{
		m_iEmptyTexture = vgui::surface()->CreateNewTextureID();
		vgui::surface()->DrawSetTextureFile(m_iEmptyTexture,emptyTexture,true,false);

		if ( m_iEmptyTexture != 0 )
		{
			int wide, tall;
			vgui::surface()->DrawGetTextureSize(m_iEmptyTexture, wide, tall );
			m_fEmptyTextureUV[0] = inResourceData->GetInt( "emptyTextureUVMinX", 0 ) / (float)wide;
			m_fEmptyTextureUV[1] = inResourceData->GetInt( "emptyTextureUVMinY", 0 ) / (float)tall;
			m_fEmptyTextureUV[2] = inResourceData->GetInt( "emptyTextureUVMaxX", wide ) / (float)wide;
			m_fEmptyTextureUV[3] = inResourceData->GetInt( "emptyTextureUVMaxY", tall ) / (float)tall;
		}
	}
	m_iItemType = inResourceData->GetInt("itemType", -1);
}


//-----------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------
void ButtonSlot::ApplySchemeSettings(IScheme *pScheme)
{
	Reset_Settings = true;
	BaseClass::ApplySchemeSettings(pScheme);

	if ( m_iEmptyTexture != 0 )
	{
		SetButtonBorderEnabled(false);
		SetDefaultBorder(NULL);
		SetDepressedBorder( pScheme->GetBorder("ButtonSlotDepressedBorder") );
		SetBorder(NULL);
	}
}



//-----------------------------------------------------------------------------
// PAINT
//-----------------------------------------------------------------------------
void ButtonSlot::Paint(void)
{
	C_BasePlayer *Player = C_BasePlayer::GetLocalPlayer();
	C_BaseCombatWeapon* Weapon = NULL;
	if (!Player) { return; }

	
	if (Player->Get_Active_Case())
	{ Object_Scrollbar_Size = (reinterpret_cast<C_Item_Case*>(Player->Get_Active_Case()))->Get_Scrollbar_Size(); }


	if (m_iSlot != -1 && Player)
	{
		if (m_bPlayerWeapon) { Weapon = Player->GetWeapon(m_iSlot); } // DevMsg("Player Weapon\n"); }
		else if (Object_Scrollbar_Size < 1) { Weapon = Player->Get_Player_Case(m_iSlot); } // DevMsg("PlayerInventory Weapon\n"); }
		else if (Player->Get_Active_Case())
		{ Weapon = reinterpret_cast<C_Item_Case*>(Player->Get_Active_Case())->m_nCaseItems[m_iSlot]; } // DevMsg("Crate Weapon\n"); }
			
	}


	if (Weapon && Show_Slot)
	{	CHud_Weapon_Bar *Weapon_Bar; // Grabbing Variables from other Class
		
		if (Reset_Settings || Weapon_Bar->Get_Changes()) { Resetting_Settings(); }


		C_BaseCombatWeapon *Active_Weapon = Player->GetActiveWeapon();
		surface()->DrawSetColor(255, 255, 255, 255);


		Item_Type = 0;
		Item_Type = Weapon->GetHealth();

		// DevMsg("Weapon Name is %s\n", (char*)Weapon->GetName());

		bool Is_Item = false;

		if (Weapon_Bar->Compare((char*)Weapon->GetName(), "weapon_item"))
		{
			Is_Item = true;
		}
		



		// Inverted Selection means contrast to darker Background, otherwise we want a brighter panel texture to work with darker icons!	
		if (Invert_Selection)
		{   if (Weapon == Active_Weapon && Weapon->GetWpnData().iconActive)
			{   
				if (Is_Item) { Set_Item_Icon(Item_Type); }
				else
				{
					s_Img_Selection = Weapon->GetWpnData().iconActive->szTextureFile;

					if (Weapon_Bar->Compare(s_Img_Selection, "WeaponIconsSelected") && Weapon->GetWpnData().iconInactive) // If returns default value
					{
						Buffer[0] = 0; // We try to append _Lit to the fully colored version
						sprintf(Buffer, "%s_Lit", Weapon->GetWpnData().iconInactive->szTextureFile);
						s_Img_Selection = Buffer;
					}
				}

				if (Show_Selector && !Weapon_Bar->Compare(s_Img_Selection, "WeaponIcons_Lit")) 
				{ Draw_Icon(Texture_Selected); } 
				else if (Show_Background && m_bPlayerWeapon) { Draw_Icon(Texture_Slot); }
			}
			else if (Weapon->GetWpnData().iconInactive)
			{   
				if (Is_Item) { Set_Item_Icon_Inverted(Item_Type); }
				else { s_Img_Selection = Weapon->GetWpnData().iconInactive->szTextureFile; }

				if (Show_Background && m_bPlayerWeapon) { Draw_Icon(Texture_Slot); }
			}			
		}
		else if (!Invert_Selection)
		{
			if (Weapon == Active_Weapon && Weapon->GetWpnData().iconInactive)
			{
				if (Is_Item) { Set_Item_Icon(Item_Type); }
				else { s_Img_Selection = Weapon->GetWpnData().iconInactive->szTextureFile; }

				if (Show_Selector && !Weapon_Bar->Compare(s_Img_Selection, "WeaponIcons_Lit")) { Draw_Icon(Texture_Slot); }  // if (Show_Background) 
				else if (Show_Background && m_bPlayerWeapon) { Draw_Icon(Texture_Selected); }
			}
			else if (Weapon->GetWpnData().iconActive)
			{
				if (Is_Item) { Set_Item_Icon_Inverted(Item_Type); }
				else 
				{
					s_Img_Selection = Weapon->GetWpnData().iconActive->szTextureFile;

					if (Weapon_Bar->Compare(s_Img_Selection, "WeaponIconsSelected") && Weapon->GetWpnData().iconInactive)
					{
						Buffer[0] = 0;
						sprintf(Buffer, "%s_Lit", Weapon->GetWpnData().iconInactive->szTextureFile);
						s_Img_Selection = Buffer;
					}
				}

				if (Show_Background && m_bPlayerWeapon) { Draw_Icon(Texture_Selected); }
			}
		}


		//---------------------------------------------------------
		// Drawing the actual icon over the background texture:
		//---------------------------------------------------------
		bool Has_Icon = true; // we assume


		// if (s_Img_Selection != null && Character_Count(Weapon->GetWpnData().iconActive->szTextureFile) > 2)
		if (s_Img_Selection[0]) // && Weapon->GetWpnData().iconActive)
		{
			if (!Weapon_Bar->Compare(s_Img_Selection, "WeaponIcons") && !Weapon_Bar->Compare(s_Img_Selection, "WeaponIcons_Lit")
				&& !Weapon_Bar->Compare(s_Img_Selection, (char*)"WeaponIconsSelected") && !Weapon_Bar->Compare(s_Img_Selection, (char*)"WeaponIconsSelectedLit") )			
			{   
				bool Draw_Small_Icon = false;

				if (Weapon->GetItemType() == 0 && !m_bPlayerWeapon) // Large Slot Rifles for Crate/Case items
				{
					char s_Img_Selection_Large[156]; // Append _Large to the texture name we elaborated above
					Q_snprintf(s_Img_Selection_Large, sizeof(s_Img_Selection_Large), "%s_Large", s_Img_Selection);

					if (s_Img_Selection_Large[0])
					{   surface()->DrawSetTextureFile(Weapon_Icon, s_Img_Selection_Large, true, true);
						Draw_Large_Icon(Weapon_Icon);
					}
					else if (s_Img_Selection[0] && Weapon->GetWpnData().iconInactive) { Draw_Small_Icon = true; }
				} else { Draw_Small_Icon = true; }


				if (Draw_Small_Icon)
				{   surface()->DrawSetTextureFile(Weapon_Icon, s_Img_Selection, true, true);
					Draw_Icon(Weapon_Icon);
				}

			} else { Has_Icon = false; }
		} else { Has_Icon = false; }



		if (!Has_Icon) // Then we draw its text instad
		{
			char* Temp_A = Weapon_Bar->Get_Name(Weapon); // (char*)Weapon->GetWpnData().szPrintName;
			
			if (Character_Count(Temp_A) < 15)
			{	Draw_Text(Xpos + 1 + Width / 4, Ypos + 1 + Height / 3, Temp_A, Color_2R, Color_2G, Color_2B, Color_2Alpha);
				// Shadow Text will fail to Draw unless it is drawn from a second Get_Name(), don't ask me why...
				Draw_Text(Xpos + Width / 4, Ypos + Height / 3, Weapon_Bar->Get_Name(Weapon), Color_1R, Color_1G, Color_1B, Color_1Alpha);
			}
			else // Otherwise start drawing at left border for space purpose.
			{   Weapon_Bar->Draw_Text(Xpos + 1 + Width / 10, Ypos + 1 + Height / 3, Temp_A, Color_2R, Color_2G, Color_2B, Color_2Alpha);
				Weapon_Bar->Draw_Text(Xpos + Width / 10, Ypos + Height / 3, Weapon_Bar->Get_Name(Weapon), Color_1R, Color_1G, Color_1B, Color_1Alpha);
			}
		}


		/* // Old Code by CameleonTH
		if (weapon->GetWpnData().iconInactive)
		{
			if (weapon->GetWpnData().iconInactive->bRenderUsingFont)
			{	
				//vgui::surface()->DrawSetTextScale(fRatio,fRatio);
			}
			//weapon->GetWpnData().iconInactive->DrawSelf(xoff,yoff,GetWide()-xoff*2,GetTall()-yoff*2,Color(255,255,255,255));
			weapon->GetWpnData().iconInactive->DrawSelfCropped(xoff,yoff,
				0,0,
				weapon->GetWpnData().iconInactive->Width(), weapon->GetWpnData().iconInactive->Height(),
				GetWide()-xoff*2,GetTall()-yoff*2,
				Color(255,255,255,255));
		}*/

	}
	else if ( m_iEmptyTexture != 0 )
	{
		surface()->DrawSetColor(255, 255, 255, 255);
		/*
		m_pEmptyTexture->SetPos(0, 0);
		m_pEmptyTexture->SetSize(GetWide(),GetTall());
		m_pEmptyTexture->Paint();
		*/
		surface()->DrawSetTexture(m_iEmptyTexture);
		surface()->DrawTexturedSubRect(0, 0, GetWide(), GetTall(),
		m_fEmptyTextureUV[0], m_fEmptyTextureUV[1], m_fEmptyTextureUV[2], m_fEmptyTextureUV[3]);	
	}

	//BaseClass::Paint();
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void ButtonSlot::Set_Item_Icon(int Item_Type)
{
	/*	char * Image_All = "Hud/Weapons_Hl2/Machinegun";
	char * Image_Map = "Hud/Weapons/Machete";
	char * Image_Note = "Hud/Weapons/Medkit";
	char * Image_Item = "Hud/Weapons/Grenade";
	char * Image_Objective = "Hud/Weapons/Radio";*/

	// DevMsg("The Item Type is %d\n", Item_Type);

	// Type:
	// 0 = Items + ID Cards and Keys, 1 = Notes, 2 = Objectives, 3 = Maps;
	if (Item_Type == 7) { s_Img_Selection = Image_Note; }
	else if (Item_Type == 8) { s_Img_Selection = Image_Objective; }
	else if (Item_Type == 9) { s_Img_Selection = Image_Map; }
	else { s_Img_Selection = Image_Item; }
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void ButtonSlot::Set_Item_Icon_Inverted(int Item_Type)
{
	// DevMsg("Item Type is %d\n", Item_Type);

	// 0 = Items + ID Cards and Keys, 1 = Notes, 2 = Objectives, 3 = Maps;
	if (Item_Type == 7) { s_Img_Selection = Image_Note; }
	else if (Item_Type == 8) { s_Img_Selection = Image_Objective; }
	else if (Item_Type == 9) { s_Img_Selection = Image_Map; }
	else { s_Img_Selection = Image_Item; }

}



//-----------------------------------------------------------------------------
// Purpose: Draw Tile Background
//-----------------------------------------------------------------------------
void ButtonSlot::Draw_Icon(int texture)
{   surface()->DrawSetTexture(texture);
	surface()->DrawTexturedRect(Xpos, Ypos, Xpos + Width, Ypos +  Height);
}

void ButtonSlot::Draw_Large_Icon(int texture)
{   surface()->DrawSetTexture(texture);
	surface()->DrawTexturedRect(Xpos, Ypos, Xpos + (Width / 2) * 5, Ypos + Height);
}


//-----------------------------------------------------------------------------
// Purpose: Count Characters (testing whether char arrays are empty)
//-----------------------------------------------------------------------------
int ButtonSlot::Character_Count(char* value_1)
{   // Looping through all Chars in the value_1 array
	int Counter = 0;

	for (int i = 0; i < sizeof(value_1); i++)
	{
		if (value_1[i] == '\0') { return Counter; }
		Counter++;
	}
	return Counter;
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void ButtonSlot::Draw_Text(int x, int y, char* text, int R, int G, int B, int Alpha)
{   surface()->DrawSetTextFont(hFont2);
	surface()->DrawSetTextColor(R, G, B, Alpha);
	surface()->DrawSetTextPos(x, y);

	mbstowcs(&unicode[0], text, MAX_WEAPON_STRING); // Max 80 characters
	surface()->DrawPrintText(unicode, wcslen(unicode));
}


//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
bool ButtonSlot::IsDragEnabled() const
{
	if (Get_Weapon_Slot()) { return BaseClass::IsDragEnabled(); }
	return false;
}


//-----------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------
bool ButtonSlot::IsDroppable( CUtlVector< KeyValues * >& msglist )
{
	// DevMsg("IsDroppable %s\n", GetName());

	ButtonSlot *slot = static_cast<vgui::ButtonSlot*>(msglist[0]->GetPtr("panel"));
	if (slot)
	{
		// DevMsg("Droppable %d %d\n", m_iItemType, slot->m_iItemType );

		// Determinating whether the Weapon gets into a large or a small Inventory Case Slot
		// Also determinating wether a slot is -1, which means empty and free to place a new weapon into it
		// not defined = 0 = large, 1 = medium (handgun), 2 = small (ammo), 3 = inventory_hidden, 4 = no_discard, 5 = 5 round revolver, 6 = 6 round revolver, 7 = item 
		if (m_iItemType == slot->m_iItemType) { return true; }
		else 
		{
			C_BaseCombatWeapon* Slot_Weapon = slot->Get_Weapon_Slot();
			C_BaseCombatWeapon* Player_Weapon = Get_Weapon_Slot();

			//If empty slot or if the Item of this slot is same as the other slot we're aiming at
			if (Player_Weapon == NULL) 
			{ 
				if (m_iItemType == -1 || m_iItemType == Slot_Weapon->m_iItemType) { return true; }

				else if (m_iItemType == 1 && Slot_Weapon->m_iItemType > 4) // If slot type 1 (handgun) is matched to a weapon of these types:
				{   // If no_discard item or if it's a Revolver
					// if (Slot_Weapon->m_iItemType == 3 || Slot_Weapon->m_iItemType == 5 || Slot_Weapon->m_iItemType == 6) { return true; }
					return true;
				}
			}
			// Exchanging 2 Weapons: If Slotweapon and Playerweapon are of the same type. Character_Count < 8 means its located at the player
			else if (Slot_Weapon->m_iItemType == Player_Weapon->m_iItemType
				// Because Handguns and Items are a compatible type of slot
				|| Slot_Weapon->m_iItemType == 1 && Player_Weapon->m_iItemType > 4 )
			{ return true; } //  || Character_Count((char*)slot->GetName()) < 8
		}	

	}
	return false; // Different m_iItemType matched, you can't drop item into a slot of the wrong size. 
}


//-----------------------------------------------------------------------------
// Scrolling the Mouse Wheel - Imperial
//-----------------------------------------------------------------------------
void ButtonSlot::OnMouseWheeled(int delta)
{
	Mouse_Wheeled_Slots += -delta;



	if (delta > Last_Delta) 
	{   // Depending whether the user inverted this convar from hud_weapon_bars.h
		if (iHud_Invert_Scroll.GetBool()) { Current_Mouse_Wheel = 1; } // Doesent work..
		else { Current_Mouse_Wheel = -1; }
	}
	else if (delta < Last_Delta)
	{   if (iHud_Invert_Scroll.GetBool()) { Current_Mouse_Wheel = -1; }
		else { Current_Mouse_Wheel = 1; }
	}
	else { Current_Mouse_Wheel = 0; }

	Last_Delta = delta;
	// DevMsg("Zoom is %d\n", Mouse_Wheeled_Slots);
	// float m_fZoom = -delta * 2; // *2 to double Zoom Speed
	// DevMsg("Zoom is %f\n", m_fZoom);

}

//-----------------------------------------------------------------------------
// Mouse Events
//-----------------------------------------------------------------------------
void ButtonSlot::OnPanelDropped( CUtlVector< KeyValues * >& msglist )
{
	vgui::Panel* panel = (vgui::Panel*)msglist[0]->GetPtr("panel");
	if (panel)
	{
		ButtonSlot *slot = dynamic_cast<vgui::ButtonSlot*>(panel);
		if (slot)
		{
			// This line intercepts the exchange of weapon slots and instead it gives Notes/Items/Maps to the user.
			// if (!Give_Item(slot)) { Switch(slot); }	
			Switch(slot);
			
		}
	}

	ivgui()->PostMessage( 
		GetVPanel(), 
		new KeyValues( "MouseReleased", "code", MOUSE_LEFT ), 
		GetVPanel() );
	ivgui()->PostMessage( 
		GetParent()->GetVPanel(), 
		new KeyValues( "MouseReleased", "code", MOUSE_LEFT ), 
		GetVPanel() );
	if (panel)
	{
		ivgui()->PostMessage( 
			panel->GetVPanel(), 
			new KeyValues( "MouseReleased", "code", MOUSE_LEFT ), 
			GetVPanel() );
	}
	
	Panel::OnPanelDropped( msglist );
}


//-----------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------
void ButtonSlot::OnDroppablePanelPaint( CUtlVector< KeyValues * >& msglist, CUtlVector< Panel * >& dragPanels )
{
	//BaseClass::Paint();	

	if ( dragPanels.Count() >= 1 && dragPanels[0] )
	{
		((ButtonSlot*)dragPanels[0])->DisplayDragWeapon();		
	}

	/*int w, h;
	GetSize( w, h );

	int x, y;
	x = y = 0;
	LocalToScreen( x, y );

	surface()->DrawSetColor( 128,128,128,128 );
	// Draw 2 pixel frame
	surface()->DrawOutlinedRect( x, y, x + w, y + h );
	surface()->DrawOutlinedRect( x+1, y+1, x + w-1, y + h-1 );
	*/

	// Draw all droppable area
	
	//bool isitem;

	//C_BasePlayer *player = C_BasePlayer::GetLocalPlayer();

	/*const char* name = GetName();
	char *num = "0";
	num[0] = name[strlen(name)-1];
	isitem = player->GetWeapon(atoi(num))->IsItem();*/

	//Panel *target = GetParent()->GetDropTarget( msglist ); 

	/*
	Panel *child;
	int w, h;
	int x, y;
	int sw, sh;
	
	for (int i=0;i<GetParent()->GetChildCount();i++)
	{
		child = GetParent()->GetChild(i);
		if (!child)
			continue;
		
		if (child->IsDropEnabled())
		{
			x = y = 0;
			child->LocalToScreen( x, y );
			child->GetSize( sw, sh );
			//w = min( sw, 80 );
			//h = min( sh, 80 );
			w=sw;
			h=sh;

			surface()->DrawSetColor( 128,128,128,128 );
			
			// Draw 2 pixel frame
			surface()->DrawOutlinedRect( x, y, x + w, y + h );
			surface()->DrawOutlinedRect( x+1, y+1, x + w-1, y + h-1 );
		}
	}

	if (target)
	{
		target->LocalToScreen( x, y );
		target->GetSize( sw, sh );
		surface()->DrawSetColor( 128,128,0,128 );
			
		// Draw 2 pixel frame
		surface()->DrawOutlinedRect( x, y, x + w, y + h );
		surface()->DrawOutlinedRect( x+1, y+1, x + w-1, y + h-1 );
	}*/
	BaseClass::OnDroppablePanelPaint( msglist, dragPanels ); 
}


//-----------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------
void ButtonSlot::OnDraggablePanelPaint()
{
	//BaseClass::Paint();

	DisplayDragWeapon();

	/*int x, y;
	int w, h;
	int sw, sh;

	GetSize( sw, sh );
	input()->GetCursorPos( x, y );
	
	w = min( sw, 80 );
	h = min( sh, 80 );
	x -= ( w >> 1 );
	y -= ( h >> 1 );

	surface()->DrawSetColor( 128,128,128,128 );
	surface()->DrawOutlinedRect( x, y, x + w, y + h );
	wchar_t weapon[32];
	GetText(weapon,sizeof(weapon));
	surface()->DrawSetTextPos(x, y);
	surface()->DrawUnicodeString(weapon);

	/*if ( m_pDragDrop->m_DragPanels.Count() > 1 )
	{
		surface()->DrawSetTextColor( m_clrDragFrame );
		surface()->DrawSetTextFont( m_infoFont );
		surface()->DrawSetTextPos( x + 5, y + 2 );

		wchar_t sz[ 64 ];
		_snwprintf( sz, 64, L"[ %i ]", m_pDragDrop->m_DragPanels.Count() );

		surface()->DrawPrintText( sz, wcslen( sz ) );
	}*/
	
	//// Draw all droppable area
	//Panel *child;
	////int x, y;
	//for (int i=0;i<GetParent()->GetChildCount();i++)
	//{
	//	child = GetParent()->GetChild(i);
	//	if (!child)
	//		continue;

	//	if (child->IsDropEnabled())
	//	{			
	//		x = y = 0;
	//		child->LocalToScreen( x, y );
	//		child->GetSize( sw, sh );
	//		//w = min( sw, 80 );
	//		//h = min( sh, 80 );

	//		surface()->DrawSetColor( 128,128,128,128 );
	//		// Draw 2 pixel frame
	//		surface()->DrawOutlinedRect( x, y, x + w, y + h );
	//		surface()->DrawOutlinedRect( x+1, y+1, x + w-1, y + h-1 );
	//	}
	//}*/
}



//-----------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------
void ButtonSlot::DisplayDragWeapon()
{
	C_BasePlayer *player = C_BasePlayer::GetLocalPlayer();
	C_BaseCombatWeapon *Weapon = NULL;

	if (m_iSlot != -1 && player)
	{   if (m_bPlayerWeapon) { Weapon = player->GetWeapon(m_iSlot); }
		else if (Object_Scrollbar_Size < 1) { Weapon = player->Get_Player_Case(m_iSlot); }
		else if (player->Get_Active_Case())
		{ Weapon = reinterpret_cast<C_Item_Case*>(player->Get_Active_Case())->m_nCaseItems[m_iSlot]; }					
	}


	if (Weapon)
	{	
		CHud_Weapon_Bar *Weapon_Bar;
		surface()->DrawSetColor(255, 255, 255, 255);
		C_BaseCombatWeapon *Active_Weapon = player->GetActiveWeapon();

		
		Item_Type = 0;
		Item_Type = Weapon->GetHealth();
		

		bool Is_Item = false;

		if (Weapon_Bar->Compare((char*)Weapon->GetName(), "weapon_item"))
		{
			Is_Item = true;
		}



		// This works exactly the inverted way from the "Invert_Selection" drawn above for the icons.	
		if (Invert_Selection)
		{   if (Weapon == Active_Weapon && Weapon->GetWpnData().iconInactive)
			{
				if (Is_Item) { Set_Item_Icon_Inverted(Item_Type); }
				else { s_Img_Selection = (char*)Weapon->GetWpnData().iconInactive->szTextureFile; }
				// if (Show_Background  && m_bPlayerWeapon) { Draw_Icon(Texture_Slot); }
			}
			else if (Weapon->GetWpnData().iconActive)
			{
				if (Is_Item) { Set_Item_Icon(Item_Type); }				
				else 
				{
					s_Img_Selection = Weapon->GetWpnData().iconActive->szTextureFile;

					if (Weapon_Bar->Compare(s_Img_Selection, "WeaponIconsSelected") && Weapon->GetWpnData().iconInactive)
					{
						Buffer[0] = 0;
						sprintf(Buffer, "%s_Lit", Weapon->GetWpnData().iconInactive->szTextureFile);
						s_Img_Selection = Buffer;
					}
				}

				// if (Show_Background && m_bPlayerWeapon) { Draw_Icon(Texture_Selected); }
			}	
		}
		else
		{   if (Weapon == Active_Weapon && Weapon->GetWpnData().iconActive)
			{
				if (Is_Item) { Set_Item_Icon(Item_Type); }
				else 
				{
					s_Img_Selection = Weapon->GetWpnData().iconActive->szTextureFile;

					if (Weapon_Bar->Compare(s_Img_Selection, "WeaponIconsSelected") && Weapon->GetWpnData().iconInactive)
					{
						Buffer[0] = 0;
						sprintf(Buffer, "%s_Lit", Weapon->GetWpnData().iconInactive->szTextureFile);
						s_Img_Selection = Buffer;
					}
				}

				// if (Show_Background && m_bPlayerWeapon) { Draw_Icon(Texture_Selected); }
			}
			else if (Weapon->GetWpnData().iconInactive)
			{
				if (Is_Item) { Set_Item_Icon_Inverted(Item_Type); }
				else { s_Img_Selection = (char*)Weapon->GetWpnData().iconInactive->szTextureFile; }
				// if (Show_Background && m_bPlayerWeapon) { Draw_Icon(Texture_Slot); }
			}
		}




		if (!Weapon_Bar->Compare(s_Img_Selection, (char*)"WeaponIcons") && !Weapon_Bar->Compare(s_Img_Selection, (char*)"WeaponIconsSelected")
			&& !Weapon_Bar->Compare(s_Img_Selection, (char*)"WeaponIcons_Lit") && !Weapon_Bar->Compare(s_Img_Selection, (char*)"WeaponIconsSelected_Lit"))
		{   // if (s_Img_Selection != null && Character_Count((char*)Weapon->GetWpnData().iconInactive->szTextureFile) > 2)
			if (s_Img_Selection != null && Weapon->GetWpnData().iconInactive)
			{   surface()->DrawSetTextureFile(Weapon_Tile, s_Img_Selection, true, true);
				Draw_Preview(Weapon_Tile);			
			}
		}	
		else // Otherwise there is no icon so we just print the Name
		{   		
			char* Temp_A = Weapon_Bar->Get_Name(Weapon);  // (char*)Weapon->GetWpnData().szPrintName;
		
			int x, y;
			input()->GetCursorPos(x, y);
			x = x - Width / 2;

			if (Character_Count(Temp_A) < 15)
			{   Draw_Text(x + 1 + Width / 4, y + 1 + Height / 3, Temp_A, Color_2R, Color_2G, Color_2B, Color_2Alpha);
				// Shadow Text will fail to Draw unless it is drawn from a second Get_Name(), don't ask me why...
				Draw_Text(x + Width / 4, y + Height / 3, Weapon_Bar->Get_Name(Weapon), Color_1R, Color_1G, Color_1B, Color_1Alpha);
			}
			else // Otherwise start drawing at left border for space purpose.
			{   Draw_Text(x + 1 + Width / 10, y + 1 + Height / 3, Temp_A, Color_2R, Color_2G, Color_2B, Color_2Alpha);
				Draw_Text(x + Width / 10, y + Height / 3, Weapon_Bar->Get_Name(Weapon), Color_1R, Color_1G, Color_1B, Color_1Alpha);
			}
		}
	}

}

//-----------------------------------------------------------------------------
// Purpose: Draw Tile Background
//-----------------------------------------------------------------------------
void ButtonSlot::Draw_Preview(int texture)
{
	int x, y, width, height;
	input()->GetCursorPos(x, y);
	width = 128;
	height = 64;
	x = x - width / 2;

	surface()->DrawSetTexture(texture);
	surface()->DrawTexturedRect(x, y, x + width, y + height);
}

/* // Old Code by CameleonTH
void ButtonSlot::DisplayDragWeapon()
{
	int x, y;
	int sw, sh;

	GetSize( sw, sh );
	sw=128;
	sh=128;
	input()->GetCursorPos( x, y );

	C_BasePlayer *player = C_BasePlayer::GetLocalPlayer();
	C_BaseCombatWeapon *weapon=NULL;
	if (m_iSlot!=-1 && player)
	{
		if (m_bPlayerWeapon)
			weapon = player->GetWeapon(m_iSlot);
		else
			weapon = player->Get_Case_Item(m_iSlot);
	}

	if (weapon)
	{	
		if (weapon->GetWpnData().iconInactive)
		{
			float fRatio;
			fRatio = sw/(float)weapon->GetWpnData().iconInactive->Width();
			fRatio = min(fRatio,sh/(float)weapon->GetWpnData().iconInactive->Height());
			float xoff = (sw - weapon->GetWpnData().iconInactive->Width()*fRatio)/2.0;
			float yoff = (sh - weapon->GetWpnData().iconInactive->Height()*fRatio)/2.0;
			if (weapon->GetWpnData().iconInactive->bRenderUsingFont)
			{	
				vgui::surface()->DrawSetTextScale(fRatio,fRatio);
			}
			weapon->GetWpnData().iconInactive->DrawSelf(x-sw/2+xoff,y-sh/2+yoff,sw-xoff*2,sh-yoff*2,Color(255,255,255,255));
		}
	}
}*/


//-----------------------------------------------------------------------------
// Purpose: Ressetting Settings
//-----------------------------------------------------------------------------
void ButtonSlot::Resetting_Settings()
{ 
	CHud_Weapon_Bar *Weapon_Bar;
	Show_Background = Weapon_Bar->Get_Show_Background();
	Invert_Selection = Weapon_Bar->Get_Invert_Selection();
	Show_Selector = Weapon_Bar->Get_Show_Selector();


	// Loading color values from Convar	
	//------------------- Texture Color Filter
	char* Color_for_Textures = Weapon_Bar->Get_Color_Filter_Value();
	// char* Color_1 = (char*)iHud_Color_1.GetString();
	// Not smaller then 80, or the Game might crash!
	char Col_TexR[80];
	char Col_TexG[80];
	char Col_TexB[80];
	char Col_TexA[80];

	char delimiter = ','; // Both variants legit
	if (Weapon_Bar->Compare(Color_for_Textures, ".")) { delimiter = '.'; }

	// Splitting and extracting color values into a string array
	Weapon_Bar->Split_Array(Color_for_Textures, Col_TexR, delimiter, 1);
	Weapon_Bar->Split_Array(Color_for_Textures, Col_TexG, delimiter, 2);
	Weapon_Bar->Split_Array(Color_for_Textures, Col_TexB, delimiter, 3);
	Weapon_Bar->Split_Array(Color_for_Textures, Col_TexA, delimiter, 4);

	Color_Texture_R = V_atoi(Col_TexR);
	Color_Texture_G = V_atoi(Col_TexG);
	Color_Texture_B = V_atoi(Col_TexB);
	Color_Texture_A = V_atoi(Col_TexA);


	//------------------- Texture 1

	char Col_R[80];
	char Col_G[80];
	char Col_B[80];
	char Col_Alpha[80];

	// Extracting color values into a string array
	char* Color_1 = Weapon_Bar->Get_Color_1_Value();

	delimiter = ','; // Both variants legit
	if (Weapon_Bar->Compare(Color_1, ".")) { delimiter = '.'; }

	Weapon_Bar->Split_Array(Color_1, Col_R, delimiter, 1);
	Weapon_Bar->Split_Array(Color_1, Col_G, delimiter, 2);
	Weapon_Bar->Split_Array(Color_1, Col_B, delimiter, 3);
	Weapon_Bar->Split_Array(Color_1, Col_Alpha, delimiter, 4);

	Color_1R = V_atof(Col_R);
	Color_1G = V_atof(Col_G);
	Color_1B = V_atof(Col_B);
	Color_1Alpha = V_atof(Col_Alpha);


	//------------------- Texture 2
	char* Color_2 = Weapon_Bar->Get_Color_2_Value();

	delimiter = ','; // Both variants legit
	if (Weapon_Bar->Compare(Color_2, ".")) { delimiter = '.'; }

	Weapon_Bar->Split_Array(Color_2, Col_R, delimiter, 1);
	Weapon_Bar->Split_Array(Color_2, Col_G, delimiter, 2);
	Weapon_Bar->Split_Array(Color_2, Col_B, delimiter, 3);
	Weapon_Bar->Split_Array(Color_2, Col_Alpha, delimiter, 4);

	Color_2R = V_atof(Col_R);
	Color_2G = V_atof(Col_G);
	Color_2B = V_atof(Col_B);
	Color_2Alpha = V_atof(Col_Alpha);

	//-------------------
	// If inactive it will stay in responsive test mode
	Reset_Settings = false;

}