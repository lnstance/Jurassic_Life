## Jurassic Life Source SDK Code 2013

Here is the code of Jurassic life, so we are sharing it for convenience purpose and collaboration, that reduce the maintance tasks and the ease of use for collaborationHere is the code of Jurassic life, so we are sharing it for convenience purpose,and it will also help to reduce the adminitration task of the repository tasks and the ease of use for collaboration
If you want to collaborate seriously you can send a message via github at fuzzzzzz_@_gmail_DOT_com .

[Jurrassic life Website] ( http://jurassic-life.com )

Contact : fuzzzzzz_@_gmail_DOT_com ( Nicolas Kirsch )

## Credits

* Nicolas Kirsch
* Imperial
* Thibault Hennequin
* Biohazard ( Shader Editor )



- John McBroom jimbomcb ( https://github.com/jimbomcb )
 - Fixed typo causing makefile to not copy to _srv.so binaries on compile.
 - Fixed g_StringTableGameRules not being reset allowing server/client gamerules to get out of sync. 

- yaakov-h ( https://github.com/yaakov-h )
 - Compiler warning fixes in NPC, bugbait and beam_shared
 - ...
 
-  alanedwardes ( https://github.com/alanedwardes ) 
 -  Added protocol to the vgui::HTML custom URL handler message (as in Source 2007).
 - VBSP now checks all search paths for an FGD file.   @alanedwardes  (value changed from "" to NULL)

- FlaminSarge ( https://github.com/FlaminSarge )
 - Fixed functionality of player jingles in SP/MP c_baseplayer.cpp  
